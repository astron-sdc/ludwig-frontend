import tailwindConfig from "@astron-sdc/design-system/tailwind.config.js";

/** @type {import("tailwindcss").Config} */

export default {
  ...tailwindConfig,
  content: [
    "./index.html",
    "./src/main.tsx",
    "./src/components/**/*.{ts,tsx,js,jsx,mdx}",
    "./src/pages/**/*.{ts,tsx,js,jsx,mdx}",
  ],
};
