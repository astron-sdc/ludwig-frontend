const palette = {
  baseWhite: "#FFFFFF",
  baseBlack: "#000000",
  lightGrey: "#DADADA",
  grey: "#777777",
  mediumGrey: "#313131",
  darkModeBlue: "#000022",
  darkBlue: "#181828",
  lightBlue: "#00ADEE",
  blue: "#3A5896",
  yellow: "#EEEE22",
  orange: "#FF9933",
  red: "#FF4444",
  green: "#25CC25",
  violet: "#DD55FF",
  turquoise: "#11BBAA",
};

/** @type {import('tailwindcss').Config} */

export default {
  content: [
    "./index.html",
    "./src/main.tsx",
    "./src/components/**/*.{ts,tsx,js,jsx,mdx}",
    "./src/pages/**/*.{ts,tsx,js,jsx,mdx}",
  ],
  theme: {
    colors: {
      primary: palette.lightBlue,
      secondary: palette.blue,
      warning: palette.orange,
      positive: palette.green,
      negative: palette.red,
      ...palette,
    },
  },
  darkMode: "class",
  plugins: [],
};
