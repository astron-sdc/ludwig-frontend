/**
 *  Copyright 2024 ASTRON
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import { useRouteError } from "react-router-dom";
import { GeneralError, NoAccess, Typography } from "@astron-sdc/design-system";
import Layout from "src/components/Layout.tsx";
import ThemeProvider from "src/components/Theme/Provider.tsx";
import { AuthProvider } from "src/auth/AuthContext.tsx";

/**
 * Error boundary in the root, which displays a message in case of an unrecoverable error.
 */
const RootError = () => {
  // Allow any error to be caught
  // eslint-disable-next-line
  let err: any = useRouteError();
  // try for err.error is an Error
  if (!(err instanceof Error)) {
    err = err.error;
  }

  return (
    <ThemeProvider>
      <AuthProvider>
        <Layout title="">
          {err.message.includes("401") || err.message.includes("404") ? (
            <NoAccess
              title="You need permission to access"
              content={<Typography text="Please contact admin for access rights." variant="paragraph" />}
            />
          ) : (
            <GeneralError title="Something went wrong!" content="" />
          )}
        </Layout>
      </AuthProvider>
    </ThemeProvider>
  );
};

export default RootError;
