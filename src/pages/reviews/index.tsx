import Layout from "src/components/Layout.tsx";
import { Button, Spinner, Table } from "@astron-sdc/design-system";
import { DataTableValueArray } from "primereact/datatable";
import useSWR from "swr";
import Api from "src/api/Api.ts";
import { formatDateTime } from "src/utils/DateTime.ts";
import { Link, useNavigate } from "react-router-dom";
import { piFromReviewedProposal } from "src/utils/Proposal";
import ReviewedProposal from "src/api/models/ReviewedProposal.ts";
import ProposalStatusBadge from "src/components/ProposalStatusBadge";
import { ReviewerType } from "src/api/models/Review";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/",
  },
  {
    name: "Reviews",
    href: "",
  },
];

const COLUMNS = [
  {
    header: "Title",
    field: "title",
  },
  {
    header: "Code",
    field: "code",
  },
  {
    header: "Status",
    field: "status",
  },
  {
    header: "Created at",
    field: "created_at",
  },
  {
    header: "Submitted at",
    field: "submitted_at",
  },
  {
    header: "PI",
    field: "pi",
  },
  {
    header: "Primary reviewer",
    field: "primary_reviewer",
  },
  {
    header: "Secondary reviewer",
    field: "secondary_reviewer",
  },
  {
    header: "Action",
    field: "action",
  },
];

const ReviewTableAction = (props: { proposalId: number }) => {
  const navigate = useNavigate();
  return (
    <div className="flex flex-row gap-3 w-fit">
      <Button
        color="primary"
        icon="Magnifier"
        label="Review"
        onClick={() => navigate(`/proposals/${props.proposalId}/view`)}
      />
    </div>
  );
};

const fetchProposals = async () => {
  const proposals = await Api.getProposalsForReviewer();
  return proposals.map((proposal: ReviewedProposal) => ({
    ...proposal,
    key: proposal.id,
    title: (
      <Link className="underline" to={`/proposals/${proposal.id}/`}>
        {proposal.title}
      </Link>
    ),
    pi: piFromReviewedProposal(proposal),
    created_at: formatDateTime(proposal.created_at),
    submitted_at: proposal.submitted_at ? formatDateTime(proposal.submitted_at) : "-",
    code: proposal.project_code,
    primary_reviewer: proposal.reviews.filter((review) => review.reviewer_type === ReviewerType.primary)[0]?.reviewer_name,
    secondary_reviewer: proposal.reviews.filter((review) => review.reviewer_type === ReviewerType.secondary)[0]
      ?.reviewer_name,
    status: <ProposalStatusBadge status={proposal.status!} />,
    action: <ReviewTableAction proposalId={proposal.id!} />,
  }));
};

const ReviewOverview = () => {
  const { data, error, isLoading } = useSWR(`/api/v1/proposals/as_reviewer`, fetchProposals);
  if (error) throw Error(error);

  return (
    <Layout title="Reviews" breadcrumbs={CRUMBS}>
      {isLoading ? (
        <Spinner label="Fetching Proposals for Reviewer" />
      ) : (
        <Table columns={COLUMNS} rows={data as DataTableValueArray} />
      )}
    </Layout>
  );
};

export default ReviewOverview;
