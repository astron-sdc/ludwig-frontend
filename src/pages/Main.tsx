import Layout from "src/components/Layout.tsx";
import { Button, Typography } from "@astron-sdc/design-system";
import Panel from "src/components/Panel/Panel.tsx";
import { Link } from "react-router-dom";

const SupportAppUrl = `${process.env.SUPPORT_APP_URL}`;

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/",
  },
];

const MainPage = () => {
  return (
    <Layout title="Proposal Tool" breadcrumbs={CRUMBS}>
      <Typography text="Write and shine!" variant="h2" />
      <Typography text="This is the Proposal tool for access to the telescopes provided by ASTRON." variant="paragraph" />

      <Panel className="mt-10 mb-10">
        <Typography text="Let's start!" variant="h4" />
        <div className="flex flex-row">
          <Link to="/proposals/create/" className="flex-1">
            <div>
              <Typography text="New proposal" variant="h5" />
              <Typography text="Start submitting a new proposal" variant="paragraph" />
            </div>
          </Link>

          <Link to="/overview" className="flex-1">
            <div>
              <Typography text="View your proposals" variant="h5" />
              <Typography text="Show all your proposals" variant="paragraph" />
            </div>
          </Link>
        </div>
      </Panel>

      <div>
        <Typography text="Help & Support" variant="h2" />
        <Link to={SupportAppUrl} target="_blank" rel="noopener noreferrer">
          <Button label="SDC Helpdesk" />
        </Link>
      </div>
    </Layout>
  );
};

export default MainPage;
