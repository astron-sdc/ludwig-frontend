import { NavLink } from "react-router-dom";
import Layout from "src/components/Layout.tsx";
import { Typography } from "@astron-sdc/design-system";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "",
  },
];

const MainPage = () => {
  return (
    <Layout title="Proposal Tool" breadcrumbs={CRUMBS}>
      <NavLink to="/cycles/">
        <Typography text="Cycle Overview" variant="paragraph" />
      </NavLink>
      <br />
      <NavLink to="/overview/">
        <Typography text="Proposals Overview" variant="paragraph" />
      </NavLink>
    </Layout>
  );
};

export default MainPage;
