import { Button, Tabs, TextInput, Typography } from "@astron-sdc/design-system";
import { useContext, useState } from "react";
import { useLoaderData, useParams, useRevalidator } from "react-router-dom";
import Proposal, { ProposalAction, ProposalStatus } from "src/api/models/Proposal";
import FormGrid from "src/components/FormGrid";
import Justification from "src/components/Justification/Justification";
import SpecificationOverView from "src/components/Specification/SpecificationOverview";
import Layout from "src/components/Layout.tsx";
import TargetList from "src/components/Targets/TargetList";
import ToastContext from "src/components/Toast/Context";
import Api from "src/api/Api.ts";
import FlexRow from "src/components/FlexRow.tsx";
import ReviewCommentList from "src/components/Review/ReviewComment/ReviewCommentList";
import ReviewRating from "src/components/Review/ReviewScore/ReviewRating";
import TextEditor from "src/components/TextEditor/TextEditor";
import {
  abstractInvalidMessage,
  getProposalMemberById,
  maxAbstractWordCount,
  validateAbstract,
} from "src/helper/ProposalHelper";
import { formatDateTime } from "src/utils/DateTime";
import CreateEditViewProposalConfiguration from "src/components/Configuration/CreateEditViewConfiguration";
import { openProjectInTMSS } from "src/utils/external";
import ApiTmss from "src/api/ApiTmss";
import AcceptOrReject from "src/components/AcceptOrReject/AcceptOrReject";
import ProposalPermissions from "src/api/models/ProposalPermissions";
import { SynchronizationResult } from "src/api/models/SynchronizationResult";
import ApiSpecification from "src/api/ApiSpecification";
import SpecificationRow from "src/components/Specification/SpecificationRow";
import TargetRow from "src/components/Targets/Models/TargetRow";
import Panel from "src/components/Panel/Panel";
import ProposalMembersTab from "src/components/MembersTab/ProposalMembersTab.tsx";
import { formatContactInfo } from "src/helper/CollaborationHelper";
import ProposalStatusBadge from "src/components/ProposalStatusBadge";
import { AuthContext } from "src/auth/AuthContext";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/",
  },
  {
    name: "Edit Proposal",
    href: "",
  },
];

const ProposalEdit = () => {
  const [proposal, permissions] = useLoaderData() as [Proposal, ProposalPermissions];
  const { permissions: globalPermissions } = useContext(AuthContext);
  const revalidator = useRevalidator();
  const [abstract, setAbstract] = useState(proposal.abstract);
  const [originalTitle, setOriginalTitle] = useState(proposal.title);
  const [originalRank, setOriginalRank] = useState(proposal.rank);
  const [newTitle, setNewTitle] = useState(proposal.title);
  const [isEditingTitle, setIsEditingTitle] = useState(false);
  const [isEditingRank, setIsEditingRank] = useState(false);
  const [newRank, setNewRank] = useState(proposal.rank ?? "");
  const [isSavingTitle, setIsSavingTitle] = useState(false);
  const [isSavingRank, setIsSavingRank] = useState(false);
  const [isSyncing, setIsSyncing] = useState(false);
  const [syncLog, setSyncLog] = useState<string[]>([]);
  const { setToastMessage } = useContext(ToastContext);
  const params = useParams();
  const tabkey = params.tabkey;
  const sync = params.sync;
  const isOnSyncPage = sync === "sync";
  const canReadReviewComments = permissions.read_science_review || permissions.read_technical_review; // FIXME: use ReadReviewComments permissions
  const canRateProposal = permissions.read_science_review; // TODO: Check if this is the correct permission.
  const canEditRank = globalPermissions?.is_admin; // FIXME: make EditRankpermission

  const tabs = [
    {
      key: "abstract",
      title: "Abstract",
      content: (
        <TextEditor
          proposalId={Number(proposal.id)}
          text={abstract}
          setText={setAbstract}
          disabled={isOnSyncPage || !permissions.write_proposal}
          label="Abstract"
          maxWordCount={maxAbstractWordCount}
          fieldname="abstract"
          validateText={validateAbstract}
          invalidMessage={abstractInvalidMessage}
        />
      ),
    },
    {
      key: "justification",
      title: "Justification",
      content: <Justification proposalId={proposal.id} disabled={!permissions.write_proposal} />,
    },
    {
      key: "specification",
      title: "Specification",
      content: (
        <SpecificationOverView proposalId={Number(proposal.id)} permissions={permissions} isOnSyncPage={isOnSyncPage} />
      ),
    },
    {
      title: "Target objects",
      key: "target",
      content: <TargetList proposalId={Number(proposal.id)} permissions={permissions} />,
    },
    {
      title: "Members",
      key: "members",
      content: <ProposalMembersTab proposal={proposal} permissions={permissions} />,
    },
    {
      title: "Configuration",
      key: "config",
      content: (
        <CreateEditViewProposalConfiguration
          proposalId={Number(proposal.id)}
          canEditConfiguration={permissions.write_proposal}
        />
      ),
    },
  ];

  const saveTitle = async () => {
    try {
      setIsSavingTitle(true);
      await Api.updateProposal(proposal.id!, { title: newTitle });
      setToastMessage({
        title: "Success",
        message: "Title was saved successfully",
        alertType: "positive",
        expireAfter: 5000,
      });
      setOriginalTitle(newTitle);
    } catch {
      setToastMessage({
        title: "Error",
        message: "Failed to save title",
        alertType: "negative",
        expireAfter: 5000,
      });
      setNewTitle(originalTitle);
    } finally {
      setIsEditingTitle(false);
      setIsSavingTitle(false);
    }
  };

  const saveRank = async () => {
    try {
      setIsSavingRank(true);
      await Api.updateProposal(proposal.id!, { rank: newRank });
      setToastMessage({
        title: "Success",
        message: "Rank was saved successfully",
        alertType: "positive",
        expireAfter: 5000,
      });
      setOriginalRank(newRank);
    } catch {
      setToastMessage({
        title: "Error",
        message: "Failed to save rank",
        alertType: "negative",
        expireAfter: 5000,
      });
    } finally {
      setIsSavingRank(false);
      setIsEditingRank(false);
    }
  };

  const proposalTitle = isEditingTitle ? (
    <div className="py-2">
      <TextInput label="" value={newTitle} onValueChange={setNewTitle} />
    </div>
  ) : (
    <Typography text={newTitle} variant="h3"></Typography>
  );

  const setSyncToast = (result: SynchronizationResult) => {
    if (result?.did_succeed) {
      setToastMessage({
        title: "Synchronized",
        message: "Proposal was synchronized successfully",
        alertType: "positive",
        expireAfter: 3000,
      });
    } else {
      setToastMessage({
        title: "Error Synchronizing  proposal",
        message: "check logging",
        alertType: "negative",
        expireAfter: 3000,
      });
      addToSyncLog("- Proposal synchronization has failed");
    }
  };

  const synchronize = async () => {
    setIsSyncing(true);
    setToastMessage({
      title: "Synchronize starting",
      message: "Proposal synchronized has started",
      alertType: "positive",
      expireAfter: 2000,
    });
    try {
      if (!proposal?.id) return;
      setSyncLog([]);
      addToSyncLog("Synchronization has started");
      addToSyncLog("- Synchronizating Proposal and Configuration ");
      const result = await Api.synchronize(proposal.id);
      setSyncToast(result);
      if (result.did_succeed) {
        addToSyncLog("- Proposal synchronization did succeed");
        await synchronizeSpecifications(proposal.id);
        await Api.finishSync(proposal.id);
      }
    } catch (error) {
      setSyncToast({ did_succeed: false, message: [""] });
      addToSyncLog("Synch error" + error);
    } finally {
      addToSyncLog("Synchronization has finished");
      setIsSyncing(false);
    }
  };

  const addToSyncLog = (logline: string) => {
    setSyncLog((prevLogs) => [...prevLogs, logline]);
  };

  const synchronizeSpecifications = async (proposalid: number) => {
    // We do this client side. It might take time per Sync, and we have no websocket in place on Ludwig
    // so this gives us a bit better monitoring of the progress + allolws us for some cool stuff like chery picking in the future
    // 1. Get all Specifications
    const specificationRows = (await ApiSpecification.getSpecifications(proposalid)) as SpecificationRow[];
    // 2. Get all Targets
    const targetRows = (await Api.getTargets(proposalid)) as TargetRow[];
    // 3. Loop through each specification row and sync
    for (const specificationRow of specificationRows) {
      await synchronizeSpecification(proposalid, specificationRow, targetRows); // Optionally await for each call
    }
  };

  const synchronizeSpecification = async (
    proposalid: number,
    specificationRow: SpecificationRow,
    targetRows: TargetRow[],
  ) => {
    if (!specificationRow?.id) return;
    const specificationId: number = specificationRow.id;
    addToSyncLog(
      "\u00A0+ Syncing specification id:" +
        specificationId +
        " " +
        specificationRow.name +
        " ( group " +
        specificationRow.group_id_number +
        " )",
    );
    const specificationSyncResult = await ApiSpecification.syncSpecification(specificationId, proposalid);
    console.log("specificationSyncResult", specificationSyncResult);

    // let's get all the targets belonging to this specification!
    const relatedToSpecificationTargetRows = targetRows
      .filter((targetRow) => targetRow.group === (specificationRow.group_id_number as string))
      .sort((a, b) => Number(a.subgroup) - Number(b.subgroup));

    // and then sync each target filled in by
    let previousSubGroup = "";
    let isSynced = false;
    for (const targetRow of relatedToSpecificationTargetRows) {
      try {
        if (previousSubGroup !== targetRow.subgroup) {
          await synchronizeSubgroup(proposalid, specificationId, targetRow);
          isSynced = true;
        }
      } catch (ex) {
        console.log(ex);
      }
      logTarget(targetRow, isSynced);
      previousSubGroup = targetRow.subgroup;
    }

    // so we want to sync a specification
    // the specification could either be
    // a) Based upon itself. It's part of a target group, but No targets are in this group, so all data is usef from the specification
    // b) A Mix-On. The Target location is mixed in from the targets belonging to the group.
  };

  const logTarget = async (target: TargetRow, isSynced: boolean) => {
    let message = " failed";
    if (isSynced) message = "";
    addToSyncLog(
      "\u00A0 \u00A0 \u00A0 \u00A0 \u00A0 - Syncing target id:" +
        target.id +
        " " +
        target.name +
        " " +
        target.x_hms_formatted +
        " / " +
        target.y_dms_formatted +
        message,
    );
  };

  const synchronizeSubgroup = async (proposalId: number, specificationId: number, target: TargetRow) => {
    addToSyncLog("\u00A0 \u00A0 \u00A0  +  Syncing targets combined in subgroup " + target.subgroup);
    const subGroupSyncResult = await ApiSpecification.syncSubgroup(specificationId, proposalId, target.subgroup);
    console.log("subgroupSync", subGroupSyncResult);
  };

  const editTitleButton =
    permissions.write_proposal && !isEditingTitle && !isOnSyncPage ? (
      <Button label="Edit title" icon="Edit" color="secondary" onClick={() => setIsEditingTitle(true)} />
    ) : null;

  const editRankButton =
    canEditRank && !isEditingRank && !isOnSyncPage ? (
      <Button label="Edit rank" icon="Edit" color="secondary" onClick={() => setIsEditingRank(true)} />
    ) : null;

  const saveTitleButton = isEditingTitle ? (
    <Button
      showLoader={isSavingTitle}
      isDisabled={isSavingTitle || isOnSyncPage}
      label="Save"
      loadingText="Saving..."
      color="positive"
      onClick={saveTitle}
    />
  ) : null;

  const saveRankButton = isEditingRank ? (
    <Button
      showLoader={isSavingRank}
      isDisabled={isSavingRank || isOnSyncPage}
      label="Save"
      loadingText="Saving..."
      color="positive"
      onClick={saveRank}
    />
  ) : null;

  const cancelSaveTitleButton = isEditingTitle ? (
    <Button
      isDisabled={isSavingTitle || isOnSyncPage}
      label="Cancel"
      color="negative"
      onClick={() => {
        setIsEditingTitle(false);
        setNewTitle(originalTitle);
      }}
    />
  ) : null;

  const cancelSaveRankButton = isEditingRank ? (
    <Button
      isDisabled={isSavingRank || isOnSyncPage}
      label="Cancel"
      color="negative"
      onClick={() => {
        setIsEditingRank(false);
        setNewRank(originalRank || "");
      }}
    />
  ) : null;

  const handleSubmit = async () => {
    const response = await Api.proposalActions(proposal.id!, ProposalAction.submit);
    if (response.ok) {
      setToastMessage({
        title: "Submitted",
        message: "Proposal was submitted successfully",
        alertType: "positive",
        expireAfter: 3000,
      });
      revalidator.revalidate();
    } else {
      response.json().then((value) => {
        setToastMessage({
          title: "Error submitting proposal",
          message: value.error,
          alertType: "negative",
          expireAfter: 3000,
        });
      });
    }
  };

  const handleUnsubmit = async () => {
    const response = await Api.proposalActions(proposal.id!, ProposalAction.unsubmit);
    if (response.ok) {
      setToastMessage({
        title: "Unsubmitted",
        message: "Proposal was unsubmitted successfully",
        alertType: "positive",
        expireAfter: 3000,
      });
      revalidator.revalidate();
    } else {
      response.json().then((value) => {
        setToastMessage({
          title: "Error unsubmitting proposal",
          message: value.error,
          alertType: "negative",
          expireAfter: 3000,
        });
      });
    }
  };

  return (
    <Layout title="Edit Proposal" breadcrumbs={CRUMBS} isWithFooter={true}>
      <div className="flex flex-col">
        <FlexRow customClass="items-center">
          {proposalTitle}
          {editTitleButton}
          {saveTitleButton}
          {cancelSaveTitleButton}
        </FlexRow>
        <FormGrid>
          <FlexRow>
            <div className="w-1/3 flex flex-col gap-2 mr-2">
              <TextInput labelPlacement="outside-left" label="Facility" isReadOnly={true} isDisabled={true} value={""} />
              <TextInput labelPlacement="outside-left" label="Semester" isReadOnly={true} isDisabled={true} value={""} />
              <TextInput labelPlacement="outside-left" label="Type" isReadOnly={true} isDisabled={true} value={""} />
              <div className="flex justify-start flex-row items-baseline">
                {/* TODO: add 'label' variant to Typography */}
                <Typography customClass="w-1/3 font-semibold leading-normal mb-1" text="Status" variant="paragraph" />
                <ProposalStatusBadge status={proposal.status} />
              </div>
            </div>

            <div className="w-1/3 flex flex-col gap-2 ml-2">
              <TextInput labelPlacement="outside-left" label="Queue" isReadOnly={true} isDisabled={true} value={""} />
              <TextInput
                labelPlacement="outside-left"
                label="Submitter"
                isReadOnly={true}
                isDisabled={true}
                value={getProposalMemberById(proposal, proposal.created_by)?.name ?? ""}
              />
              <TextInput
                labelPlacement="outside-left"
                label="Submitter affiliation"
                isReadOnly={true}
                isDisabled={true}
                value={getProposalMemberById(proposal, proposal.created_by)?.schac_home_organisation ?? ""}
              />
              <TextInput
                labelPlacement="outside-left"
                label="Contact Author"
                isReadOnly={true}
                isDisabled={true}
                value={formatContactInfo(getProposalMemberById(proposal, proposal.contact_author))}
              />
              <div>
                {!isSyncing ? (
                  <TextInput
                    labelPlacement="outside-left"
                    label="Last TMSS Sync"
                    isReadOnly={true}
                    isDisabled={true}
                    value={formatDateTime(proposal.tmss_project_id_synced_at)}
                  />
                ) : (
                  <TextInput
                    labelPlacement="outside-left"
                    label="Last TMSS Sync"
                    isReadOnly={true}
                    isDisabled={true}
                    value={"Sync in Progress"}
                  />
                )}
              </div>
            </div>
            <div className="w-1/3 flex flex-col gap-2 ml-4">
              <FlexRow>
                <div className="w-4/5">
                  <TextInput
                    labelPlacement="outside-left"
                    label="Rank"
                    isDisabled={!isEditingRank}
                    value={newRank}
                    onValueChange={setNewRank}
                  />
                </div>
                {editRankButton}
                {saveRankButton}
                {cancelSaveRankButton}
              </FlexRow>
            </div>
          </FlexRow>
        </FormGrid>
        {syncLog?.length > 0 && (
          <Panel>
            <div className="text-sm">
              {syncLog.map((line) => (
                <div key={line}>{line}</div>
              ))}
            </div>
            <div className="float-right">
              <Button
                isDisabled={isSyncing}
                label="Close Log"
                color="primary"
                onClick={() => {
                  setSyncLog([]);
                }}
              />
            </div>
          </Panel>
        )}
        {!isSyncing && (
          <FlexRow>
            <div
              className={
                "flex-initial " +
                (proposal.status === ProposalStatus.underReview && canReadReviewComments ? "w-2/3" : "w-full")
              }
            >
              <Tabs tabs={tabs} selectedTabKey={tabkey}></Tabs>
            </div>
            {proposal.status === ProposalStatus.underReview && canReadReviewComments && (
              <div className="flex-initial w-1/3">
                <ReviewCommentList proposalId={Number(proposal.id)} permissions={permissions} />
              </div>
            )}
          </FlexRow>
        )}
      </div>
      <FlexRow justify="end" customClass="sticky bottom-0 bg-background-sidebar py-3 pl-20 pr-32 -mb-6 -mx-32 gap-x-24">
        {proposal.tmss_project_id !== ApiTmss.CLEANPROJECTID && (
          <Button
            label="Open Proposal in  TMSS"
            icon="TMSS"
            color="secondary"
            onClick={() => openProjectInTMSS(proposal)}
          />
        )}
        <Button
          isDisabled={isSyncing || !globalPermissions?.is_admin}
          label="Sync to TMSS"
          icon="TMSS"
          color="secondary"
          onClick={synchronize}
        />
        {permissions.submit_proposal && proposal.status === ProposalStatus.draft && !isOnSyncPage && (
          <Button label="Submit proposal" onClick={handleSubmit} isDisabled={!proposal.call_is_open} color="positive" />
        )}
        {permissions.submit_proposal && proposal.status === ProposalStatus.submitted && !isOnSyncPage && (
          <Button label="Unsubmit proposal" onClick={handleUnsubmit} isDisabled={!proposal.call_is_open} color="warning" />
        )}
        {proposal.status === ProposalStatus.underReview && !isOnSyncPage && !!proposal.id && (
          <>
            {canRateProposal && <ReviewRating proposalId={proposal.id} />}
            {permissions.accept_proposal && permissions.reject_proposal && (
              <AcceptOrReject proposalId={proposal.id} onStatusChanged={() => revalidator.revalidate()} />
            )}
          </>
        )}
      </FlexRow>
    </Layout>
  );
};

export default ProposalEdit;
