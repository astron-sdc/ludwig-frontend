import { Button, Spinner, Table } from "@astron-sdc/design-system";
import Api from "src/api/Api.ts";
import { Link, useNavigate } from "react-router-dom";
import useSWR from "swr";
import Layout from "src/components/Layout";
import Proposal from "src/api/models/Proposal";
import { openProjectInTMSS } from "src/utils/external";
import ApiTmss from "src/api/ApiTmss";
import { DataTableValueArray } from "primereact/datatable";
import ProposalStatusBadge from "src/components/ProposalStatusBadge";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/",
  },
  {
    name: "Proposal Overview",
    href: "",
  },
];

const COLUMNS = [
  {
    field: "title",
    header: "Title",
  },
  {
    field: "code",
    header: "Code",
  },
  {
    field: "status",
    header: "Status",
  },
  {
    field: "actions",
    header: "Action",
  },
];

/**
 * Action buttons for the Proposal table
 * @param props
 * @constructor
 */
const ProposalTableAction = (props: { proposal?: Proposal }) => {
  const navigate = useNavigate();
  const handleClick = () => {
    if (!props.proposal) return;
    navigate(`/proposals/${props.proposal.id}/`);
  };

  return (
    <div className="flex flex-row gap-3 w-fit">
      <Button color="neutral" icon="Eye" label="View" onClick={handleClick} />
      {props.proposal?.tmss_project_id !== ApiTmss.CLEANPROJECTID && (
        <Button
          color="primary"
          icon="TMSS"
          label="Open in TMSS"
          onClick={() => {
            openProjectInTMSS(props?.proposal);
          }}
        />
      )}
    </div>
  );
};

/**
 * fetchProposals; adds key and action on Data
 */
const fetchProposals = async () => {
  const proposals = await Api.getProposals();
  const mapped = proposals.map((proposal) => ({
    ...proposal,
    key: proposal.id,
    code: proposal.project_code || "",
    title: (
      <Link className="underline" to={`/proposals/${proposal.id}/`}>
        {proposal.title}
      </Link>
    ),
    status: <ProposalStatusBadge status={proposal.status ?? ""} />,
    actions: <ProposalTableAction proposal={proposal} />,
  }));

  return mapped;
};

const ProposalsOverview = () => {
  const navigate = useNavigate();
  const { data, error, isLoading } = useSWR("/api/v1/proposals", fetchProposals);

  const handleProposalCreate = () => {
    navigate("/proposals/create/");
  };

  if (error) {
    throw Error(error);
  }

  return (
    <Layout title="Proposal Overview" breadcrumbs={CRUMBS}>
      {isLoading ? (
        <Spinner label="Fetching Proposals" />
      ) : (
        <Table
          columns={COLUMNS}
          headerContent={
            <div className="flex flex-col">
              <div className="flex flex-row gap-4 pb-[10px]">
                <Button color="positive" icon="Plus" label="Create Proposal" onClick={handleProposalCreate} />
              </div>
            </div>
          }
          rows={(isLoading || !data ? [] : data) as DataTableValueArray}
        />
      )}
    </Layout>
  );
};

export default ProposalsOverview;
