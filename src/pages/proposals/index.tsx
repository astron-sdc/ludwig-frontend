import { Button, Table } from "@astron-sdc/design-system";
import Api from "src/api/Api.ts";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import useSWR from "swr";
import Layout from "src/components/Layout";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/proposal/",
  },
  {
    name: "Proposal Overview",
    href: "",
  },
];

const COLUMNS = [
  {
    field: "title",
    header: "Title",
  },
  {
    field: "status",
    header: "Status",
  },
  {
    field: "actions",
    header: "Action",
  },
];

interface State {
  showFilters: boolean;
  filters: object[];
}

/**
 * Action buttons for the Proposal table
 * @param props
 * @constructor
 */
const ProposalTableAction = (props: { proposalId?: number }) => {
  const navigate = useNavigate();
  const { proposalId } = props;

  const handleClick = () => {
    navigate(`/proposal/${proposalId}/`);
  };

  return (
    <div className="flex flex-row gap-3 w-fit">
      <Button
        color="secondary"
        icon="Edit"
        label="Edit"
        onClick={handleClick}
      />
    </div>
  );
};

/**
 * fetchProposals; adds key and action on Data
 */
const fetchProposals = async () => {
  const cycles = await Api.getProposals();
  const mapped = cycles.map((proposal) => ({
    ...proposal,
    key: proposal.id,
    title: proposal.title,
    actions: <ProposalTableAction proposalId={proposal.id} />,
  }));

  return mapped;
};

const ProposalsOverview = () => {
  const navigate = useNavigate();
  const { data, error, isLoading } = useSWR(
    "/api/v1/proposals",
    fetchProposals,
  );
  const [state, setState] = useState<State>({
    showFilters: false,
    filters: [],
  });

  const handleProposalCreate = () => {
    navigate("/proposal/create/");
  };

  const handleShowFilters = () => {
    setState((prev) => ({ ...prev, showFilters: !prev.showFilters }));
  };

  // TODO: error state
  if (error) {
    console.log(error);
    return <div>Something went wrong</div>;
  }

  return (
    <Layout title="Proposal Overview" breadcrumbs={CRUMBS}>
      <Table
        columns={COLUMNS}
        headerContent={
          <div className="flex flex-col">
            <div className="flex flex-row gap-4 pb-[10px]">
              <Button
                color="secondary"
                icon={state.showFilters ? "Up" : "Dropdown"}
                label="Filter"
                onClick={handleShowFilters}
              />
              <Button
                color="positive"
                icon="Plus"
                label="Create Proposal"
                onClick={handleProposalCreate}
              />
            </div>
            {state.showFilters && (
              <div className="flex flex-row gap-4 pb-[10px]">
                <span>Filters go here</span>
              </div>
            )}
          </div>
        }
        rows={isLoading ? [] : data}
      />
    </Layout>
  );
};

export default ProposalsOverview;
