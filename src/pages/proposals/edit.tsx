import { Button, Typography } from "@astron-sdc/design-system";
import { useState } from "react";
import { useParams } from "react-router-dom";
import FlexRow from "src/components/FlexRow";
import FormGrid from "src/components/FormGrid";
import Layout from "src/components/Layout.tsx";
import Overlay from "src/components/Overlay";
import TargetList from "src/components/Targets/TargetList";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/proposal/",
  },
  {
    name: "Edit Proposal",
    href: "",
  },
];

const ProposalEdit = () => {
  const [showTargetOverlay, setShowTargetOverlay] = useState(false);
  const onShowTargets = () => setShowTargetOverlay(!showTargetOverlay);

  const params = useParams();
  const targetOverlay = showTargetOverlay ? (
    <Overlay title="Target objects">
      <TargetList proposalId={Number(params.proposalId)} />
    </Overlay>
  ) : null;

  return (
    <Layout title="Edit Proposal" breadcrumbs={CRUMBS}>
      <div className="grid grid-cols-2 gap-3">
        <div className="col">
          <Typography text="Proposal" variant="h3" />
          <FormGrid>
            <></>
          </FormGrid>

          <Typography text="Targets" variant="h3" />
          <FormGrid>Target summary info</FormGrid>
          <FlexRow justify="start">
            <Button
              label={showTargetOverlay ? "Hide targets" : "Edit targets"}
              icon="Edit"
              color="secondary"
              onClick={onShowTargets}
            />
            <Button
              label="Upload a list"
              icon="Plus"
              color="neutral"
              type="submit"
            />
          </FlexRow>
        </div>
        <div className="col">
          <Typography text="Justification" variant="h3" />
          <FormGrid>
            <></>
          </FormGrid>

          <Typography text="Members" variant="h3" />
          <FormGrid>
            <></>
          </FormGrid>
        </div>
      </div>

      {targetOverlay}
    </Layout>
  );
};

export default ProposalEdit;
