import Layout from "src/components/Layout.tsx";
import {
  Button,
  Dropdown,
  FileInput,
  TextArea,
  TextInput,
} from "@astron-sdc/design-system";
import FormGrid from "src/components/FormGrid.tsx";
import { ReactEventHandler, useMemo, useState } from "react";
import Api from "src/api/Api.ts";
import { useLoaderData, useNavigate } from "react-router-dom";
import Call from "src/api/models/Call";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/proposal/",
  },
  {
    name: "Submit Proposal",
    href: "",
  },
];

const ProposalCreate = () => {
  const navigate = useNavigate();

  const [title, setTitle] = useState("");
  const [abstract, setAbstract] = useState("");
  const [files, setFiles] = useState<File[]>([]);
  const [call, setCall] = useState<string | undefined>();

  const calls = useLoaderData() as Call[];

  const handleSumbit: ReactEventHandler<HTMLFormElement> = (e) => {
    e.preventDefault();
    e.stopPropagation();

    if (!call) {
      // TODO: show validation error?
      return;
    }

    Api.createProposal({
      title: title,
      abstract: abstract,
      scientific_justification: files[0],
      call_id: parseInt(call),
    })
      .then((proposal) => {
        navigate(`/proposal/${proposal.id}`);
      })
      .catch((reason) => {
        console.log(reason);
        // TODO: show error alert.
      });
  };

  const maxAbstractWordCount = 180;
  const validateAbstract = (abstract: string) => {
    return abstract.split(" ").length <= maxAbstractWordCount;
  };
  const isAbstractInvalid = useMemo(() => {
    return !validateAbstract(abstract);
  }, [abstract]);

  return (
    <Layout title="Create Proposal" breadcrumbs={CRUMBS}>
      <form onSubmit={handleSumbit}>
        <FormGrid>
          <TextInput
            value={title}
            onValueChange={setTitle}
            label="Title"
            isRequired={true}
            maxLength={150}
          />
          <TextArea
            value={abstract}
            onValueChange={setAbstract}
            label="Abstract"
            isRequired={true}
            isInvalid={isAbstractInvalid}
            errorMessage={
              isAbstractInvalid ? "Abstract must be fewer than 180 words." : ""
            }
          />
          <FileInput
            label="Scientific Justification"
            files={files}
            onFilesChange={setFiles}
          />
          <Dropdown
            label="Proposal Call"
            value={call ? call : ""} // fixes warning about changing to controlled component
            valueOptions={calls.map((call) => ({
              label: call.name,
              value: call.id.toString(),
            }))}
            onValueChange={setCall}
          />
        </FormGrid>
        <Button type="submit" label="create" />
      </form>
    </Layout>
  );
};

export default ProposalCreate;
