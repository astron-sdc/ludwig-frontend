import Layout from "src/components/Layout.tsx";
import { Button, Dropdown, FileInput, TextArea, TextInput } from "@astron-sdc/design-system";
import FormGrid from "src/components/FormGrid.tsx";
import { ReactEventHandler, useContext, useMemo, useState } from "react";
import Api from "src/api/Api.ts";
import { useLoaderData, useNavigate, useSearchParams } from "react-router-dom";
import Call from "src/api/models/Call";
import { abstractInvalidMessage, maxAbstractWordCount, validateAbstract } from "src/helper/ProposalHelper";
import ToastContext from "src/components/Toast/Context";
import { getWordCount } from "src/utils/Validation";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/",
  },
  {
    name: "Submit Proposal",
    href: "",
  },
];

const ProposalCreate = () => {
  const navigate = useNavigate();
  const { setToastMessage } = useContext(ToastContext);
  const [title, setTitle] = useState("");
  const [abstract, setAbstract] = useState("");
  const [files, setFiles] = useState<File[]>([]);
  const [call, setCall] = useState<string | undefined>();
  const [isSaving, setIsSaving] = useState(false);

  const [params] = useSearchParams();
  useMemo(() => {
    if (params.get("call")) setCall(params.get("call") as string);
  }, [params]);

  const calls = useLoaderData() as Call[];

  const handleSubmit: ReactEventHandler<HTMLFormElement> = async (e) => {
    e.preventDefault();
    e.stopPropagation();

    setIsSaving(true);

    try {
      const proposal = await Api.createProposal({
        title: title,
        abstract: abstract,
        scientific_justification: files[0],
        call_id: parseInt(call as string),
      });
      setToastMessage({
        title: "Success",
        message: "Proposal was created successfully",
        alertType: "positive",
        expireAfter: 5000,
      });
      navigate(`/proposals/${proposal.id}`);
    } catch {
      setIsSaving(false);
      setToastMessage({
        title: "Error",
        message: "Failed to create proposal",
        alertType: "negative",
        expireAfter: 5000,
      });
    }
  };

  const isAbstractInvalid = useMemo(() => {
    return !validateAbstract(abstract);
  }, [abstract]);

  return (
    <Layout title="Create Proposal" breadcrumbs={CRUMBS}>
      <form onSubmit={handleSubmit}>
        <FormGrid>
          <TextInput value={title} onValueChange={setTitle} label="Title" isRequired={true} maxLength={150} />
          <TextArea
            value={abstract}
            onValueChange={setAbstract}
            label={`Abstract (${getWordCount(abstract)}/${maxAbstractWordCount} words)`}
            isRequired={true}
            minRows={8}
            isInvalid={isAbstractInvalid}
            errorMessage={isAbstractInvalid ? abstractInvalidMessage : ""}
          />
          <FileInput label="Scientific Justification" files={files} onFilesChange={setFiles} />
          <Dropdown
            label="Proposal Call"
            value={call ?? ""} // fixes warning about changing to controlled component
            valueOptions={calls.map((call) => ({
              label: call.name,
              value: call.id.toString(),
            }))}
            onValueChange={setCall}
            isRequired={true}
          />
        </FormGrid>
        <Button
          type="submit"
          label="Create"
          showLoader={isSaving}
          loadingText="Creating..."
          isDisabled={isSaving || isAbstractInvalid}
        />
      </form>
    </Layout>
  );
};

export default ProposalCreate;
