import { NavLink, useParams } from "react-router-dom";

const CycleDetails = () => {
  const params = useParams();
  return (
    <>
      <div>Editing cycle: {params["cycleId"]}</div>
      <div>Form fields etc.</div>
      <div>Show table with all calls?</div>
      <NavLink to="/cycles/">Back to overview</NavLink>
    </>
  );
};

export default CycleDetails;
