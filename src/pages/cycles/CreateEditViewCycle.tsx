import { Button, DateInput, Dropdown, TextArea, TextInput } from "@astron-sdc/design-system";
import React, { useContext, useEffect, useState } from "react";
import FlexRow from "src/components/FlexRow";
import FormGrid from "src/components/FormGrid";
import Layout from "src/components/Layout";
import { useLoaderData, useNavigate, useParams } from "react-router-dom";
import ToastContext from "src/components/Toast/Context.ts";
import { formatDateTimeForApi } from "src/utils/DateTime.ts";
import Cycle from "src/api/models/Cycle";
import CycleHelper from "src/helper/CycleHelper";
import { Cycle as TMSSCycle } from "@astron-sd/telescope-specification-models";
import useSWRImmutable from "swr/immutable";
import ApiTmss from "src/api/ApiTmss";
import { AuthContext } from "src/auth/AuthContext";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/",
  },
  {
    name: " Cycles",
    href: "/cycles/",
  },
];
/* TMSS is used on request to clone the cycle.  */
const CreateEditViewCycle = () => {
  const navigate = useNavigate();
  const { permissions: globalPermissions } = useContext(AuthContext);
  const { setToastMessage } = useContext(ToastContext);
  const params = useParams();
  const loadedCycle = useLoaderData() as Cycle;
  const canEdit: boolean = globalPermissions?.create_cycle ?? false;
  const isViewMode = params["command"] === "view";

  const [name, setName] = useState(loadedCycle?.name ?? "");
  const [code, setCode] = useState<string>(loadedCycle?.code ?? "");
  const [tmssCycleUrl, setTmssCycleUrl] = useState<string | undefined>("");
  const [description, setDescription] = useState(loadedCycle?.description ?? "");
  const [startDate, setStartDate] = useState<Date>((loadedCycle?.start as Date) ?? new Date());
  const [endDate, setEndDate] = useState<Date>((loadedCycle?.end as Date) ?? new Date());
  const { data: tmssCycles } = useSWRImmutable<TMSSCycle[]>("tmssCycles", () => ApiTmss.getCycles());
  const getTitle = (): string => {
    if (isViewMode) {
      return "View Cycle";
    }
    if (loadedCycle) {
      return "Edit Cycle";
    }
    return "Create Cycle";
  };

  useEffect(() => {
    const tmssCycle = tmssCycles?.find((x) => x.url == tmssCycleUrl);
    if (!tmssCycle || tmssCycles?.length == 0) return;
    const TMSSIdentifier = tmssCycle.url.split("/");
    setCode(TMSSIdentifier[TMSSIdentifier.length - 1].replace("Cycle%20", ""));
    setName(tmssCycle.name);
    setDescription(tmssCycle.description);
    setStartDate(new Date(tmssCycle.start));
    setEndDate(new Date(tmssCycle.stop));
  }, [tmssCycleUrl, tmssCycles]);

  const handleSaveCycle = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    e.stopPropagation();

    const tmsscycleid = tmssCycleUrl && tmssCycleUrl !== "" ? tmssCycleUrl : loadedCycle?.tmss_cycle_id;
    CycleHelper.saveCycle({
      name: name,
      code: code,
      description: description,
      start: formatDateTimeForApi(startDate),
      end: formatDateTimeForApi(endDate),
      id: loadedCycle?.id,
      tmss_cycle_id: tmsscycleid,
    })
      .then(() => {
        navigate(`/cycles/`);
      })
      .catch((error) => {
        setToastMessage({
          title: "Error saving Cycle",
          message: error.message,
          alertType: "negative",
          expireAfter: 5000,
        });
      });
  };

  const getCycleValues = (): { value: string; label: string }[] => {
    if (!tmssCycles) return [{ value: "", label: "" }];
    return tmssCycles.map((singleCycle) => ({
      label: singleCycle.name,
      value: singleCycle.url,
    }));
  };

  return (
    <Layout title={getTitle()} breadcrumbs={CRUMBS}>
      <form onSubmit={handleSaveCycle}>
        <FormGrid>
          <Dropdown
            labelPlacement="outside-left"
            label="Clone from TMSS Cycle "
            value={tmssCycleUrl}
            valueOptions={getCycleValues()}
            onValueChange={setTmssCycleUrl}
            isDisabled={isViewMode || !canEdit}
            isRequired={true}
          ></Dropdown>
        </FormGrid>
        <FormGrid>
          <TextInput
            value={name}
            onValueChange={setName}
            label="Name"
            maxLength={150}
            isRequired={true}
            labelPlacement="outside-left"
            isReadOnly={isViewMode || !canEdit}
          />

          <TextInput
            value={code}
            onValueChange={setCode}
            label="Code"
            maxLength={150}
            isRequired={true}
            labelPlacement="outside-left"
            isReadOnly={isViewMode || !canEdit}
          />

          <TextArea
            value={description}
            onValueChange={setDescription}
            isRequired={true}
            label="Description"
            labelPlacement="outside-left"
            isReadOnly={isViewMode || !canEdit}
          />
        </FormGrid>
        <FormGrid>
          <DateInput
            value={startDate}
            onValueChange={(value) => setStartDate(value as Date)}
            label="Start of observing"
            isDisabled={isViewMode || !canEdit}
            forceUTC={true}
          />
          <DateInput
            value={endDate}
            onValueChange={(value) => setStartDate(value as Date)}
            label="End of observing"
            isDisabled={isViewMode || !canEdit}
            forceUTC={true}
          />
        </FormGrid>
        <FlexRow justify="start">
          {canEdit && !isViewMode ? (
            <>
              <Button label="Cancel" icon="Cross" color="negative" onClick={() => navigate("/cycles")} />
              <Button label="Save" icon="Next" color="positive" type="submit" />
            </>
          ) : (
            <Button label="Back" icon="Previous" onClick={() => navigate("/cycles")} />
          )}
        </FlexRow>
      </form>
    </Layout>
  );
};

export default CreateEditViewCycle;
