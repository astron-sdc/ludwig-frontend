import {
  Button,
  DateInput,
  TextArea,
  TextInput,
} from "@astron-sdc/design-system";
import React, { useState } from "react";
import FlexRow from "src/components/FlexRow";
import FormGrid from "src/components/FormGrid";
import Layout from "src/components/Layout";
import { useNavigate } from "react-router-dom";
import api from "src/api/Api.ts";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/proposal/",
  },
  {
    name: "Create Cycle",
    href: "/proposal/cycles/create/",
  },
];

const CreateCycle = () => {
  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [code, setCode] = useState("");
  const [description, setDescription] = useState("");
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  const handleCreateCycle = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    e.stopPropagation();
    // TODO: check Date conversion
    api
      .createCycle({
        name: name,
        code: code,
        description: description,
        start: startDate,
        end: endDate,
      })
      .then((cycle) => {
        // TODO: or should it navigate to /cycles/<ID>/?
        console.log(cycle);
        navigate("/cycles/");
      })
      .catch((err) => {
        console.log(err);
        // TODO: show error in toast/alert
      });
  };

  return (
    <Layout title="Create Cycle" breadcrumbs={CRUMBS}>
      <form onSubmit={handleCreateCycle}>
        <FormGrid>
          <TextInput
            value={name}
            onValueChange={setName}
            label="Name"
            maxLength={150}
            isRequired={true}
            labelPlacement="outside-left"
          />
          <TextInput
            value={code}
            onValueChange={setCode}
            maxLength={5}
            isRequired={true}
            label="Code"
            labelPlacement="outside-left"
          />
          <TextArea
            value={description}
            onValueChange={setDescription}
            isRequired={true}
            label="Description"
            labelPlacement="outside-left"
          />
        </FormGrid>
        <FormGrid>
          <DateInput
            value={startDate}
            onValueChange={(value: unknown) => setStartDate(value as Date)}
            isRequired={true}
            showTimeSelect={true}
            label="Start of observing"
            labelPlacement="outside-left"
          />
          <DateInput
            value={endDate}
            onValueChange={(value: unknown) => setEndDate(value as Date)}
            isRequired={true}
            showTimeSelect={true}
            label="End of observing"
            labelPlacement="outside-left"
          />
        </FormGrid>
        {/* TODO queue, closing date */}
        <FlexRow justify="start">
          <Button label="Cancel" icon="Cross" color="negative" />
          <Button label="Create" icon="Next" color="positive" type="submit" />
        </FlexRow>
      </form>
    </Layout>
  );
};

export default CreateCycle;
