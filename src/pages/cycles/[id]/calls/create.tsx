import React, { useState } from "react";
import FlexRow from "src/components/FlexRow.tsx";
import FormGrid from "src/components/FormGrid.tsx";
import {
  Button,
  DateInput,
  TextArea,
  TextInput,
  Toggle,
} from "@astron-sdc/design-system";
import Layout from "src/components/Layout.tsx";
import api from "src/api/Api.ts";
import { useNavigate, useParams } from "react-router-dom";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/proposal/",
  },
  {
    name: "Calls for proposals",
    href: "/proposal/calls/",
  },
  {
    name: "Create call for proposal",
    href: "",
  },
];

const CreateCall = () => {
  const navigate = useNavigate();
  const params = useParams();

  const [title, setTitle] = useState("");
  const [code, setCode] = useState("");
  const [description, setDescription] = useState("");
  const [cycle] = useState(
    Number.isInteger(Number(params.cycleId)) ? (params.cycleId as string) : "",
  );
  const [startDate, setStartDate] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());
  const [isDirectAction, setIsDirectAction] = useState(false);

  const handleCreateCall = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    e.stopPropagation();
    // TODO: check Date conversion
    api
      .createCall({
        name: title,
        code: code,
        description: description,
        start: startDate,
        end: endDate,
        requires_direct_action: isDirectAction,
        cycle_id: parseInt(cycle), // TODO: cycle reference?
      })
      .then((call) => {
        console.log(call);
        // TODO: or should it navigate to /calls/<ID>/? or calls overview?
        // navigate(`/calls/${call.id}/`);
        navigate("/cycles/");
      })
      .catch((err) => {
        console.log(err);
        // TODO: show error in toast/alert
      });
  };

  return (
    <Layout title="Create call for proposals" breadcrumbs={CRUMBS}>
      <form onSubmit={handleCreateCall}>
        <FormGrid>
          <TextInput
            value={title}
            onValueChange={setTitle}
            label="Title"
            labelPlacement="outside-left"
          />
          <TextInput
            value={code}
            onValueChange={setCode}
            label="Code"
            labelPlacement="outside-left"
          />
          <TextArea
            value={description}
            onValueChange={setDescription}
            label="Description"
            labelPlacement="outside-left"
          />
          <TextInput
            value={cycle}
            isDisabled={true}
            label="Cycle"
            labelPlacement="outside-left"
          />
        </FormGrid>
        <FormGrid>
          {/* TODO design system: DateInput should support rendering with custom timezone (UTC) */}
          <DateInput
            value={startDate}
            onValueChange={(value: unknown) => setStartDate(value as Date)}
            label="Opening"
            labelPlacement="outside-left"
            showTimeSelect={true}
          />
          <DateInput
            value={endDate}
            onValueChange={(value: unknown) => setEndDate(value as Date)}
            label="Submission deadline"
            labelPlacement="outside-left"
            showTimeSelect={true}
          />
          <Toggle
            isSelected={isDirectAction}
            setIsSelected={setIsDirectAction}
            label="Direct action"
            labelPlacement="left"
          />
        </FormGrid>
        <FlexRow justify="start">
          <Button label="Cancel" icon="Cross" color="negative" />
          <Button label="Create" icon="Next" color="positive" type="submit" />
        </FlexRow>
      </form>
    </Layout>
  );
};

export default CreateCall;
