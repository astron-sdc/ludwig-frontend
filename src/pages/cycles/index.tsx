import { Button, Table } from "@astron-sdc/design-system";
import Api from "src/api/Api.ts";
import { useState } from "react";
import { useNavigate } from "react-router-dom";
import useSWR from "swr";
import Layout from "src/components/Layout.tsx";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/proposal/",
  },
  {
    name: "Cycle Overview",
    href: "/proposal/cycles/",
  },
];

const COLUMNS = [
  {
    field: "name",
    header: "Cycle",
  },
  {
    field: "start",
    header: "Start observing",
  },
  {
    field: "end",
    header: "End of observing",
  },
  {
    field: "code",
    header: "Code",
  },
  {
    field: "status",
    header: "Status",
  },
  {
    field: "actions",
    header: "Action",
  },
];

interface State {
  showFilters: boolean;
  filters: object[];
}

/**
 * Action buttons for the Cycle table
 * @param props
 * @constructor
 */
const CycleTableAction = (props: { cycleId: number }) => {
  const navigate = useNavigate();
  const { cycleId } = props;

  const navigateToCycleEdit = () => {
    navigate(`/cycles/${cycleId}/`);
  };

  const navigateToCallCreate = () => {
    navigate(`/cycles/${cycleId}/calls/create/`);
  };

  return (
    <div className="flex flex-row gap-3 w-fit">
      <Button
        color="secondary"
        icon="Edit"
        label="Edit"
        onClick={navigateToCycleEdit}
      />
      <Button
        color="positive"
        icon="Download"
        label="Create call"
        onClick={navigateToCallCreate}
      />
    </div>
  );
};

/**
 * fetchCycles; adds key and action on Data
 */
const fetchCycles = async () => {
  const cycles = await Api.getCycles();
  const mapped = cycles.map((cycle) => ({
    ...cycle,
    key: cycle.id,
    start: new Date(cycle.start).toLocaleDateString(),
    end: new Date(cycle.end).toLocaleDateString(),
    actions: <CycleTableAction cycleId={cycle.id} />,
  }));

  return mapped;
};

const CyclesOverview = () => {
  const navigate = useNavigate();
  const { data, error, isLoading } = useSWR("/api/v1/cycles", fetchCycles);
  const [state, setState] = useState<State>({
    showFilters: false,
    filters: [],
  });

  const handleCycleCreate = () => {
    navigate("/cycles/create/");
  };

  const handleShowFilters = () => {
    setState((prev) => ({ ...prev, showFilters: !prev.showFilters }));
  };

  // TODO: error state
  if (error) {
    console.log(error);
    return <div>Something went wrong</div>;
  }

  return (
    <Layout title="Cycles Overview" breadcrumbs={CRUMBS}>
      <Table
        columns={COLUMNS}
        headerContent={
          <div className="flex flex-col">
            <div className="flex flex-row gap-4 pb-[10px]">
              <Button
                color="secondary"
                icon={state.showFilters ? "Up" : "Dropdown"}
                label="Filter"
                onClick={handleShowFilters}
              />
              <Button
                color="neutral"
                icon="Layer"
                label="Create Cycle"
                onClick={handleCycleCreate}
              />
            </div>
            {state.showFilters && (
              <div className="flex flex-row gap-4 pb-[10px]">
                <span>Filters go here</span>
              </div>
            )}
          </div>
        }
        rows={isLoading ? [] : data}
      />
    </Layout>
  );
};

export default CyclesOverview;
