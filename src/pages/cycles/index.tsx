import { Button, Table } from "@astron-sdc/design-system";
import Api from "src/api/Api.ts";
import { Link, useNavigate } from "react-router-dom";
import useSWR from "swr";
import Layout from "src/components/Layout.tsx";
import FlexRow from "src/components/FlexRow.tsx";
import { formatDateTime } from "src/utils/DateTime.ts";
import Cycle from "src/api/models/Cycle";
import { useContext } from "react";
import { AuthContext } from "src/auth/AuthContext";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/",
  },
  {
    name: "Cycle Overview",
    href: "/cycles/",
  },
];

const COLUMNS = [
  {
    field: "name",
    header: "Cycle",
  },
  {
    field: "start",
    header: "Start observing",
  },
  {
    field: "end",
    header: "End of observing",
  },
  {
    field: "code",
    header: "Code",
  },
  {
    field: "actions",
    header: "Action",
  },
];

/**
 * Action buttons for the Cycle table
 * @param props
 * @constructor
 */
const CycleTableAction = (props: { cycleId: number; cycles: Cycle[] }) => {
  const { permissions } = useContext(AuthContext);
  const navigate = useNavigate();
  const { cycleId, cycles } = props;

  const navigateToCycleEdit = () => {
    navigate(`/cycles/${cycleId}/edit`);
  };

  const navigateToCycleCreate = () => {
    navigate(`/cycles/${cycleId}/calls/create/`, { state: { cycles: cycles } });
  };

  return (
    <FlexRow customClass="w-fit">
      <Button label="View" icon="Eye" color="neutral" onClick={() => navigate(`/cycles/${cycleId}/view`)} />
      {permissions?.create_call && (
        <>
          <Button color="secondary" icon="Edit" label="Edit" onClick={navigateToCycleEdit} />
          <Button color="positive" icon="Plus" label="Create call for proposals" onClick={navigateToCycleCreate} />
        </>
      )}
    </FlexRow>
  );
};

/**
 * fetchCycles; adds key and action on Data
 */
const fetchCycles = async () => {
  const cycles = await Api.getCycles();
  return cycles.map((cycle) => ({
    ...cycle,
    name: (
      <Link className="underline" to={`/cycles/${cycle.id}/view`}>
        {cycle.name}
      </Link>
    ),
    key: cycle.id,
    start: formatDateTime(cycle.start),
    end: formatDateTime(cycle.end),
    actions: <CycleTableAction cycleId={cycle.id} cycles={cycles} />,
  }));
};

const CyclesOverview = () => {
  const { permissions: globalPermissions } = useContext(AuthContext);
  const navigate = useNavigate();
  const { data, error, isLoading } = useSWR("/api/v1/cycles", fetchCycles);
  const handleCycleCreate = () => {
    navigate("/cycles/create/");
  };

  if (error) {
    throw Error(error);
  }

  return (
    <Layout title="Cycles Overview" breadcrumbs={CRUMBS}>
      <Table
        columns={COLUMNS}
        headerContent={
          globalPermissions?.create_cycle && (
            <div className="flex flex-col">
              <FlexRow customClass="pb-[10px]">
                <Button color="neutral" icon="Layer" label="Create Cycle" onClick={handleCycleCreate} />
              </FlexRow>
            </div>
          )
        }
        rows={isLoading || !data ? [] : data}
      />
    </Layout>
  );
};

export default CyclesOverview;
