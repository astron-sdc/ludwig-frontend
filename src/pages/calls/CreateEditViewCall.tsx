import React, { useContext, useState } from "react";
import FlexRow from "src/components/FlexRow";
import FormGrid from "src/components/FormGrid";
import { Button, DateInput, Dropdown, Tabs, TextArea, TextInput, Toggle } from "@astron-sdc/design-system";
import Layout from "src/components/Layout";
import { useLoaderData, useLocation, useNavigate, useParams } from "react-router-dom";
import ToastContext from "src/components/Toast/Context";
import { formatDateTimeForApi } from "src/utils/DateTime";
import Call from "src/api/models/Call";
import { Category } from "src/api/constants/Category";
import Cycle from "src/api/models/Cycle";
import ReviewAssignmentTable from "src/components/Review/ReviewAssignment/ReviewAssignmentTable";
import CallPermissions from "src/api/models/CallPermissions";
import Api from "src/api/Api.ts";
import CallMembersTab from "src/components/MembersTab/CallMembersTab";
import { saveCall } from "src/helper/CallHelper";
import useSWRImmutable from "swr/immutable";
import { AuthContext } from "src/auth/AuthContext";
import CallResourceEditor from "src/components/Call/CallResourceEditor";
import CallResources from "src/api/models/CallResources";
import { mutate } from "swr";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/",
  },
  {
    name: "Calls for proposals",
    href: "/calls/",
  },
  {
    name: "Call for proposal",
    href: "",
  },
];

interface NavigationState {
  cycles: Cycle[];
}

const CreateEditViewCall = () => {
  const location = useLocation();
  const { permissions: globalPermissions } = useContext(AuthContext);
  const navigate = useNavigate();
  const params = useParams();
  const { setToastMessage } = useContext(ToastContext);

  const loaderData = useLoaderData() as [Call, CallPermissions];
  const [loadedCall, permissions] = loaderData ?? [null, {}];

  const [name, setName] = useState(loadedCall?.name ?? "");
  const [code, setCode] = useState(loadedCall?.code ?? "");
  const [description, setDescription] = useState(loadedCall?.description ?? "");
  const [cycle, setCycle] = useState<number>(
    loadedCall?.cycle_id ?? (Number.isInteger(Number(params.cycleId)) ? params.cycleId : 0),
  );

  const { data: callCollaboration } = useSWRImmutable([loadedCall, permissions], ([call, permissions]) => {
    if (permissions.view_call_members && call) return Api.getCallMembers(call.id);
  });

  const canEdit: boolean = globalPermissions?.create_call ?? false;
  const { cycles } = (location.state as NavigationState) ?? { cycles: [] };
  const alignedCycle = cycles.find((singlecycle) => singlecycle.id === (cycle ? Number(cycle) : NaN));
  const [cycleName] = useState(loadedCall?.cycle_name ?? alignedCycle?.name ?? "");
  const [startDate, setStartDate] = useState((loadedCall?.start as Date) ?? new Date());
  const [endDate, setEndDate] = useState((loadedCall?.end as Date) ?? new Date());
  const [isDirectAction, setIsDirectAction] = useState(loadedCall?.requires_direct_action ?? false);
  const [category, setCategory] = useState<string | undefined>(loadedCall?.category ?? "regular");
  const isViewMode = params["command"] === "view";

  const [callResources, setCallResources] = useState<CallResources>(
    (loadedCall as CallResources) ?? {
      observing_time_seconds: 0,
      cpu_time_seconds: 0,
      storage_space_bytes: 0,
      support_time_seconds: 0,
    },
  );

  const getTitle = (): string => {
    if (isViewMode) {
      return "View Call for Proposals";
    }
    if (loadedCall) {
      return "Edit Call for Proposals";
    }
    return "Create Call for Proposals";
  };

  const handleCreateCall = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    e.stopPropagation();
    saveCall({
      id: loadedCall?.id,
      name: name,
      code: code,
      description: description,
      start: formatDateTimeForApi(startDate),
      end: formatDateTimeForApi(endDate),
      requires_direct_action: isDirectAction,
      cycle_id: cycle,
      category: category,
      ...callResources,
    })
      .then(() => {
        navigate(`/calls/`);
      })
      .catch((error) => {
        setToastMessage({
          title: "Error saving Call",
          message: error.message,
          alertType: "negative",
          expireAfter: 5000,
        });
      });
  };

  const tabkey = "proposals"; // TODO: make this a param (and make switching tabs set the params!)

  const tabs = [
    {
      key: "proposals",
      title: "Proposals",
      content: loadedCall ? <ReviewAssignmentTable call={loadedCall} permissions={permissions} /> : <></>,
    },
    {
      key: "members",
      title: "Members",
      disabled: !permissions.view_call_members,
      content: (
        <CallMembersTab
          callCollaboration={callCollaboration}
          permissions={permissions}
          callId={loadedCall?.id}
          onMembersChanged={() => {
            mutate([loadedCall, permissions]);
          }}
        />
      ),
    },
    {
      key: "available_resources",
      title: "Available Resources",
      disabled: !permissions.view_call_members,
      content: <CallResourceEditor isReadOnly={true} onResourcesUpdated={setCallResources} callResources={callResources} />,
    },
  ];

  return (
    <Layout title={getTitle()} breadcrumbs={CRUMBS}>
      <form onSubmit={handleCreateCall}>
        <FormGrid>
          <TextInput
            value={name}
            onValueChange={setName}
            label="Title"
            labelPlacement="outside-left"
            isReadOnly={isViewMode || !canEdit}
          />
          <TextInput
            value={code}
            onValueChange={setCode}
            label="Code"
            labelPlacement="outside-left"
            isReadOnly={isViewMode || !canEdit}
          />
          <TextArea
            value={description}
            onValueChange={setDescription}
            label="Description"
            labelPlacement="outside-left"
            isReadOnly={isViewMode || !canEdit}
          />
          {isViewMode || loadedCall ? (
            <TextInput value={cycleName} isReadOnly={true} label="Cycle" labelPlacement="outside-left" />
          ) : (
            <Dropdown
              labelPlacement="outside-left"
              label="Cycle"
              value={`${cycle ?? ""}`}
              valueOptions={cycles.map((call) => ({
                label: call.name,
                value: call.id.toString(),
              }))}
              onValueChange={(c) => setCycle(Number(c))}
              isRequired={true}
            />
          )}

          <Dropdown
            labelPlacement="outside-left"
            label="Category"
            value={category}
            valueOptions={Category}
            onValueChange={setCategory}
            isRequired={true}
            isDisabled={isViewMode || !canEdit}
          ></Dropdown>
        </FormGrid>
        <FormGrid>
          <DateInput
            value={startDate}
            onValueChange={(value) => setStartDate(value as Date)}
            label="Opening"
            labelPlacement="outside-left"
            showTimeSelect={true}
            isDisabled={isViewMode || !canEdit}
            forceUTC={true}
          />
          <DateInput
            value={endDate}
            onValueChange={(value) => setEndDate(value as Date)}
            label="Submission deadline"
            labelPlacement="outside-left"
            showTimeSelect={true}
            isDisabled={isViewMode || !canEdit}
            forceUTC={true}
          />
          <Toggle
            isSelected={isDirectAction}
            setIsSelected={setIsDirectAction}
            label="Direct action"
            labelPlacement="left"
            isDisabled={isViewMode || !canEdit}
          />
        </FormGrid>
        {canEdit && !isViewMode && (
          <CallResourceEditor isReadOnly={!canEdit} onResourcesUpdated={setCallResources} callResources={callResources} />
        )}
        {isViewMode && (
          <FlexRow>
            <div className="flex-initial w-full mb-4">
              <Tabs tabs={tabs} selectedTabKey={tabkey}></Tabs>
            </div>
          </FlexRow>
        )}

        <FlexRow justify="start">
          {canEdit && !isViewMode ? (
            <>
              <Button label="Cancel" icon="Cross" color="negative" onClick={() => navigate("/calls")} />
              <Button label="Save" icon="Next" color="positive" type="submit" />
            </>
          ) : (
            <Button label="Back" icon="Previous" onClick={() => navigate("/calls")} />
          )}
        </FlexRow>
      </form>
    </Layout>
  );
};

export default CreateEditViewCall;
