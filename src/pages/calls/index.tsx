import Layout from "src/components/Layout.tsx";
import { Badge, Button, Table } from "@astron-sdc/design-system";
import useSWR from "swr";
import Api from "src/api/Api.ts";
import { CallStatus } from "src/api/models/Call.ts";
import { Link, useNavigate } from "react-router-dom";
import { formatDateTime } from "src/utils/DateTime.ts";
import FlexRow from "src/components/FlexRow";
import { useContext } from "react";
import { AuthContext } from "src/auth/AuthContext";

const pageTitle = "Calls for Proposals";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/",
  },
  {
    name: pageTitle,
    href: "/calls/",
  },
];

const COLUMNS = [
  {
    field: "name",
    header: "Call",
  },
  {
    field: "cycle",
    header: "Period",
  },
  {
    field: "category",
    header: "Category",
  },
  {
    field: "cycle_start",
    header: "Start observing",
  },
  {
    field: "cycle_end",
    header: "End of observing",
  },
  {
    field: "submission_deadline",
    header: "Submission deadline",
  },

  {
    field: "status",
    header: "Status",
  },
  {
    field: "actions",
    header: "Actions",
  },
];

const badgeColor = (status: CallStatus) => {
  switch (status) {
    case "open":
      return "positive";
    case "closed":
      return "negative";
  }
};

const ActionButtons = (props: { callId: number; callStatus: CallStatus }) => {
  const { permissions: globalPermissions } = useContext(AuthContext);
  const navigate = useNavigate();

  return (
    <FlexRow customClass="w-fit">
      <Button label="View" icon="Eye" color="neutral" onClick={() => navigate(`/calls/${props.callId}/view`)} />
      {props.callStatus === "open" && (
        <>
          {globalPermissions?.create_call && (
            <Button icon="Edit" color="secondary" onClick={() => navigate(`/calls/${props.callId}/edit`)} />
          )}
          <Button
            label="Create new proposal"
            icon="Download"
            color="positive"
            onClick={() => navigate(`/proposals/create?call=${props.callId}`)}
          />
        </>
      )}
    </FlexRow>
  );
};

const fetchCalls = async () => {
  const calls = await Api.getCalls();
  return calls.map((call) => ({
    ...call,
    name: (
      <Link className="underline" to={`/calls/${call.id}/view`}>
        {call.name}
      </Link>
    ),
    key: call.id,
    cycle: call.cycle_id,
    cycle_start: formatDateTime(call.cycle_start),
    cycle_end: formatDateTime(call.cycle_end),
    submission_deadline: formatDateTime(call.end),
    status: <Badge text={call.status} color={badgeColor(call.status)} />,
    actions: <ActionButtons callId={call.id} callStatus={call.status} />,
  }));
};

const CallsOverview = () => {
  const { permissions: globalPermissions } = useContext(AuthContext);
  const { data, error, isLoading } = useSWR("/api/v1/calls", fetchCalls);
  const navigate = useNavigate();
  const navigateToCallCreate = async () => {
    navigate("/calls/create/", { state: { cycles: await Api.getCycles() } });
  };

  if (error) {
    throw Error(error);
  }
  return (
    <Layout title={pageTitle} breadcrumbs={CRUMBS}>
      <Table
        columns={COLUMNS}
        headerContent={
          globalPermissions?.create_call && (
            <div className="flex flex-col">
              <div className="flex flex-row gap-4 pb-[10px]">
                <Button color="positive" icon="Plus" label="Create call for proposals" onClick={navigateToCallCreate} />
              </div>
            </div>
          )
        }
        rows={isLoading || !data ? [] : data}
      />
    </Layout>
  );
};

export default CallsOverview;
