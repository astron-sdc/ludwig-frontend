import { useLoaderData } from "react-router-dom";
import Call from "src/api/models/Call.ts";
import useSWRImmutable from "swr/immutable";
import Api from "src/api/Api.ts";
import { Spinner } from "@astron-sdc/design-system";
import ReviewTable from "src/components/Review/ReviewTable.tsx";
import Layout from "src/components/Layout.tsx";

const CallReview = () => {
  const loadedCall = useLoaderData() as Call;

  const { data: proposals, isLoading } = useSWRImmutable(`${loadedCall.id}/proposals`, () => {
    return Api.getProposalsForCall(loadedCall.id);
  });

  const CRUMBS = [
    {
      name: "Science Portal",
      href: "/",
    },
    {
      name: "Proposal Tool",
      href: "/proposal/",
    },
    {
      name: "Calls for proposals",
      href: "/calls/",
    },
    {
      name: "Call for proposal",
      href: `/calls/${loadedCall.id}/view`,
    },
    {
      name: "Call Review",
      href: "",
    },
  ];

  return (
    <Layout title={"Call Review"} breadcrumbs={CRUMBS}>
      {isLoading ? <Spinner label="Fetching Proposals" /> : <ReviewTable proposals={proposals} />}
    </Layout>
  );
};

export default CallReview;
