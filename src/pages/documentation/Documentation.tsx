import { useState, useEffect, ImgHTMLAttributes, HTMLAttributes } from "react";
import Markdown from "markdown-to-jsx";
import { useParams } from "react-router-dom";
import Layout from "src/components/Layout";
import DocumentationHeader from "src/components/documentation/DocumentationHeader.tsx";
import MarkdownImage from "src/components/documentation/MarkdownImage.tsx";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/",
  },
  {
    name: "Documentation",
    href: "/documentation/",
  },
];

const Documentation = () => {
  const [markdownContent, setMarkdownContent] = useState("");
  const [docPathFolder, setDocPathFolder] = useState("");
  const { "*": docPath } = useParams();

  useEffect(() => {
    const fetchMarkdown = async () => {
      const url = `${import.meta.env.BASE_URL}/docs/${docPath || "index.md"}`;

      try {
        const response = await fetch(url);
        if (!response.ok) {
          throw new Error("Failed to fetch Markdown");
        }

        const markdownText = await response.text();
        setMarkdownContent(markdownText);

        const docPathFolder = url?.split("/").slice(0, -1).join("/");
        setDocPathFolder(docPathFolder);
      } catch (error) {
        console.error("Error fetching Markdown:", error);
      }
    };
    fetchMarkdown();
  }, [docPath]);

  const options = {
    overrides: {
      h1: {
        component: (htmlProps: HTMLAttributes<HTMLElement>) => DocumentationHeader(htmlProps, "h1"),
      },
      h2: {
        component: (htmlProps: HTMLAttributes<HTMLElement>) => DocumentationHeader(htmlProps, "h2"),
      },
      h3: {
        component: (htmlProps: HTMLAttributes<HTMLElement>) => DocumentationHeader(htmlProps, "h3"),
      },
      h4: {
        component: (htmlProps: HTMLAttributes<HTMLElement>) => DocumentationHeader(htmlProps, "h4"),
      },
      h5: {
        component: (htmlProps: HTMLAttributes<HTMLElement>) => DocumentationHeader(htmlProps, "h5"),
      },
      ul: {
        props: {
          className: "list-disc pl-4",
        },
      },
      a: {
        props: {
          className: "underline",
        },
      },
      img: {
        component: (props: ImgHTMLAttributes<HTMLImageElement>) => MarkdownImage(props, docPathFolder),
      },
    },
  };

  return (
    <Layout title="Documentation" breadcrumbs={CRUMBS}>
      <Markdown options={options}>{markdownContent}</Markdown>
    </Layout>
  );
};

export default Documentation;
