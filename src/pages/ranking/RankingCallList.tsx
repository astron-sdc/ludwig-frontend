import { Link, useNavigate } from "react-router-dom";
import FlexRow from "src/components/FlexRow";
import { Button, Table } from "@astron-sdc/design-system";
import Api from "src/api/Api";
import { formatDateTime } from "src/utils/DateTime";
import useSWR from "swr";
import Layout from "src/components/Layout";
import { CallStatus } from "src/api/models/Call";

const pageTitle = "Ranking";

const CRUMBS = [
  {
    name: "Science Portal",
    href: "/",
  },
  {
    name: "Proposal Tool",
    href: "/",
  },
  {
    name: "Ranking Overview",
    href: "/ranking/",
  },
];

const COLUMNS = [
  {
    field: "name",
    header: "Call",
  },
  {
    field: "cycle_start",
    header: "Start observing",
  },
  {
    field: "cycle_end",
    header: "End of observing",
  },
  {
    field: "submission_deadline",
    header: "Submission deadline",
  },
  {
    field: "actions",
    header: "Actions",
  },
];

const ActionButtons = (props: { callId: number; callStatus: CallStatus }) => {
  const navigate = useNavigate();

  return (
    <FlexRow customClass="w-fit">
      <Button
        label="Allocate proposals"
        icon="TMSS"
        color="neutral"
        onClick={() => navigate(`/allocation/${props.callId}/`)}
      />
      {/* TODO: check for finished status */}
    </FlexRow>
  );
};

const fetchCalls = async () => {
  const calls = await Api.getCalls(false);
  return calls.map((call) => ({
    ...call,
    name: (
      <Link className="underline" to={`/calls/${call.id}/view`}>
        {call.name}
      </Link>
    ),
    key: call.id,
    cycle_start: formatDateTime(call.cycle_start),
    cycle_end: formatDateTime(call.cycle_end),
    submission_deadline: formatDateTime(call.end),
    actions: <ActionButtons callId={call.id} callStatus={call.status} />,
  }));
};

const RankingCallList = () => {
  const { data, error, isLoading } = useSWR("/api/v1/calls", fetchCalls);
  if (error) {
    throw Error(error);
  }
  return (
    <Layout title={pageTitle} breadcrumbs={CRUMBS}>
      <Table columns={COLUMNS} rows={isLoading || !data ? [] : data} />
    </Layout>
  );
};

export default RankingCallList;
