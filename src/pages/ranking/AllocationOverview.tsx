import { FC, useState } from "react";
import { useLoaderData } from "react-router-dom";
import Call from "src/api/models/Call";
import CallPermissions from "src/api/models/CallPermissions";
import Layout from "src/components/Layout";
import AllocationDetailsPanel from "src/components/AllocationDetailsPanel";
import ResourceAllocationTable from "src/components/Ranking/ResourceAllocationTable";
import Api from "src/api/Api";
import useSWRImmutable from "swr/immutable";
import { Spinner } from "@astron-sdc/design-system";
import CallTotalsTable from "./CallTotalsTable";

const AllocationOverview: FC = () => {
  const [call, callPermissions] = useLoaderData() as [Call, CallPermissions];
  const [selectedProposalIds, setSelectedProposalIds] = useState<number[]>([]);
  const {
    data: proposals,
    isLoading,
    error,
  } = useSWRImmutable(`${call.id}/proposals`, () => Api.getProposalsForCall(call.id));
  const CRUMBS = [
    {
      name: "Science Portal",
      href: "/",
    },
    {
      name: "Proposal Tool",
      href: "/",
    },
    {
      name: "Ranking Overview",
      href: "/ranking/",
    },
    {
      name: call.name,
      href: `/ranking/${call.id}`,
    },
  ];

  if (error) throw Error(error);

  return (
    <Layout title={`Allocation: ${call.name}`} breadcrumbs={CRUMBS}>
      {isLoading ? (
        <Spinner />
      ) : (
        <div className="grid grid-cols-12 gap-2">
          <div className="col-span-4">
            <CallTotalsTable call={call} />
          </div>
          <div className="col-span-8">
            <AllocationDetailsPanel call={call} selectedProposalsIds={selectedProposalIds} />
          </div>
          <div className="col-span-12">
            <ResourceAllocationTable
              call={call}
              permissions={callPermissions}
              proposals={proposals ?? []}
              selectedProposalsIds={selectedProposalIds}
              selectedProposalsChanged={setSelectedProposalIds}
            />
          </div>
        </div>
      )}
    </Layout>
  );
};

export default AllocationOverview;
