import { Table, Typography } from "@astron-sdc/design-system";
import Call from "src/api/models/Call";
import Panel from "src/components/Panel/Panel";
import { bytesToGigaBytes, secondsToHours } from "src/utils/Numbers";

interface CallTotalsTableProps {
  call: Call;
}

const CallTotalsTable = ({ call }: CallTotalsTableProps) => {
  const rows = [
    {
      resource: "Storage",
      call_total: bytesToGigaBytes(call.storage_space_bytes),
      assigned: 0,
      difference: 0,
    },
    {
      resource: "Support",
      call_total: secondsToHours(call.support_time_seconds),
      assigned: 0,
      difference: 0,
    },
    {
      resource: "Observation time",
      call_total: secondsToHours(call.observing_time_seconds),
      assigned: 0,
      difference: 0,
    },
    {
      resource: "Compute time",
      call_total: secondsToHours(call.cpu_time_seconds),
      assigned: 0,
      difference: 0,
    },
  ];

  const COLUMNS = [
    {
      field: "resource",
      header: "Resource",
    },
    {
      field: "call_total",
      header: "Call total",
    },
    {
      field: "assigned",
      header: "Assigned",
    },
    {
      field: "difference",
      header: "Difference",
    },
  ];

  return (
    <Panel>
      <Typography text="Totals" variant="h5" />
      <Table columns={COLUMNS} rows={rows} />
    </Panel>
  );
};

export default CallTotalsTable;
