import { NextUIProvider } from "@nextui-org/react";
import { QueryClient, QueryClientProvider } from "react-query";
import { Outlet, useNavigate } from "react-router-dom";
import { AuthProvider } from "src/auth/AuthContext";
import ThemeProvider from "src/components/Theme/Provider";
import ToastProvider from "src/components/Toast/Provider";

const Root = () => {
  const queryClient = new QueryClient();
  const navigate = useNavigate();
  return (
    <NextUIProvider navigate={navigate}>
      <ThemeProvider>
        <AuthProvider>
          <QueryClientProvider client={queryClient}>
            <ToastProvider>
              <Outlet />
            </ToastProvider>
          </QueryClientProvider>
        </AuthProvider>
      </ThemeProvider>
    </NextUIProvider>
  );
};

export default Root;
