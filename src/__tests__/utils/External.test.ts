// openProjectInTMSS.test.ts
import Proposal, { ProposalStatus } from "src/api/models/Proposal";
import { openProjectInTMSS } from "src/utils/external";
import { describe, it, expect, vi, afterEach, beforeEach } from "vitest";

describe("openProjectInTMSS", () => {
  const originalOpen = window.open;

  beforeEach(() => {
    window.open = vi.fn();
    vi.resetModules();
  });

  afterEach(() => {
    window.open = originalOpen;
  });

  it("should do nothing if the proposal is undefined", () => {
    openProjectInTMSS(undefined);
    expect(window.open).not.toHaveBeenCalled();
  });

  it("should open the correct URL if proposal is provided", () => {
    const originalEnv = process.env.TMSS_APP_URL;
    process.env.TMSS_APP_URL = "http://tmss.lofar.eu";

    // Mock proposal object
    const mockProposal: Proposal = {
      project_code: "X",
      status: ProposalStatus.draft,
      collaboration_url: "",
      groups_url: "",
      members_url: "",
      join_requests_url: "",
      members: [],
      groups: [],
      invite_link: "",
      tmss_project_id: "12345",
      title: "",
      abstract: "",
      call_id: 0,
      id: 42,
    };

    openProjectInTMSS(mockProposal);

    const expectedUrl = "http://tmss.lofar.eu/project/view/12345";
    expect(window.open).toHaveBeenCalledWith(expectedUrl, "_blank");

    process.env.TMSS_APP_URL = originalEnv;
  });
});
