import ReviewedProposal from "src/api/models/ReviewedProposal";
import { calculateQuartile, piFromReviewedProposal } from "src/utils/Proposal";
import { describe, it, expect } from "vitest";
import { reviewedProposalFactory } from "src/__tests__/factories/Proposal.factory";

describe("piFromReviewedProposal", () => {
  it("should return the PI from the reviewed proposal", () => {
    const reviewedProposals: ReviewedProposal[] = [
      {
        id: 1,
        submitted_at: "2024-07-16T12:42:40.404113Z",
        title: "MY PROPOSAL",
        groups: [
          {
            name: "PI",
            description: "some group description",
            short_name: "pi",
            members: [
              {
                name: "John Doe",
                eduperson_unique_id: "pipo@sram.surf.nl",
                email: "john@example.com",
                scoped_affiliation: "employee@astron.nl",
              },
              {
                name: "Foo Bar",
                eduperson_unique_id: "foo@sram.surf.nl",
                email: "lorem@ipsum.com",
                scoped_affiliation: "random@person.nl",
              },
            ],
          },
        ],
        reviews: [
          {
            id: 1,
            ranking: 1,
            reviewer_id: "abc@sram.surf.nl",
            proposal_id: 1,
          },
        ],
      },
    ] as ReviewedProposal[];

    const result = piFromReviewedProposal(reviewedProposals[0]);
    expect(result).toBe("John Doe, Foo Bar");
  });
});

describe("calculateQuartile", () => {
  // TODO add tests
  it("should return the correct quartile for the review ratings", () => {
    const reviewedProposals = reviewedProposalFactory.buildList(10).map((proposal, index) => ({
      ...proposal,
      review_rating: index + 1,
    }));
    const lowestReviewRating = Math.min(...reviewedProposals.map((p) => p.review_rating));
    const highestReviewRating = Math.max(...reviewedProposals.map((p) => p.review_rating));

    expect(calculateQuartile(lowestReviewRating, reviewedProposals)).toBe(1);
    expect(calculateQuartile(highestReviewRating, reviewedProposals)).toBe(4);
  });
});
