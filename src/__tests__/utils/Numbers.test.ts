import { describe, it, expect } from "vitest";
import {
  teraBytesToBytes,
  bytesToTeraBytes,
  hoursToSeconds,
  secondsToHours,
  gigaBytesToBytes,
  bytesToGigaBytes,
  gibiBytesToBytes,
  bytesToGibiBytes,
} from "src/utils/Numbers";
import { formatBytesAsGibibytes, formatSecondsAsHours } from "src/utils/Resources";

describe("Number Convertion", () => {
  it("should Convert back and forth Bytes to Terabytes", () => {
    const bytes = teraBytesToBytes(1);
    const tb = bytesToTeraBytes(bytes);
    expect(bytes).toBe(1000000000000);
    expect(tb).toBe("1");
  });

  it("should Convert back and forth Bytes to Gigabytes", () => {
    const bytes = gigaBytesToBytes(1);
    const gb = bytesToGigaBytes(bytes);
    expect(bytes).toBe(1000000000);
    expect(gb).toBe("1");
  });

  it("should Convert back and forth Bytes to Gibibytes", () => {
    const bytes = gibiBytesToBytes(1);
    const gib = bytesToGibiBytes(bytes);
    expect(bytes).toBe(1073741824);
    expect(gib).toBe("1");
  });

  it("should Convert back and forth Hours", () => {
    const seconds = hoursToSeconds(1);
    const hours = secondsToHours(seconds);
    expect(seconds).toBe(3600);
    expect(hours).toBe("1");
    expect(hoursToSeconds(0.1666)).toBe(600);
    expect(secondsToHours(600)).toBe((1 / 6).toString());
    expect(secondsToHours(1)).toBe((1 / 3600).toString());
    expect(hoursToSeconds("0.00027")).toBe(1);
    expect(hoursToSeconds("1.00028")).toBe(3601);
  });

  it("should format bytes as number to gibibytes as string with decimals", () => {
    const gib = formatBytesAsGibibytes(1000_000_000);
    expect(gib).toEqual("0.93");
  });

  it("should format seconds as number to hours as string", () => {
    const hours = formatSecondsAsHours(1111);
    expect(hours).toEqual("0.309");
  });
});
