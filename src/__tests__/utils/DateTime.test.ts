import { describe, it, expect, beforeEach, afterEach, vi } from "vitest";
import { formatDateYYYYMMDD, formatDateTimeForApi, dateIsTodayOrInFuture } from "src/utils/DateTime"; // Update with the correct path

describe("formatDateTimeForApi", () => {
  it("should return ISO string for Date object", () => {
    const date = new Date("2023-07-21T10:20:30Z");
    const result = formatDateTimeForApi(date);
    expect(result).toBe(date.toISOString());
  });

  it("should return the same string for a date string input", () => {
    const dateString = "2023-07-21T10:20:30Z";
    const result = formatDateTimeForApi(dateString);
    expect(result).toBe(dateString);
  });
});

describe("formatDateYYYYMMDD", () => {
  it("should convert the date string to YYYY-MM-DD", () => {
    const dateString = "2024-01-21T10:20:30Z";
    const expectedString = "2024-01-21";
    const result = formatDateYYYYMMDD(dateString);
    expect(result).toBe(expectedString);
  });
});

describe("dateIsTodayOrInFuture", () => {
  beforeEach(() => {
    vi.useFakeTimers();
  });

  afterEach(() => {
    vi.useRealTimers();
  });

  it.each([
    ["2025-02-09", "2025-02-10", false],
    ["1960-02-09", "2025-02-10", false],
    ["2024-02-29", "2024-02-29", true],
    ["2024-12-31", "2024-12-31", true],
    ["2025-01-01", "2025-01-01", true],
    ["9999-12-31", "9999-12-31", true],
    ["2025-02-12T13:00:00.000Z", "2025-02-12T23:00:00.000Z", true], // should ignore the time
    [new Date().toISOString(), new Date().toISOString(), true],
    [new Date(Date.now() + 86400000).toISOString(), new Date(Date.now()).toISOString(), true],
  ])("should handle date %s when today is %s and return %s", (inputDate, mockToday, expected) => {
    const date = new Date(inputDate);
    vi.setSystemTime(new Date(mockToday));
    const result = dateIsTodayOrInFuture(date);
    expect(result).toBe(expected);
  });
});
