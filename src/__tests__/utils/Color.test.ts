import { describe, it, expect } from "vitest";
import Proposal, { ProposalStatus } from "src/api/models/Proposal";
import { statusToColor } from "src/utils/color";
import { ColorType } from "@astron-sdc/design-system";

// Define test cases in an array
const testCases: { status: ProposalStatus | undefined; expectedColor: ColorType }[] = [
  { status: ProposalStatus.draft, expectedColor: "neutral" },
  { status: ProposalStatus.submitted, expectedColor: "primary" },
  { status: ProposalStatus.retracted, expectedColor: "warning" },
  { status: ProposalStatus.underReview, expectedColor: "secondary" },
  { status: ProposalStatus.rejected, expectedColor: "negative" },
  { status: ProposalStatus.accepted, expectedColor: "positive" },
  { status: undefined, expectedColor: "warning" },
];

// Test suite for statusToColor using a loop
describe("statusToColor", () => {
  testCases.forEach(({ status, expectedColor }) => {
    it(`should return '${expectedColor}' for status '${status ?? "undefined"}'`, () => {
      const proposal: Partial<Proposal> = {
        status,
        title: "",
        abstract: "",
        call_id: 0,
      };
      const color = statusToColor(proposal.status);
      expect(color).toBe(expectedColor);
    });
  });

  it("should return 'warning' for an invalid status", () => {
    const proposal: Partial<Proposal> = {
      status: "nonExistentStatus" as ProposalStatus,
      title: "",
      abstract: "",
      call_id: 0,
    };
    const color = statusToColor(proposal.status);
    expect(color).toBe("warning");
  });
});
