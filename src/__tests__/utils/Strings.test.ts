import { describe, it, expect } from "vitest";
import { capitalizeFirstLetter } from "src/utils/Strings"; // Update with the correct path

describe("capitalizeFirstLetter", () => {
  it("should capitalize the first character of the string", () => {
    const testString = "hello";
    const result = capitalizeFirstLetter(testString);
    expect(result).toBe("Hello");
  });
});
