import { Factory } from "fishery";
import ProposalPermissions from "src/api/models/ProposalPermissions";

const proposalPermissionFactory = Factory.define<ProposalPermissions>(() => ({
  write_proposal: false,
  submit_proposal: false,
  read_technical_review: false,
  write_technical_review: false,
  read_science_review: false,
  write_science_review: false,
  manage_members: false,
  accept_proposal: false,
  reject_proposal: false,
  read_calculation: false,
  write_calculation: false,
}));

export const noPermissions = proposalPermissionFactory.build();

export const readOnlyPermissions = proposalPermissionFactory.build({
  read_technical_review: true,
  read_science_review: true,
  read_calculation: true,
});

export const readAndWritePermissions = proposalPermissionFactory.build({
  write_proposal: true,
  submit_proposal: false,
  read_technical_review: true,
  write_technical_review: true,
  read_science_review: true,
  write_science_review: true,
  manage_members: false,
  accept_proposal: false,
  reject_proposal: false,
  read_calculation: true,
  write_calculation: true,
});

export const allPermissions = proposalPermissionFactory.build({
  write_proposal: true,
  submit_proposal: true,
  read_technical_review: true,
  write_technical_review: true,
  read_science_review: true,
  write_science_review: true,
  manage_members: true,
  accept_proposal: true,
  reject_proposal: true,
  read_calculation: true,
  write_calculation: true,
});

export default proposalPermissionFactory;
