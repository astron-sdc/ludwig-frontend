import { Factory } from "fishery";
import Userinfo from "src/api/models/Userinfo";
import { AuthProps } from "src/auth/AuthContext";
import globalPermissionsFactory, { adminPermission } from "src/__tests__/factories/GlobalPermissions.factory";

const authPropsFactory = Factory.define<AuthProps>(() => ({
  authenticated: true,
  user: { name: "user name" } as Userinfo,
  permissions: globalPermissionsFactory.build(),
  login: () => {},
  logout: () => {},
  restartSession: () => {},
}));

export const userFactory = Factory.define<Userinfo>(({ sequence }) => ({
  name: "Piebel Gealle",
  uid: `${sequence}@sram.surf.nl`,
  preferred_username: "pgaell",
  given_name: "Piebe",
  familiy_name: "Gealle",
  email: "gealle@example.com",
  email_verified: "true",
  eduperson_entitlement: [],
}));

export const adminAuthProps = () => authPropsFactory.build({ permissions: adminPermission() });
export const userAuthProps = () => authPropsFactory.build();

export default authPropsFactory;
