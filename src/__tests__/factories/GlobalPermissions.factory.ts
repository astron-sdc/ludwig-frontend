import { Factory } from "fishery";
import GlobalPermissions from "src/api/models/GlobalPermissions";

const globalPermissionsFactory = Factory.define<GlobalPermissions>(() => ({
  create_call: false,
  create_cycle: false,
  is_admin: false,
  view_call_ranking: false,
}));

export const adminPermission = () =>
  globalPermissionsFactory.build({ create_call: true, create_cycle: true, is_admin: true, view_call_ranking: true });

export default globalPermissionsFactory;
