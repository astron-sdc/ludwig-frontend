import { Factory } from "fishery";
import CollaborationGroup, {
  collaborationGroupDescriptions,
  collaborationGroupNames,
  CollaborationGroupShortName,
} from "src/api/models/CollaborationGroup.ts";

const collaborationGroupFactory = Factory.define<CollaborationGroup>(({ sequence }) => ({
  identifier: sequence.toString(),
  name: "PI",
  short_name: CollaborationGroupShortName.pi,
  description: "Description of the role",
  members: [],
}));

/**
 * Shorthand for default groups (Roles) for collaborations for proposals with no members
 */
export const defaultProposalCollaborationGroups = () => [
  collaborationGroupFactory.build({ name: collaborationGroupNames.pi, short_name: CollaborationGroupShortName.pi }),
  collaborationGroupFactory.build({
    name: collaborationGroupNames.co_author,
    short_name: CollaborationGroupShortName.co_author,
  }),
  collaborationGroupFactory.build({
    name: collaborationGroupNames.co_author_ro,
    short_name: CollaborationGroupShortName.co_author_ro,
  }),
];

/**
 * Shorthand for default groups (Roles) for collaborations for calls with no members
 */
export const defaultCallCollaborationGroups = () => [
  collaborationGroupFactory.build({
    description: collaborationGroupDescriptions.committee_chair,
    name: collaborationGroupNames.committee_chair,
    short_name: CollaborationGroupShortName.committee_chair,
  }),
  collaborationGroupFactory.build({
    description: collaborationGroupDescriptions.sci_reviewer,
    name: collaborationGroupNames.sci_reviewer,
    short_name: CollaborationGroupShortName.sci_reviewer,
  }),
  collaborationGroupFactory.build({
    description: collaborationGroupDescriptions.sci_reviewer_ch,
    name: collaborationGroupNames.sci_reviewer_ch,
    short_name: CollaborationGroupShortName.sci_reviewer_ch,
  }),
  collaborationGroupFactory.build({
    description: collaborationGroupDescriptions.tech_reviewer,
    name: collaborationGroupNames.tech_reviewer,
    short_name: CollaborationGroupShortName.tech_reviewer,
  }),
  collaborationGroupFactory.build({
    description: collaborationGroupDescriptions.tech_reviewer_ch,
    name: collaborationGroupNames.tech_reviewer_ch,
    short_name: CollaborationGroupShortName.tech_reviewer_ch,
  }),
];

export default collaborationGroupFactory;
