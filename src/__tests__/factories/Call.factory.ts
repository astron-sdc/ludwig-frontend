import { Factory } from "fishery";
import Call, { CallStatus } from "src/api/models/Call";

const callFactory = Factory.define<Call>(({ sequence }) => {
  return {
    id: sequence,
    name: "Name of call",
    code: "LC",
    description: "Call for proposals",
    start: new Date("2024-01-01"),
    end: new Date("2024-12-31"),
    category: "regular",
    requires_direct_action: false,
    status: "open" as CallStatus,
    cycle_id: sequence,
    cycle_start: new Date("2024-01-01"),
    cycle_end: new Date("2024-12-31"),
    cycle_name: "Cycle name",
    observing_time_seconds: sequence * 3600,
    cpu_time_seconds: sequence * 3600,
    storage_space_bytes: sequence * 1e9,
    support_time_seconds: sequence * 3600,
  };
});

export default callFactory;
