import { Factory } from "fishery";
import Cycle from "src/api/models/Cycle";

const cycleFactory = Factory.define<Cycle>(({ sequence }) => ({
  id: sequence,
  name: "Cycle",
  code: "23",
  description: "",
  start: "2023-01-01",
  end: "2023-12-31",
}));

export default cycleFactory;
