import { Factory } from "fishery";
import Review, { PanelType, ReviewerType } from "src/api/models/Review";

const reviewFactory = Factory.define<Review>(({ sequence }) => {
  return {
    id: sequence,
    ranking: 1,
    reviewer_id: `${sequence}@example.com`,
    proposal_id: 1,
    panel_type: PanelType.scientific,
    reviewer_type: ReviewerType.primary,
    reviewer_name: sequence.toString(),
  };
});

export default reviewFactory;
