import { Factory } from "fishery";
import Target from "src/api/models/Target";

const targetFactory = Factory.define<Target>(({ sequence }) => ({
  id: sequence,
  name: `MY TARGET ${sequence}`,
  x_hms_formatted: "19:53:46.99",
  y_dms_formatted: "+18:46:45.0",
  system: "ICRS",
  notes: "",
  group: "1",
  subgroup: "1",
  field_order: "1",
  assigned_priority: "B",
  requested_priority: "B",
}));

export const m31Target = targetFactory.build({
  id: 145,
  name: "M31",
  x_hms_formatted: "00:42:44.33",
  y_dms_formatted: "+41:16:07.5",
  system: "J2000",
  notes: "",
  group: "1",
  subgroup: "1",
  field_order: "1",
  assigned_priority: "B",
  requested_priority: "A",
});

export default targetFactory;
