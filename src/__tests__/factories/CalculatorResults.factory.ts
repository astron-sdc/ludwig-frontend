import { Factory } from "fishery";
import CalculatorResult from "src/api/models/CalculatorOutputs";

const calculatorResultsFactory = Factory.define<CalculatorResult>(({ sequence }) => {
  return {
    proposal: sequence,
    target: sequence,
    target_name: sequence.toString(),
    raw_data_size_bytes: sequence,
    processed_data_size_bytes: sequence,
    pipeline_processing_time_seconds: sequence,
    mean_elevation: sequence,
    theoretical_rms_jansky_beam: sequence,
    effective_rms_jansky_beam: sequence,
    effective_rms_jansky_beam_err: sequence,
  };
});

export default calculatorResultsFactory;
