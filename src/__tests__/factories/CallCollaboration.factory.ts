import { Factory } from "fishery";
import CallCollaboration from "src/api/models/CallCollaboration.ts";
import CollaborationMemberFactory from "./CollaborationMember.factory.ts";
import { defaultCallCollaborationGroups } from "./CollaborationGroup.factory.ts";

const callCollaborationFactory = Factory.define<CallCollaboration>((sequence) => {
  const member = CollaborationMemberFactory.build();
  const groups = defaultCallCollaborationGroups();
  groups.find((g) => g.short_name === "committee_chair")?.members.push(member);

  return {
    members: [member],
    groups: groups,
    collaboration_url: `https://example.com/collaboration/${sequence}`,
    members_url: `https://example.com/collaboration/${sequence}/members`,
    groups_url: `https://example.com/collaboration/${sequence}/groups`,
    invite_link: `https://example.com/register?collaboration=${sequence}`,
    join_requests_url: `https://example.com/collaboration/${sequence}/joinrequests`,
  };
});

export default callCollaborationFactory;
