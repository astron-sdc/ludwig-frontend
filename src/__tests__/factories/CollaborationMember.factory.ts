import { Factory } from "fishery";

import CollaborationMember from "src/api/models/CollaborationMember.ts";

const collaborationMemberFactory = Factory.define<CollaborationMember>(({ sequence }) => ({
  eduperson_unique_id: `${sequence}@example.com`,
  name: "Piebe Gealle",
  email: "gealle@example.com",
  schac_home_organisation: "example.com",
  scoped_affiliation: "role@example.com",
}));

export default collaborationMemberFactory;
