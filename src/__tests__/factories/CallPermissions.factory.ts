import { Factory } from "fishery";
import CallPermissions from "src/api/models/CallPermissions";

const callPermissionsFactory = Factory.define<CallPermissions>(() => ({
  write_call: false,
  view_call_members: false,
  manage_members_science_panel: false,
  manage_members_technical_panel: false,
  export_call: false,
  accept_proposal: false,
  reject_proposal: false,
}));

export default callPermissionsFactory;
