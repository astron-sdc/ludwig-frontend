import { Factory } from "fishery";
import CallResources from "src/api/models/CallResources";

const callResourcesFactory = Factory.define<CallResources>(({ sequence }) => {
  return {
    observing_time_seconds: 3600 * sequence,
    cpu_time_seconds: 7200 * sequence,
    storage_space_bytes: 1000000000 * sequence,
    support_time_seconds: 1800 * sequence,
  };
});

export default callResourcesFactory;
