import Proposal, { ProposalStatus } from "src/api/models/Proposal";
import { Factory } from "fishery";
import ReviewedProposal from "src/api/models/ReviewedProposal";
import { defaultProposalCollaborationGroups } from "./CollaborationGroup.factory";
import CollaborationMemberFactory from "./CollaborationMember.factory";

const proposalFactory = Factory.define<Proposal>(({ sequence }) => {
  const member = CollaborationMemberFactory.build();
  const groups = defaultProposalCollaborationGroups();
  // Add member to PI group.
  groups.find((g) => g.short_name === "pi")?.members.push(member);

  return {
    id: sequence,
    title: "My Proposal",
    abstract: "A short description of my proposal",
    call_id: sequence,
    project_code: "LC_042",
    created_by: member.eduperson_unique_id,
    contact_author: member.eduperson_unique_id,
    created_at: new Date(),
    status: ProposalStatus.draft,
    members: [member],
    groups: groups,
    collaboration_url: `https://example.com/collaboration/${sequence}`,
    members_url: `https://example.com/collaboration/${sequence}/members`,
    groups_url: `https://example.com/collaboration/${sequence}/groups`,
    invite_link: `https://example.com/register?collaboration=${sequence}`,
    join_requests_url: `https://example.com/collaboration/${sequence}/joinrequests`,
  };
});

export const reviewedProposalFactory = Factory.define<ReviewedProposal>(() => {
  const proposal = proposalFactory.build();
  return {
    ...proposal,
    ranking: 1,
    submitted_at: new Date(),
    review_rating: 0,
    reviews: [],
  };
});

export default proposalFactory;
