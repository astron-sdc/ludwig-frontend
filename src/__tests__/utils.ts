import userEvent from "@testing-library/user-event";
import { MockResponseInitFunction } from "vitest-fetch-mock/types";

/**
 * Utility for creating fetch Responses
 * @param data
 * @returns
 */
export function createFetchResponse<T>(data: T, status = 200): MockResponseInitFunction {
  return () => ({ body: JSON.stringify(data), init: { status: status } });
}

export const user = userEvent.setup();

export const noop = () => {};

/**
 * Helper class for the DataTranserMock
 * Provides a minimal implementation of the `items` FileList
 */
class MyFileList {
  values = [] as File[];

  add(item: File) {
    this.values.push(item);
  }

  length() {
    return this.values.length;
  }
}

/**
 * Mock of the DataTransfer class (not available in NodeJS)
 */
export class DataTransferMock {
  constructor() {}

  items = new MyFileList();
}
