import { render, screen, waitFor } from "@testing-library/react";
import { describe, expect, it } from "vitest";
import Proposal from "src/api/models/Proposal.ts";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import proposalPermissionFactory from "src/__tests__/factories/ProposalPermission.factory";
import ProposalMembersTab from "src/components/MembersTab/ProposalMembersTab";
import ProposalPermissions from "src/api/models/ProposalPermissions";
import proposalFactory from "src/__tests__/factories/Proposal.factory";

const proposal: Proposal = proposalFactory.build({ invite_link: "disney.com" });

describe("ProposalMembersTab", () => {
  const createRouter = (permissions: ProposalPermissions) => {
    const routes = [
      {
        path: "/membersTab",
        element: <ProposalMembersTab proposal={proposal} permissions={permissions} />,
      } as RouteObject,
    ];

    return createMemoryRouter(routes, { initialEntries: ["/membersTab"] });
  };

  it("renders correctly with manage members permissions", async () => {
    const manageMemberPermission = proposalPermissionFactory.build({ manage_members: true });

    const { container } = render(<RouterProvider router={createRouter(manageMemberPermission)}></RouterProvider>);
    await waitFor(() => expect(container).toBeTruthy());

    expect(screen.getByText("Manage collaboration in SRAM")).toBeInTheDocument();
    expect(screen.getByText("Manage members in SRAM")).toBeInTheDocument();
    expect(screen.getByText("Manage groups in SRAM")).toBeInTheDocument();
    expect(screen.getByText("Manage join requests in SRAM")).toBeInTheDocument();
    expect(screen.getByDisplayValue(proposal.invite_link)).toBeInTheDocument();
  });

  it("renders correctly without manage members permissions", async () => {
    const noPermissions = proposalPermissionFactory.build();

    const { container } = render(<RouterProvider router={createRouter(noPermissions)}></RouterProvider>);
    await waitFor(() => expect(container).toBeTruthy());

    expect(screen.queryByText("Manage collaboration in SRAM")).not.toBeInTheDocument();
    expect(screen.queryByText("Manage members in SRAM")).not.toBeInTheDocument();
    expect(screen.queryByText("Manage groups in SRAM")).not.toBeInTheDocument();
    expect(screen.queryByText("Manage join requests in SRAM")).not.toBeInTheDocument();
    expect(screen.queryByDisplayValue(proposal.invite_link)).not.toBeInTheDocument();
  });
});
