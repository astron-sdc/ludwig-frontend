import { describe, expect, it } from "vitest";
import CallMembersTab from "src/components/MembersTab/CallMembersTab";
import { render, screen } from "@testing-library/react";
import callPermissionsFactory from "src/__tests__/factories/CallPermissions.factory";
import callCollaborationFactory from "src/__tests__/factories/CallCollaboration.factory";
import { user } from "src/__tests__/utils";

describe("CallMembersTab", () => {
  it("renders correctly when callCollaboration is not yet ready", () => {
    const permissions = callPermissionsFactory.build();

    render(<CallMembersTab callCollaboration={undefined} permissions={permissions} callId={0} />);

    expect(screen.queryByText("Manage collaboration in SRAM")).not.toBeInTheDocument();
  });

  it("renders correctly without any permissions", () => {
    const permissions = callPermissionsFactory.build();
    const callCollaboration = callCollaborationFactory.build();

    render(<CallMembersTab callCollaboration={callCollaboration} permissions={permissions} callId={0} />);

    expect(screen.queryByText("Manage collaboration in SRAM")).not.toBeInTheDocument();
  });

  it("renders correctly with permission", () => {
    const permissions = callPermissionsFactory.build({ view_call_members: true });
    const callCollaboration = callCollaborationFactory.build();

    render(<CallMembersTab callCollaboration={callCollaboration} permissions={permissions} callId={0} />);

    expect(screen.queryByText("Manage collaboration in SRAM")).toBeInTheDocument();
  });

  it.each([[true], [false]])("should show/hide the 'Manage review panel' button", async (canManageReviewPanel) => {
    const permissions = callPermissionsFactory.build({
      manage_members_science_panel: canManageReviewPanel,
      manage_members_technical_panel: canManageReviewPanel,
    });

    const callCollaboration = callCollaborationFactory.build();

    render(<CallMembersTab callCollaboration={callCollaboration} permissions={permissions} callId={0} />);

    if (canManageReviewPanel) {
      expect(screen.queryByText("Manage review panel")).toBeInTheDocument();
    } else {
      expect(screen.queryByText("Manage review panel")).not.toBeInTheDocument();
    }
  });

  it("shows a modal when the 'Manage review panel' button is clicked", async () => {
    const permissions = callPermissionsFactory.build({
      manage_members_science_panel: true,
      manage_members_technical_panel: true,
    });
    const callCollaboration = callCollaborationFactory.build();

    render(<CallMembersTab callCollaboration={callCollaboration} permissions={permissions} callId={0} />);

    await user.click(await screen.findByText("Manage review panel"));
    expect(screen.queryByText("Assign selected member(s) to review panel")).toBeInTheDocument();
  });

  it("closes the modal when the 'Cancel' button is clicked", async () => {
    const permissions = callPermissionsFactory.build({
      manage_members_science_panel: true,
      manage_members_technical_panel: true,
    });

    const callCollaboration = callCollaborationFactory.build();

    render(<CallMembersTab callCollaboration={callCollaboration} permissions={permissions} callId={0} />);

    await user.click(await screen.findByText("Manage review panel"));
    await user.click(await screen.findByText("Cancel"));
    expect(screen.queryByText("Assign selected member(s) to review panel")).not.toBeInTheDocument();
  });

  it("closes the modal when the 'Assign' button is clicked", async () => {
    const permissions = callPermissionsFactory.build({
      manage_members_science_panel: true,
      manage_members_technical_panel: true,
    });

    const callCollaboration = callCollaborationFactory.build();

    render(<CallMembersTab callCollaboration={callCollaboration} permissions={permissions} callId={0} />);

    await user.click(await screen.findByText("Manage review panel"));

    const memberDropdownSelectButton = (
      await screen.findByLabelText("Select member(s)")
    ).parentElement?.parentElement?.getElementsByTagName("Button")[0];

    await user.click(memberDropdownSelectButton!);
    await user.click(screen.getAllByText(`${callCollaboration.members[0].name}`)[1]);

    await user.click(screen.getByText("Assign members")); // click on the modal title to get the multiselect dropdown to close

    const panelDropdownSelectButton = (
      await screen.findByLabelText("Assign selected member(s) to review panel")
    ).parentElement?.parentElement?.getElementsByTagName("Button")[0];

    await user.click(panelDropdownSelectButton!);
    await user.click(screen.getByText("Scientific"));

    await user.click(await screen.findByText("Assign"));
    expect(screen.queryByText("Assign selected member(s) to review panel")).not.toBeInTheDocument();
  });
});
