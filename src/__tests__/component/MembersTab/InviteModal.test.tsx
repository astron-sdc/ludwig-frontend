import { describe, expect, it, vi } from "vitest";
import InviteModal from "src/components/MembersTab/InviteModal";
import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import ProposalFactory from "src/__tests__/factories/Proposal.factory";
import Api from "src/api/Api";
import { user } from "src/__tests__/utils";

describe("InviteModal", () => {
  it("Should render InviteModal", async () => {
    const proposal = ProposalFactory.build();
    render(<InviteModal inviteOpen={true} setInviteOpen={() => {}} proposal={proposal} />);
    expect(screen.getByText("Invite members")).toBeInTheDocument();
  });

  it("Should send invite", async () => {
    const proposal = ProposalFactory.build();

    const inviteSpy = vi.spyOn(Api, "inviteMembers");
    render(<InviteModal inviteOpen={true} setInviteOpen={() => {}} proposal={proposal} />);

    await waitFor(
      async () => {
        await user.type(screen.getByLabelText<HTMLInputElement>("Email", { exact: false }), "test@example.com");
        fireEvent.click(screen.getByText("Send"));
      },
      { timeout: 5000 },
    );

    expect(inviteSpy).toHaveBeenCalled();
  });
});
