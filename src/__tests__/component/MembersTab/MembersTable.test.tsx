import { render, screen, waitFor } from "@testing-library/react";
import { describe, expect, it } from "vitest";
import ProposalMembersTable from "src/components/MembersTab/ProposalMembersTable";
import ProposalFactory from "src/__tests__/factories/Proposal.factory.ts";
import { defaultProposalCollaborationGroups } from "src/__tests__/factories/CollaborationGroup.factory.ts";
import collaborationMemberFactory from "src/__tests__/factories/CollaborationMember.factory.ts";
import { noPermissions } from "src/__tests__/factories/ProposalPermission.factory";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import Proposal from "src/api/models/Proposal";
import ProposalPermissions from "src/api/models/ProposalPermissions";
import { user } from "src/__tests__/utils";

describe("MembersTable", () => {
  const editMemberButtons = () => screen.getAllByTestId("Edit member");
  const modalUpdateButton = () => screen.getByText("Update");
  const groupDropDown = () => screen.getByRole("combobox");

  const createRouter = (proposal: Proposal, permissions: ProposalPermissions) => {
    const routes = [
      {
        path: "/membersTable",
        element: <ProposalMembersTable proposal={proposal} permissions={permissions} />,
      } as RouteObject,
    ];

    return createMemoryRouter(routes, { initialEntries: ["/membersTable"] });
  };

  const members = [
    collaborationMemberFactory.build({ name: "Piebe Gealle" }),
    collaborationMemberFactory.build({ name: "Henk Hendriks" }),
  ];

  const groups = defaultProposalCollaborationGroups();
  groups.find((g) => g.short_name === "pi")?.members.push(members[0]);
  groups.find((g) => g.short_name === "co_author")?.members.push(members[1]);

  const proposalWithMembers = ProposalFactory.build({
    members: members,
    groups: groups,
    created_by: members[0].eduperson_unique_id,
  });

  it("should render a proposal without any members", async () => {
    const proposalWithoutMembers = ProposalFactory.build({ members: [], groups: defaultProposalCollaborationGroups() });

    const router = createRouter(proposalWithoutMembers, noPermissions);
    const { container } = render(<RouterProvider router={router}></RouterProvider>);
    await waitFor(() => expect(container).toBeTruthy());

    expect(screen.queryByText("No results found.")).toBeInTheDocument();
  });

  it("should render a proposal with members", async () => {
    const router = createRouter(proposalWithMembers, noPermissions);
    const { container } = render(<RouterProvider router={router}></RouterProvider>);
    await waitFor(() => expect(container).toBeTruthy());

    expect(screen.queryByText("No results found")).not.toBeInTheDocument();

    expect(screen.queryByText("Piebe Gealle")).toBeInTheDocument();
    expect(screen.queryByText("Henk Hendriks")).toBeInTheDocument();

    expect(screen.queryByText("PI")).toBeInTheDocument();
    expect(screen.queryByText("CO Author")).toBeInTheDocument();
  });

  it("should show the correct modal when the member edit button is clicked", async () => {
    const manageMemberPermission = { ...noPermissions, manage_members: true };
    const router = createRouter(proposalWithMembers, manageMemberPermission);
    const { container } = render(<RouterProvider router={router}></RouterProvider>);
    await waitFor(() => expect(container).toBeTruthy());

    const buttons = editMemberButtons();
    await user.click(buttons[1].firstElementChild!);

    expect(modalUpdateButton()).toBeInTheDocument();
    expect(groupDropDown()).toHaveDisplayValue("CO Author");
  });
});
