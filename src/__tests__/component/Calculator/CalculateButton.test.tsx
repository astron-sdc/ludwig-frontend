import { describe, expect, it, vi } from "vitest";

import { render, screen } from "@testing-library/react";
import ToastContext from "src/components/Toast/Context";
import { MemoryRouter } from "react-router-dom";
import { readAndWritePermissions, readOnlyPermissions } from "src/__tests__/factories/ProposalPermission.factory";
import CalculateButton from "src/components/Targets/Buttons/CalculateButton";
import TargetRow from "src/components/Targets/Models/TargetRow";
import targetFactory from "src/__tests__/factories/Target.factory";

const setToastMessageSpy = vi.fn();

describe("CalculateButton", () => {
  const target = targetFactory.build();
  const targetRow: TargetRow = {
    ...target,
    rowId: target.id.toString(),
    calculate: <></>,
    resolveName: <></>,
    delete: <></>,
  };

  it("should render view results button without error", async () => {
    render(
      <MemoryRouter>
        <ToastContext.Provider
          value={{
            toastMessage: null,
            setToastMessage: setToastMessageSpy,
            setSuccessToastMessage: vi.fn(),
            setErrorToastMessage: vi.fn(),
          }}
        >
          <CalculateButton targetRow={targetRow} permissions={readOnlyPermissions} setSelectedCalculateTarget={() => {}} />
        </ToastContext.Provider>
      </MemoryRouter>,
    );
    expect(screen.queryByText("View results")).toBeInTheDocument();
  });

  it("should render calculate button without error", async () => {
    render(
      <MemoryRouter>
        <ToastContext.Provider
          value={{
            toastMessage: null,
            setToastMessage: setToastMessageSpy,
            setSuccessToastMessage: vi.fn(),
            setErrorToastMessage: vi.fn(),
          }}
        >
          <CalculateButton
            targetRow={targetRow}
            permissions={readAndWritePermissions}
            setSelectedCalculateTarget={() => {}}
          />
        </ToastContext.Provider>
      </MemoryRouter>,
    );
    expect(screen.queryByText("Calculate")).toBeInTheDocument();
  });
});
