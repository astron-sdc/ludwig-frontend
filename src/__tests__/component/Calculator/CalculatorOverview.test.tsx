import { describe, expect, it, vi } from "vitest";

import { render, screen, waitFor } from "@testing-library/react";
import Api from "src/api/Api";

import ToastContext from "src/components/Toast/Context";
import Target from "src/api/models/Target";
import { MemoryRouter } from "react-router-dom";
import CalculatorOverview from "src/components/Calculator/CalculatorOverview";
import proposalPermissionFactory, { readAndWritePermissions } from "src/__tests__/factories/ProposalPermission.factory";
import targetFactory, { m31Target } from "src/__tests__/factories/Target.factory";
import { user } from "src/__tests__/utils";
import { SWRConfig } from "swr";

const setToastMessageSpy = vi.fn();

const targets = [targetFactory.build(), m31Target] as Target[];

describe("Calculator overview", () => {
  it("should fetch the targets for the proposal", async () => {
    Api.getTargets = vi.fn().mockResolvedValue(targets);
    render(
      <SWRConfig value={{ provider: () => new Map() }}>
        <MemoryRouter>
          <ToastContext.Provider
            value={{
              toastMessage: null,
              setToastMessage: setToastMessageSpy,
              setSuccessToastMessage: vi.fn(),
              setErrorToastMessage: vi.fn(),
            }}
          >
            <CalculatorOverview proposalId={42} permissions={proposalPermissionFactory.build()} />
          </ToastContext.Provider>
        </MemoryRouter>
      </SWRConfig>,
    );
    expect(Api.getTargets).toHaveBeenCalledWith(42);
  });

  it("should call the calculate method when the calculate button is pressed", async () => {
    Api.getTargets = vi.fn().mockResolvedValue(targets);
    Api.getCalculatorInputs = vi.fn();
    Api.getCalculatorResults = vi.fn();
    Api.calculateResults = vi.fn();

    render(
      <SWRConfig value={{ provider: () => new Map() }}>
        <MemoryRouter>
          <ToastContext.Provider
            value={{
              toastMessage: null,
              setToastMessage: setToastMessageSpy,
              setSuccessToastMessage: vi.fn(),
              setErrorToastMessage: vi.fn(),
            }}
          >
            <CalculatorOverview selectedTargetId={targets[0].id} proposalId={42} permissions={readAndWritePermissions} />
          </ToastContext.Provider>
        </MemoryRouter>
      </SWRConfig>,
    );
    expect(Api.getTargets).toHaveBeenCalledWith(42);
    expect(Api.getCalculatorInputs).toHaveBeenCalledWith(42);
    expect(Api.getCalculatorResults).toHaveBeenCalledWith(42);

    const calculateButton = await screen.findByText("Calculate");
    await waitFor(
      async () => {
        await user.click(calculateButton);
        expect(Api.calculateResults).toHaveBeenCalled();
      },
      { timeout: 10000 },
    );
  });
});
