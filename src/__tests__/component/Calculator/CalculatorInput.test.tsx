import { describe, expect, it, vi } from "vitest";

import { render, screen } from "@testing-library/react";
import CalculatorInput from "src/components/Calculator/CalculatorInput";
import ToastContext from "src/components/Toast/Context";
import { MemoryRouter } from "react-router-dom";
import { defaultCalculatorInputs } from "src/api/models/CalculatorInputs";
import proposalPermissionFactory from "src/__tests__/factories/ProposalPermission.factory";

const setToastMessageSpy = vi.fn();

describe("CalculatorInput", () => {
  it("should render without error", async () => {
    render(
      <MemoryRouter>
        <ToastContext.Provider
          value={{
            toastMessage: null,
            setToastMessage: setToastMessageSpy,
            setSuccessToastMessage: vi.fn(),
            setErrorToastMessage: vi.fn(),
          }}
        >
          <CalculatorInput
            proposalId={0}
            permissions={proposalPermissionFactory.build()}
            isCollapsed={false}
            toggleCollapsed={undefined}
            onCalculateClicked={async () => {}}
            input={defaultCalculatorInputs}
            targets={[]}
          />
        </ToastContext.Provider>
      </MemoryRouter>,
    );
    expect(screen.queryByText("Observational setup")).toBeInTheDocument();
    expect(screen.queryByText("Target setup")).toBeInTheDocument();
    expect(screen.queryByText("Pipeline setup")).toBeInTheDocument();
  });
});
