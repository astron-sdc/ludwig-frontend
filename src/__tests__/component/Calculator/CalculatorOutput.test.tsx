import { describe, expect, it } from "vitest";

import { render, screen } from "@testing-library/react";
import CalculatorOutput from "src/components/Calculator/CalculatorOutput";
import calculatorResultsFactory from "src/__tests__/factories/CalculatorResults.factory";
import { MemoryRouter } from "react-router-dom";

describe("CalculatorOutput", () => {
  it("should render without error", async () => {
    render(
      <MemoryRouter>
        <CalculatorOutput result={calculatorResultsFactory.build()} onCloseClicked={() => {}} />
      </MemoryRouter>,
    );
    expect(screen.queryByText("LOFAR Unified Calculator for Imaging")).toBeInTheDocument();
  });

  it("should show empty message if result is undefined", async () => {
    render(
      <MemoryRouter>
        <CalculatorOutput result={undefined} onCloseClicked={() => {}} />
      </MemoryRouter>,
    );
    expect(screen.queryByText("There are no calculator results for this target.")).toBeInTheDocument();
  });
});
