import { render, screen, waitFor } from "@testing-library/react";
import { createFetchResponse, user } from "src/__tests__/utils";
import Target from "src/api/models/Target";
import TargetList from "src/components/Targets/TargetList";
import ToastContext from "src/components/Toast/Context";
import { beforeEach, describe, expect, it, vi } from "vitest";
import { FetchMock } from "vitest-fetch-mock/types";
import proposalPermissionFactory from "src/__tests__/factories/ProposalPermission.factory";

const fetchMock = fetch as FetchMock;

const fetchTargetsPattern = /.*\/proposals\/\d+\/targets$/;
const fetchMultipleTargetsMock = () =>
  fetchMock.mockIf(
    fetchTargetsPattern,
    createFetchResponse<Target[]>(
      [
        {
          id: 144,
          name: "MY TARGET 1",
          x_hms_formatted: "19:53:46.99",
          y_dms_formatted: "+18:46:45.0",
          system: "ICRS",
          notes: "",
          group: "1",
          subgroup: "1",
          field_order: "1",
          assigned_priority: "B",
          requested_priority: "B",
        },
        {
          id: 145,
          name: "MY TARGET 2",
          x_hms_formatted: "12:53:46.99",
          y_dms_formatted: "+11:46:45.0",
          system: "ICRS",
          notes: "",
          group: "1",
          subgroup: "1",
          field_order: "1",
          assigned_priority: "B",
          requested_priority: "A",
        },
      ],
      200,
    ),
  );

describe("TargetList", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
    fetchMultipleTargetsMock();
  });

  it("renders a row for each fetched target", async () => {
    render(<TargetList permissions={proposalPermissionFactory.build({ write_proposal: true })} proposalId={10} />);
    expect(await screen.findAllByText("ICRS")).toHaveLength(2);
  });

  it("adds a row when the Add Target button is clicked", async () => {
    render(<TargetList permissions={proposalPermissionFactory.build({ write_proposal: true })} proposalId={10} />);
    const addTargetButton = await screen.findByText("Add Target");
    await waitFor(
      async () => {
        await user.click(addTargetButton);
        expect(await screen.findAllByText("J2000")).toHaveLength(3);
        await user.click(addTargetButton);
        expect(await screen.findAllByText("J2000")).toHaveLength(4);
      },
      { timeout: 10000 },
    );
  });
});

describe("TargetList name resolve", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
    fetchMultipleTargetsMock();
  });

  const errorResolvingToast = {
    title: "Error",
    message: "Failed to resolve name MY TARGET 2",
    alertType: "negative",
    expireAfter: 5000,
  };

  it("shows a toast message on error mentioning the target name that failed to resolve", async () => {
    const setToastMessageSpy = vi.fn();

    render(
      <ToastContext.Provider
        value={{
          toastMessage: null,
          setToastMessage: setToastMessageSpy,
          setSuccessToastMessage: vi.fn(),
          setErrorToastMessage: vi.fn(),
        }}
      >
        <TargetList
          permissions={proposalPermissionFactory.build({ write_proposal: true })}
          proposalId={11}
          resolveTimeout={0}
        />
      </ToastContext.Provider>,
    );
    const resolveNameButtons = await screen.findAllByText("Resolve");
    await waitFor(async () => {
      await user.click(resolveNameButtons[1]);
    });

    expect(setToastMessageSpy).toHaveBeenCalledWith(errorResolvingToast);
  });
});

describe("TargetList error", () => {
  const fetchTargetsErrorMock = () => fetchMock.mockIf(fetchTargetsPattern, createFetchResponse({}, 500));

  beforeEach(() => {
    fetchMock.resetMocks();
    fetchTargetsErrorMock();
  });

  it("shows the GeneralError component when fetchTargets gives an error", async () => {
    render(<TargetList permissions={proposalPermissionFactory.build({ write_proposal: true })} proposalId={1} />);
    expect(await screen.findByText("Error encountered when fetching targets.")).toBeTruthy();
  });
});

describe("TargetList empty", () => {
  const fetchTargetsEmptyMock = () => fetchMock.mockIf(fetchTargetsPattern, createFetchResponse([], 200));

  beforeEach(() => {
    fetchMock.resetMocks();
    fetchTargetsEmptyMock();
  });

  it("shows the NoEntries component when fetchTargets gives no targets", async () => {
    render(<TargetList permissions={proposalPermissionFactory.build({ write_proposal: true })} proposalId={2} />);
    expect(await screen.findByText("Click 'Add Target' to add new targets.")).toBeTruthy();
  });

  it("does not show the Add Target button if unauthorized to add targets", async () => {
    render(<TargetList permissions={proposalPermissionFactory.build({ write_proposal: false })} proposalId={2} />);
    expect(screen.queryByText("Click 'Add Target' to add new targets.")).not.toBeInTheDocument();
  });
});
