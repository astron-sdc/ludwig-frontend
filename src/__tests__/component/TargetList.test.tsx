import { render } from "@testing-library/react";
import TargetList from "src/components/Targets/TargetList";
import { describe, expect, it } from "vitest";

describe("TargetList", async () => {
  it("renders correctly", async () => {
    const container = render(<TargetList proposalId={10} />);
    expect(container).toBeTruthy();
  });
});
