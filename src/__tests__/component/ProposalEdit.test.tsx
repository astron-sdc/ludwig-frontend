import { act, render, screen, waitFor } from "@testing-library/react";
import { RouterProvider } from "react-router-dom";
import Proposal, { ProposalStatus } from "src/api/models/Proposal";
import { describe, expect, it, vi } from "vitest";
import { createFetchResponse, user } from "src/__tests__/utils";
import { FetchMock } from "vitest-fetch-mock/types";
import ReviewComment from "src/api/models/ReviewComment";
import proposalFactory from "src/__tests__/factories/Proposal.factory";
import ProposalRouter from "src/__tests__/ProposalRouter";
import { AuthContext } from "src/auth/AuthContext";
import { adminAuthProps } from "src/__tests__/factories/AuthProps.factory";
import { abstractInvalidMessage, maxAbstractWordCount } from "src/helper/ProposalHelper";
import { allPermissions } from "src/__tests__/factories/ProposalPermission.factory";

const fetchMock = fetch as FetchMock;
const setToastMessageSpy = vi.fn();
const authProps = adminAuthProps();
const proposalBaseProps: Proposal = proposalFactory.build({ title: "I propose to thee" });

describe("Synchronization", async () => {
  it("should have a Clickable Sync Button and not crash", async () => {
    const router = ProposalRouter(
      {
        ...proposalBaseProps,
        call_is_open: true,
        status: ProposalStatus.draft,
      },
      setToastMessageSpy,
      allPermissions,
    );
    render(
      <AuthContext.Provider value={authProps}>
        <RouterProvider router={router}></RouterProvider>
      </AuthContext.Provider>,
    );
    expect(await screen.findByText("Sync to TMSS")).toBeInTheDocument();
    const syncButton = await screen.findByText("Sync to TMSS");
    await act(async () => {
      await waitFor(async () => {
        await user.click(syncButton);
      });
    });
    expect(setToastMessageSpy).toHaveBeenCalled();
  });

  it("should have a Clickable Sync Button", async () => {
    fetchMock.enableMocks();
    fetchMock.mockResponse((request) => {
      if (request.url.endsWith("/sync/")) {
        return JSON.stringify({ did_succeed: true });
      }
      return JSON.stringify([
        { id: 1, name: "a", group_id_number: 2, group: 2, x_hms_formatted: "2", y_dms_formatted: "3" },
      ]);
    });

    const router = ProposalRouter(
      {
        ...proposalBaseProps,
        call_is_open: true,
        status: ProposalStatus.draft,
      },
      setToastMessageSpy,
      allPermissions,
    );
    render(
      <AuthContext.Provider value={authProps}>
        <RouterProvider router={router}></RouterProvider>
      </AuthContext.Provider>,
    );
    expect(await screen.findByText("Sync to TMSS")).toBeInTheDocument();
    const syncButton = await screen.findByText("Sync to TMSS");
    await act(async () => {
      await waitFor(async () => {
        await user.click(syncButton);
      });
    });
    expect(setToastMessageSpy).toHaveBeenCalled();
  });
});

describe("Edit rank", async () => {
  it("should have a Edit rank", async () => {
    const router = ProposalRouter(
      {
        ...proposalBaseProps,
        call_is_open: true,
        status: ProposalStatus.draft,
      },
      setToastMessageSpy,
      allPermissions,
    );
    render(
      <AuthContext.Provider value={authProps}>
        <RouterProvider router={router}></RouterProvider>
      </AuthContext.Provider>,
    );
    expect(await screen.findByText("Edit rank")).toBeInTheDocument();
    const editRankButton = await screen.findByText("Edit rank");
    await act(async () => {
      await waitFor(async () => {
        await user.click(editRankButton);
      });
    });
    const saveRankButton = await screen.findByText("Save");
    await act(async () => {
      await waitFor(async () => {
        await user.click(saveRankButton);
      });
    });

    expect(setToastMessageSpy).toHaveBeenCalled();
  });
});

describe("ProposalEdit", async () => {
  it("should render", async () => {
    const router = ProposalRouter(proposalBaseProps, vi.fn(), allPermissions);
    render(<RouterProvider router={router}></RouterProvider>);
    expect(await screen.findByText("I propose to thee")).toBeInTheDocument();
  });

  const successMock = () => {
    fetchMock.mockResponse(
      createFetchResponse<Partial<Proposal>>(
        {
          id: 1,
          title: "My proposal",
          abstract: "My concrete abstract",
          call_id: 0,
        },
        200,
      ),
    );
  };

  const errorMock = () => {
    fetchMock.mockResponse(createFetchResponse<unknown>({}, 500));
  };

  const successToast = {
    title: "Success",
    message: "Title was saved successfully",
    alertType: "positive",
    expireAfter: 5000,
  };

  const errorToast = {
    title: "Error",
    message: "Failed to save title",
    alertType: "negative",
    expireAfter: 5000,
  };

  it.each([
    ["error", errorToast, errorMock],
    ["success", successToast, successMock],
  ])("should show the correct toast message on %s", async (_, toast, mock) => {
    mock();
    const router = ProposalRouter(proposalBaseProps, setToastMessageSpy, allPermissions);
    render(<RouterProvider router={router}></RouterProvider>);
    const editTitleButton = await screen.findByText("Edit title");
    await user.click(editTitleButton);

    const saveTitleButton = await screen.findByText("Save");
    await user.click(saveTitleButton);

    expect(setToastMessageSpy).toHaveBeenCalledWith(toast);
  });

  describe("When the user does not have write permissions", async () => {
    it("should not show the Edit title and Edit rank buttons", async () => {
      const router = ProposalRouter(proposalBaseProps, setToastMessageSpy, {
        ...allPermissions,
        write_proposal: false,
      });

      const { container } = render(<RouterProvider router={router}></RouterProvider>);
      await waitFor(() => expect(container).toBeTruthy());

      const editTitleButton = screen.queryByText("Edit title");
      const editRankButton = screen.queryByText("Edit rank");
      expect(editTitleButton).not.toBeInTheDocument();
      expect(editRankButton).not.toBeInTheDocument();
    });
  });

  it("should disable the Save button and show a warning if the abstract is too long", async () => {
    const router = ProposalRouter({ ...proposalBaseProps, abstract: "f ".repeat(301) }, vi.fn(), allPermissions);
    render(<RouterProvider router={router}></RouterProvider>);
    expect(await screen.findByText("I propose to thee")).toBeInTheDocument();
    expect(await screen.findByText("Abstract")).toBeInTheDocument();

    const saveButton = await screen.findByText(`Save (301/${maxAbstractWordCount} words)`);
    expect(saveButton).toBeInTheDocument();
    expect(screen.queryByText(abstractInvalidMessage)).toBeInTheDocument();
    expect(saveButton.closest("button")).toBeDisabled();
  });
});

describe("Proposal submission flow", () => {
  const errorMessage = "my error message";
  const successMock = () => {
    fetchMock.mockResponse(createFetchResponse({}, 200));
  };

  const errorMock = () => {
    fetchMock.mockResponse(createFetchResponse({ error: errorMessage }, 500));
  };

  describe("handleSubmit", () => {
    const successToast = {
      title: "Submitted",
      message: "Proposal was submitted successfully",
      alertType: "positive",
      expireAfter: 3000,
    };

    const errorToast = {
      title: "Error submitting proposal",
      message: errorMessage,
      alertType: "negative",
      expireAfter: 3000,
    };

    it.each([
      ["error", errorToast, errorMock],
      ["success", successToast, successMock],
    ])("should show the correct toast message on %s", async (_, toast, mock) => {
      mock();
      const router = ProposalRouter(
        {
          ...proposalBaseProps,
          call_is_open: true,
          status: ProposalStatus.draft,
        },
        setToastMessageSpy,
        allPermissions,
      );
      render(<RouterProvider router={router}></RouterProvider>);
      const submitButton = await screen.findByText("Submit proposal");
      await user.click(submitButton);
      expect(setToastMessageSpy).toHaveBeenCalledWith(toast);
      const unsubmitButton = screen.queryByText("Unsubmit proposal");
      expect(unsubmitButton).not.toBeInTheDocument();
    });
  });

  describe("handleUnsubmit", () => {
    const successToast = {
      title: "Unsubmitted",
      message: "Proposal was unsubmitted successfully",
      alertType: "positive",
      expireAfter: 3000,
    };

    const errorToast = {
      title: "Error unsubmitting proposal",
      message: errorMessage,
      alertType: "negative",
      expireAfter: 3000,
    };

    it.each([
      ["error", errorToast, errorMock],
      ["success", successToast, successMock],
    ])("should show the correct toast message on %s", async (_, toast, mock) => {
      mock();
      const router = ProposalRouter(
        {
          ...proposalBaseProps,
          call_is_open: true,
          status: ProposalStatus.submitted,
        },
        setToastMessageSpy,
        allPermissions,
      );
      render(<RouterProvider router={router}></RouterProvider>);
      const unsubmitButton = await screen.findByText("Unsubmit proposal");
      await user.click(unsubmitButton);
      expect(setToastMessageSpy).toHaveBeenCalledWith(toast);
      const submitButton = screen.queryByText("Submit proposal");
      expect(submitButton).not.toBeInTheDocument();
    });
  });

  describe("When proposal's call is not open", async () => {
    it("submit button should be disabled", async () => {
      const router = ProposalRouter(
        {
          ...proposalBaseProps,
          call_is_open: false,
          status: ProposalStatus.draft,
        },
        setToastMessageSpy,
        allPermissions,
      );

      const { container } = render(<RouterProvider router={router}></RouterProvider>);
      await waitFor(() => expect(container).toBeTruthy());

      const submitButton = await screen.findByText("Submit proposal");
      expect(submitButton.parentElement).toBeDisabled();
    });

    it("Unsubmit button should be disabled", async () => {
      const router = ProposalRouter(
        {
          ...proposalBaseProps,
          call_is_open: false,
          status: ProposalStatus.submitted,
        },
        setToastMessageSpy,
        allPermissions,
      );

      const { container } = render(<RouterProvider router={router}></RouterProvider>);
      await waitFor(() => expect(container).toBeTruthy());

      const unsubmitButton = await screen.findByText("Unsubmit proposal");
      expect(unsubmitButton.parentElement).toBeDisabled();
    });
  });
});

describe("ReviewCommentList", async () => {
  const fetchReviewCommentsPattern = /.*\/proposals\/\d+\/comments$/;
  const fetchMultipleReviewCommentsMock = () =>
    fetchMock.mockOnceIf(
      fetchReviewCommentsPattern,
      createFetchResponse<ReviewComment[]>(
        [
          {
            id: 17,
            text: "Amazing stuff, would buy again!",
            created_at: "2024-07-16T12:45:24.234774Z",
            updated_at: "2024-07-16T12:45:24.234774Z",
            type: "PROPOSAL",
            review_id: 15,
            reviewer_name: "Saru Man",
          },
          {
            id: 12,
            text: "Amazing stuff, would buy again!",
            created_at: "2024-07-17T12:45:24.234774Z",
            updated_at: "2024-07-17T12:45:24.234774Z",
            type: "PROPOSAL",
            review_id: 15,
            reviewer_name: "Saru Man",
          },
        ],
        200,
      ),
    );

  it("should not render the review comment list when user is not authorized", async () => {
    fetchMultipleReviewCommentsMock();
    const router = ProposalRouter(
      {
        ...proposalBaseProps,
        status: ProposalStatus.underReview,
      },
      setToastMessageSpy,
      {
        ...allPermissions,
        read_science_review: false,
        read_technical_review: false,
      },
    );

    const { container } = render(<RouterProvider router={router}></RouterProvider>);
    await waitFor(() => expect(container).toBeTruthy());

    const reviewCommentList = screen.queryByText("Feedback");
    expect(reviewCommentList).not.toBeInTheDocument();
  });
});
