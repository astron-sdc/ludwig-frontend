import { render, screen, waitFor } from "@testing-library/react";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import Call from "src/api/models/Call";
import CallsOverview from "src/pages/calls";
import { beforeEach, describe, expect, it, vi } from "vitest";
import { FetchMock } from "vitest-fetch-mock/types";
import { createFetchResponse, user } from "src/__tests__/utils";
import callFactory from "src/__tests__/factories/Call.factory";
import { AuthContext } from "src/auth/AuthContext";
import Api from "src/api/Api";
import { adminAuthProps, userAuthProps } from "src/__tests__/factories/AuthProps.factory";

const fetchMock = fetch as FetchMock;

describe("CallsOverview", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  const routes: RouteObject[] = [
    {
      path: "/calls",
      element: <CallsOverview />,
    },
  ];

  const router = createMemoryRouter(routes, {
    initialEntries: ["/calls"],
  });

  const createCallButton = async () => await screen.findByText("Create call for proposals");

  it("should render a row for each call", async () => {
    fetchMock.mockResponse(createFetchResponse<Call[]>(callFactory.buildList(3, { name: "nemo" }), 200));

    render(<RouterProvider router={router}></RouterProvider>);
    expect(await screen.findAllByText("nemo")).toHaveLength(3);
  });

  it("should not show create call button for regular users", async () => {
    const authProps = userAuthProps();

    fetchMock.mockResponse(createFetchResponse<Call[]>(callFactory.buildList(3, { name: "nemo" }), 200));
    render(
      <AuthContext.Provider value={authProps}>
        <RouterProvider router={router}></RouterProvider>
      </AuthContext.Provider>,
    );
    expect(screen.queryByText("Create call for proposals")).not.toBeInTheDocument();
  });

  it("should show create call button for administrators and navigate to call creation when clicked", async () => {
    const authProps = adminAuthProps();

    fetchMock.mockResponse(createFetchResponse<Call[]>(callFactory.buildList(3, { name: "nemo" }), 200));
    render(
      <AuthContext.Provider value={authProps}>
        <RouterProvider router={router}></RouterProvider>
      </AuthContext.Provider>,
    );
    expect(await createCallButton()).toBeTruthy();
    const navigateMock = vi.fn();
    Api.getCycles = vi.fn().mockResolvedValue([]);
    vi.spyOn(router, "navigate").mockImplementation(navigateMock);

    await waitFor(
      async () => {
        await user.click(await createCallButton());
        expect(Api.getCycles).toHaveBeenCalled();
        expect(navigateMock).toHaveBeenCalledWith("/calls/create/", { fromRouteId: "0", state: { cycles: [] } });
      },
      { timeout: 1000 },
    );
  });
});
