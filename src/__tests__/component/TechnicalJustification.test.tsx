import { render, screen } from "@testing-library/react";
import TechnicalJustification from "src/components/Justification/TechnicalJustification";
import { beforeEach, describe, expect, it, vi } from "vitest";
import { FetchMock } from "vitest-fetch-mock/types";
import { createFetchResponse, user } from "src/__tests__/utils";
import TextQuestion from "src/api/models/Question/TextQuestion";
import NumberQuestion from "src/api/models/Question/NumberQuestion";
import ChoiceQuestion from "src/api/models/Question/ChoiceQuestion";
import ToastContext from "src/components/Toast/Context";

const fetchMock = fetch as FetchMock;

const fetchQuestionsPattern = /.*\/proposals\/\d+\/questionnaire\/current$/;
const fetchQuestionsMock = () =>
  fetchMock.mockIf(
    fetchQuestionsPattern,
    createFetchResponse(
      {
        questions: [
          {
            id: 1,
            title: "QUESTION: What is your name?",
            is_required: false,
            order: 1,
            type: "TextQuestion",
            min_char_length: 0,
            max_char_length: 10,
            min_word_count: 0,
            max_word_count: 20,
            requires_elaboration: true,
          } as TextQuestion,
          {
            id: 2,
            title: "QUESTION: What is your age?",
            is_required: false,
            order: 2,
            type: "NumberQuestion",
            min_value: 0,
            max_value: 100,
            unit: "years",
            requires_elaboration: false,
          } as NumberQuestion,
          {
            id: 3,
            title: "QUESTION: What is your favorite color?",
            is_required: false,
            order: 3,
            type: "ChoiceQuestion",
            choices: ["Red", "Green", "Blue"],
            requires_elaboration: false,
          } as ChoiceQuestion,
        ],
      },
      200,
    ),
  );

describe("TechnicalJustification", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
    fetchQuestionsMock();
  });

  it("renders correctly", async () => {
    const component = render(<TechnicalJustification proposalId={12} disabled={false} />);
    expect(component).toBeTruthy();
  });

  it("renders a question component for each question", async () => {
    render(<TechnicalJustification proposalId={12} disabled={false} />);
    expect(await screen.findAllByText(/^QUESTION: What is your.*$/)).toHaveLength(3);
  });

  it("displays an error label when the answer is invalid", async () => {
    render(<TechnicalJustification proposalId={12} disabled={false} />);
    const nameInput = (await screen.findByText(/^QUESTION: What is your name\?.*$/)) as HTMLInputElement;

    await user.type(nameInput, "This is a long name");
    expect(await screen.findByText("Please enter a valid text", { exact: false })).toBeTruthy();
  });

  it("shows a toast message on error", async () => {
    const setToastMessageSpy = vi.fn();

    render(
      <ToastContext.Provider
        value={{
          toastMessage: null,
          setToastMessage: setToastMessageSpy,
          setSuccessToastMessage: vi.fn(),
          setErrorToastMessage: vi.fn(),
        }}
      >
        <TechnicalJustification proposalId={12} disabled={false} />
      </ToastContext.Provider>,
    );

    const saveButton = await screen.findByText("Save technical justification");
    await user.click(saveButton);

    const errorSavingToast = {
      title: "Error",
      message: "Failed to save answers",
      alertType: "negative",
      expireAfter: 5000,
    };

    expect(setToastMessageSpy).toHaveBeenCalledWith(errorSavingToast);
  });
});
