import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import MenuBar from "src/components/Menu";
import { describe, expect, it } from "vitest";

describe("Menu", () => {
  it("should render correctly without Bread Crumbs", async () => {
    render(
      <MemoryRouter>
        <MenuBar title="Hello World!" />
      </MemoryRouter>,
    );

    expect(await screen.findByText("Hello World!")).toBeTruthy();
  });
});
