import { render, screen, waitFor } from "@testing-library/react";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import { createFetchResponse, user } from "src/__tests__/utils";
import Cycle from "src/api/models/Cycle";
import ToastContext from "src/components/Toast/Context.ts";
import CreateEditViewCycle from "src/pages/cycles/CreateEditViewCycle";
import { beforeEach, describe, expect, it, vi } from "vitest";
import { FetchMock } from "vitest-fetch-mock/types";
import cycleFactory from "src/__tests__/factories/Cycle.factory";
import { adminAuthProps } from "src/__tests__/factories/AuthProps.factory";
import { AuthContext } from "src/auth/AuthContext";

const fetchMock = fetch as FetchMock;

describe("CreateCycle", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });
  const setToastMessageSpy = vi.fn();
  const authProps = adminAuthProps();
  const routes: RouteObject[] = [
    {
      path: "/cycles/create/",
      element: (
        <AuthContext.Provider value={authProps}>
          <ToastContext.Provider
            value={{
              toastMessage: null,
              setToastMessage: setToastMessageSpy,
              setSuccessToastMessage: vi.fn(),
              setErrorToastMessage: vi.fn(),
            }}
          >
            <CreateEditViewCycle />
          </ToastContext.Provider>
        </AuthContext.Provider>
      ),
    },
  ];

  const router = createMemoryRouter(routes, {
    initialEntries: ["/cycles/create/"],
  });

  const titleElement = async () => await screen.findByLabelText<HTMLInputElement>("Name");

  const codeElement = async () => await screen.findByLabelText<HTMLInputElement>("Code");

  const descriptionElement = async () => await screen.findByLabelText<HTMLInputElement>("Description");

  const submitElement = async () => await screen.findByText<HTMLButtonElement>("Save");

  const setFormFields = async () => {
    await waitFor(
      async () => {
        await user.type(await titleElement(), "Cycle uno");
        await user.type(await codeElement(), "CY1");
        await user.type(await descriptionElement(), "Never ending cycle");
        await user.click(await submitElement());
      },
      { timeout: 10000 },
    );
  };

  it("navigate back to the overview page after successfully creating a cycle", async () => {
    fetchMock.mockResponse(createFetchResponse<Cycle>(cycleFactory.build(), 200));

    render(<RouterProvider router={router}></RouterProvider>);

    const navigateMock = vi.fn();
    vi.spyOn(router, "navigate").mockImplementation(navigateMock);

    await setFormFields();

    expect(navigateMock).toHaveBeenCalledWith("/cycles/", {
      fromRouteId: "0",
    });
  });

  it("shows a toast when creating the cycle gives an error", async () => {
    fetchMock.mockResponse(createFetchResponse({}, 403));
    render(<RouterProvider router={router}></RouterProvider>);

    await setFormFields();
    expect(setToastMessageSpy).toHaveBeenCalledWith({
      title: "Error saving Cycle",
      message: "Forbidden",
      alertType: "negative",
      expireAfter: 5000,
    });
  });
});

// test ToastMessage
