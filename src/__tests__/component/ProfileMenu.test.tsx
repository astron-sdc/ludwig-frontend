import { fireEvent, render, screen } from "@testing-library/react";
import { afterEach, describe, expect, it, vi } from "vitest";
import ProfileMenu from "src/components/ProfileMenu.tsx";
import { AuthContext } from "src/auth/AuthContext.tsx";

describe("Layout", () => {
  afterEach(() => {
    vi.resetAllMocks();
  });

  it("should render a login button when not logged in", async () => {
    const authContext = {
      authenticated: false,
      user: undefined,
      login: () => {},
      logout: () => {},
      restartSession: () => {},
    };

    render(
      <AuthContext.Provider value={authContext}>
        <ProfileMenu />
      </AuthContext.Provider>,
    );

    expect(await screen.findByText("Login")).toBeTruthy();
  });

  it("should render user info when logged in", async () => {
    const authContext = {
      authenticated: true,
      user: {
        uid: "",
        name: "Piebe Gealle",
        preferred_username: "Piebe",
        given_name: "Piebe",
        familiy_name: "Gealle",
        email: "none@example.com",
        email_verified: "true",
        eduperson_entitlement: [],
      },
      login: () => {},
      logout: () => {},
      restartSession: () => {},
    };

    render(
      <AuthContext.Provider value={authContext}>
        <ProfileMenu />
      </AuthContext.Provider>,
    );

    expect(await screen.findByText("Piebe Gealle")).toBeTruthy();
  });

  it("should call login on clicking the login button", async () => {
    const authContext = {
      authenticated: false,
      user: undefined,
      login: () => {},
      logout: () => {},
      restartSession: () => {},
    };

    const loginSpy = vi.spyOn(authContext, "login");

    render(
      <AuthContext.Provider value={authContext}>
        <ProfileMenu />
      </AuthContext.Provider>,
    );

    fireEvent.click(await screen.findByText("Login"));
    expect(loginSpy).toHaveBeenCalledOnce();
  });

  it("should be able to click logout", async () => {
    const authContext = {
      authenticated: true,
      user: {
        uid: "",
        name: "Piebe Gealle",
        preferred_username: "Piebe",
        given_name: "Piebe",
        familiy_name: "Gealle",
        email: "none@example.com",
        email_verified: "true",
        eduperson_entitlement: [],
      },
      login: () => {},
      logout: () => {},
      restartSession: () => {},
    };

    const logoutSpy = vi.spyOn(authContext, "logout");

    render(
      <AuthContext.Provider value={authContext}>
        <ProfileMenu />
      </AuthContext.Provider>,
    );

    fireEvent.click(await screen.findByText("Piebe Gealle"));
    fireEvent.click(await screen.findByText("Logout"));
    expect(logoutSpy).toHaveBeenCalledOnce();
  });
});
