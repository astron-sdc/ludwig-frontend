import { render, screen, waitFor } from "@testing-library/react";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import { createFetchResponse, user } from "src/__tests__/utils";
import Call from "src/api/models/Call";
import ToastContext from "src/components/Toast/Context";
import CreateCall from "src/pages/calls/CreateEditViewCall";
import { beforeEach, describe, expect, it, vi } from "vitest";
import { FetchMock } from "vitest-fetch-mock/types";
import callFactory from "src/__tests__/factories/Call.factory";
import { adminAuthProps } from "src/__tests__/factories/AuthProps.factory";
import { AuthContext } from "src/auth/AuthContext";

const fetchMock = fetch as FetchMock;
describe("CreateCall", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });
  const setToastMessageSpy = vi.fn();
  const authProps = adminAuthProps();
  const routes: RouteObject[] = [
    {
      path: "/cycles/:cycleId/calls/create/",
      element: (
        <AuthContext.Provider value={authProps}>
          <ToastContext.Provider
            value={{
              toastMessage: null,
              setToastMessage: setToastMessageSpy,
              setSuccessToastMessage: vi.fn(),
              setErrorToastMessage: vi.fn(),
            }}
          >
            <CreateCall />
          </ToastContext.Provider>
        </AuthContext.Provider>
      ),
    },
  ];

  const router = createMemoryRouter(routes, {
    initialEntries: ["/cycles/13/calls/create/"],
  });

  const titleElement = async () => await screen.findByLabelText<HTMLInputElement>("Title");

  const codeElement = async () => await screen.findByLabelText<HTMLInputElement>("Code");

  const descriptionElement = async () => await screen.findByLabelText<HTMLInputElement>("Description");

  const submitElement = async () => await screen.findByText<HTMLButtonElement>("Save");

  const setFormFields = async () => {
    await user.type(await titleElement(), "First call");
    await user.type(await codeElement(), "C1");
    await user.type(await descriptionElement(), "The call to adventure");
    await user.click(await submitElement());
  };

  it("navigate back to the cycle overview page after successfully creating a call", async () => {
    fetchMock.mockResponse(createFetchResponse<Call>(callFactory.build({ status: "closed" }), 200));

    render(<RouterProvider router={router}></RouterProvider>);

    const navigateMock = vi.fn();
    vi.spyOn(router, "navigate").mockImplementation(navigateMock);

    await waitFor(
      async () => {
        await setFormFields();

        expect(navigateMock).toHaveBeenCalledWith("/calls/", {
          fromRouteId: "0",
        });
      },
      { timeout: 10000 },
    );
  });

  it("shows a toast when creating the call gives an error", async () => {
    fetchMock.mockResponse(createFetchResponse({}, 502));
    render(<RouterProvider router={router}></RouterProvider>);

    await waitFor(
      async () => {
        await setFormFields();
        console.log(setToastMessageSpy.mock.calls);
        expect(setToastMessageSpy).toHaveBeenCalledWith({
          title: "Error saving Call",
          message: "Bad Gateway",
          alertType: "negative",
          expireAfter: 5000,
        });
      },
      { timeout: 10000 },
    );
  });
});
