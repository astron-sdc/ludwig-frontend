import { render, screen } from "@testing-library/react";
import { MemoryRouter } from "react-router-dom";
import Layout from "src/components/Layout";
import ThemeProvider from "src/components/Theme/Provider";
import { afterEach, describe, expect, it, vi } from "vitest";
import { AuthContext } from "src/auth/AuthContext.tsx";
import Userinfo from "src/api/models/Userinfo";
import globalPermissionsFactory, { adminPermission } from "src/__tests__/factories/GlobalPermissions.factory";

describe("Layout", () => {
  afterEach(() => {
    vi.resetAllMocks();
    sessionStorage.clear();
  });

  it("should render correctly", async () => {
    render(
      <MemoryRouter>
        <Layout title="Hello World!" />
      </MemoryRouter>,
    );

    expect(await screen.findByText("Hello World!")).toBeTruthy();
  });

  it("should get darkmode from session storage", async () => {
    sessionStorage.setItem("darkmode", JSON.stringify(true));
    render(
      <ThemeProvider>
        <div></div>
      </ThemeProvider>,
    );

    expect(document.documentElement).toHaveClass("dark");
  });

  it("should render correctly in dark mode", async () => {
    SpyOnWindow();

    render(
      <MemoryRouter>
        <ThemeProvider>
          <Layout title="Hello World!" />
        </ThemeProvider>
      </MemoryRouter>,
    );

    expect(document.documentElement).toHaveClass("dark");
  });

  it("should show cycles in sidebar for administrators", async () => {
    const authContext = {
      authenticated: true,
      user: { name: "Foo Bar" } as Userinfo,
      permissions: adminPermission(),
      login: () => {},
      logout: () => {},
      restartSession: () => {},
    };

    render(
      <MemoryRouter>
        <AuthContext.Provider value={authContext}>
          <ThemeProvider>
            <Layout title="Proposal Tool" breadcrumbs={[]}></Layout>
          </ThemeProvider>
        </AuthContext.Provider>
      </MemoryRouter>,
    );

    const linkElements = screen.getAllByRole<HTMLAnchorElement>("link");
    const linkElement = linkElements.find((link) => link.getAttribute("href") === "/cycles/");
    expect(linkElement).toBeInTheDocument();
  });

  it("should not show cycles in sidebar for administrators", async () => {
    const authContext = {
      authenticated: true,
      user: { name: "Foo Bar" } as Userinfo,
      permissions: globalPermissionsFactory.build(),
      login: () => {},
      logout: () => {},
      restartSession: () => {},
    };

    render(
      <MemoryRouter>
        <AuthContext.Provider value={authContext}>
          <ThemeProvider>
            <Layout title="Proposal Tool" breadcrumbs={[]}></Layout>
          </ThemeProvider>
        </AuthContext.Provider>
      </MemoryRouter>,
    );

    const linkElements = screen.getAllByRole<HTMLAnchorElement>("link");
    const linkElement = linkElements.find((link) => link.getAttribute("href") === "/cycles/");
    expect(linkElement).toBeUndefined();
  });
});

function SpyOnWindow() {
  vi.spyOn(window, "matchMedia").mockReturnValue({
    matches: true, // any query matches
    media: "",
    onchange: vi.fn(),
    addEventListener: vi.fn(),
    removeEventListener: vi.fn(),
    addListener: vi.fn(),
    removeListener: vi.fn(),
    dispatchEvent: vi.fn(),
  });
}
