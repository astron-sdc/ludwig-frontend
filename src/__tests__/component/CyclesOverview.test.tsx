import { render, screen } from "@testing-library/react";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import Cycle from "src/api/models/Cycle";
import CyclesOverview from "src/pages/cycles";
import { beforeEach, describe, expect, it, vi } from "vitest";
import { FetchMock } from "vitest-fetch-mock/types";
import { createFetchResponse, user } from "src/__tests__/utils";
import cycleFactory from "src/__tests__/factories/Cycle.factory";
import { adminAuthProps, userAuthProps } from "src/__tests__/factories/AuthProps.factory";
import { AuthContext } from "src/auth/AuthContext";

const fetchMock = fetch as FetchMock;

describe("CyclesOverview", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  const routes: RouteObject[] = [
    {
      path: "/cycles",
      element: <CyclesOverview />,
    },
  ];

  const router = createMemoryRouter(routes, {
    initialEntries: ["/cycles"],
  });

  const createCycleButton = async () => await screen.findByText("Create Cycle");

  it("should render a row for each cycle", async () => {
    const authProps = userAuthProps();
    fetchMock.mockResponse(createFetchResponse<Cycle[]>(cycleFactory.buildList(3, { code: "CYC" }), 200));

    render(
      <AuthContext.Provider value={authProps}>
        <RouterProvider router={router}></RouterProvider>
      </AuthContext.Provider>,
    );
    expect(await screen.findAllByText("CYC")).toHaveLength(3);
  });

  it("should show the Create call and Create Cycle buttons for administrators and navigate to cycle creation when clicked", async () => {
    const authProps = adminAuthProps();
    fetchMock.mockResponse(createFetchResponse<Cycle[]>(cycleFactory.buildList(3, { code: "CYC" }), 200));
    const navigateMock = vi.fn();
    vi.spyOn(router, "navigate").mockImplementation(navigateMock);
    render(
      <AuthContext.Provider value={authProps}>
        <RouterProvider router={router}></RouterProvider>
      </AuthContext.Provider>,
    );
    expect(await screen.findAllByText("CYC")).toHaveLength(3);
    expect(await createCycleButton()).toBeInTheDocument();
    expect(await screen.findAllByText("Create call for proposals")).toHaveLength(3);

    await user.click(await createCycleButton());
    expect(navigateMock).toHaveBeenCalledWith("/cycles/create/", { fromRouteId: "0" });
  });

  it("should show not the Create call button for regular users", async () => {
    const authProps = userAuthProps();
    fetchMock.mockResponse(createFetchResponse<Cycle[]>(cycleFactory.buildList(3, { code: "CYC" }), 200));

    render(
      <AuthContext.Provider value={authProps}>
        <RouterProvider router={router}></RouterProvider>
      </AuthContext.Provider>,
    );
    expect(await screen.findAllByText("CYC")).toHaveLength(3);
    expect(screen.queryByText("Create Cycle")).not.toBeInTheDocument();
    expect(screen.queryByText("Create call for proposals")).not.toBeInTheDocument();
  });
});
