import { fireEvent, render, screen, waitFor } from "@testing-library/react";
import { createFetchResponse, DataTransferMock } from "src/__tests__/utils";
import ScientificJustification from "src/components/Justification/ScientificJustification";
import ToastContext from "src/components/Toast/Context";
import { beforeEach, describe, expect, it, vi } from "vitest";
import { FetchMock } from "vitest-fetch-mock/types";

vi.stubGlobal("DataTransfer", DataTransferMock);

const fetchMock = fetch as FetchMock;

const proposalWithouthScientificJustificationMock = () =>
  fetchMock.mockResponse(
    createFetchResponse(
      {
        id: 1,
        title: "My proposal",
        abstract: "My concrete abstract",
        call_id: 0,
        scientific_justification_filename: "",
        scientific_justification_filesize: 0,
      },
      200,
    ),
  );

const proposalWithScientificJustificationMock = () =>
  fetchMock.mockResponse(
    createFetchResponse(
      {
        id: 1,
        title: "My proposal",
        abstract: "My concrete abstract",
        call_id: 0,
        scientific_justification_filename: "MyJustification.pdf",
        scientific_justification_filesize: "1337",
      },
      200,
    ),
  );

const uploadFailedMock = () =>
  fetchMock.mockResponse(
    createFetchResponse(
      {
        reason: "Invalid Proposal",
      },
      400,
    ),
  );

describe("ScientificJustification", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  it("renders with an upload button", async () => {
    proposalWithouthScientificJustificationMock();
    const component = render(<ScientificJustification proposalId={12} disabled={false} />);
    expect(component).toBeTruthy();

    const uploadButton = await component.findByText("Upload");
    expect(uploadButton).toBeTruthy();
  });

  it("renders no link for proposal without justification", async () => {
    proposalWithouthScientificJustificationMock();
    const component = render(<ScientificJustification proposalId={12} disabled={false} />);
    expect(component).toBeTruthy();

    const downloadButton = component.queryByTestId("download-link");
    expect(downloadButton).toBeNull();
  });

  it("renders a link for proposal with justification", async () => {
    proposalWithScientificJustificationMock();
    const component = render(<ScientificJustification proposalId={12} disabled={false} />);
    expect(component).toBeTruthy();

    const downloadButton = await component.findByTestId("download-link");
    expect(downloadButton).toBeTruthy();
  });

  it("shows a toast message on upload error", async () => {
    const setToastMessageSpy = vi.fn();
    proposalWithScientificJustificationMock();

    render(
      <ToastContext.Provider
        value={{
          toastMessage: null,
          setToastMessage: setToastMessageSpy,
          setSuccessToastMessage: vi.fn(),
          setErrorToastMessage: vi.fn(),
        }}
      >
        <ScientificJustification proposalId={12} disabled={false} />
      </ToastContext.Provider>,
    );

    // TODO: use proper mocks for each uri :see-no-evil:
    uploadFailedMock();

    const fileInput = screen.getByLabelText("Upload new document");
    await waitFor(() => {
      fireEvent.change(fileInput, {
        target: {
          files: [new File(["1337"], "proposal.pdf", { type: "application/pdf" })],
        },
      });
    });

    const uploadButton = screen.getByText("Upload");
    await waitFor(() => {
      fireEvent.click(uploadButton);
    });

    const errorUploadingToast = {
      title: "Could not upload file",
      message: "Error: Bad Request",
      alertType: "negative",
      expireAfter: 5000,
    };

    expect(setToastMessageSpy).toHaveBeenCalledWith(errorUploadingToast);
  });

  it("shows a toast message on upload success", async () => {
    const setToastMessageSpy = vi.fn();
    proposalWithScientificJustificationMock();

    render(
      <ToastContext.Provider
        value={{
          toastMessage: null,
          setToastMessage: setToastMessageSpy,
          setSuccessToastMessage: vi.fn(),
          setErrorToastMessage: vi.fn(),
        }}
      >
        <ScientificJustification proposalId={12} disabled={false} />
      </ToastContext.Provider>,
    );

    const fileInput = screen.getByLabelText("Upload new document");
    await waitFor(() => {
      fireEvent.change(fileInput, {
        target: {
          files: [new File(["1337"], "proposal.pdf", { type: "application/pdf" })],
        },
      });
    });

    const uploadButton = screen.getByText("Upload");
    await waitFor(() => {
      fireEvent.click(uploadButton);
    });

    const successToast = {
      title: "File uploaded",
      message: "MyJustification.pdf has been uploaded",
      alertType: "positive",
      expireAfter: 5000,
    };

    expect(setToastMessageSpy).toHaveBeenCalledWith(successToast);
  });
});
