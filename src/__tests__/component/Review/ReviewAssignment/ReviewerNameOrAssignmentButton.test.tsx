import { describe, expect, it, vitest } from "vitest";
import { render, screen } from "@testing-library/react";
import { user } from "src/__tests__/utils";
import { AuthContext } from "src/auth/AuthContext";
import CallCollaboration from "src/api/models/CallCollaboration";
import ReviewerNameOrAssignmentButton from "src/components/Review/ReviewAssignment/ReviewerNameOrAssignmentButton";
import authPropsFactory from "src/__tests__/factories/AuthProps.factory";
import { MemoryRouter } from "react-router-dom";
import callPermissionsFactory from "src/__tests__/factories/CallPermissions.factory";

describe("ReviewerNameOrAssignmentButton", () => {
  const handleClick = vitest.fn();
  const userProps = authPropsFactory.build({ user: { uid: "userid123", name: "My Name" } });
  const permissiveCallPermission = callPermissionsFactory.build({
    manage_members_science_panel: true,
    manage_members_technical_panel: true,
  });

  const callCollaboration = {
    members: [{ eduperson_unique_id: "reviewer123", name: "John Doe" }],
  } as CallCollaboration;

  it("renders 'Assign reviewer' button when reviewerId is undefined", async () => {
    render(
      <MemoryRouter>
        <AuthContext.Provider value={userProps}>
          <ReviewerNameOrAssignmentButton
            permissions={permissiveCallPermission}
            reviewerId={undefined}
            callCollaboration={callCollaboration}
            handleClick={handleClick}
          />
        </AuthContext.Provider>
      </MemoryRouter>,
    );
    const button = screen.getByText("Assign reviewer");
    expect(button).toBeInTheDocument();
    await user.click(button);
    expect(handleClick).toHaveBeenCalled();
  });

  it("renders 'Assigned to me' when reviewerId is the same as user id", () => {
    render(
      <AuthContext.Provider value={userProps}>
        <ReviewerNameOrAssignmentButton
          permissions={permissiveCallPermission}
          reviewerId={userProps.user?.uid}
          callCollaboration={callCollaboration}
          handleClick={handleClick}
        />
      </AuthContext.Provider>,
    );

    const assignedText = screen.getByText("Assigned to me");
    expect(assignedText).toBeInTheDocument();
  });

  it("renders reviewer's name when reviewerId is different from user id", () => {
    render(
      <AuthContext.Provider value={userProps}>
        <ReviewerNameOrAssignmentButton
          permissions={permissiveCallPermission}
          reviewerId="reviewer123"
          callCollaboration={callCollaboration}
          handleClick={handleClick}
        />
      </AuthContext.Provider>,
    );
    const reviewerName = screen.getByText("John Doe");
    expect(reviewerName).toBeInTheDocument();
  });

  it("renders nothing when reviewerId is different from user id and user is not permitted to assign reviewers", async () => {
    render(
      <MemoryRouter>
        <AuthContext.Provider value={userProps}>
          <ReviewerNameOrAssignmentButton
            permissions={callPermissionsFactory.build()}
            reviewerId={"reviewer name"}
            callCollaboration={callCollaboration}
            handleClick={handleClick}
          />
        </AuthContext.Provider>
      </MemoryRouter>,
    );
    expect(screen.queryByText("Assign reviewer")).not.toBeInTheDocument();
    expect(screen.queryByText("Assigned to me")).not.toBeInTheDocument();
    expect(screen.queryByText("reviewer name")).not.toBeInTheDocument();
  });
});
