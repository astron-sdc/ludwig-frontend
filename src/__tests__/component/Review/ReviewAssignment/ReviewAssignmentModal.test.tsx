import { render, screen } from "@testing-library/react";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import CollaborationMember from "src/api/models/CollaborationMember";
import { PanelType, ProposalPanelTypePairing, ReviewerType } from "src/api/models/Review";
import ReviewAssignmentModal from "src/components/Review/ReviewAssignment/ReviewAssignmentModal";
import { describe, expect, it } from "vitest";
import { ProposalStatus } from "src/api/models/Proposal.ts";
import callCollaborationFactory from "src/__tests__/factories/CallCollaboration.factory";
import { getPanelMembersPerPanelType } from "src/helper/CollaborationHelper";
import CallCollaboration from "src/api/models/CallCollaboration";
import { defaultCallCollaborationGroups } from "src/__tests__/factories/CollaborationGroup.factory";

const pairings: ProposalPanelTypePairing[] = [
  {
    proposal: {
      id: 4,
      submitted_at: "",
      members: [],
      groups: [],
      reviews: [],
      review_rating: null,
      title: "AssignableProposal",
      abstract: "",
      call_id: 0,
      project_code: "X",
      members_url: "",
      invite_link: "",
      join_requests_url: "",
      collaboration_url: "",
      groups_url: "",
      status: ProposalStatus.underReview,
    },
    panelType: PanelType.technical,
  },
  {
    proposal: {
      id: 5,
      submitted_at: "",
      members: [],
      groups: [],
      reviews: [],
      review_rating: null,
      title: "AssignableProposalTwo",
      abstract: "",
      call_id: 0,
      project_code: "X",
      members_url: "",
      invite_link: "",
      join_requests_url: "",
      collaboration_url: "",
      groups_url: "",
      status: ProposalStatus.underReview,
    },
    panelType: PanelType.scientific,
  },
];

const callMembers = [
  {
    eduperson_unique_id: "foo@example.com",
    name: "Foo",
  },
  {
    eduperson_unique_id: "foo2@example.com",
    name: "John Doe",
  },
  {
    eduperson_unique_id: "foo3@example.com",
    name: "Jane Doe",
  },
] as CollaborationMember[];

const callCollaboration = {
  ...callCollaborationFactory.build(),
  members: callMembers,
  groups: defaultCallCollaborationGroups(),
};

const reviewAssignmentModalRouter = (
  callCollaboration: CallCollaboration,
  reviewerType: ReviewerType,
  selectedProposalPanelTypePairs: ProposalPanelTypePairing[],
  showLoader = false,
) => {
  const routes: RouteObject[] = [
    {
      path: "/proposals/:proposalId/",
      element: (
        <ReviewAssignmentModal
          membersPerPanelType={getPanelMembersPerPanelType(callCollaboration)}
          reviewerType={reviewerType}
          selectedProposalPanelTypePairs={selectedProposalPanelTypePairs}
          showLoader={showLoader}
        />
      ),
    },
  ];

  return createMemoryRouter(routes, {
    initialEntries: ["/proposals/4/"],
  });
};

const assignButton = async () => await screen.findByText<HTMLButtonElement>("Assign");
const cancelButton = async () => await screen.findByText<HTMLButtonElement>("Cancel");

describe("Review assignment modal flow", async () => {
  it("Modal should be displayed with proposal titles", async () => {
    const router = reviewAssignmentModalRouter(callCollaboration, ReviewerType.primary, pairings, false);

    render(<RouterProvider router={router}></RouterProvider>);
    expect(await screen.findByTestId("reviewer-dropdowntechnical")).toBeInTheDocument();
    expect(await assignButton()).toBeInTheDocument();
    expect(await cancelButton()).toBeInTheDocument();
    expect(await screen.findAllByText("Assign primary reviewers")).toHaveLength(1);
    expect(await screen.findAllByText("Proposal title: AssignableProposal")).toHaveLength(1);
    expect(await screen.findAllByText("Proposal title: AssignableProposalTwo")).toHaveLength(1);
    expect(await screen.findByLabelText("Select Primary Technical Reviewer")).toBeInTheDocument();
  });

  it("Reviewer dropdown should be displayed with secondary reviewer type label", async () => {
    const router = reviewAssignmentModalRouter(callCollaboration, ReviewerType.secondary, pairings, false);

    render(<RouterProvider router={router}></RouterProvider>);
    expect(await screen.findByTestId("reviewer-dropdowntechnical")).toBeInTheDocument();
    expect(await assignButton()).toBeInTheDocument();
    expect(await cancelButton()).toBeInTheDocument();
    expect(await screen.findByLabelText("Select Secondary Technical Reviewer")).toBeInTheDocument();
  });

  it("Warning text should be shown when the call has no members", async () => {
    const router = reviewAssignmentModalRouter({} as CallCollaboration, ReviewerType.secondary, pairings, false);

    render(<RouterProvider router={router}></RouterProvider>);
    expect(await screen.findByTestId("reviewer-dropdowntechnical")).toBeInTheDocument();
    expect(await assignButton()).toBeInTheDocument();
    expect(await cancelButton()).toBeInTheDocument();
    expect(await screen.findByText("This call has no technical panel members.")).toBeInTheDocument();
  });
});
