import { render, screen } from "@testing-library/react";
import { RouteObject, RouterProvider, createMemoryRouter } from "react-router-dom";
import CollaborationMember from "src/api/models/CollaborationMember";
import { PanelType } from "src/api/models/Review";
import ReviewPreAssignmentModal from "src/components/Review/ReviewAssignment/ReviewPreAssignmentModal";
import { describe, expect, it } from "vitest";
import { user } from "src/__tests__/utils";

const reviewPreAssignmentModalRouter = (
  callMembers: CollaborationMember[],
  enabledPanelTypes: PanelType[],
  showLoader = false,
) => {
  const routes: RouteObject[] = [
    {
      path: "/proposals/:proposalId/",
      element: (
        <ReviewPreAssignmentModal callMembers={callMembers} showLoader={showLoader} enabledPanelTypes={enabledPanelTypes} />
      ),
    },
  ];

  return createMemoryRouter(routes, {
    initialEntries: ["/proposals/4/"],
  });
};

const callMembers = [
  {
    eduperson_unique_id: "saruman@mordor.me",
    name: "Saru Man",
  },
  {
    eduperson_unique_id: "legolas@mirkwood.me",
    name: "Legolas",
  },
] as CollaborationMember[];

describe("Review pre assignment modal", async () => {
  it("should display", async () => {
    const router = reviewPreAssignmentModalRouter(callMembers, [], false);
    render(<RouterProvider router={router}></RouterProvider>);
    expect(await screen.findByTestId("reviewer-dropdown")).toBeInTheDocument();
    expect(await screen.findByTestId("panelType-dropdown")).toBeInTheDocument();
  });

  it("should show a warning text when the call has no members", async () => {
    const router = reviewPreAssignmentModalRouter([], [], false);
    render(<RouterProvider router={router}></RouterProvider>);
    expect(await screen.findByText("This call has no members.")).toBeInTheDocument();
  });

  it("should show a warning text when the call has no members", async () => {
    const router = reviewPreAssignmentModalRouter([], [], false);
    render(<RouterProvider router={router}></RouterProvider>);
    expect(await screen.findByText("This call has no members.")).toBeInTheDocument();
  });

  it.each([
    [PanelType.scientific, "Scientific"],
    [PanelType.technical, "Technical"],
  ])("should show the enabled panel types in the dropdown", async (panelType, expectedText) => {
    const router = reviewPreAssignmentModalRouter([], [panelType], false);
    render(<RouterProvider router={router}></RouterProvider>);

    const panelDropdownSelectButton = (
      await screen.findByLabelText("Assign selected member(s) to review panel")
    ).parentElement?.parentElement?.getElementsByTagName("Button")[0];

    await user.click(panelDropdownSelectButton!);
    expect(await screen.findByText(expectedText)).toBeInTheDocument();
  });
});
