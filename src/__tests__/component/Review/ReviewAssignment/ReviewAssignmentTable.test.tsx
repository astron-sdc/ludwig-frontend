import { act, render, RenderResult, waitFor } from "@testing-library/react";
import { beforeEach, describe, expect, it, vi } from "vitest";
import ReviewAssignmentTable from "src/components/Review/ReviewAssignment/ReviewAssignmentTable";
import { AuthContext, AuthProps } from "src/auth/AuthContext";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import Api from "src/api/Api";
import { user } from "src/__tests__/utils";
import ToastContext from "src/components/Toast/Context";
import callFactory from "src/__tests__/factories/Call.factory.ts";
import { adminPermission } from "src/__tests__/factories/GlobalPermissions.factory";
import collaborationMemberFactory from "src/__tests__/factories/CollaborationMember.factory";
import callCollaborationFactory from "src/__tests__/factories/CallCollaboration.factory";
import { reviewedProposalFactory } from "src/__tests__/factories/Proposal.factory";
import { ProposalStatus } from "src/api/models/Proposal";
import Review from "src/api/models/Review";
import callPermissionsFactory from "src/__tests__/factories/CallPermissions.factory";

vi.mock("src/api/Api");
const setToastMessage = vi.fn();

const callId = 12345;
const callResponse = callFactory.build({ id: callId });

const members = [
  collaborationMemberFactory.build({ name: "Piebe Gealle" }),
  collaborationMemberFactory.build({ name: "Foo Bar" }),
  collaborationMemberFactory.build({ name: "John Doe" }),
];

const callCollaboration = { ...callCollaborationFactory.build(), members: members };
callCollaboration.groups.find((g) => g.short_name === "tech_reviewer")?.members.push(members[0]);
callCollaboration.groups.find((g) => g.short_name === "sci_reviewer")?.members.push(members[1]);
callCollaboration.groups.find((g) => g.short_name === "sci_reviewer")?.members.push(members[2]);

const reviews = [
  {
    id: 2,
    ranking: 1,
    reviewer_id: "1@example.com",
    proposal_id: 1,
    panel_type: "technical",
    reviewer_type: "primary",
    reviewer_name: "Piebe Gaelle",
  },
  {
    id: 3,
    ranking: 1,
    reviewer_id: "2@example.com",
    proposal_id: 2,
    panel_type: "technical",
    reviewer_type: "secondary",
    reviewer_name: "Foo Bar",
  },
  {
    id: 1,
    ranking: 1,
    reviewer_id: "3@example.com",
    proposal_id: 3,
    panel_type: "scientific",
    reviewer_type: "secondary",
    reviewer_name: "John Doe",
  },
] as Review[];

const reviewedProposals = reviewedProposalFactory.buildList(3, {
  status: ProposalStatus.underReview,
  groups: callCollaboration.groups,
  submitted_at: "2024-07-16T12:42:40.404113Z",
  project_code: "ProjectCode",
});

reviewedProposals[0] = { ...reviewedProposals[0], status: ProposalStatus.draft };
reviewedProposals[1] = { ...reviewedProposals[1], reviews: [reviews[0], reviews[1]] };
reviewedProposals[2] = { ...reviewedProposals[2], reviews: [reviews[2]] };
reviewedProposals.forEach((proposal) => {
  proposal.title = `MY ${proposal.status.toUpperCase()} PROPOSAL`;
});

const authContext: AuthProps = {
  authenticated: true,
  user: {
    uid: "1@example.com",
    name: "Piebe Gealle",
    preferred_username: "Piebe",
    given_name: "Piebe",
    familiy_name: "Gealle",
    email: "none@example.com",
    email_verified: "true",
    eduperson_entitlement: [],
  },
  permissions: adminPermission(),
  login: () => {},
  logout: () => {},
  restartSession: () => {},
};

const routes: RouteObject[] = [
  {
    path: `/calls/${callId}/view`,
    element: (
      <AuthContext.Provider value={authContext}>
        <ToastContext.Provider
          value={{
            toastMessage: null,
            setToastMessage: setToastMessage,
            setSuccessToastMessage: vi.fn(),
            setErrorToastMessage: vi.fn(),
          }}
        >
          <ReviewAssignmentTable call={callResponse} permissions={callPermissionsFactory.build()} />
        </ToastContext.Provider>
      </AuthContext.Provider>
    ),
    loader: () => [Promise.resolve(callResponse), Promise.resolve({})],
  },
];

const router = createMemoryRouter(routes, {
  initialEntries: [`/calls/${callId}/view`],
});

describe("ReviewAssignmentTable", () => {
  beforeEach(() => {
    vi.clearAllMocks();
  });

  const reviewerAssignmentModal = (renderResult: RenderResult | undefined) => renderResult?.queryByRole("dialog");
  const proposalCheckboxes = (renderResult: RenderResult | undefined) => renderResult?.getAllByRole("checkbox");
  const assignReviewersDropdown = (renderResult: RenderResult | undefined) =>
    renderResult?.getAllByText("Assign reviewers")[0];
  const assignPrimaryReviewerButton = (renderResult: RenderResult | undefined) =>
    renderResult?.getAllByText("Primary reviewer")[0];
  const proposalFilterSelector = async (renderResult: RenderResult | undefined) =>
    renderResult?.findByLabelText("Status filter:");
  Api.getCallMembers = vi.fn().mockResolvedValue(callCollaboration);
  Api.getProposalsForCall = vi.fn().mockResolvedValue(reviewedProposals);

  it("Should show the review assignment table with assigned reviews", async () => {
    let renderResult: RenderResult | undefined;
    await act(async () => {
      renderResult = render(<RouterProvider router={router} />);
    });

    expect(Api.getProposalsForCall).toHaveBeenCalledWith(callResponse.id);
    expect(Api.getCallMembers).toHaveBeenCalledWith(callResponse.id);
    expect(renderResult?.asFragment()).toBeDefined();

    expect(await proposalFilterSelector(renderResult!)).toBeInTheDocument();
    expect(await renderResult!.findAllByText("7/16/2024, 12:42:40 UTC")).toHaveLength(4);
    expect(await renderResult!.findAllByText("MY UNDER_REVIEW PROPOSAL")).toHaveLength(4);
    expect(renderResult!.queryByText("MY DRAFT PROPOSAL")).not.toBeInTheDocument();
    expect(await renderResult!.findAllByText("under review")).toHaveLength(4);
    expect(await renderResult!.findAllByText("ProjectCode")).toHaveLength(4);
    expect(await renderResult!.findAllByText("John Doe")).toHaveLength(1);
    expect(await renderResult!.findAllByText("Assigned to me")).toHaveLength(1);
    expect(await renderResult!.findAllByText("Assign reviewer")).toHaveLength(5);
    expect(await renderResult!.findAllByText("Technical")).toHaveLength(2);
    expect(await renderResult!.findAllByText("Scientific")).toHaveLength(2);

    // Add Draft status to active filters
    await user.click((await proposalFilterSelector(renderResult))!);
    await user.click(renderResult!.getByText(`Draft`));
    expect(renderResult!.queryByText("MY DRAFT PROPOSAL")).toBeInTheDocument();
    expect(await renderResult!.findAllByText("7/16/2024, 12:42:40 UTC")).toHaveLength(5);
    expect(await renderResult!.findAllByText("ProjectCode")).toHaveLength(5);
  });

  it("Should only show checkboxes for proposals that are assignable", async () => {
    let renderResult: RenderResult | undefined;
    await act(async () => {
      renderResult = render(<RouterProvider router={router} />);
    });
    expect(renderResult?.asFragment()).toBeDefined();
    expect(proposalCheckboxes(renderResult!)!.length).toBe(3);
    expect(proposalCheckboxes(renderResult)![0]!).not.toBeChecked();
    await user.click(proposalCheckboxes(renderResult)![0]);
    expect(proposalCheckboxes(renderResult)![0]!).toBeChecked();
    await user.click(proposalCheckboxes(renderResult)![0]);
    expect(proposalCheckboxes(renderResult)![0]!).not.toBeChecked();
  });

  it("Should show modal when Assign reviewers is clicked and at least one proposal review pairing is checked", async () => {
    let renderResult: RenderResult | undefined;
    await act(async () => {
      renderResult = render(<RouterProvider router={router} />);
    });
    expect(renderResult?.asFragment()).toBeDefined();
    expect(renderResult!.queryByText("Select Primary Reviewer")).not.toBeInTheDocument();
    await user.click(proposalCheckboxes(renderResult)![0]);
    expect(proposalCheckboxes(renderResult)![0]!).toBeChecked();
    expect(reviewerAssignmentModal(renderResult)!).not.toBeInTheDocument();
    await user.click(assignReviewersDropdown(renderResult)!);
    await user.click(assignPrimaryReviewerButton(renderResult)!);

    expect(reviewerAssignmentModal(renderResult!)).toBeInTheDocument();
    expect(await renderResult!.findAllByText("Select Primary Scientific Reviewer")).toHaveLength(1);
  });

  it("Should show modal when the Assign reviewer table cell is clicked", async () => {
    let renderResult: RenderResult | undefined;
    await act(async () => {
      renderResult = render(<RouterProvider router={router} />);
    });
    expect(renderResult?.asFragment()).toBeDefined();
    expect(renderResult!.queryByText("Select Primary Reviewer")).not.toBeInTheDocument();
    expect(reviewerAssignmentModal(renderResult!)).not.toBeInTheDocument();
    await user.click(renderResult!.getAllByText("Assign reviewer")[0]);
    expect(reviewerAssignmentModal(renderResult!)).toBeInTheDocument();
    expect(renderResult!.queryByText("Proposal title: MY UNDER_REVIEW PROPOSAL")).toBeInTheDocument();
    expect(await renderResult!.findAllByText("Select Primary Scientific Reviewer")).toHaveLength(1);
  });

  it("Should disable Assign reviewers button when no proposals are checked", async () => {
    let renderResult: RenderResult | undefined;
    await act(async () => {
      renderResult = render(
        <AuthContext.Provider value={authContext}>
          <RouterProvider router={router} />
        </AuthContext.Provider>,
      );
    });
    expect(renderResult?.asFragment()).toBeDefined();
    expect(reviewerAssignmentModal(renderResult!)).not.toBeInTheDocument();
    expect(assignReviewersDropdown(renderResult)!.closest("button")).toBeDisabled();
  });

  it("Should reset the checkboxes after the modal is closed", async () => {
    let renderResult: RenderResult | undefined;
    await act(async () => {
      renderResult = render(<RouterProvider router={router} />);
    });

    expect(renderResult?.asFragment()).toBeDefined();

    await user.click(proposalCheckboxes(renderResult)![0]);
    expect(proposalCheckboxes(renderResult)![0]!).toBeChecked();

    await user.click(assignReviewersDropdown(renderResult)!);
    await user.click(assignPrimaryReviewerButton(renderResult)!);

    await waitFor(() => {
      expect(reviewerAssignmentModal(renderResult!)).toBeVisible();
      expect(renderResult!.queryByText("Select Primary Scientific Reviewer")).toBeInTheDocument();
    });

    await user.click(renderResult!.getAllByText("Cancel")[0]);

    await waitFor(() => {
      expect(renderResult!.queryByText("Select Primary Scientific Reviewer")).not.toBeInTheDocument();
      expect(proposalCheckboxes(renderResult)![0]!).not.toBeChecked();
    });
  });

  it("Should show the success toast when a reviewer is assigned in the modal", async () => {
    let renderResult: RenderResult | undefined;
    await act(async () => {
      renderResult = render(<RouterProvider router={router} />);
    });
    expect(renderResult?.asFragment()).toBeDefined();

    await user.click(proposalCheckboxes(renderResult)![0]);
    await user.click(proposalCheckboxes(renderResult)![1]);
    await user.click(assignReviewersDropdown(renderResult)!);
    await user.click(assignPrimaryReviewerButton(renderResult)!);

    await waitFor(() => {
      expect(reviewerAssignmentModal(renderResult)!).toBeVisible();
      expect(renderResult!.queryByText("Select Primary Scientific Reviewer")).toBeInTheDocument();
      expect(renderResult!.queryByText("Select Primary Technical Reviewer")).toBeInTheDocument();
      expect(renderResult!.queryByText("This call has no technical panel members.")).not.toBeInTheDocument();
      expect(renderResult!.queryByText("This call has no scientific panel members.")).not.toBeInTheDocument();
    });

    const modalAssignButton = renderResult!.getAllByText("Assign")[0];
    expect(modalAssignButton.closest("button")).toBeDisabled();

    expect(await renderResult!.findAllByText(`${callCollaboration.members[1].name}`)).toHaveLength(1);

    // Click first user in the first reviewer selection dropdown
    await user.click(await renderResult!.findByLabelText("Select Primary Scientific Reviewer"));
    expect(await renderResult!.findAllByText(`${callCollaboration.members[1].name}`)).toHaveLength(2);
    await user.click(renderResult!.getAllByText(`${callCollaboration.members[1].name}`)[0]);

    // Click the first user in the second reviewer selection dropdown
    expect(renderResult!.queryByText(`${callCollaboration.members[0].name}`)).not.toBeInTheDocument();
    await user.click(await renderResult!.findByLabelText("Select Primary Technical Reviewer"));
    expect(renderResult!.queryByText(`${callCollaboration.members[0].name}`)).toBeInTheDocument();
    await user.click(renderResult!.getAllByText(`${callCollaboration.members[0].name}`)[0]);

    expect(modalAssignButton.closest("button")).toBeEnabled();
    await user.click(modalAssignButton);

    expect(Api.assignProposalReviewers).toHaveBeenCalled();
    expect(setToastMessage).toHaveBeenCalledWith({
      title: "Success",
      message: "Reviewer was successfully assigned.",
      alertType: "positive",
      expireAfter: 3000,
    });
  });
});
