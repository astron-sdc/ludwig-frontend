import { render, screen, waitFor } from "@testing-library/react";
import { readAndWritePermissions, readOnlyPermissions } from "src/__tests__/factories/ProposalPermission.factory";
import { createFetchResponse, user } from "src/__tests__/utils";
import ReviewComment from "src/api/models/ReviewComment";
import ReviewCommentList from "src/components/Review/ReviewComment/ReviewCommentList";
import { beforeEach, describe, expect, it } from "vitest";
import { FetchMock } from "vitest-fetch-mock/types";

const fetchMock = fetch as FetchMock;

const fetchReviewCommentsPattern = /.*\/proposals\/\d+\/comments$/;
const postCommentPattern = /.*\/proposals\/\d+\/comments\/$/;
const fetchMultipleReviewCommentsMock = () =>
  fetchMock.mockOnceIf(
    fetchReviewCommentsPattern,
    createFetchResponse<ReviewComment[]>(
      [
        {
          id: 17,
          text: "Amazing stuff, would buy again!",
          created_at: "2024-07-16T12:45:24.234774Z",
          updated_at: "2024-07-16T12:45:24.234774Z",
          type: "PROPOSAL",
          review_id: 15,
          reviewer_name: "Saru Man",
        },
        {
          id: 12,
          text: "Amazing stuff, would buy again!",
          created_at: "2024-07-17T12:45:24.234774Z",
          updated_at: "2024-07-17T12:45:24.234774Z",
          type: "PROPOSAL",
          review_id: 15,
          reviewer_name: "Saru Man",
        },
      ],
      200,
    ),
  );

const postCommentMock = () =>
  fetchMock.mockOnceIf(
    postCommentPattern,
    createFetchResponse<ReviewComment>(
      {
        id: 25,
        text: "Amazing stuff, would buy again!",
        created_at: "2024-07-16T12:45:24.234774Z",
        updated_at: "2024-07-16T12:45:24.234774Z",
        type: "PROPOSAL",
        review_id: 15,
        reviewer_name: "Saru Man",
      },
      200,
    ),
  );

describe("ReviewCommentList", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  it("should render correctly", async () => {
    const component = render(<ReviewCommentList proposalId={12} permissions={readOnlyPermissions} />);
    expect(component).toBeTruthy();
  });

  it("does not render 'add review' controls when unauthorized", async () => {
    fetchMultipleReviewCommentsMock();

    const { container } = render(<ReviewCommentList proposalId={12} permissions={readOnlyPermissions} />);
    await waitFor(() => expect(container).toBeTruthy());

    const addCommentButton = screen.queryByText("Add comment");
    expect(addCommentButton).not.toBeInTheDocument();
  });

  it("renders a detail item for each fetched review comment", async () => {
    fetchMultipleReviewCommentsMock();

    render(<ReviewCommentList proposalId={12} permissions={readOnlyPermissions} />);
    expect(await screen.findAllByText("Amazing stuff, would buy again!")).toHaveLength(2);
  });

  it("adds a row when the Add comment button is clicked", async () => {
    postCommentMock();

    render(<ReviewCommentList proposalId={12} permissions={readAndWritePermissions} />);
    const addCommentButton = await screen.findByText("Add comment as scientific reviewer");
    const input = await screen.findByText((_, element) => element?.tagName.toLowerCase() === "textarea");

    await user.type(input, "Amazing stuff, would buy again!");
    await user.click(addCommentButton);

    expect(await screen.findAllByText("Amazing stuff, would buy again!")).toHaveLength(3);
  });
});
