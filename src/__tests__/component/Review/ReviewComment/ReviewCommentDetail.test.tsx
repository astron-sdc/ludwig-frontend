import { render, screen } from "@testing-library/react";
import ReviewComment from "src/api/models/ReviewComment";
import ReviewCommentDetail from "src/components/Review/ReviewComment/ReviewCommentDetail";
import { describe, expect, it } from "vitest";

describe("ReviewCommentDetail", async () => {
  const reviewComment: ReviewComment = {
    id: 12,
    text: "I, for one, welcome our new insect overlords",
    created_at: "2024-07-16T12:45:24.234774Z",
    updated_at: "2024-07-16T12:45:24.234774Z",
    type: "PROPOSAL",
    review_id: 15,
    reviewer_name: "Saru Man",
  };

  it("should render correctly", async () => {
    const component = render(<ReviewCommentDetail reviewComment={reviewComment} />);
    expect(component).toBeTruthy();
  });

  it("displays the correct user name", async () => {
    render(<ReviewCommentDetail reviewComment={reviewComment} />);
    expect(await screen.getByText("Saru Man")).toBeTruthy();
  });

  it("displays the correct date", async () => {
    render(<ReviewCommentDetail reviewComment={reviewComment} />);
    expect(await screen.getByText("7/16/2024, 12:45:24 UTC")).toBeTruthy();
  });

  it("displays the correct text", async () => {
    render(<ReviewCommentDetail reviewComment={reviewComment} />);
    expect(await screen.getByText("I, for one, welcome our new insect overlords")).toBeTruthy();
  });
});
