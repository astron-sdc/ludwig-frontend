import { render, screen } from "@testing-library/react";
import { createFetchResponse, user } from "src/__tests__/utils";
import Review from "src/api/models/Review";
import ReviewRating from "src/components/Review/ReviewScore/ReviewRating";
import { beforeEach, describe, expect, it } from "vitest";
import { FetchMock } from "vitest-fetch-mock/types";

const fetchMock = fetch as FetchMock;

const fetchReviewPattern = /.*\/proposals\/\d+\/reviews\/current_user_review\/$/;
const saveReviewPattern = /.*\/proposals\/\d+\/reviews\/$/;

const fetchCurrentUserReviewMock = () =>
  fetchMock.mockOnceIf(
    fetchReviewPattern,
    createFetchResponse<Review>(
      {
        id: 17,
        ranking: 4,
        reviewer_id: "4c92622e-1500-46f8-aa04-4f7346c863e3@saruman.mordor.nl",
        proposal_id: 12,
        reviewer_name: "Saru Man",
      },
      200,
    ),
  );

const saveRatingMock = () =>
  fetchMock.mockOnceIf(
    saveReviewPattern,
    createFetchResponse<Review>(
      {
        id: 17,
        ranking: 4,
        reviewer_id: "4c92622e-1500-46f8-aa04-4f7346c863e3@saruman.mordor.nl",
        proposal_id: 12,
        reviewer_name: "Saru Man",
      },
      200,
    ),
  );

describe("ReviewRating", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  it("renders correctly", async () => {
    const component = render(<ReviewRating proposalId={12} />);
    expect(component).toBeTruthy();
  });

  it("displays the rating", async () => {
    fetchCurrentUserReviewMock();
    render(<ReviewRating proposalId={12} />);

    const ratingInput = await screen.findByLabelText("Please give an overall numerical rating", { exact: false });

    expect(ratingInput).toHaveValue("4");
  });

  it("saves a valid rating", async () => {
    saveRatingMock();

    render(<ReviewRating proposalId={12} />);

    const ratingInput = await screen.findByLabelText("Please give an overall numerical rating", { exact: false });
    await user.clear(ratingInput);
    await user.type(ratingInput, "2");

    const gradeProposalButton = await screen.findByText("Grade proposal");
    await user.click(gradeProposalButton);

    expect(fetchMock).toHaveBeenCalledWith(
      expect.stringMatching(saveReviewPattern),
      expect.objectContaining({
        method: "POST",
        body: JSON.stringify({ ranking: 2 }),
      }),
    );
  });

  it("does not save an invalid rating", async () => {
    saveRatingMock();

    render(<ReviewRating proposalId={12} />);

    const ratingInput = await screen.findByLabelText("Please give an overall numerical rating", { exact: false });
    await user.clear(ratingInput);
    await user.type(ratingInput, "abcefgh");

    const gradeProposalButton = await screen.findByText("Grade proposal");
    await user.click(gradeProposalButton);

    expect(fetchMock).not.toHaveBeenCalled();
  });
});
