import { render, screen } from "@testing-library/react";
import { beforeEach, describe, expect, it } from "vitest";
import { MemoryRouter } from "react-router-dom";
import ReviewTable from "src/components/Review/ReviewTable";
import ReviewedProposal from "src/api/models/ReviewedProposal.ts";

describe("ReviewTable", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  it("should render an empty table", async () => {
    const proposals: ReviewedProposal[] = [];

    render(
      <MemoryRouter>
        <ReviewTable proposals={proposals} />
      </MemoryRouter>,
    );
    expect(await screen.findAllByText("No results found.")).toHaveLength(1);
  });

  it("should render with some proposals", async () => {
    const proposals: ReviewedProposal[] = [
      {
        id: 1,
        submitted_at: "2024-07-16T12:42:40.404113Z",
        title: "MY PROPOSAL",
        groups: [
          {
            name: "PI",
            description: "some group description",
            short_name: "pi",
            members: [
              {
                name: "Piebe Gealle",
                eduperson_unique_id: "abc@sram.surf.nl",
                email: "gealle@example.com",
                scoped_affiliation: "employee@astron.nl",
              },
            ],
          },
        ],
        reviews: [
          {
            id: 1,
            ranking: 1,
            reviewer_id: "abc@sram.surf.nl",
            proposal_id: 1,
          },
        ],
      },
    ] as ReviewedProposal[];

    render(
      <MemoryRouter>
        <ReviewTable proposals={proposals} />
      </MemoryRouter>,
    );
    expect(await screen.findAllByText("MY PROPOSAL")).toHaveLength(1); // Title
    expect(await screen.findAllByText("Piebe Gealle")).toHaveLength(1); // PI
  });
});
