import { render, screen, RenderResult } from "@testing-library/react";
import { describe, it, expect, vi } from "vitest";
import { MemoryRouter } from "react-router-dom";
import ToastContext from "src/components/Toast/Context";
import CreateEditViewProposalConfiguration from "src/components/Configuration/CreateEditViewConfiguration";
import createFetchMock from "vitest-fetch-mock";
import { act } from "react";

const mockProposalConfig = {
  id: 1,
  auto_pin: true,
  piggyback_allowed_aartfaac: true,
  piggyback_allowed_tbb: true,
  telescope: "LOFAR",
  can_trigger: true,
  expert: true,
  private_data: true,
  send_qa_workflow_notifications: true,
  filler: false,
  trigger_priority: 1000,
  primary_storage_bytes: 1000 ** 4, // 1 TB in bytess of scal1 1000s... (don't ask me why they are using not 1024)
  secondary_storage_bytes: 1000 ** 4, // 1 TB in bytes
  processing_time_seconds: 3600, // 1 hour
  support_time_seconds: 7200, // 2 hours
  observing_time_prio_A_awarded_seconds: 3600, // 1 hour
  observing_time_prio_B_awarded_seconds: 1800, // 30 mins
  observing_time_combined_awarded_seconds: 5400, // 1.5 hours
  observing_time_commissioning_awarded_seconds: 900, // 15 mins
};

function PrepareMocks() {
  fetchMock.mockResponse(async (req) => {
    if (req.url.endsWith("current")) {
      return JSON.stringify(mockProposalConfig);
    }
    return JSON.stringify([{ result: [{}] }]);
  });
}

describe("CreateEditViewProposalConfiguration", () => {
  it("renders the form with loaded data", async () => {
    const fetchMocker = createFetchMock(vi);
    fetchMocker.enableMocks();
    PrepareMocks();

    const setToastMessageSpy = vi.fn();

    let renderResult: RenderResult | undefined;
    await act(async () => {
      renderResult = render(
        <ToastContext.Provider
          value={{
            toastMessage: null,
            setToastMessage: setToastMessageSpy,
            setSuccessToastMessage: vi.fn(),
            setErrorToastMessage: vi.fn(),
          }}
        >
          <MemoryRouter>
            <CreateEditViewProposalConfiguration canEditConfiguration={true} proposalId={1} />
          </MemoryRouter>
        </ToastContext.Provider>,
      );
    });
    expect(renderResult?.asFragment()).toBeDefined();
    expect(screen.getByLabelText(/Destination telescope/i)).toHaveValue("LOFAR");
  });
});
