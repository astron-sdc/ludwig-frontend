import { FetchMock } from "vitest-fetch-mock";
import { render, screen } from "@testing-library/react";
import { beforeEach, describe, expect, it, vi } from "vitest";
import { createFetchResponse, noop, user } from "src/__tests__/utils";
import Proposal from "src/api/models/Proposal";
import ToastContext from "src/components/Toast/Context";
import TextEditor from "src/components/TextEditor/TextEditor";
import { abstractInvalidMessage, maxAbstractWordCount, validateAbstract } from "src/helper/ProposalHelper";
import proposalFactory from "src/__tests__/factories/Proposal.factory";

const fetchMock = fetch as FetchMock;

const MockProposal = proposalFactory.build();

describe("Abstract", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  const successMock = () => {
    fetchMock.mockResponse(createFetchResponse<Proposal>(MockProposal, 200));
  };

  const errorMock = () => {
    fetchMock.mockResponse(createFetchResponse<unknown>({}, 500));
  };

  it("renders correctly", async () => {
    const component = render(
      <TextEditor
        text={MockProposal.abstract}
        setText={noop}
        proposalId={MockProposal.id}
        disabled={false}
        validateText={validateAbstract}
        invalidMessage={abstractInvalidMessage}
        label="Abstract"
        fieldname="abstract "
      />,
    );

    expect(component).toBeTruthy();
  });

  it("renders correctly with empty text", async () => {
    const component = render(
      <TextEditor
        text={MockProposal.abstract}
        setText={noop}
        proposalId={MockProposal.id}
        disabled={false}
        validateText={validateAbstract}
        invalidMessage={abstractInvalidMessage}
        label="Abstract"
        fieldname="abstract"
      />,
    );

    expect(component).toBeTruthy();
  });

  it("displays supplied text in a text area", async () => {
    render(
      <TextEditor
        text={MockProposal.abstract}
        setText={noop}
        proposalId={MockProposal.id}
        disabled={false}
        validateText={validateAbstract}
        invalidMessage={abstractInvalidMessage}
        label="Abstract"
        fieldname="abstract"
      />,
    );
    const textArea = await screen.findByDisplayValue(MockProposal.abstract);
    expect(textArea).toBeInTheDocument();
    expect(textArea.tagName).toBe("TEXTAREA");
  });

  it("displays the word count", async () => {
    const text = "My concrete abstract";
    render(
      <TextEditor
        text={text}
        setText={noop}
        proposalId={MockProposal.id}
        disabled={false}
        maxWordCount={300}
        validateText={validateAbstract}
        invalidMessage={abstractInvalidMessage}
        label="Abstract"
        fieldname="abstract"
      />,
    );
    const wordCount = await screen.findByText(`Save (3/${maxAbstractWordCount} words)`);
    expect(wordCount).toBeInTheDocument();
  });

  it("displays the word count", async () => {
    const text = "My concrete abstract";
    render(
      <TextEditor
        text={text}
        setText={noop}
        proposalId={MockProposal.id}
        disabled={false}
        maxWordCount={3}
        validateText={validateAbstract}
        invalidMessage={abstractInvalidMessage}
        label="Abstract"
        fieldname="abstract"
      />,
    );
    const wordCount = await screen.findByText("Save (3/3 words)");
    expect(wordCount).toBeInTheDocument();
  });

  const originalAbstract = "My concrete abstract";
  const newAbstract = "New abstract";

  const successToast = {
    title: "Success",
    message: "Abstract was saved successfully",
    alertType: "positive",
    expireAfter: 5000,
  };

  const errorToast = {
    title: "Error",
    message: "Failed to save Abstract",
    alertType: "negative",
    expireAfter: 5000,
  };

  it.each([
    ["error", originalAbstract, newAbstract, errorToast, errorMock],
    ["success", originalAbstract, newAbstract, successToast, successMock],
  ])("shows the correct toast message on %s", async (_, originalValue, newValue, expectedToast, mock) => {
    mock();

    const setToastMessageSpy = vi.fn();
    render(
      <ToastContext.Provider
        value={{
          toastMessage: null,
          setToastMessage: setToastMessageSpy,
          setSuccessToastMessage: vi.fn(),
          setErrorToastMessage: vi.fn(),
        }}
      >
        <TextEditor
          text={originalValue}
          setText={noop}
          proposalId={MockProposal.id}
          disabled={false}
          validateText={validateAbstract}
          invalidMessage={abstractInvalidMessage}
          label="Abstract"
          fieldname="abstract"
        />
        ,
      </ToastContext.Provider>,
    );

    const input = (await screen.findByDisplayValue(originalValue)) as HTMLTextAreaElement;

    await user.type(input, "{backspace}".repeat(originalValue.length));
    await user.type(input, newValue);

    const saveButton = await screen.findByText("Save", { exact: false });

    await user.click(saveButton);

    expect(setToastMessageSpy).toHaveBeenCalledWith(expectedToast);
  });
});
