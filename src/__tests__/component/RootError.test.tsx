import { describe, expect, it } from "vitest";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import ProposalEdit from "src/pages/proposals/[id].tsx";
import RootError from "src/pages/RootError.tsx";
import { render, screen } from "@testing-library/react";

describe("RootError", () => {
  it.each([
    ["404", "You need permission to access"],
    ["not found", "Something went wrong!"],
  ])("should render the correct message when data fetching gives an error", async (error, text) => {
    const routes: RouteObject[] = [
      {
        path: "/proposals/:proposalId/",
        element: <ProposalEdit />,
        errorElement: <RootError />,
        loader: () => {
          throw Error(error);
        },
      },
    ];
    const router = createMemoryRouter(routes, {
      initialEntries: ["/proposals/4/"],
    });
    render(<RouterProvider router={router}></RouterProvider>);

    expect(await screen.findByText(text)).toBeTruthy();
  });
});
