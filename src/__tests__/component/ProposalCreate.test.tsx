import { render, screen } from "@testing-library/react";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import ProposalCreate from "src/pages/proposals/create";
import { afterAll, beforeAll, beforeEach, describe, expect, it, vi } from "vitest";
import { createFetchResponse, user } from "src/__tests__/utils";
import { FetchMock } from "vitest-fetch-mock/types";
import Proposal from "src/api/models/Proposal";
import callFactory from "src/__tests__/factories/Call.factory";
import proposalFactory from "src/__tests__/factories/Proposal.factory.ts";

const fetchMock = fetch as FetchMock;

describe("ProposalCreate", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  beforeAll(() => {
    vi.stubGlobal("DataTransfer", Object);
  });

  afterAll(() => {
    vi.unstubAllGlobals();
  });

  const titleElement = async () => await screen.findByLabelText<HTMLInputElement>("Title");

  const abstractElement = async () => await screen.findByLabelText<HTMLInputElement>("Abstract", { exact: false });

  const callElement = async () => await screen.findByRole<HTMLInputElement>("combobox");

  const submitElement = async () => await screen.findByText<HTMLButtonElement>("Create");

  const calls = [callFactory.build({ name: "Call 1" }), callFactory.build({ name: "Call 2" })];

  const routes: RouteObject[] = [
    {
      path: "/proposals/create/",
      element: <ProposalCreate />,
      loader: () => Promise.resolve(calls),
    },
  ];

  const router = createMemoryRouter(routes, {
    initialEntries: ["/proposals/create/?call=2"],
  });

  async function setFormFields() {
    await user.type(await titleElement(), "For science");
    await user.type(await abstractElement(), "This is a test abstract");

    const call = await callElement();
    await user.dblClick(call);
    await user.selectOptions(screen.getByRole("listbox"), screen.getByRole("option", { name: "Call 2" }));

    await user.click(await submitElement());
  }

  it("should not navigate on error", async () => {
    fetchMock.mockResponse(createFetchResponse<Proposal>(proposalFactory.build(), 500));

    render(<RouterProvider router={router}></RouterProvider>);

    const navigateMock = vi.fn();
    vi.spyOn(router, "navigate").mockImplementation(navigateMock);

    await setFormFields();

    expect(navigateMock).not.toHaveBeenCalled();
  });

  it("should post the correct data and then navigate to the created proposal on success", async () => {
    fetchMock.mockResponse(
      createFetchResponse<Proposal>(
        proposalFactory.build({ call_id: 2, title: "For science", abstract: "this is a test abstract" }),
        200,
      ),
    );

    render(<RouterProvider router={router}></RouterProvider>);

    const navigateMock = vi.fn();
    vi.spyOn(router, "navigate").mockImplementation(navigateMock);

    await setFormFields();

    const formData = fetchMock.mock.lastCall?.[1]?.body as FormData;

    expect(formData.get("title")).toBe("For science");
    expect(formData.get("abstract")).toBe("This is a test abstract");
    expect(formData.get("call_id")).toBe("2");

    expect(navigateMock).toHaveBeenCalledWith("/proposals/2", {
      fromRouteId: "0",
    });
  });
});
