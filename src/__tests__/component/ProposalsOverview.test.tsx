import { render, screen } from "@testing-library/react";
import ProposalsOverview from "src/pages/proposals";
import { createFetchResponse } from "src/__tests__/utils";
import { beforeEach, describe, expect, it } from "vitest";
import { MemoryRouter } from "react-router-dom";
import { FetchMock } from "vitest-fetch-mock/types";
import Proposal, { ProposalStatus } from "src/api/models/Proposal";

const fetchMock = fetch as FetchMock;

describe("ProposalsOverview", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  it("should render a row for each proposal", async () => {
    fetchMock.mockResponse(
      createFetchResponse(
        [
          {
            id: 1,
            title: "MY PROPOSAL",
            status: ProposalStatus.draft,
          } as Proposal,
          {
            id: 2,
            title: "MY PROPOSAL",
            status: ProposalStatus.draft,
          } as Proposal,
        ],
        200,
      ),
    );

    render(
      <MemoryRouter>
        <ProposalsOverview />
      </MemoryRouter>,
    );

    expect(await screen.findAllByText("MY PROPOSAL")).toHaveLength(2);
  });
});
