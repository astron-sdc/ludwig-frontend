import { render, RenderResult } from "@testing-library/react";
import { describe, expect, test } from "vitest";
import { MemoryRouter } from "react-router-dom";
import { act } from "react";
import SpecificationOverView from "src/components/Specification/SpecificationOverview";
import proposalPermissionFactory from "src/__tests__/factories/ProposalPermission.factory";

describe("SpecificationDetails", () => {
  test("renders SpecificationDetails ", async () => {
    let renderresult: RenderResult | undefined;

    await act(async () => {
      renderresult = render(
        <MemoryRouter>
          <SpecificationOverView
            isOnSyncPage={true}
            proposalId={2}
            permissions={proposalPermissionFactory.build({ write_proposal: true })}
          />
        </MemoryRouter>,
      );
    });
    expect(renderresult?.asFragment()).toBeDefined();
  });
});
