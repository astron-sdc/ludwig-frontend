import { render, RenderResult, screen } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import { beforeEach, describe, expect, test, vi } from "vitest";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import { act } from "react";
import { FetchMock } from "vitest-fetch-mock/types";
import { createFetchResponse } from "src/__tests__/utils";
import SpecificationList from "src/components/Specification/SpecificationList";
import Specification from "src/api/models/Specification";

const fetchMock = fetch as FetchMock;

describe("SpecificationList", () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  const routes: RouteObject[] = [
    {
      path: "proposals/1/specifications",
      element: <SpecificationList canEditSpecification={true} proposalId={1} />,
    },
  ];

  const router = createMemoryRouter(routes, {
    initialEntries: ["/proposals/1/specifications"],
  });

  test("renders SpecificationList without Data", async () => {
    fetchMock.disableMocks();
    let renderresult: RenderResult | undefined;
    await act(async () => {
      renderresult = render(<RouterProvider router={router}></RouterProvider>);
    });
    expect(renderresult?.asFragment()).toBeDefined();
    expect(await screen.findByText("Error encountered when fetching Specifications.")).toBeInTheDocument();
  });

  test("renders SpecificationList with Data", async () => {
    fetchMock.enableMocks();
    vi.mock("react-router-dom", async () => {
      const mod = await vi.importActual<typeof import("react-router-dom")>("react-router-dom");
      return {
        ...mod,
        useNavigate: () => {
          return vi.fn();
        },
      };
    });

    fetchMock.mockResponse(
      createFetchResponse<Specification[]>(
        [
          {
            id: 1,
            name: "Path Finder",
            description: "Spec",
            strategy: "https://tmss:one",
          },
          {
            id: 2,
            name: "nemo",
            description: "Uno",
            strategy: "https://tmss:two",
          },
        ],
        200,
      ),
    );

    await act(async () => {
      render(<RouterProvider router={router}></RouterProvider>);
    });

    expect(await screen.findByText("Add Specification")).toBeInTheDocument();
    expect(await screen.findByText("Path Finder")).toBeInTheDocument();

    const deleteButtons = screen.getAllByRole("button", { name: /delete/i });
    expect(deleteButtons.length).toBe(2);

    act(async () => {
      await userEvent.click(deleteButtons[0]);
    });
    expect(deleteButtons.length).toBe(2);

    fetchMock.mockResponse(createFetchResponse<unknown>([{}], 204));
    act(async () => {
      await userEvent.click(deleteButtons[0]);
    });

    const editButtons = screen.getAllByRole("button", { name: /edit/i });
    await userEvent.click(editButtons[0]);
  });
});
