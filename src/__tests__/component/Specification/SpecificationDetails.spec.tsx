import { render, RenderResult, screen, waitFor, fireEvent } from "@testing-library/react";
import { describe, expect, test, vi } from "vitest";
import createFetchMock from "vitest-fetch-mock";
import { createMemoryRouter, RouterProvider } from "react-router-dom";
import userEvent from "@testing-library/user-event";
import { Router } from "@remix-run/router";
import { act } from "react";
import SpecificationDetails from "src/components/Specification/SpecificationDetails";
import ApiTmss from "src/api/ApiTmss";
import commontemplate from "./__mocks__/commontemplate.json";
import strategies from "./__mocks__/strategies.json";
import tasktemplates from "./__mocks__/tasktemplates.json";
import schedulingconstraints from "./__mocks__/schedulingcontraints.json";
import taskdraft from "./__mocks__/taskdraft.json";

describe("SpecificationDetails", () => {
  async function RenderDetailScreen(PrepareMocks: () => void, router: Router) {
    let renderResult: RenderResult | undefined;
    const fetchMocker = createFetchMock(vi);
    fetchMocker.enableMocks();
    PrepareMocks();
    await act(async () => {
      renderResult = render(<RouterProvider router={router} />);
    });
    return renderResult;
  }

  function PrepareMocks() {
    fetchMock.mockResponse(async (req) => {
      if (req.url.endsWith("common_template/")) {
        return JSON.stringify(commontemplate);
      }
      if (req.url.endsWith("/strategies/")) {
        return JSON.stringify(strategies);
      }

      if (req.url.endsWith("task_template/?limit=100")) {
        return JSON.stringify(tasktemplates);
      }

      if (req.url.endsWith("scheduling_constraints_template/")) {
        return JSON.stringify(schedulingconstraints);
      }

      if (req.url.endsWith("taskdraft/")) {
        return JSON.stringify(taskdraft);
      }
      if (req.url.endsWith("utc_now/")) {
        return JSON.stringify(["2024-06-05T12:28:45.529370"]);
      }
      return JSON.stringify([{ result: [{}] }]);
    });
  }

  const routes = [
    {
      path: "/proposals/:proposalId/specification/:specificationId/edit",
      element: <SpecificationDetails />,
      loader: () => ({
        // Mock the loader data
        id: 1,
        name: "Test Specification",
        description: "This is a test description",
        strategy: "https://tmss.lofar.eu/api/scheduling_unit_observing_strategy_template/744",
        // add other fields as necessary
      }),
    },
    {
      path: "/proposals/:proposalId/specification/create",
      element: <SpecificationDetails />,
    },
  ];

  const router = createMemoryRouter(routes, {
    initialEntries: ["/proposals/1/specification/1/edit"],
  });

  const createrouter = createMemoryRouter(routes, {
    initialEntries: ["/proposals/1/specification/create"],
  });

  test("renders SpecificationDetails for new", async () => {
    sessionStorage.clear();
    const fetchMocker = createFetchMock(vi);
    fetchMocker.enableMocks();
    PrepareMocks();

    let renderResult: RenderResult | undefined;
    await act(async () => {
      renderResult = render(<RouterProvider router={createrouter} />);
    });
    expect(renderResult?.asFragment()).toBeDefined();
    await waitFor(
      () => {
        expect(screen.getByLabelText("Description")).toBeInTheDocument();
      },
      { timeout: 15000 },
    );
  });

  test("renders SpecificationDetails ", async () => {
    const renderResult: RenderResult | undefined = await RenderDetailScreen(PrepareMocks, router);

    expect(renderResult?.asFragment()).toBeDefined();
    await waitFor(
      () => {
        expect(screen.getByLabelText("Description")).toBeInTheDocument();
      },
      { timeout: 15000 },
    );

    const descriptionTextArea = screen.getByLabelText("Description");
    await act(async () => {
      userEvent.type(descriptionTextArea, "the description");
      userEvent.tab();
    });
    const nameInput = screen.getByLabelText("Name");
    await act(async () => {
      await userEvent.clear(nameInput);
      await userEvent.type(nameInput, "the name");
    });
    await waitFor(
      () => {
        expect(screen.getByLabelText("Description")).toBeInTheDocument();
      },
      { timeout: 15000 },
    );
  });

  test("it should validated observability", async () => {
    vi.spyOn(ApiTmss, "isObservable").mockResolvedValue({
      is_observable: { message: "Observable", valid: true },
      server_response_status_code: 200,
    });
    await RenderDetailScreen(PrepareMocks, router);
    const validateButton = screen.getByText("Validate Observability");
    fireEvent.click(validateButton);

    await waitFor(() => {
      expect(ApiTmss.isObservable).toHaveBeenCalled();
      expect(screen.getByText(/Observable/)).toBeInTheDocument();
    });
    vi.restoreAllMocks();
  });
});
