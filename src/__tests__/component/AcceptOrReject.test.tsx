import { render, screen, waitFor } from "@testing-library/react";
import { user } from "src/__tests__/utils";
import { beforeEach, describe, expect, it, vi } from "vitest";
import AcceptOrReject from "src/components/AcceptOrReject/AcceptOrReject";
import ToastContext from "src/components/Toast/Context";
import Api from "src/api/Api";
import { ProposalAction } from "src/api/models/Proposal";
import proposalFactory from "src/__tests__/factories/Proposal.factory.ts";

vi.mock("src/api/Api");

describe("AcceptOrReject", () => {
  const proposal = proposalFactory.build();

  const setToastMessage = vi.fn();
  const onStatusChanged = vi.fn();

  const renderComponent = () => {
    render(
      <ToastContext.Provider
        value={{
          toastMessage: null,
          setToastMessage: setToastMessage,
          setSuccessToastMessage: vi.fn(),
          setErrorToastMessage: vi.fn(),
        }}
      >
        <AcceptOrReject proposalId={proposal.id} onStatusChanged={onStatusChanged} />
      </ToastContext.Provider>,
    );
  };

  beforeEach(() => {
    vi.clearAllMocks();
  });

  it("should render accept and reject buttons", () => {
    renderComponent();
    expect(screen.getByText("Accept Proposal")).toBeInTheDocument();
    expect(screen.getByText("Reject Proposal")).toBeInTheDocument();
  });

  it("should call API and show success toast on accept", async () => {
    Api.proposalActions = vi.fn().mockResolvedValue({ ok: true });

    renderComponent();
    await user.click(screen.getByText("Accept Proposal"));

    await waitFor(() => {
      expect(Api.proposalActions).toHaveBeenCalledWith(proposal.id, ProposalAction.accept);
      expect(setToastMessage).toHaveBeenCalledWith({
        title: "Success",
        message: "Proposal action 'accept' was executed successfully",
        alertType: "positive",
        expireAfter: 3000,
      });
      expect(onStatusChanged).toHaveBeenCalled();
    });
  });

  it("should call API and show success toast on reject", async () => {
    Api.proposalActions = vi.fn().mockResolvedValue({ ok: true });

    renderComponent();
    await user.click(screen.getByText("Reject Proposal"));

    await waitFor(() => {
      expect(Api.proposalActions).toHaveBeenCalledWith(proposal.id, ProposalAction.reject);
      expect(setToastMessage).toHaveBeenCalledWith({
        title: "Success",
        message: "Proposal action 'reject' was executed successfully",
        alertType: "positive",
        expireAfter: 3000,
      });
      expect(onStatusChanged).toHaveBeenCalled();
    });
  });

  it("should show error toast on API failure for accept", async () => {
    Api.proposalActions = vi.fn().mockResolvedValue({
      ok: false,
      json: async () => ({ error: "Some error" }),
    });

    renderComponent();
    await user.click(screen.getByText("Accept Proposal"));

    await waitFor(() => {
      expect(Api.proposalActions).toHaveBeenCalledWith(proposal.id, ProposalAction.accept);
      expect(setToastMessage).toHaveBeenCalledWith({
        title: "Error executing proposal action 'accept'",
        message: "Some error",
        alertType: "negative",
        expireAfter: 3000,
      });
    });
  });

  it("should show error toast on API failure for reject", async () => {
    Api.proposalActions = vi.fn().mockResolvedValue({
      ok: false,
      json: async () => ({ error: "Some error" }),
    });

    renderComponent();
    await user.click(screen.getByText("Reject Proposal"));

    await waitFor(() => {
      expect(Api.proposalActions).toHaveBeenCalledWith(proposal.id, ProposalAction.reject);
      expect(setToastMessage).toHaveBeenCalledWith({
        title: "Error executing proposal action 'reject'",
        message: "Some error",
        alertType: "negative",
        expireAfter: 3000,
      });
    });
  });
});
