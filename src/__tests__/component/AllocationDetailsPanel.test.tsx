import { describe, expect, it, vi } from "vitest";

import { SWRConfig } from "swr";
import AllocationDetailsPanel from "src/components/AllocationDetailsPanel.tsx";
import { render, screen } from "@testing-library/react";
import callFactory from "src/__tests__/factories/Call.factory.ts";
import skyDistributionFactory from "src/__tests__/factories/SkyDistribution.factory.ts";
import Api from "src/api/Api.ts";

describe("Allocation Details Panel", async () => {
  it("renders correctly with an empty array", async () => {
    const mockSkyDistribution = vi.spyOn(Api, "getSkyDistribution");
    mockSkyDistribution.mockResolvedValue([]);

    render(
      <SWRConfig value={{ provider: () => new Map() }}>
        <AllocationDetailsPanel call={callFactory.build()} selectedProposalsIds={[]} />
      </SWRConfig>,
    );

    expect(mockSkyDistribution).toBeCalled();
    expect(screen.queryByText("Allocation details")).toBeInTheDocument();
  });

  it("renders correctly with a sky distribution", async () => {
    const mockSkyDistribution = vi.spyOn(Api, "getSkyDistribution");
    const sky_distribution = skyDistributionFactory.buildList(10);
    mockSkyDistribution.mockResolvedValue(sky_distribution);

    render(
      <SWRConfig value={{ provider: () => new Map() }}>
        <AllocationDetailsPanel call={callFactory.build()} selectedProposalsIds={[]} />
      </SWRConfig>,
    );

    expect(mockSkyDistribution).toBeCalled();
    expect(screen.queryByText("Allocation details")).toBeInTheDocument();
  });

  it("renders correctly with a sky distribution filtering on selected", async () => {
    const mockSkyDistribution = vi.spyOn(Api, "getSkyDistribution");
    const sky_distribution = skyDistributionFactory.buildList(10, { sky_distribution: { 7: 12 } });
    const selectedProposalsIds = [sky_distribution[0].proposal_id];

    mockSkyDistribution.mockResolvedValue(sky_distribution);

    render(
      <SWRConfig value={{ provider: () => new Map() }}>
        <AllocationDetailsPanel call={callFactory.build()} selectedProposalsIds={selectedProposalsIds} />
      </SWRConfig>,
    );

    expect(mockSkyDistribution).toBeCalled();
    expect(screen.queryByText("Allocation details")).toBeInTheDocument();
  });
});
