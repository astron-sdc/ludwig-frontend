import { describe, expect, it } from "vitest";

import { render, screen } from "@testing-library/react";
import CallResourceEditor from "src/components/Call/CallResourceEditor";
import callResourcesFactory from "src/__tests__/factories/CallResources.factory";

describe("CallResourceEditor", async () => {
  const callResources = callResourcesFactory.build();

  it("renders correctly", async () => {
    render(<CallResourceEditor callResources={callResources} isReadOnly={false} />);
    expect(screen.queryByText("Available resources")).toBeInTheDocument();
    const cpuTimeInput = await screen.findByLabelText("CPU time (in hours)", { exact: false });
    const storageInput = await screen.findByLabelText("Storage space (in GB)", { exact: false });
    expect(cpuTimeInput).toBeInTheDocument();
    expect(cpuTimeInput).toHaveValue(2);
    expect(storageInput).toHaveValue(1);
    expect(cpuTimeInput).toBeEnabled();
  });

  it("renders correctly in read only mode", async () => {
    render(<CallResourceEditor callResources={callResources} isReadOnly={true} />);
    const cpuTimeInput = await screen.findByLabelText("CPU time (in hours)", { exact: false });
    expect(cpuTimeInput).toBeInTheDocument();
    expect(cpuTimeInput).toHaveValue(2);
    expect(cpuTimeInput).toBeDisabled();
  });
});
