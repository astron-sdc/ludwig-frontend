import { RouteObject, createMemoryRouter } from "react-router-dom";
import Proposal from "src/api/models/Proposal";
import ProposalPermissions from "src/api/models/ProposalPermissions";
import ToastContext from "src/components/Toast/Context";
import ProposalEdit from "src/pages/proposals/[id]";
import { Mock, vi } from "vitest";

const ProposalRouter = (proposalProps: Proposal, setToastMessageSpy: Mock, proposalPermissions: ProposalPermissions) => {
  const routes: RouteObject[] = [
    {
      path: "/proposals/:proposalId/",
      element: (
        <ToastContext.Provider
          value={{
            toastMessage: null,
            setToastMessage: setToastMessageSpy,
            setSuccessToastMessage: vi.fn(),
            setErrorToastMessage: vi.fn(),
          }}
        >
          <ProposalEdit />
        </ToastContext.Provider>
      ),
      loader: () => Promise.resolve([proposalProps as Proposal, proposalPermissions]),
    },
  ];
  return createMemoryRouter(routes, {
    initialEntries: ["/proposals/4/"],
  });
};

export default ProposalRouter;
