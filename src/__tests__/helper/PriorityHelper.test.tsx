import { PriorityQueue } from "@astron-sd/telescope-specification-models";
import { getSpecificPriorityValues, PriorityQueueAllAssignd } from "src/helper/PriorityHelper";
import { describe, it, expect } from "vitest";

describe("Priority Queue Tests", () => {
  it("should return assigned priority values correctly", () => {
    const result = getSpecificPriorityValues(false);
    expect(result.length).toBe(PriorityQueueAllAssignd.length);
  });
});

describe("Priority Queue Tests All", () => {
  it("should return all priority values correctly", () => {
    const result = getSpecificPriorityValues(true);
    expect(result.length).toBe(PriorityQueue.AllQueues.length);
  });
});
