import { render, RenderResult } from "@testing-library/react";
import { describe, expect, test } from "vitest";
import { MemoryRouter } from "react-router-dom";
import { act } from "react";

import MainPage from "src/pages/Main";

describe("Main", () => {
  test("renders main ", async () => {
    let renderresult: RenderResult | undefined;

    await act(async () => {
      renderresult = render(
        <MemoryRouter>
          <MainPage />
        </MemoryRouter>,
      );
    });

    expect(renderresult?.asFragment()).toMatchSnapshot();
  });
});
