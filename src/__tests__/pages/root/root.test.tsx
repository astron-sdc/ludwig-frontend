import { render, RenderResult, act } from "@testing-library/react";
import { describe, expect, test } from "vitest";
import { MemoryRouter } from "react-router-dom";
import Root from "src/pages/Root";

describe("Root", () => {
  test("renders root ", async () => {
    let renderresult: RenderResult | undefined;

    await act(async () => {
      renderresult = render(
        <MemoryRouter>
          <Root />
        </MemoryRouter>,
      );
    });

    expect(renderresult?.asFragment()).toMatchSnapshot();
  });
});
