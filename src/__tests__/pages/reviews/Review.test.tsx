import { render, screen } from "@testing-library/react";
import { afterEach, beforeEach, describe, expect, it, vi } from "vitest";
import { FetchMock } from "vitest-fetch-mock/types";
import { createFetchResponse } from "src/__tests__/utils";
import { QueryClient, QueryClientProvider } from "react-query";
import { MemoryRouter } from "react-router-dom";
import ReviewOverview from "src/pages/reviews";

const fetchMock = fetch as FetchMock;

describe("ReviewPage", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  afterEach(() => {
    vi.restoreAllMocks();
  });

  it("should render an empty table", async () => {
    fetchMock.resetMocks();
    fetchMock.mockResponseOnce(createFetchResponse([], 200));
    const queryClient = new QueryClient();
    render(
      <MemoryRouter>
        <QueryClientProvider client={queryClient}>
          <ReviewOverview />
        </QueryClientProvider>
      </MemoryRouter>,
    );

    expect(await screen.findAllByText("No results found.")).toHaveLength(1);
  });
});
