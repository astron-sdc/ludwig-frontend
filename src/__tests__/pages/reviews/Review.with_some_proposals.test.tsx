import { render } from "@testing-library/react";
import { describe, expect, it, vi } from "vitest";
import ReviewedProposal from "src/api/models/ReviewedProposal.ts";
import { QueryClient, QueryClientProvider } from "react-query";
import { MemoryRouter } from "react-router-dom";
import ReviewOverview from "src/pages/reviews";
import { act } from "react";
import Api from "src/api/Api.ts";

// The test breaks when put together with the other test
// It seems as if the useSWR state somehow get's te be kept around
// and thus the new spy/mock is never applied...
describe("ReviewPageWithProposals", async () => {
  it("should render with some proposals", async () => {
    const proposals: ReviewedProposal[] = [
      {
        id: 1,
        submitted_at: "2024-07-16T12:42:40.404113Z",
        title: "MY PROPOSAL",
        groups: [
          {
            name: "PI",
            description: "some group description",
            short_name: "pi",
            members: [
              {
                name: "Piebe Gealle",
                eduperson_unique_id: "abc@sram.surf.nl",
                email: "gealle@example.com",
                scoped_affiliation: "employee@astron.nl",
              },
            ],
          },
        ],
        reviews: [
          {
            id: 1,
            ranking: 1,
            reviewer_id: "abc@sram.surf.nl",
            reviewer_type: "primary",
            proposal_id: 1,
          },
        ],
      },
    ] as ReviewedProposal[];

    vi.spyOn(Api, "getProposalsForReviewer").mockImplementation(async () => {
      return proposals;
    });

    const queryClient = new QueryClient();

    let renderResult = undefined;
    act(() => {
      renderResult = render(
        <MemoryRouter>
          <QueryClientProvider client={queryClient}>
            <ReviewOverview />
          </QueryClientProvider>
        </MemoryRouter>,
      );
    });

    expect(await renderResult!.findAllByText("Piebe Gealle")).toHaveLength(1);
    expect(await renderResult!.findAllByText("MY PROPOSAL")).toHaveLength(1);
  });
});
