import { render, RenderResult, screen } from "@testing-library/react";
import { beforeEach, describe, expect, it, Mock, vi } from "vitest";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import { FetchMock } from "vitest-fetch-mock/types";
import ReviewedProposal from "src/api/models/ReviewedProposal.ts";
import CallReview from "src/pages/calls/CallReview";
import { createFetchResponse } from "src/__tests__/utils";
import ToastContext from "src/components/Toast/Context";
import Call from "src/api/models/Call.ts";
import { act } from "react";

const fetchMock = fetch as FetchMock;

const callRouter = (call: Call, spy: Mock) => {
  const routes: RouteObject[] = [
    {
      path: "/calls/:callId/",
      element: (
        <ToastContext.Provider
          value={{
            toastMessage: null,
            setToastMessage: spy,
            setSuccessToastMessage: vi.fn(),
            setErrorToastMessage: vi.fn(),
          }}
        >
          <CallReview />
        </ToastContext.Provider>
      ),
      loader: () => Promise.resolve(call),
    },
  ];
  return createMemoryRouter(routes, {
    initialEntries: ["/calls/42/"],
  });
};

describe("CallReviewPage", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  it("should render an empty table", async () => {
    fetchMock.mockResponse(createFetchResponse([], 200));
    const router = callRouter({ id: 42 } as Call, vi.fn());
    render(<RouterProvider router={router}></RouterProvider>);
    expect(await screen.findAllByText("No results found.")).toHaveLength(1);
  });

  it("should render with some proposals", async () => {
    const proposals: ReviewedProposal[] = [
      {
        id: 1,
        submitted_at: "2024-07-16T12:42:40.404113Z",
        title: "MY PROPOSAL",
        groups: [
          {
            name: "PI",
            description: "some group description",
            short_name: "pi",
            members: [
              {
                name: "Piebe Gealle",
                eduperson_unique_id: "abc@sram.surf.nl",
                email: "gealle@example.com",
                scoped_affiliation: "employee@astron.nl",
              },
            ],
          },
        ],
        reviews: [
          {
            id: 1,
            ranking: 1,
            reviewer_id: "abc@sram.surf.nl",
            proposal_id: 1,
          },
        ],
      },
    ] as ReviewedProposal[];

    fetchMock.mockResponse(createFetchResponse(proposals, 200));
    const router = callRouter({ id: 1 } as Call, vi.fn());

    let renderResult: RenderResult | undefined;
    await act(async () => {
      renderResult = render(<RouterProvider router={router}></RouterProvider>);
    });

    expect(renderResult?.asFragment()).toMatchSnapshot();
    expect(await renderResult?.findAllByText("Piebe Gealle")).toHaveLength(1);
    expect(await renderResult?.findAllByText("MY PROPOSAL")).toHaveLength(1);
  });
});
