import { describe, expect, it, vi } from "vitest";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import { render, screen, waitFor } from "@testing-library/react";
import Root from "src/pages/Root";
import callFactory from "src/__tests__/factories/Call.factory";
import RootError from "src/pages/RootError";
import * as ApiUser from "src/auth/ApiUser";
import { SWRConfig } from "swr";
import { userFactory } from "src/__tests__/factories/AuthProps.factory.ts";
import ResourceAllocationTable from "src/components/Ranking/ResourceAllocationTable";
import { reviewedProposalFactory } from "src/__tests__/factories/Proposal.factory";
import callPermissionsFactory from "src/__tests__/factories/CallPermissions.factory";

const router = () => {
  const mockUserInfo = vi.spyOn(ApiUser, "getUserinfo");
  mockUserInfo.mockImplementation(() => Promise.resolve(userFactory.build()));

  const routes: RouteObject[] = [
    {
      path: "/",
      element: (
        <SWRConfig value={{ provider: () => new Map() }}>
          <Root />
        </SWRConfig>
      ),
      errorElement: <RootError />,
      children: [
        {
          path: "/ranking/:rankId/",
          element: (
            <ResourceAllocationTable
              proposals={reviewedProposalFactory.buildList(2)}
              call={callFactory.build()}
              selectedProposalsIds={[]}
              selectedProposalsChanged={() => {}}
              permissions={callPermissionsFactory.build({ export_call: true })}
            />
          ),
        },
      ],
    },
  ];
  return createMemoryRouter(routes, {
    initialEntries: ["/ranking/42/"],
  });
};

describe("ResourceAllocationTable", async () => {
  it("renders correctly", async () => {
    await waitFor(() => {
      render(<RouterProvider router={router()}></RouterProvider>);
    });

    expect(screen.queryByText("Rank")).toBeInTheDocument();
    expect(screen.queryByText("Code")).toBeInTheDocument();
  });
});
