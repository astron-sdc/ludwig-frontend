import { describe, expect, it, vi } from "vitest";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import { render, screen, waitFor } from "@testing-library/react";
import Root from "src/pages/Root";
import callFactory from "src/__tests__/factories/Call.factory";
import RootError from "src/pages/RootError";
import * as ApiUser from "src/auth/ApiUser";
import { SWRConfig } from "swr";
import { userFactory } from "src/__tests__/factories/AuthProps.factory.ts";
import CallTotalsTable from "src/pages/ranking/CallTotalsTable";

const router = () => {
  const mockUserInfo = vi.spyOn(ApiUser, "getUserinfo");
  mockUserInfo.mockImplementation(() => Promise.resolve(userFactory.build()));

  const routes: RouteObject[] = [
    {
      path: "/",
      element: (
        <SWRConfig value={{ provider: () => new Map() }}>
          <Root />
        </SWRConfig>
      ),
      errorElement: <RootError />,
      children: [
        {
          path: "/ranking/:rankId/",
          element: <CallTotalsTable call={callFactory.build()} />,
        },
      ],
    },
  ];
  return createMemoryRouter(routes, {
    initialEntries: ["/ranking/42/"],
  });
};

describe("CallTotalsTable", async () => {
  it("renders correctly", async () => {
    await waitFor(() => {
      render(<RouterProvider router={router()}></RouterProvider>);
    });

    expect(screen.queryByText("Totals")).toBeInTheDocument();
    expect(screen.queryByText("Difference")).toBeInTheDocument();
  });
});
