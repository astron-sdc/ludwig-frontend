import { beforeEach, describe, expect, it, vi } from "vitest";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import { render, screen, waitFor } from "@testing-library/react";
import RankingCallList from "src/pages/ranking/RankingCallList";
import Root from "src/pages/Root";
import callFactory from "src/__tests__/factories/Call.factory";
import Api from "src/api/Api";
import RootError from "src/pages/RootError";
import * as ApiUser from "src/auth/ApiUser";
import { SWRConfig } from "swr";
import { userFactory } from "src/__tests__/factories/AuthProps.factory.ts";

const router = () => {
  const mockUserInfo = vi.spyOn(ApiUser, "getUserinfo");
  mockUserInfo.mockResolvedValue(userFactory.build());

  // Note that <SWRConfig value={{ provider: () => new Map() }}>
  // resets the useSWR cache
  const routes: RouteObject[] = [
    {
      path: "/",
      element: (
        <SWRConfig value={{ provider: () => new Map() }}>
          <Root />
        </SWRConfig>
      ),
      errorElement: <RootError />,
      children: [
        {
          path: "/ranking/",
          element: <RankingCallList />,
        },
      ],
    },
  ];
  return createMemoryRouter(routes, {
    initialEntries: ["/ranking/"],
  });
};

describe("RankingCallLists", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
    vi.restoreAllMocks();
  });

  it("renders correctly without any calls", async () => {
    const mockGetCalls = vi.spyOn(Api, "getCalls");
    mockGetCalls.mockImplementationOnce(() => Promise.resolve([]));

    await waitFor(() => {
      render(<RouterProvider router={router()}></RouterProvider>);
    });
    expect(mockGetCalls).toBeCalled();
    expect(screen.queryByText("No results found.")).toBeInTheDocument();
  });

  it("renders correctly with calls", async () => {
    const mockGetCalls = vi.spyOn(Api, "getCalls");
    mockGetCalls.mockImplementationOnce(() => Promise.resolve(callFactory.buildList(10)));

    await waitFor(() => {
      render(<RouterProvider router={router()}></RouterProvider>);
    });

    expect(mockGetCalls).toBeCalled();
    expect(screen.queryByText("No results found.")).not.toBeInTheDocument();
  });
});
