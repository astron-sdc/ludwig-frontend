import { describe, expect, it, vi } from "vitest";
import { createMemoryRouter, RouteObject, RouterProvider } from "react-router-dom";
import { render, screen, waitFor } from "@testing-library/react";
import Root from "src/pages/Root";
import callFactory from "src/__tests__/factories/Call.factory";
import RootError from "src/pages/RootError";
import * as ApiUser from "src/auth/ApiUser";
import { SWRConfig } from "swr";
import { userFactory } from "src/__tests__/factories/AuthProps.factory";
import AllocationOverview from "src/pages/ranking/AllocationOverview";
import callPermissionsFactory from "src/__tests__/factories/CallPermissions.factory";
import Api from "src/api/Api";
import globalPermissionsFactory from "src/__tests__/factories/GlobalPermissions.factory";

const router = () => {
  const mockUserInfo = vi.spyOn(ApiUser, "getUserinfo");
  mockUserInfo.mockResolvedValue(userFactory.build());
  const mockPermissions = vi.spyOn(ApiUser, "getUserPermissions");
  mockPermissions.mockResolvedValue(globalPermissionsFactory.build());

  // Note that <SWRConfig value={{ provider: () => new Map() }}>
  // resets the useSWR cache
  const routes: RouteObject[] = [
    {
      path: "/",
      element: (
        <SWRConfig value={{ provider: () => new Map() }}>
          <Root />
        </SWRConfig>
      ),
      errorElement: <RootError />,
      children: [
        {
          path: "/ranking/:rankId/",
          element: <AllocationOverview />,
          loader: () => Promise.resolve([callFactory.build(), callPermissionsFactory.build()]),
        },
      ],
    },
  ];
  return createMemoryRouter(routes, {
    initialEntries: ["/ranking/42/"],
  });
};

describe("AllocationOverview", async () => {
  it("renders correctly", async () => {
    const mockSkyDistribution = vi.spyOn(Api, "getSkyDistribution");
    mockSkyDistribution.mockResolvedValue([]);
    const mockGetProposals = vi.spyOn(Api, "getProposalsForCall");
    mockGetProposals.mockResolvedValue([]);

    await waitFor(() => {
      render(<RouterProvider router={router()}></RouterProvider>);
    });

    expect(screen.queryByText("Allocation details")).toBeInTheDocument();
  });
});
