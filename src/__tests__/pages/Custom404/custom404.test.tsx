import { render, RenderResult, act } from "@testing-library/react";
import { describe, expect, test } from "vitest";
import { MemoryRouter } from "react-router-dom";
import Custom404 from "src/pages/404";

describe("Custom404", () => {
  test("renders 404 ", async () => {
    let renderresult: RenderResult | undefined;

    await act(async () => {
      renderresult = render(
        <MemoryRouter>
          <Custom404 />
        </MemoryRouter>,
      );
    });

    expect(renderresult?.asFragment()).toMatchSnapshot();
  });
});
