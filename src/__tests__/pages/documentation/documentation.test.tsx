import { render } from "@testing-library/react";
import { describe, test, vi } from "vitest";
import Documentation from "src/pages/documentation/Documentation";
import { MemoryRouter } from "react-router-dom";
import { act } from "react";
import createFetchMock from "vitest-fetch-mock";

const testmarkdownContent = `
# Example Markdown

This is just some markdown.
`;

describe("Documentation", () => {
  test("renders documentation correctly", async () => {
    const fetchMocker = createFetchMock(vi);
    fetchMocker.enableMocks();
    fetchMocker.mockOnce(testmarkdownContent);

    await act(async () => {
      render(
        <MemoryRouter>
          <Documentation />
        </MemoryRouter>,
      );
    });
  });
});
