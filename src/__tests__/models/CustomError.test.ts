import { describe, it, expect } from "vitest";
import CustomError from "src/api/models/CustomError";

describe("CustomError", () => {
  it("should format the error message correctly", () => {
    const responseBody = {
      errors: ["Error1", "Error2"],
      warnings: ["Warning1", "Warning2"],
    };
    const expectedMessage = "Errors: Error1, Error2\nWarnings: Warning1, Warning2";
    const cerror = new CustomError("Bad Request", responseBody);
    expect(cerror.message).toBe(expectedMessage);
  });

  it("should set the statusText correctly", () => {
    const responseBody = {
      errors: ["Error1"],
    };
    const statusText = "Bad Request";
    const error = new CustomError(statusText, responseBody);
    expect(error.statusText).toBe(statusText);
  });

  it("should set the responseBody correctly", () => {
    const responseBody = {
      errors: ["Error1"],
    };
    const error = new CustomError("Bad Request", responseBody);
    expect(error.responseBody).toBe(responseBody);
  });
});
