// proposalConfiguration.test.ts
import { ProposalConfiguration, Telescopes } from "src/api/models/ProposalConfiguration";
import { describe, it, expect } from "vitest";

describe("ProposalConfiguration", () => {
  it("should create an instance with the default values", () => {
    const config = new ProposalConfiguration();

    expect(config).toBeInstanceOf(ProposalConfiguration);
    expect(config.id).toBeUndefined();
    expect(config.telescope).toBe(Telescopes.LOFAR);
    expect(config.auto_ingest).toBe(true);
    expect(config.auto_pin).toBe(false);
    expect(config.can_trigger).toBe(false);
    expect(config.expert).toBe(false);
    expect(config.filler).toBe(false);
    expect(config.trigger_allowed_numbers).toBe(0);
    expect(config.trigger_usedNumbers).toBe(0);
    expect(config.trigger_priority).toBe(0);
    expect(config.piggyback_allowed_aartfaac).toBe(false);
    expect(config.piggyback_allowed_tbb).toBe(false);
    expect(config.private_data).toBe(false);
    expect(config.send_qa_workflow_notifications).toBe(true);
    expect(config.primary_storage_bytes).toBe(0);
    expect(config.secondary_storage_bytes).toBe(0);
    expect(config.processing_time_seconds).toBe(0);
    expect(config.support_time_seconds).toBe(0);
    expect(config.observing_time_prio_A_awarded_seconds).toBe(0);
    expect(config.observing_time_prio_B_awarded_seconds).toBe(0);
    expect(config.observing_time_combined_awarded_seconds).toBe(0);
    expect(config.observing_time_commissioning_awarded_seconds).toBe(0);
  });

  it("should allow setting custom values for the properties", () => {
    const config = new ProposalConfiguration();
    config.id = "anotherid";
    config.telescope = Telescopes.LOFAR;
    config.auto_ingest = false;
    config.trigger_allowed_numbers = 5;

    expect(config.id).toBe("anotherid");
    expect(config.telescope).toBe(Telescopes.LOFAR);
    expect(config.auto_ingest).toBe(false);
    expect(config.trigger_allowed_numbers).toBe(5);
  });
});
