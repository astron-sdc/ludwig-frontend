import { describe, expect, it, beforeEach } from "vitest";
import ApiSpecification from "src/api/ApiSpecification";
import { FetchMock } from "vitest-fetch-mock/types";
import { createFetchResponse } from "src/__tests__/utils";
import Specification from "src/api/models/Specification";
import Api from "src/api/Api";
import { ProposalConfiguration } from "src/api/models/ProposalConfiguration";
const fetchMock = fetch as FetchMock;
interface ExpectedError {
  type: string;
}

describe("Specification tests", () => {
  describe("Delete Specification", () => {
    beforeEach(() => {
      fetchMock.enableMocks();
      fetchMock.resetMocks();
    });

    it("should not delete when there is no server connection", async () => {
      const result = await ApiSpecification.deleteSpecification(1, 2);
      expect(result.isSuccessful).toEqual(false);
    });

    it("should handle server error", async () => {
      fetchMock.disableMocks();
      fetchMock.mockResponse(createFetchResponse<unknown>([{}], 500));
      const result = await ApiSpecification.deleteSpecification(1, 2);
      expect(result.isSuccessful).toEqual(false);
    });

    it("should  delete when there is a server connection", async () => {
      fetchMock.mockResponse(createFetchResponse<unknown>([{}], 204));
      const result = await ApiSpecification.deleteSpecification(1, 2);
      expect(result.isSuccessful).toEqual(true);
    });
  });

  describe("Get Specification", () => {
    it("should not accept mallformed url", async () => {
      fetchMock.disableMocks();
      fetchMock.mockResponse(createFetchResponse<unknown>([{}], 500));
      try {
        await ApiSpecification.getSpecification(1, 2);
      } catch (ex: unknown) {
        expect((ex as TypeError).message).toEqual("Only absolute URLs are supported");
      }
    });
  });

  describe("Sync Specification", () => {
    it("should sync Specification", async () => {
      fetchMock.enableMocks();
      const json = {};
      fetchMock.mockResponse(createFetchResponse<unknown>(json, 200));
      const result = await ApiSpecification.syncSpecification(1, 2);
      expect(result).toBeDefined();
    });
  });

  describe("Sync Proposal", () => {
    it("should sync Specification", async () => {
      fetchMock.enableMocks();
      fetchMock.mockResponse(createFetchResponse<unknown>({}, 200));
      const result = await Api.synchronize(1);
      expect(result).toBeDefined();
    });
  });

  describe(" Finish Proposal", () => {
    it("should sync Specification", async () => {
      fetchMock.enableMocks();
      fetchMock.mockResponse(createFetchResponse<unknown>({}, 200));
      const result = await Api.finishSync(1);
      expect(result).toBeDefined();
    });
  });

  describe("Sync Proposal", () => {
    it("should sync Specification", async () => {
      fetchMock.enableMocks();
      fetchMock.mockResponse(createFetchResponse<unknown>({}, 200));
      const result = await ApiSpecification.syncSubgroup(1, 2, "3");
      expect(result).toBeDefined();
    });
  });

  describe("Update Targets", () => {
    it("should Update Targets", async () => {
      fetchMock.enableMocks();
      fetchMock.mockResponse(createFetchResponse<unknown>({}, 200));
      const result = await Api.updateTargets(1, []);
      expect(result).toBeDefined();
    });
  });

  describe("Configuration", () => {
    it("create Proposal Configurfation", async () => {
      fetchMock.enableMocks();
      fetchMock.mockResponse(createFetchResponse<unknown>({}, 200));
      const result = await Api.createProposalConfiguration(1, new ProposalConfiguration());
      expect(result).toBeDefined();
    });

    it("Update Proposal Configurfation", async () => {
      fetchMock.enableMocks();
      fetchMock.mockResponse(createFetchResponse<unknown>({}, 200));
      const result = await Api.updateProposalConfiguration(1, new ProposalConfiguration());
      expect(result).toBeDefined();
    });
  });

  describe("Update Specification", () => {
    it("should not Update  when there is no server connection", async () => {
      fetchMock.enableMocks();
      try {
        const spec: Specification = {
          description: "test",
          name: "name",
          strategy: "stratego",
        };
        await ApiSpecification.updateSpecification(spec);
      } catch (ex: unknown) {
        expect((ex as ExpectedError).type).toBe("invalid-json");
      }
    });
  });

  describe("Create Specification", () => {
    it("should not Create when there is no server connection", async () => {
      fetchMock.enableMocks();
      try {
        const spec: Specification = {
          description: "test",
          name: "name",
          strategy: "stratego",
        };
        await ApiSpecification.createSpecification(spec);
      } catch (ex: unknown) {
        expect((ex as ExpectedError).type).toBe("invalid-json");
      }
    });
  });
});

describe("inviteMembers", () => {
  it("should send invites", async () => {
    fetchMock.enableMocks();
    fetchMock.mockResponse(createFetchResponse<unknown>({}, 202));
    const res = await Api.inviteMembers(42, ["test@example.com"], ["SomeGroupId"]);
    expect(fetchMock.requests().length).toBe(1);
    expect(res.status).toBe(202);
  });
});
