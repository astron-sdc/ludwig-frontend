import { describe, expect, it, beforeEach } from "vitest";
import Api from "src/api/ApiTmss";
import { FetchMock } from "vitest-fetch-mock/types";
import { createFetchResponse } from "src/__tests__/utils";
const fetchMock = fetch as FetchMock;

describe("Api TMSS tests", () => {
  describe("GetUTC", () => {
    beforeEach(() => {
      fetchMock.enableMocks();
      fetchMock.resetMocks();
    });

    it("should Not Get the Time", async () => {
      const result = await Api.getUtcNow();
      expect(result).toBe("");
    });

    it("should Get the Time", async () => {
      const momentinTime = "2024-06-05T12:28:45.529370";
      fetchMock.mockResponse(createFetchResponse<unknown>([momentinTime], 200));
      const result = await Api.getUtcNow();
      expect(result).toBe(momentinTime);
    });
  });

  describe("getTMssUrl bad flow", () => {
    it("should give the localhost on total failure of TMSS URL", async () => {
      const result = await Api.getTMssUrl();
      expect(result).toBe(Api.TMSS_FALLBACK_URL);
    });
  });

  describe("getTMssUrl hapy flow", () => {
    beforeEach(() => {
      fetchMock.enableMocks();
      fetchMock.resetMocks();
    });

    it("should give the right result for TMSS URL", async () => {
      const fakeurl = "http://nowhere";
      fetchMock.mockResponse(createFetchResponse<unknown>(fakeurl, 200));
      const result = await Api.getTMssUrl();
      const secondresult = await Api.getTMssUrl(); // cached url
      expect(result).toBe(fakeurl);
      expect(result).toBe(secondresult);
    });
  });
});
