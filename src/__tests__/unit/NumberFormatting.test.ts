import { describe, expect, it } from "vitest";
import { formatNumber } from "src/utils/Numbers.ts";

describe("NumberFormatting", () => {
  it.each([
    [0, "0"],
    [123, "123"],
    [-1, "-1"],
    [-42, "-42"],
    [3.1415, "3"],
    [3.456, "3"],
  ])("Should format to integers", (value, expected) => {
    expect(formatNumber(value)).toBe(expected);
  });

  it.each([
    [0, "0"],
    [123, "123"],
    [-1, "-1"],
    [-42, "-42"],
    [3.1415, "3.14"],
    [3.456, "3.46"],
  ])("Should format with optional decimals", (value, expected) => {
    expect(formatNumber(value, { fractionDigits: 2 })).toBe(expected);
  });
});
