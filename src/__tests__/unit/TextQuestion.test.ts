import TextQuestion, { validateTextQuestionAnswer } from "src/api/models/Question/TextQuestion";
import { describe, it, expect } from "vitest";

describe("TextQuestion", () => {
  describe("validateTextQuestionAnswer", () => {
    const testQuestionWithDefaults = {
      answer: "",
      min_char_length: 0,
      max_char_length: 0,
      min_word_count: 0,
      max_word_count: 0,
    } as TextQuestion;

    const testQuestionBelowMinCharLength = {
      answer: "abcd",
      min_char_length: 5,
      max_char_length: 10,
      min_word_count: 0,
      max_word_count: 100,
    } as TextQuestion;

    const testQuestionAtMinCharLength = {
      answer: "abcdf",
      min_char_length: 5,
      max_char_length: 10,
      min_word_count: 0,
      max_word_count: 100,
    } as TextQuestion;

    const testQuestionAboveMaxCharLength = {
      answer: "abcdefghijk",
      min_char_length: 5,
      max_char_length: 10,
      min_word_count: 0,
      max_word_count: 100,
    } as TextQuestion;

    const testQuestionAtMaxCharLength = {
      answer: "abcdefghij",
      min_char_length: 5,
      max_char_length: 10,
      min_word_count: 0,
      max_word_count: 100,
    } as TextQuestion;

    const testQuestionBelowMinWordCount = {
      answer: "abc",
      min_char_length: 0,
      max_char_length: 100,
      min_word_count: 2,
      max_word_count: 4,
    } as TextQuestion;

    const testQuestionAtMinWordCount = {
      answer: "abc def",
      min_char_length: 0,
      max_char_length: 100,
      min_word_count: 2,
      max_word_count: 4,
    } as TextQuestion;

    const testQuestionAboveMaxWordCount = {
      answer: "abc def ghi jkl mno",
      min_char_length: 0,
      max_char_length: 100,
      min_word_count: 2,
      max_word_count: 4,
    } as TextQuestion;

    const testQuestionAtMaxWordCount = {
      answer: "abc def ghi jkl",
      min_char_length: 0,
      max_char_length: 100,
      min_word_count: 2,
      max_word_count: 4,
    } as TextQuestion;

    it.each([
      [testQuestionBelowMinCharLength, false],
      [testQuestionAboveMaxCharLength, false],
      [testQuestionBelowMinWordCount, false],
      [testQuestionAboveMaxWordCount, false],
      [testQuestionWithDefaults, true],
      [testQuestionAtMinCharLength, true],
      [testQuestionAtMaxCharLength, true],
      [testQuestionAtMinWordCount, true],
      [testQuestionAtMaxWordCount, true],
    ])("validates %s as %s", (question, expectation) => {
      expect(validateTextQuestionAnswer(question)).toBe(expectation);
    });
  });
});
