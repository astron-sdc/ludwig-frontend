import { getTMSSURL } from "src/utils/external";
import { describe, it, expect, vi } from "vitest";

describe("getTMSSURL", () => {
  const setMockHostname = (hostname: string) => {
    vi.stubGlobal("window", { location: { hostname } });
  };

  it('should return tser when hostname includes "sdc-dev"', () => {
    setMockHostname("sdc-dev.example.com");
    const result = getTMSSURL();
    expect(result).toBe("https://tmss.tser.org");
  });

  it('should return "https://tmss.lofar.eu" when hostname includes "sdc.astron.nl"', () => {
    setMockHostname("sdc.astron.nl");
    const result = getTMSSURL();
    expect(result).toBe("https://tmss.lofar.eu");
  });

  it("should return the value from process.env.TMSS_APP_URL for other hostnames", () => {
    setMockHostname("localhost");
    // Mocking process.env.TMSS_APP_URL
    vi.stubGlobal("process", { env: { TMSS_APP_URL: "http://localhost:3000" } });
    const result = getTMSSURL();
    expect(result).toBe("http://localhost:3000");
  });
});
