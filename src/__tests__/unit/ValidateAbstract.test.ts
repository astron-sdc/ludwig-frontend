import { maxAbstractWordCount, validateAbstract } from "src/helper/ProposalHelper";
import { describe, expect, it } from "vitest";

describe("ValidateAbstract", () => {
  describe("validateAbstract", () => {
    const limit = maxAbstractWordCount;
    it("should return true for the maximum amount of words", () => {
      const abstract = new Array(limit).fill("word").join(" ");
      expect(validateAbstract(abstract)).toBe(true);
    });

    it("should return false for the maximum amount of words + 1", () => {
      const abstract = new Array(limit + 1).fill("word").join(" ");
      expect(validateAbstract(abstract)).toBe(false);
    });
  });
});
