import { createFetchResponse } from "src/__tests__/utils";
import Userinfo from "src/api/models/Userinfo";
import { checkAuth, getUserinfo } from "src/auth/ApiUser";
import { beforeEach, describe, expect, it } from "vitest";
import { FetchMock } from "vitest-fetch-mock/types";

const fetchMock = fetch as FetchMock;
const authenticatedMock = () => {
  fetchMock.mockResponse(
    createFetchResponse<Userinfo>(
      {
        uid: "41",
        name: "Piebe Gealle",
        preferred_username: "Piebe",
        given_name: "Piebe",
        familiy_name: "Gealle",
        email: "example@example.com",
        email_verified: "true",
        eduperson_entitlement: [],
      },
      204,
    ),
  );
};
const unAuthenticatedMock = () => {
  fetchMock.mockResponse(createFetchResponse<undefined>(undefined, 401));
};

describe("AuthUtils", async () => {
  beforeEach(() => {
    fetchMock.resetMocks();
  });

  it("checkAuth should return true if authenticated", async () => {
    authenticatedMock();
    expect(await checkAuth()).toBeTruthy();
  });

  it("checkAuth should return false if not authenticated", async () => {
    unAuthenticatedMock();
    expect(await checkAuth()).toBeFalsy();
  });

  it("getUserInfo should return a valid user object if authenticated", async () => {
    authenticatedMock();
    expect(await getUserinfo()).toBeTruthy();
  });

  it("getUserInfo should return undefined if not authenticated", async () => {
    unAuthenticatedMock();
    expect(await getUserinfo()).toBeUndefined();
  });
});
