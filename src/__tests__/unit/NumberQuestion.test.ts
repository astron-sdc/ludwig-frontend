import NumberQuestion, { validateNumberQuestionAnswer } from "src/api/models/Question/NumberQuestion";
import { describe, it, expect } from "vitest";

describe("NumberQuestion", () => {
  describe("validateNumberQuestionAnswer", () => {
    const invalidNumberQuestion = {
      answer: "abc",
      min_value: 0,
      max_value: 10,
    } as NumberQuestion;

    const validNumberQuestionWithDefaults = {
      answer: "0",
      min_value: 0,
      max_value: 0,
    } as NumberQuestion;

    const validNumberQuestionBelowMinValue = {
      answer: "9.99",
      min_value: 10,
      max_value: 20,
    } as NumberQuestion;

    const validNumberQuestionAtMinValue = {
      answer: "10",
      min_value: 10,
      max_value: 20,
    } as NumberQuestion;

    const validNumberQuestionAboveMaxValue = {
      answer: "20.01",
      min_value: 10,
      max_value: 20,
    } as NumberQuestion;

    const validNumberQuestionAtMaxValue = {
      answer: "20",
      min_value: 10,
      max_value: 20,
    } as NumberQuestion;

    it.each([
      [invalidNumberQuestion, false],
      [validNumberQuestionBelowMinValue, false],
      [validNumberQuestionAboveMaxValue, false],
      [validNumberQuestionWithDefaults, true],
      [validNumberQuestionAtMinValue, true],
      [validNumberQuestionAtMaxValue, true],
    ])("validates %s as %s", (question, expectation) => {
      expect(validateNumberQuestionAnswer(question)).toBe(expectation);
    });
  });
});
