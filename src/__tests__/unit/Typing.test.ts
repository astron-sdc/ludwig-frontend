import { propertyNameOf } from "src/utils/Typing";
import { describe, expect, it } from "vitest";

describe("Typing", () => {
  describe("propertyNameOf", () => {
    it("should return the requested property name (strongly typed)", () => {
      interface Dummy {
        name: string;
        id: number;
      }

      expect(propertyNameOf<Dummy>()("id")).toBe("id");
    });
  });
});
