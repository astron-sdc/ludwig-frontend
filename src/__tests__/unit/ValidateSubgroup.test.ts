import { validateSubgroup } from "src/components/Targets/Utils/ValidateTarget";
import { describe, expect, it } from "vitest";

describe("validateSubgroup", () => {
  it("should return false for a non-number as string", () => {
    expect(validateSubgroup("abc")).toBe(false);
  });

  it("should return true for a number as string", () => {
    expect(validateSubgroup("72")).toBe(true);
  });

  it("should return true for an empty string", () => {
    expect(validateSubgroup("")).toBe(true);
  });
});
