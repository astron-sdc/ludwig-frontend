import { getWordCount } from "src/utils/Validation";
import { describe, it, expect } from "vitest";

describe("Validation", () => {
  describe("getWordCount", () => {
    it("should return 0 for an empty string", () => {
      expect(getWordCount("")).toBe(0);
    });

    it("should return 1 for a single word", () => {
      expect(getWordCount("hello")).toBe(1);
    });

    it("should return x for x words", () => {
      expect(getWordCount("one two three four five")).toBe(5);
    });

    it("should disregard leading and trailing whitespace", () => {
      expect(getWordCount("\n  hel lo  \t")).toBe(2);
    });

    it("should disregard subsequent spaces", () => {
      expect(getWordCount("\n  hel    lo  \t")).toBe(2);
    });
  });
});
