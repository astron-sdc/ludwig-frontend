import { validateDmsCoordinate, validateHmsCoordinate } from "src/components/Targets/Utils/ValidateTarget";
import { describe, expect, it } from "vitest";

describe("ValidateTarget", () => {
  describe("validateHmsCoordinate", () => {
    it.each([
      ["19:53:46.99", true],
      ["19:53:46", true],
      ["00:53:46.99", true],
      ["00:53:46", true],
      ["19:53:46.9", true],
      ["19:53:46.999", true],
      ["19:53:46.", false],
      ["19:53:4", false],
      ["", false],
      ["abc", false],
    ])("should validate %s as %s", (coordinate, expected) => {
      expect(validateHmsCoordinate(coordinate)).toBe(expected);
    });
  });

  describe("validateDmsCoordinate", () => {
    it.each([
      ["+19:53:46", true],
      ["-19:53:46", true],
      ["+00:53:46", true],
      ["-00:53:46", true],
      ["+19:53:46.9", true],
      ["-19:53:46.9", true],
      ["+19:53:46.99", true],
      ["-19:53:46.99", true],
      ["+19:53:46.", false],
      ["-19:53:46.", false],
      ["+19:53:4", false],
      ["-19:53:4", false],
      ["", false],
      ["abc", false],
    ])("should validate %s as %s", (coordinate, expected) => {
      expect(validateDmsCoordinate(coordinate)).toBe(expected);
    });
  });
});
