/**
 *  Copyright 2024 ASTRON
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, LoaderFunctionArgs, redirect, RouteObject, RouterProvider } from "react-router-dom";
import "@astron-sdc/design-system/styles.css"; // needs to be after index.css
import "@fortawesome/fontawesome-free/css/all.css";
import "react-datepicker/dist/react-datepicker.css";
import "primeicons/primeicons.css";
import "src/index.css";

import Api from "src/api/Api";
import Custom404 from "src/pages/404";
import CallsOverview from "src/pages/calls/index.tsx";
import CyclesOverview from "src/pages/cycles";
import CreateEditViewCycle from "src/pages/cycles/CreateEditViewCycle";
import CreateEditViewCall from "src/pages/calls/CreateEditViewCall";
import MainPage from "src/pages/Main";
import ProposalsOverview from "src/pages/proposals";
import ProposalEdit from "src/pages/proposals/[id].tsx";
import ProposalCreate from "src/pages/proposals/create.tsx";
import Root from "src/pages/Root";
import RootError from "src/pages/RootError";
import Documentation from "src/pages/documentation/Documentation";
import { BACKEND_BASENAME, BASENAME } from "src/utils/constants";
import SpecificationDetails from "src/components/Specification/SpecificationDetails";
import ApiSpecification from "src/api/ApiSpecification";
import CycleHelper from "src/helper/CycleHelper";
import CallReview from "src/pages/calls/CallReview.tsx";
import ReviewOverview from "src/pages/reviews/index.tsx";
import { getCallAndPermissions } from "./helper/CallHelper.ts";
import RankingCallList from "./pages/ranking/RankingCallList.tsx";
import AllocationOverview from "./pages/ranking/AllocationOverview.tsx";

const getProposalAndPermissions = async (proposalId: number) =>
  Promise.all([Api.getProposal(proposalId), Api.getProposalPermissions(proposalId)]);

const routes: RouteObject[] = [
  {
    path: "/",
    element: <Root />,
    errorElement: <RootError />,
    children: [
      {
        path: "/",
        element: <MainPage />,
      },
      {
        path: "/reviews",
        element: <ReviewOverview />,
      },
      {
        path: "/overview/",
        element: <ProposalsOverview />,
      },
      {
        path: "/proposals/create/",
        element: <ProposalCreate />,
        loader: () => Api.getOpenCalls(),
      },
      {
        path: "/proposals/:proposalId/",
        element: <ProposalEdit />,
        loader: ({ params }: LoaderFunctionArgs) => getProposalAndPermissions(Number(params.proposalId)),
      },
      {
        path: "/proposals/:proposalId/:tabkey",
        element: <ProposalEdit />,
        loader: ({ params }: LoaderFunctionArgs) => getProposalAndPermissions(Number(params.proposalId)),
      },
      {
        path: "/proposals/:proposalId/:tabkey/:sync",
        element: <ProposalEdit />,
        loader: ({ params }: LoaderFunctionArgs) => getProposalAndPermissions(Number(params.proposalId)),
      },
      {
        path: "/proposals/:proposalId/specification/create",
        element: <SpecificationDetails />,
      },
      {
        path: "/proposals/:proposalId/specification/:specificationId/edit",
        loader: ({ params }: LoaderFunctionArgs) =>
          ApiSpecification.getSpecification(Number(params.proposalId), Number(params.specificationId)),
        element: <SpecificationDetails />,
      },
      {
        path: "/cycles/",
        element: <CyclesOverview />,
      },
      {
        path: "/cycles/create/",
        element: <CreateEditViewCycle />,
      },
      {
        path: "/cycles/:cycleId/calls/create/",
        element: <CreateEditViewCall />,
      },
      {
        path: "/calls/create/",
        element: <CreateEditViewCall />,
      },
      {
        // The "link back" from SRAM goes to the index instead of the /view; so redirecting is necessary
        path: "/calls/:callId/",
        loader: (props) => {
          return redirect(`/calls/${props.params.callId}/view`);
        },
      },
      {
        path: "/calls/:callId/review",
        loader: ({ params }: LoaderFunctionArgs) => getCallAndPermissions(Number(params.callId)),
        element: <CallReview />,
      },
      {
        path: "/calls/:callId/:command",
        loader: ({ params }: LoaderFunctionArgs) => getCallAndPermissions(Number(params.callId)),
        element: <CreateEditViewCall />,
      },
      {
        path: "/cycles/:cycleId/:command",
        loader: ({ params }: LoaderFunctionArgs) => CycleHelper.getCycle(Number(params.cycleId)),
        element: <CreateEditViewCycle />,
      },
      {
        path: "/calls/",
        element: <CallsOverview />,
      },

      {
        path: "/documentation/",
        children: [
          { path: "/documentation/", element: <Documentation /> },
          { path: "/documentation/*", element: <Documentation /> },
        ],
      },
      {
        path: "/ranking/",
        element: <RankingCallList />,
      },
      {
        path: "/allocation/:callId/",
        loader: ({ params }: LoaderFunctionArgs) => getCallAndPermissions(Number(params.callId)),
        element: <AllocationOverview />,
      },
      {
        path: "/redirect/*",
        loader: ({ params }: LoaderFunctionArgs) => {
          window.location.assign(`${BACKEND_BASENAME}/oauth2/start?rd=/${encodeURIComponent(params["*"]!)}`);
          return null; // loader requires return value;
        },
      },
      {
        path: "/*",
        element: <Custom404 />,
      },
    ],
  },
];

const router = createBrowserRouter(routes, { basename: BASENAME });

const rootElement = document.getElementById("root") as HTMLElement;
const root = ReactDOM.createRoot(rootElement);
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
);
