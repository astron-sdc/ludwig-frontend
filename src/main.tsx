/**
 *  Copyright 2024 ASTRON
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import React from "react";
import ReactDOM from "react-dom/client";
import {
  createBrowserRouter,
  RouteObject,
  RouterProvider,
} from "react-router-dom";

import "react-datepicker/dist/react-datepicker.css";
import "src/index.css";
import "@astron-sdc/design-system/styles.css"; // needs to be after index.css

import Root from "src/components/Root/Root";
import RootError from "src/components/RootError/RootError";
import MainPage from "src/pages/Main";
import CyclesOverview from "src/pages/cycles";
import CreateCycle from "src/pages/cycles/create";
import CreateCall from "src/pages/cycles/[id]/calls/create";
import CycleDetails from "src/pages/cycles/[id]";
import Custom404 from "src/pages/404";
import ProposalCreate from "src/pages/proposals/create.tsx";
import ProposalsOverview from "src/pages/proposals";
import Api from "./api/Api";
import ProposalEdit from "src/pages/proposals/edit";

const routes: RouteObject[] = [
  {
    path: "/",
    element: <Root />,
    errorElement: <RootError />,
    children: [
      {
        path: "/",
        element: <MainPage />,
      },
      {
        path: "/overview/",
        element: <ProposalsOverview />,
      },
      {
        path: "/proposal/create/",
        element: <ProposalCreate />,
        loader: () => Api.getCalls(),
      },
      {
        path: "/proposal/:proposalId/",
        element: <ProposalEdit />,
      },
      {
        path: "/cycles/",
        element: <CyclesOverview />,
      },
      {
        path: "/cycles/create/",
        element: <CreateCycle />,
      },
      {
        path: "/cycles/:cycleId/calls/create/",
        element: <CreateCall />,
      },
      {
        path: "/cycles/:cycleId/",
        element: <CycleDetails />,
      },

      {
        path: "/*",
        element: <Custom404 />,
      },
    ],
  },
];

const router = createBrowserRouter(routes, { basename: "/proposal" });

const rootElement = document.getElementById("root") as HTMLElement;
const root = ReactDOM.createRoot(rootElement);
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
);
