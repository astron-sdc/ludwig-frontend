import Api from "src/api/Api";
import Call from "src/api/models/Call";

export const getCall = async (callId: number): Promise<Call> => {
  try {
    const call: Call = await Api.getCall(callId);
    call.start = new Date(call.start);
    call.end = new Date(call.end);
    return call;
  } catch (error) {
    console.error(`Error fetching call with id ${callId}:`, error);
    throw error; // Re-throw the error after logging it
  }
};

export const saveCall = async (call: Partial<Call>): Promise<Call> => {
  if (!call.id) return Api.createCall(call);
  return Api.updateCall(call);
};

export const getCallAndPermissions = async (callId: number) =>
  Promise.all([getCall(callId), Api.getCallPermissions(callId)]);
