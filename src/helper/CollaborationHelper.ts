import CallCollaboration from "src/api/models/CallCollaboration";
import { CollaborationGroupShortName } from "src/api/models/CollaborationGroup";

import CollaborationMember from "src/api/models/CollaborationMember";
import { PanelType } from "src/api/models/Review";

const formatContactInfo = (member?: CollaborationMember) => {
  if (!member) return "<No Info>";
  return `${member.name} <${member.email}>`;
};

const getPanelMembersPerPanelType = (callCollaboration?: CallCollaboration): Record<PanelType, CollaborationMember[]> => ({
  technical:
    callCollaboration?.groups
      ?.filter((group) => group.short_name === CollaborationGroupShortName.tech_reviewer)
      .flatMap((group) => group.members) || [],
  scientific:
    callCollaboration?.groups
      ?.filter((group) => group.short_name === CollaborationGroupShortName.sci_reviewer)
      .flatMap((group) => group.members) || [],
});

export { formatContactInfo, getPanelMembersPerPanelType };
