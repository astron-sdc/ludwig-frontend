import { PriorityQueue } from "@astron-sd/telescope-specification-models";

export const PriorityQueueNotDetermined = "?";
export const PriorityQueueNoPriority = "X";
export const PriorityQueueAllAssignd = [...PriorityQueue.AllQueues, PriorityQueueNotDetermined, PriorityQueueNoPriority];

export const getPriorityValues = (): { value: string; label: string }[] => {
  return PriorityQueue.AllQueues.map((queue) => ({
    label: queue,
    value: queue,
  }));
};

export const getAssignedPriorityValues = (): { value: string; label: string }[] => {
  return PriorityQueueAllAssignd.map((queue) => ({
    label: PriorityValueToLabel(queue),
    value: queue,
  }));
};

export const getSpecificPriorityValues = (isRequestedValues: boolean): { value: string; label: string }[] => {
  if (isRequestedValues) return getPriorityValues();
  return getAssignedPriorityValues();
};

export const PriorityValueToLabel = (value: string) => {
  switch (value) {
    case PriorityQueueNoPriority:
      return "No Priority";
    case PriorityQueueNotDetermined:
      return "Undetermined";
  }
  return value;
};
