import { ProposalConfiguration } from "src/api/models/ProposalConfiguration";
import { getWordCount } from "src/utils/Validation";
import Api from "src/api/Api";
import Proposal from "src/api/models/Proposal.ts";

const maxAbstractWordCount = 300;
const abstractInvalidMessage = `Abstract is required and must be ${maxAbstractWordCount} words or fewer.`;

const validateLength = (text: string, maxWordCount: number = 99999999) => getWordCount(text) <= maxWordCount;

const validateAbstract = (text: string) => validateLength(text, maxAbstractWordCount);

const saveConfiguration = async (
  proposalId: number,
  proposalConfiguration: ProposalConfiguration,
): Promise<ProposalConfiguration> => {
  if (!proposalConfiguration.id && proposalConfiguration.id !== "")
    return Api.createProposalConfiguration(proposalId, proposalConfiguration);
  return Api.updateProposalConfiguration(proposalId, proposalConfiguration);
};

function getProposalMemberById(proposal: Proposal, eduperson_unqiue_id?: string) {
  return proposal.members.find((member) => member.eduperson_unique_id === eduperson_unqiue_id);
}

export { validateAbstract, abstractInvalidMessage, saveConfiguration, maxAbstractWordCount, getProposalMemberById };
