import Api from "src/api/Api";
import Cycle from "src/api/models/Cycle";

class CycleHelper {
  async getCycle(cycleId: number): Promise<Cycle> {
    try {
      const cycle: Cycle = await Api.getCycle(cycleId);
      cycle.start = new Date(cycle.start);
      cycle.end = new Date(cycle.end);
      return cycle;
    } catch (error) {
      console.error(`Error fetching cycle with id ${cycleId}:`, error);
      throw error; // Re-throw the error after logging it
    }
  }

  async saveCycle(cycle: Partial<Cycle>): Promise<Cycle> {
    if (!cycle.id) return Api.createCycle(cycle);
    return Api.updateCycle(cycle);
  }
}

export default new CycleHelper();
