import Question from "src/api/models/Question/Question";

interface ChoiceQuestion extends Question {
  choices: string[];
}

export default ChoiceQuestion;
