import Question from "src/api/models/Question/Question";
import { validateNumber } from "src/utils/Validation";

interface NumberQuestion extends Question {
  min_value: number;
  max_value: number;
  unit: string;
}

const validateNumberQuestionAnswer = (question: NumberQuestion): boolean => {
  const value = Number(question.answer);
  return validateNumber(value, question.min_value, question.max_value);
};

export default NumberQuestion;
export { validateNumberQuestionAnswer };
