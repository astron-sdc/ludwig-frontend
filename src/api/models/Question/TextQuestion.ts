import Question from "src/api/models/Question/Question";
import { getWordCount } from "src/utils/Validation";

interface TextQuestion extends Question {
  min_char_length: number;
  max_char_length: number;
  min_word_count: number;
  max_word_count: number;
}

const validateTextQuestionAnswer = (question: TextQuestion): boolean => {
  const wordCount = getWordCount(question.answer);

  return (
    question.answer.length >= question.min_char_length &&
    question.answer.length <= question.max_char_length &&
    wordCount >= question.min_word_count &&
    wordCount <= question.max_word_count
  );
};

export default TextQuestion;
export { validateTextQuestionAnswer };
