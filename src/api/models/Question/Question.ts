import NumberQuestion, { validateNumberQuestionAnswer } from "src/api/models/Question/NumberQuestion";
import QuestionType from "src/api/models/Question/QuestionType";
import TextQuestion, { validateTextQuestionAnswer } from "src/api/models/Question/TextQuestion";

interface Question {
  id: number;
  rowId: string;
  title: string;
  is_required: boolean;
  order: number;
  type: QuestionType;
  requires_elaboration: boolean;
  answer: string;
}

const validateAnswer = (question: Question): boolean => {
  switch (question.type) {
    case "NumberQuestion":
      return validateNumberQuestionAnswer(question as NumberQuestion);

    case "TextQuestion":
      return validateTextQuestionAnswer(question as TextQuestion);

    case "ChoiceQuestion":
      return true;
  }
};
export default Question;
export { validateAnswer };
