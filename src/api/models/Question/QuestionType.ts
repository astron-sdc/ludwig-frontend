type QuestionType = "NumberQuestion" | "TextQuestion" | "ChoiceQuestion";

export default QuestionType;
