import Target from "src/api/models/Target.ts";

interface Proposal {
  id?: number;
  title: string;
  abstract: string;
  call_id: number;
  status?: string;
  scientific_justification?: string | File;
  targets?: Target[];
  created_at?: Date;
}

export default Proposal;
