import CollaborationMember from "src/api/models/CollaborationMember.ts";
import CollaborationGroup from "src/api/models/CollaborationGroup.ts";

export enum ProposalStatus {
  draft = "draft",
  submitted = "submitted",
  retracted = "retracted",
  underReview = "under_review",
  rejected = "rejected",
  accepted = "accepted",
}

export enum ProposalAction {
  accept = "accept",
  reject = "reject",
  retract = "retract",
  review = "review",
  submit = "submit",
  unsubmit = "unsubmit",
}

interface Proposal {
  readonly id: number;
  title: string;
  abstract: string;
  call_id: number;
  readonly project_code: string;
  readonly status: ProposalStatus;
  scientific_justification?: string | File;
  scientific_justification_filename?: string;
  scientific_justification_filesize?: number;
  readonly created_at?: Date | string;
  readonly created_by?: string;
  contact_author?: string;
  readonly authors?: string[];
  call_is_open?: boolean;
  tmss_project_id?: string;
  rank?: string;
  tmss_project_id_synced_at?: Date | string;
  readonly members: CollaborationMember[];
  readonly groups: CollaborationGroup[];
  readonly collaboration_url: string;
  readonly members_url: string;
  readonly groups_url: string;
  readonly invite_link: string;
  readonly join_requests_url: string;
}

export default Proposal;
