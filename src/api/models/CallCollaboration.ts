import CollaborationMember from "src/api/models/CollaborationMember.ts";
import CollaborationGroup from "src/api/models/CollaborationGroup.ts";

interface CallCollaboration {
  readonly members: CollaborationMember[];
  readonly groups: CollaborationGroup[];
  readonly collaboration_url: string;
  readonly members_url: string;
  readonly groups_url: string;
  readonly invite_link: string;
  readonly join_requests_url: string;
}

export default CallCollaboration;
