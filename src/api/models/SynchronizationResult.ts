export interface SynchronizationResult {
  did_succeed: boolean;
  message: string[];
}
