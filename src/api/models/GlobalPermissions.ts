interface GlobalPermissions {
  is_admin: boolean;
  create_cycle: boolean;
  create_call: boolean;
  view_call_ranking: boolean;
}

export default GlobalPermissions;
