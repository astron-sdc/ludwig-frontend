import Proposal from "src/api/models/Proposal.ts";
import Review from "src/api/models/Review";

interface ReviewedProposal extends Proposal {
  submitted_at: string | Date;
  reviews: Review[];
  review_rating: number | null;
}

export default ReviewedProposal;
