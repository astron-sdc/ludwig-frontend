interface Userinfo {
  uid: string;
  name: string;
  preferred_username: string;
  given_name: string;
  familiy_name: string;
  email: string;
  email_verified: string;
  eduperson_entitlement: string[];
}

export default Userinfo;
