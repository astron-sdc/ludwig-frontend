interface ReviewComment {
  id: number;
  text: string;
  created_at: string;
  updated_at: string;
  type: string;
  review_id: number;
  reviewer_name: string;
}

export default ReviewComment;
