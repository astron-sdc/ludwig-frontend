interface CallPermissions {
  write_call: boolean;
  view_call_members: boolean;
  manage_members_science_panel: boolean;
  manage_members_technical_panel: boolean;
  export_call: boolean;
  accept_proposal: boolean;
  reject_proposal: boolean;
}

export default CallPermissions;
