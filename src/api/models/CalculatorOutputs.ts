interface CalculatorResult {
  proposal: number;
  target: number;
  target_name: string;
  raw_data_size_bytes: number;
  processed_data_size_bytes: number;
  pipeline_processing_time_seconds: number;
  mean_elevation: number;
  theoretical_rms_jansky_beam: number;
  effective_rms_jansky_beam: number;
  effective_rms_jansky_beam_err: number;
}

export default CalculatorResult;
