export interface IsObservableResult {
  is_observable: ObservableValidation;
  server_response_status_code: number;
}

export interface ObservableValidation {
  message: string;
  valid: boolean;
}
