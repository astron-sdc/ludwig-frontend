import CollaborationMember from "src/api/models/CollaborationMember.ts";

export enum CollaborationGroupShortName {
  committee_chair = "committee_chair",
  tech_reviewer = "tech_reviewer",
  tech_reviewer_ch = "tech_reviewer_ch",
  sci_reviewer = "sci_reviewer",
  sci_reviewer_ch = "sci_reviewer_ch",
  pi = "pi",
  co_author = "co_author",
  co_author_ro = "co_author_ro",
}

export const collaborationGroupNames: Record<CollaborationGroupShortName, string> = {
  committee_chair: "Committee Chair",
  tech_reviewer: "Technical Reviewer",
  tech_reviewer_ch: "Technical Reviewer Chair",
  sci_reviewer: "Science Reviewer",
  sci_reviewer_ch: "Science Reviewer Chair",
  pi: "PI",
  co_author: "CO Author",
  co_author_ro: "CO Author Read Only",
};
export type CollaborationGroupName = (typeof collaborationGroupNames)[CollaborationGroupShortName];

export const collaborationGroupDescriptions: Record<CollaborationGroupShortName, string> = {
  committee_chair: "Committee Chair",
  tech_reviewer: "Technical reviewer",
  tech_reviewer_ch: "Chair of the technical reviewers",
  sci_reviewer: "Science reviewer",
  sci_reviewer_ch: "Chair of the science reviewers",
  pi: "Principal Investigator",
  co_author: "CO Author",
  co_author_ro: "CO Author with read only access",
};
export type CollaborationGroupDescription = (typeof collaborationGroupDescriptions)[CollaborationGroupShortName];

interface CollaborationGroup {
  identifier: string;
  name: CollaborationGroupName;
  short_name: CollaborationGroupShortName;
  description: CollaborationGroupDescription;
  members: CollaborationMember[];
}

export default CollaborationGroup;
