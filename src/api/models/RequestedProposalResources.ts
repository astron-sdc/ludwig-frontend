interface ProposalResources {
  proposal_id: number;
  observation_time_seconds: number;
  data_size_bytes: number;
  pipeline_processing_time_seconds: number;
  support_time_seconds: number;
}

export default ProposalResources;
