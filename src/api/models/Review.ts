import ReviewedProposal from "./ReviewedProposal";

export enum PanelType {
  technical = "technical",
  scientific = "scientific",
}

export enum ReviewerType {
  primary = "primary",
  secondary = "secondary",
}

interface Review {
  id: number;
  ranking: number;
  reviewer_id: string;
  proposal_id: number;
  panel_type?: PanelType;
  reviewer_type?: ReviewerType;
  reviewer_name: string;
}

export interface ProposalPanelTypePairing {
  panelType: PanelType;
  proposal: ReviewedProposal;
}

export default Review;
