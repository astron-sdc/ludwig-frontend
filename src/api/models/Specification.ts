import { TemplateDefinition } from "@astron-sd/telescope-specification-models";
interface Specification {
  name: string;
  description: string;
  id?: number;
  definition?: TemplateDefinition;
  strategy: string;
  proposal?: string;
  requested_priority?: string;
  assigned_priority?: string;
  group_id_number?: string; // the real number of the group
  targetgroup?: number; // group record id
}

export default Specification;
