interface Target {
  id: number;
  group: string;
  subgroup: string;
  requested_priority: string;
  assigned_priority: string;
  field_order: string;
  name: string;
  x_hms_formatted: string;
  y_dms_formatted: string;
  system: string;
  notes: string;
}

export default Target;
