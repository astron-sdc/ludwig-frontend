interface Target {
  id: number;
  name: string;
  x_hms_formatted: string;
  y_dms_formatted: string;
  system: string;
  notes: string;
}

export default Target;
