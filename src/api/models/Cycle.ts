interface Cycle {
  id: number;
  name: string;
  code: string;
  description: string;
  start: Date;
  end: Date;
}

export default Cycle;
