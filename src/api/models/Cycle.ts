interface Cycle {
  id: number;
  name: string;
  code: string;
  description: string;
  start: Date | string;
  end: Date | string;
  tmss_cycle_id?: string;
}

export default Cycle;
