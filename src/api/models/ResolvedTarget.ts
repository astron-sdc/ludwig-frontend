interface ResolvedTarget {
  reference_frame: string;
  x_degree: number;
  x_hms: number[];
  x_hms_formatted: string;
  y_degree: number;
  y_dms: number[];
  y_dms_formatted: string;
}

export default ResolvedTarget;
