interface CallResources {
  observing_time_seconds: number;
  cpu_time_seconds: number;
  storage_space_bytes: number;
  support_time_seconds: number;
}

export default CallResources;
