interface CollaborationMember {
  eduperson_unique_id: string;
  name: string;
  email: string;
  schac_home_organisation: string;
  scoped_affiliation: string;
}

export default CollaborationMember;
