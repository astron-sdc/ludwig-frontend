class CustomError extends Error {
  private static formatErrorMessage(responseBody: object): string {
    let formattedMessage = "";
    for (const [key, value] of Object.entries(responseBody)) {
      if (Array.isArray(value)) {
        formattedMessage += `${key.charAt(0).toUpperCase() + key.slice(1)}: ${value.join(", ")}\n`;
      }
    }
    return formattedMessage.trim();
  }

  statusText: string;
  responseBody: object;

  constructor(statusText: string, responseBody: object) {
    super(CustomError.formatErrorMessage(responseBody));
    this.name = "CustomError";
    this.statusText = statusText;
    this.responseBody = responseBody;
    console.log(this.message);
  }
}

export default CustomError;
