export class ProposalConfiguration {
  id?: string;
  telescope: Telescopes = Telescopes.LOFAR;
  auto_ingest: boolean = true;
  auto_pin: boolean = false;
  can_trigger: boolean = false;
  expert: boolean = false;
  filler: boolean = false;
  trigger_allowed_numbers: number = 0;
  trigger_usedNumbers?: number = 0;
  trigger_priority: number = 0;
  piggyback_allowed_aartfaac: boolean = false;
  piggyback_allowed_tbb: boolean = false;
  enable_qa_workflow: boolean = true;
  private_data: boolean = false;
  send_qa_workflow_notifications: boolean = true;
  primary_storage_bytes: number = 0;
  secondary_storage_bytes: number = 0;
  processing_time_seconds: number = 0;
  support_time_seconds: number = 0;
  observing_time_prio_A_awarded_seconds: number = 0;
  observing_time_prio_B_awarded_seconds: number = 0;
  observing_time_combined_awarded_seconds: number = 0;
  observing_time_commissioning_awarded_seconds: number = 0;
}
// Enum for Telescopes
export enum Telescopes {
  LOFAR = "LOFAR",
  // Add more telescopes as required
}
