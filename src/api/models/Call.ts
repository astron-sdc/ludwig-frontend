import CallResources from "./CallResources";

export type CallStatus = "open" | "closed";

interface Call extends CallResources {
  id: number;
  name: string;
  code: string;
  description: string;
  start: Date | string;
  category: string;
  end: Date | string;
  requires_direct_action: boolean;
  status: CallStatus;
  cycle_id: number;
  cycle_start: Date | string;
  cycle_end: Date | string;
  cycle_name: string;
}

export default Call;
