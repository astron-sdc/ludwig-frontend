interface Call {
  id: number;
  name: string;
  code: string;
  description: string;
  start: Date;
  end: Date;
  requires_direct_action: boolean;
  cycle_id: number;
}

export default Call;
