interface ProposalPermissions {
  write_proposal: boolean;
  submit_proposal: boolean;
  read_technical_review: boolean;
  write_technical_review: boolean;
  read_science_review: boolean;
  write_science_review: boolean;
  manage_members: boolean;
  accept_proposal: boolean;
  reject_proposal: boolean;
  read_calculation: boolean;
  write_calculation: boolean;
}

export default ProposalPermissions;
