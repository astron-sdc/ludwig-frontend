interface CalculatorInputs {
  observation_time_seconds: number;
  no_core_stations: number;
  no_remote_stations: number;
  no_international_stations: number;
  no_channels_per_subband: ChannelsPerSubband;
  no_subbands: number;
  integration_time_seconds: number;
  antenna_set: AntennaSet;
  target?: number;
  observation_date: string;
  calibrators: Calibrators[];
  a_team_sources: ATeamSources[];
  pipeline: Pipeline;
  enable_dysco_compression?: boolean;
  frequency_averaging_factor?: number;
  time_averaging_factor?: number;
}

export enum AntennaSet {
  "LBA Outer" = "lbaouter",
  "LBA Sparse" = "lbasparse",
  "HBA Dual" = "hbadual",
  "HBA Dual Inner" = "hbadualinner",
}

export enum Calibrators {
  C3_48 = "3C48",
  C3_147 = "3C147",
  C3_196 = "3C196",
  C3_295 = "3C295",
  C3_380 = "3C380",
}

export enum ATeamSources {
  VirA = "VirA",
  CasA = "CasA",
  CygA = "CygA",
  TauA = "TauA",
}

export enum ChannelsPerSubband {
  C64 = "64",
  C128 = "128",
  C256 = "256",
}

export enum Pipeline {
  None = "none",
  Preprocessing = "preprocessing",
}

export const MAX_NO_CORE_STATIONS = 24;
export const MAX_NO_REMOTE_STATIONS = 14;
export const MAX_NO_INTERNATIONAL_STATIONS = 14;

export const defaultCalculatorInputs: CalculatorInputs = {
  observation_time_seconds: 28800,
  no_core_stations: MAX_NO_CORE_STATIONS,
  no_remote_stations: MAX_NO_REMOTE_STATIONS,
  no_international_stations: MAX_NO_INTERNATIONAL_STATIONS,
  no_channels_per_subband: ChannelsPerSubband.C64,
  no_subbands: 488,
  integration_time_seconds: 1,
  antenna_set: AntennaSet["HBA Dual Inner"],
  target: undefined,
  observation_date: "",
  calibrators: [],
  a_team_sources: [],
  pipeline: Pipeline.None,
  enable_dysco_compression: true,
  frequency_averaging_factor: 4,
  time_averaging_factor: 1,
};

export default CalculatorInputs;
