import { ProposalStatus } from "src/api/models/Proposal";

interface SkyDistribution {
  proposal_id: number;
  proposal_title: string;
  proposal_status: ProposalStatus;
  sky_distribution: { [key: number]: number };
}

export default SkyDistribution;
