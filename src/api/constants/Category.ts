export const Category = [
  { value: "commissioning", label: "Commissioning" },
  { value: "ddt", label: "Directors discretionary time" },
  { value: "test", label: "Test" },
  { value: "regular", label: "Regular" },
  { value: "user_shared_support", label: "User shared support" },
];
