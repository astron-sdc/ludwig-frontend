import Call from "src/api/models/Call";
import Cycle from "src/api/models/Cycle";
import Proposal, { ProposalAction, ProposalStatus } from "src/api/models/Proposal";
import Question from "src/api/models/Question/Question";

import ResolvedTarget from "src/api/models/ResolvedTarget";
import Target from "src/api/models/Target";
import {
  fetchAndReadJson,
  patchAndReadJson,
  patchFormDataAndReadJson,
  postAndReadJson,
  postFormDataAndReadJson,
  putAndReadJson,
  putDataRequest,
  putRequest,
} from "src/utils/Fetch";
import { propertyNameOf } from "src/utils/Typing";
import { BACKEND_BASENAME } from "src/utils/constants";
import ReviewComment from "src/api/models/ReviewComment";
import ReviewedProposal from "src/api/models/ReviewedProposal";
import Review, { PanelType, ReviewerType } from "src/api/models/Review";
import { isValidProposal } from "src/utils/Proposal";
import { ProposalConfiguration } from "src/api/models/ProposalConfiguration";
import ProposalPermissions from "src/api/models/ProposalPermissions";
import { SynchronizationResult } from "src/api/models/SynchronizationResult";
import CallCollaboration from "src/api/models/CallCollaboration";
import CallPermissions from "src/api/models/CallPermissions";
import CalculatorResult from "src/api/models/CalculatorOutputs";
import CalculatorInputs from "src/api/models/CalculatorInputs";
import SkyDistribution from "src/api/models/SkyDistribution";
import ProposalResources from "src/api/models/RequestedProposalResources";

export const API_BASE_URL: string = `${BACKEND_BASENAME}/api/`;

class Api {
  async getCalculatorInputs(proposalId: number): Promise<CalculatorInputs[]> {
    return fetchAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/calculator-inputs/`);
  }

  async getCalculatorResults(proposalId: number): Promise<CalculatorResult[]> {
    return fetchAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/calculator-results/`);
  }

  async calculateResults(proposalId: number, calculatorInputs: CalculatorInputs): Promise<CalculatorResult> {
    return postAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/calculator-results/calculate/`, calculatorInputs);
  }

  // === Cycles ==
  async getCycles(): Promise<Cycle[]> {
    return fetchAndReadJson(`${API_BASE_URL}v1/cycles/`);
  }

  async getCycle(cycleId: number): Promise<Cycle> {
    return fetchAndReadJson(`${API_BASE_URL}v1/cycles/${cycleId}/`);
  }

  async createCycle(cycle: Partial<Cycle>): Promise<Cycle> {
    return postAndReadJson(`${API_BASE_URL}v1/cycles/`, cycle);
  }

  async updateCycle(cycle: Partial<Cycle>): Promise<Cycle> {
    return patchAndReadJson(`${API_BASE_URL}v1/cycles/${cycle.id}/`, cycle);
  }

  // === Calls ===

  async getCalls(open?: boolean): Promise<Call[]> {
    const params = new URLSearchParams();
    if (open !== undefined) params.append("open", open.toString());
    return fetchAndReadJson(`${API_BASE_URL}v1/calls/?${params.toString()}`);
  }

  async getOpenCalls(): Promise<Call[]> {
    return this.getCalls(true);
  }

  async getCall(callId: number): Promise<Call> {
    return fetchAndReadJson(`${API_BASE_URL}v1/calls/${callId}/`);
  }

  async getCallMembers(callId: number): Promise<CallCollaboration> {
    return fetchAndReadJson(`${API_BASE_URL}v1/calls/${callId}/members/`);
  }

  async getCallPermissions(callId: number): Promise<CallPermissions> {
    return fetchAndReadJson(`${API_BASE_URL}v1/calls/${callId}/access_allowed/`);
  }

  async createCall(call: Partial<Call>): Promise<Call> {
    return postAndReadJson(`${API_BASE_URL}v1/calls/`, call);
  }

  async updateCall(call: Partial<Call>): Promise<Call> {
    return patchAndReadJson(`${API_BASE_URL}v1/calls/${call.id}/`, call);
  }

  async getSkyDistribution(callId: number): Promise<SkyDistribution[]> {
    return fetchAndReadJson(`${API_BASE_URL}v1/calls/${callId}/sky_distribution`);
  }

  // === Proposals ===
  async getProposals(): Promise<Proposal[]> {
    const data = (await fetchAndReadJson(`${API_BASE_URL}v1/proposals/`)) as Proposal[];
    return data.map(
      (p) =>
        ({
          ...p,
          created_at: new Date(p.created_at ? p.created_at : 0),
        }) as Proposal,
    );
  }

  async getProposalsForReviewer(): Promise<ReviewedProposal[]> {
    const data = (await fetchAndReadJson(`${API_BASE_URL}v1/proposals/as_reviewer`)) as ReviewedProposal[];
    return data.map(
      (p) =>
        ({
          ...p,
          created_at: new Date(p.created_at ? p.created_at : 0),
          submitted_at: new Date(p.submitted_at ? p.submitted_at : 0),
        }) as ReviewedProposal,
    );
  }
  async getRequestedResourcesForCall(callId: number): Promise<ProposalResources[]> {
    return fetchAndReadJson(`${API_BASE_URL}v1/calls/${callId}/requested_resources/`);
  }

  async getProposalsUnderReviewForCall(callId: number): Promise<ReviewedProposal[]> {
    return await this.getProposalsForCall(callId, ProposalStatus.underReview);
  }

  async getProposalsForCall(callId: number, statusFilter?: ProposalStatus | undefined): Promise<ReviewedProposal[]> {
    const params = new URLSearchParams();
    if (statusFilter) {
      params.set("status", statusFilter);
    }
    const queryParams = statusFilter ? `?${params}` : "";
    const data = (await fetchAndReadJson(
      `${API_BASE_URL}v1/calls/${callId}/proposals/${queryParams}`,
    )) as ReviewedProposal[];
    return data.map(
      (p) =>
        ({
          ...p,
          created_at: new Date(p.created_at ? p.created_at : 0),
          submitted_at: p.submitted_at ? new Date(p.submitted_at) : "",
        }) as ReviewedProposal,
    );
  }

  async getProposal(proposalId: number): Promise<Proposal> {
    const data = await fetchAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}`);
    // convert to a real date
    return {
      ...data,
      created_at: new Date(data.created_at),
    } as Proposal;
  }

  async getProposalPermissions(proposalId: number): Promise<ProposalPermissions> {
    return fetchAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/access_allowed/`);
  }

  async createProposal(proposal: Partial<Proposal>): Promise<Proposal> {
    // User Form URL Encoded to support file upload

    if (!isValidProposal(proposal)) throw new Error("missing required proposal data");

    const prop = proposal as Proposal; // can be asserted after validation

    const data = new FormData();
    data.append(propertyNameOf<Proposal>()("title"), prop.title);
    data.append(propertyNameOf<Proposal>()("abstract"), prop.abstract);
    data.append(propertyNameOf<Proposal>()("call_id"), prop.call_id.toString());

    if (prop.scientific_justification) {
      data.append(propertyNameOf<Proposal>()("scientific_justification"), prop.scientific_justification); // File
    }
    return postFormDataAndReadJson(`${API_BASE_URL}v1/proposals/`, data);
  }

  async updateProposalViaForm(proposalId: number, proposalFields: Partial<Proposal>): Promise<Proposal> {
    const data = new FormData();

    for (const [key, value] of Object.entries(proposalFields)) {
      data.append(key, value as string | File); // TODO: Date?
    }

    return patchFormDataAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/`, data);
  }

  async updateProposal(proposalId: number, proposalFields: Partial<Proposal>): Promise<Proposal> {
    return patchAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/`, proposalFields);
  }

  async proposalActions(proposalId: number, action: ProposalAction): Promise<Response> {
    const uri = `${API_BASE_URL}v1/proposals/${proposalId}/${action}/`;
    return putRequest(uri);
  }

  getScientificJustificationURL(proposalId: number): string {
    return `${API_BASE_URL}v1/proposals/${proposalId}/scientific_justification/`;
  }

  async synchronize(proposalId: number): Promise<SynchronizationResult> {
    const syncUrl = `${API_BASE_URL}v1/proposals/${proposalId}/sync/`;
    return await fetchAndReadJson(syncUrl);
  }

  async finishSync(proposalId: number): Promise<SynchronizationResult> {
    const syncUrl = `${API_BASE_URL}v1/proposals/${proposalId}/finish_sync/`;
    return await fetchAndReadJson(syncUrl);
  }

  // === Members, groups, roles ===

  async updateRole(proposalId: number, userId: string, groupId?: string) {
    return putAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/members/roles/`, {
      user_id: userId,
      roles: [groupId],
    });
  }

  async inviteMembers(proposaldId: number, email_addresses: string[], groups: string[]) {
    return putDataRequest(`${API_BASE_URL}v1/proposals/${proposaldId}/members/invites/`, {
      invites: email_addresses,
      groups: groups,
    });
  }

  // === Targets ===
  async getTargets(proposalId: number): Promise<Target[]> {
    return fetchAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/targets`);
  }

  async updateTargets(proposalId: number, targets: Target[]): Promise<void> {
    return postAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/targets/update_proposal_targets/`, targets);
  }

  async resolveTargetName(targetName: string): Promise<ResolvedTarget> {
    return fetchAndReadJson(`${API_BASE_URL}v1/coordinates/resolve_name/?name=${encodeURIComponent(targetName)}`);
  }

  // === Questionnaires ===
  async getQuestions(proposalId: number): Promise<Question[]> {
    const questionnaire = await fetchAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/questionnaire/current`);
    return questionnaire.questions;
  }

  async saveAnswers(proposalId: number, answers: { question_id: number; answer: string }[]): Promise<void> {
    return postAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/questionnaire/current/answers/`, answers);
  }

  // === Reviews ===
  async getReviewComments(proposalId: number): Promise<ReviewComment[]> {
    const reviewComments: ReviewComment[] = await fetchAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/comments`);
    return reviewComments;
  }

  async saveReviewComment(proposalId: number, comment: { text: string }): Promise<ReviewComment> {
    return await postAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/comments/`, { text: comment.text });
  }

  async getProposalReview(proposalId: number): Promise<Review> {
    return await fetchAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/reviews/current_user_review/`);
  }

  async saveProposalReviewRating(proposalId: number, rating: number): Promise<Review> {
    return await postAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/reviews/`, { ranking: rating });
  }

  async assignProposalReviewers(callId: number, reviews: Review[], reviewerType: ReviewerType): Promise<void> {
    return putAndReadJson(`${API_BASE_URL}v1/calls/${callId}/assign_reviewers/`, {
      reviews: reviews,
      reviewer_type: reviewerType,
    });
  }

  async preAssignCallReviewers(callId: number, reviewerIds: string[], panelTypes: PanelType[]): Promise<CallCollaboration> {
    const call: CallCollaboration = await putAndReadJson(`${API_BASE_URL}v1/calls/${callId}/pre_assign_reviewers/`, {
      reviewer_ids: reviewerIds,
      panel_types: panelTypes,
    });

    return call;
  }

  // === Proposal Configuration
  async createProposalConfiguration(
    proposalId: number,
    proposalConfiguration: ProposalConfiguration,
  ): Promise<ProposalConfiguration> {
    return await postAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/configuration/`, proposalConfiguration);
  }

  async updateProposalConfiguration(
    proposalId: number,
    proposalConfiguration: Partial<ProposalConfiguration>,
  ): Promise<ProposalConfiguration> {
    return patchAndReadJson(
      `${API_BASE_URL}v1/proposals/${proposalId}/configuration/${proposalConfiguration.id}/`,
      proposalConfiguration,
    );
  }

  async getProposalConfiguration(proposalId: number): Promise<ProposalConfiguration> {
    const result = await fetchAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/configuration/current`);
    return result;
  }
}

export default new Api();
