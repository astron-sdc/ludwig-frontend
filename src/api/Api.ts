import Call from "./models/Call";
import Cycle from "./models/Cycle";
import {
  fetchAndReadJson,
  postAndReadJson,
  postFormDataAndReadJson,
} from "../utils/Fetch.ts";
import Proposal from "src/api/models/Proposal.ts";
import { propertyNameOf } from "src/utils/Typing.ts";
import Target from "./models/Target.ts";
import ResolvedTarget from "./models/ResolvedTarget.ts";

const BASE_URL: string = "/proposal-backend/api/";

class Api {
  // === Cycles ===
  async getCycles(): Promise<Cycle[]> {
    return fetchAndReadJson(`${BASE_URL}v1/cycles/`);
  }

  async getCycle(cycleId: number): Promise<Cycle> {
    return fetchAndReadJson(`${BASE_URL}v1/cycles/${cycleId}/`);
  }

  async createCycle(cycle: Partial<Cycle>): Promise<Cycle> {
    return postAndReadJson(`${BASE_URL}v1/cycles/`, cycle);
  }

  // === Calls ===

  async getCalls(cycleId?: number): Promise<Call[]> {
    if (cycleId) {
      return fetchAndReadJson(`${BASE_URL}v1/cycles/${cycleId}/calls/`);
    }
    return fetchAndReadJson(`${BASE_URL}v1/calls/`);
  }

  async getCall(callId: number): Promise<Call> {
    return fetchAndReadJson(`${BASE_URL}v1/calls/${callId}/`);
  }

  async createCall(call: Partial<Call>): Promise<Call> {
    return postAndReadJson(`${BASE_URL}v1/calls/`, call);
  }

  // === Proposals ===
  async getProposals(): Promise<Proposal[]> {
    const data = (await fetchAndReadJson(
      `${BASE_URL}v1/proposals/`,
    )) as Proposal[];
    const proposals = data.map(
      (p) =>
        ({
          ...p,
          created_at: new Date(p.created_at ? p.created_at : 0),
        }) as Proposal,
    );
    return proposals;
  }

  async getProposal(proposalId: number): Promise<Proposal> {
    const data = await fetchAndReadJson(
      `${BASE_URL}v1/proposals/${proposalId}`,
    );
    // convert to a real date
    const proposal = {
      ...data,
      created_at: new Date(data.created_at),
    } as Proposal;
    return proposal;
  }

  async createProposal(proposal: Proposal): Promise<Proposal> {
    // User Form URL Encoded to support file upload
    const data = new FormData();
    data.append(propertyNameOf<Proposal>()("title"), proposal.title);
    data.append(propertyNameOf<Proposal>()("abstract"), proposal.abstract);
    data.append(
      propertyNameOf<Proposal>()("call_id"),
      proposal.call_id.toString(),
    );
    data.append(propertyNameOf<Proposal>()("targets"), ""); // TODO: add target lists
    if (proposal.scientific_justification) {
      data.append(
        propertyNameOf<Proposal>()("scientific_justification"),
        proposal.scientific_justification,
      ); // File
    }

    return postFormDataAndReadJson(`${BASE_URL}v1/proposals/`, data);
  }

  // === Targets ===
  async getTargets(proposalId: number): Promise<Target[]> {
    return fetchAndReadJson(`${BASE_URL}v1/proposals/${proposalId}/targets`);
  }

  async updateTargets(proposalId: number, targets: Target[]): Promise<void> {
    return postAndReadJson(
      `${BASE_URL}v1/proposals/${proposalId}/targets/update_proposal_targets/`,
      targets,
    );
  }

  async resolveTargetName(targetName: string): Promise<ResolvedTarget> {
    return fetchAndReadJson(
      `${BASE_URL}v1/coordinates/resolve_name/?name=${encodeURIComponent(targetName)}`,
    );
  }
}

export default new Api();
