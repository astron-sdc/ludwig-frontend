import Specification from "src/api/models/Specification";
import { deleteAndReadJson, fetchAndReadJson, postAndReadJson, ServerResult, patchAndReadJson } from "src/utils/Fetch.ts";
import { API_BASE_URL } from "src/api/Api";
import { SynchronizationResult } from "./models/SynchronizationResult";
class ApiSpecification {
  async getSpecifications(proposalId: number): Promise<Specification[]> {
    return fetchAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/specifications`);
  }

  async getSpecification(proposalId: number, specificationId: number): Promise<Specification> {
    return fetchAndReadJson(`${API_BASE_URL}v1/proposals/${proposalId}/specifications/${specificationId}`);
  }

  async createSpecification(specification: Specification): Promise<Specification> {
    return postAndReadJson(`${API_BASE_URL}v1/specifications/`, specification);
  }

  async updateSpecification(specification: Specification): Promise<Specification> {
    return patchAndReadJson(`${API_BASE_URL}v1/specifications/${specification.id}/`, specification);
  }

  async deleteSpecification(specificationId?: number, proposalId?: number): Promise<ServerResult> {
    const result = await deleteAndReadJson(
      `${API_BASE_URL}v1/specifications/delete-by-ids/${proposalId}/${specificationId}/`,
    );
    return result;
  }

  async syncSpecification(specificationId: number, proposalId: number): Promise<SynchronizationResult> {
    const result = await fetchAndReadJson(`${API_BASE_URL}v1/specifications/sync/${proposalId}/${specificationId}/`);
    return result;
  }

  async syncSubgroup(specificationId: number, proposalId: number, subgroup: string): Promise<SynchronizationResult> {
    const result = await fetchAndReadJson(
      `${API_BASE_URL}v1/proposals/${proposalId}/specifications/subsync/${specificationId}/${subgroup}`,
    );
    return result;
  }
}

export default new ApiSpecification();
