import { fetchAndReadJson, postAndReadJson } from "src/utils/Fetch.ts";
import {
  SchedulingUnitObservingStrategy,
  CommonSchemaTemplate,
  TaskDraftList,
  SchedulingConstraintsTemplate,
  TaskTemplate,
  TemplateDefinition,
  Cycle,
} from "@astron-sd/telescope-specification-models";

import { IsObservableResult } from "./models/DefinitionObservableResult";
const BASE_URL: string = "/proposal-backend/api/v1/tmss";

class ApiTMSS {
  CLEANPROJECTID = "-1";
  TMSS_FALLBACK_URL = "http://localhost:8008";
  private static cachedTmssUrl: string | null = null;

  async getStrategies(): Promise<SchedulingUnitObservingStrategy[]> {
    const strategiesholder = await fetchAndReadJson(`${BASE_URL}/strategies/`);
    const strategies: SchedulingUnitObservingStrategy[] = strategiesholder.results;
    return strategies;
  }

  async getCycles(): Promise<Cycle[]> {
    return await this.getTemplate<Cycle>("cycles");
  }

  async getUtcNow(): Promise<string> {
    try {
      const utcholder = await fetchAndReadJson(`${BASE_URL}/utc_now/`);
      return utcholder[0];
    } catch (_ex) {
      return "";
    }
  }

  async getTMssUrl(): Promise<string> {
    if (ApiTMSS.cachedTmssUrl) return ApiTMSS.cachedTmssUrl;
    try {
      const tmssurl = await fetchAndReadJson(`${BASE_URL}/tmssurl/`);
      ApiTMSS.cachedTmssUrl = tmssurl;
      return tmssurl;
    } catch (_ex) {
      return this.TMSS_FALLBACK_URL;
    }
  }
  async getCommonSchema(): Promise<CommonSchemaTemplate> {
    const templates = await this.getTemplate<CommonSchemaTemplate>("common_template/");
    return templates[0];
  }

  async getTemplate<T>(endpoint: string): Promise<T[]> {
    const holder = await fetchAndReadJson(`${BASE_URL}/${endpoint}`);
    const result: T[] = holder.results;
    return result;
  }

  async getTaskFilterDefinition(): Promise<TaskDraftList> {
    const TaskdraftOptions = await fetchAndReadJson(`${BASE_URL}/task_draft/`);
    return TaskdraftOptions;
  }

  async getSchedulingConstraintsTemplate(): Promise<SchedulingConstraintsTemplate[]> {
    return await this.getTemplate<SchedulingConstraintsTemplate>("scheduling_constraints_template/");
  }

  async getTaskTemplate(): Promise<TaskTemplate[]> {
    return await this.getTemplate<TaskTemplate>("task_template/?limit=100");
  }

  async isObservable(definition: TemplateDefinition, strategyTemplateId: string): Promise<IsObservableResult> {
    return postAndReadJson(`${BASE_URL}/scheduling_is_observable/?strategy_template_id=${strategyTemplateId}`, definition);
  }
}
export default new ApiTMSS();
