function propertyNameOf<T>() {
  return <K extends keyof T>(name: K) => name;
}

export { propertyNameOf };
