/**
 * fetch and convert to JSON
 *  raises for response codes not `ok`
 * @param uri uri to fetch
 */

import CustomError from "src/api/models/CustomError";

export interface ServerResult {
  isSuccessful: boolean;
  message?: string;
}

async function fetchAndReadJson(uri: string) {
  const response = await fetch(uri);
  if (!response.ok) throw Error(response.status.toString());
  return await response.json();
}

async function postAndReadJson(uri: string, data: object) {
  return await modifyAndReadJson(uri, data);
}

async function putAndReadJson(uri: string, data: object) {
  return await modifyAndReadJson(uri, data, "PUT");
}

async function putRequest(uri: string) {
  return await fetch(uri, {
    method: "PUT",
    headers: [["Content-Type", "application/json"]],
  });
}

async function putDataRequest(uri: string, data: object) {
  const res = await fetch(uri, {
    method: "PUT",
    headers: [["Content-Type", "application/json"]],
    body: JSON.stringify(data),
  });
  if (!res.ok) throw Error(res.status.toString());
  return res;
}

async function patchAndReadJson(uri: string, data: object) {
  return await modifyAndReadJson(uri, data, "PATCH");
}

async function deleteAndReadJson(uri: string): Promise<ServerResult> {
  const options: RequestInit = {
    method: "DELETE",
    headers: [["Content-Type", "application/json"]],
  };
  try {
    const response = await fetch(uri, options);
    if (!response.ok) return { isSuccessful: false, message: response.statusText };
    if (response.status === 204) return { isSuccessful: true };
  } catch (error) {
    let message = "";
    if (error instanceof Error) message = error.message;
    return { isSuccessful: false, message: "Error during delete " + message };
  }
  return { isSuccessful: false, message: "not deleted" };
}

async function modifyAndReadJson(uri: string, data: object, method: string = "POST") {
  const options: RequestInit = {
    method: method,
    body: JSON.stringify(data),
    headers: [["Content-Type", "application/json"]],
  };

  const response = await fetch(uri, options);
  if (!response.ok) {
    const bodyResponse = await response.json();

    if ("error" in bodyResponse) {
      throw Error(bodyResponse["error"] ?? response.statusText);
    }

    if (response.status === 400) {
      throw new CustomError(response.statusText, bodyResponse);
    }
    throw Error(bodyResponse["detail"] ?? response.statusText);
  }
  if (response.status === 204) return null;
  return await response.json();
}

async function postFormDataAndReadJson(uri: string, data: FormData) {
  return await modifyFormAndReadJson(uri, data);
}

async function patchFormDataAndReadJson(uri: string, data: FormData) {
  return await modifyFormAndReadJson(uri, data, "PATCH");
}

async function modifyFormAndReadJson(uri: string, data: FormData, method: string = "POST") {
  const options: RequestInit = {
    method: method,
    body: data,
    // defaults to headers: [["Content-Type", "multipart/form-data"]],
  };
  const response = await fetch(uri, options);
  if (!response.ok) throw Error(response.statusText);
  if (response.status === 204) return null;
  return await response.json();
}

export {
  fetchAndReadJson,
  patchAndReadJson,
  patchFormDataAndReadJson,
  postAndReadJson,
  putAndReadJson,
  postFormDataAndReadJson,
  putRequest,
  putDataRequest,
  deleteAndReadJson,
};
