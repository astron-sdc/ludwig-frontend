/**
 * fetch and convert to JSON
 *  raises for response codes not `ok`
 * @param uri uri to fetch
 */
async function fetchAndReadJson(uri: string) {
  const response = await fetch(uri);
  if (!response.ok) throw Error(response.statusText);
  return await response.json();
}

async function postAndReadJson(uri: string, data: object) {
  const options: RequestInit = {
    method: "POST",
    body: JSON.stringify(data),
    headers: [["Content-Type", "application/json"]],
  };
  const response = await fetch(uri, options);

  if (!response.ok) throw Error(response.statusText);
  if (response.status === 204) return null;
  return await response.json();
}

async function postFormDataAndReadJson(uri: string, data: FormData) {
  const options: RequestInit = {
    method: "POST",
    body: data,
    //headers: [["Content-Type", "multipart/form-data"]],
  };
  const response = await fetch(uri, options);

  if (!response.ok) throw Error(response.statusText);
  return await response.json();
}

export { fetchAndReadJson, postAndReadJson, postFormDataAndReadJson };
