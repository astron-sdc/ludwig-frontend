const getWordCount = (text: string) =>
  text
    .trim()
    .split(/\s+/)
    .filter((c) => !!c).length;

const validateNumber = (value: number, min: number = 1, max: number = 5) =>
  Number.isFinite(value) && value >= min && value <= max;

const validateEmail = (value: string) => {
  const regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  return regex.test(value);
};

export { getWordCount, validateNumber, validateEmail };
