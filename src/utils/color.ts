import { ColorType } from "@astron-sdc/design-system";
import { ProposalStatus } from "src/api/models/Proposal";

const statusColorMap: Record<ProposalStatus, ColorType> = {
  [ProposalStatus.draft]: "neutral",
  [ProposalStatus.submitted]: "primary",
  [ProposalStatus.retracted]: "warning",
  [ProposalStatus.underReview]: "secondary",
  [ProposalStatus.rejected]: "negative",
  [ProposalStatus.accepted]: "positive",
};

export const statusToColor = (proposalStatus: ProposalStatus | undefined): ColorType => {
  return proposalStatus ? statusColorMap[proposalStatus] || "warning" : "warning";
};
