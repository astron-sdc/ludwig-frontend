const BASENAME = "/proposal";
const BACKEND_BASENAME = "/proposal-backend";

export { BACKEND_BASENAME, BASENAME };
