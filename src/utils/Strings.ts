export const capitalizeFirstLetter = (value: string): string => {
  return value
    .split(" ")
    .map((w) => w[0].toUpperCase() + w.substring(1))
    .join(" ");
};
