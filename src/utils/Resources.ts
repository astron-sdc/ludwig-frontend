import { bytesToGibiBytes, formatNumber, secondsToHours } from "./Numbers";

export const formatBytesAsGibibytes = (value: number, fractionDigits: number = 2): string => {
  return formatNumber(Number(bytesToGibiBytes(value)), { fractionDigits });
};

export const formatSecondsAsHours = (value: number, fractionDigits: number = 3): string => {
  return formatNumber(Number(secondsToHours(value)), {
    fractionDigits,
  });
};
