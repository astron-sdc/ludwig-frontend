const locale = navigator.language ?? "en-GB";

export const formatDateTime = (datetime: string | Date | undefined): string => {
  if (!datetime) return "";
  return new Date(datetime).toLocaleString(locale, {
    timeZone: "UTC",
    hour12: false,
    timeZoneName: "short",
  });
};

export const formatDateTimeForApi = (datetime: Date | string): string => {
  if (datetime instanceof Date) return datetime.toISOString();
  return datetime;
};

export const formatDateYYYYMMDD = (datetime: Date | string): string => {
  // YYYY-MM-DD
  return formatDateTimeForApi(datetime).split("T")[0];
};

// Returns true if the passed date is today, or in the future. Time is ignored.
export const dateIsTodayOrInFuture = (date: Date): boolean => {
  const today = new Date();
  const utcMidnightToday = new Date(Date.UTC(today.getUTCFullYear(), today.getUTCMonth(), today.getUTCDate()));
  const utcMidnightDateUnderTest = new Date(Date.UTC(date.getUTCFullYear(), date.getUTCMonth(), date.getUTCDate()));
  return utcMidnightDateUnderTest >= utcMidnightToday;
};
