import ReviewedProposal from "src/api/models/ReviewedProposal";
import Proposal from "src/api/models/Proposal.ts";

export const piFromReviewedProposal = (proposal: ReviewedProposal) => {
  const piGroup = proposal.groups.find((g) => g.short_name === "pi");
  return piGroup?.members.map((m) => m.name).join(", ");
};

/**
 * Checks required fields for creating a proposal
 * @param proposal: Partial<Proposal>
 */
export const isValidProposal = (proposal: Partial<Proposal>) => {
  return !!(proposal.title && proposal.abstract && proposal.call_id);
};

export const calculateQuartile = (reviewRating: number, proposals: ReviewedProposal[]) => {
  const sortedProposalsWithRatings = proposals
    .filter((p) => p.review_rating !== null)
    .sort((a, b) => a.review_rating! - b.review_rating!);
  const positionOfProposal = sortedProposalsWithRatings.findIndex((p) => p.review_rating === reviewRating);
  return Math.floor((positionOfProposal / sortedProposalsWithRatings.length) * 4) + 1;
};
