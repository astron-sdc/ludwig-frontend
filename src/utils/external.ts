import Proposal from "src/api/models/Proposal";

export const openProjectInTMSS = (proposal: Proposal | undefined) => {
  if (!proposal) return;
  const TMSSAppUrl = `${getTMSSURL()}/project/view/${proposal.tmss_project_id}`;
  window.open(`${TMSSAppUrl}`, "_blank");
};

export const getTMSSURL = () => {
  const hostname = window.location.hostname;
  if (hostname.includes("sdc-dev")) {
    // Case 2: We are on sdc-dev, use the temporary URL
    return "https://tmss.tser.org";
  } else if (hostname.includes("sdc.astron.nl")) {
    // Case 3: We are on sdc.astron.nl, use the production URL
    return "https://tmss.lofar.eu";
  }
  return `${process.env.TMSS_APP_URL}`; // Adjust the port and URL as needed for your local docker container
};
