import { ProposalStatus } from "src/api/models/Proposal.ts";

export const badgeColor = (status: ProposalStatus) => {
  switch (status) {
    case "draft":
      return "neutral";
    case "accepted":
      return "positive";
    case "under_review":
      return "primary";
    case "rejected":
      return "negative";
    case "submitted":
      return "primary";
    case "retracted":
      return "warning";
    default:
      return undefined;
  }
};
