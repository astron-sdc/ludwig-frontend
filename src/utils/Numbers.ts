const locale = navigator.language ?? "en-GB";

type StringOrNumberOrUndefined = string | number | undefined;

export const formatNumber = (value: number, options = { fractionDigits: 0 }): string => {
  return Intl.NumberFormat(locale, {
    minimumFractionDigits: 0,
    maximumFractionDigits: options.fractionDigits,
  }).format(value);
};

export const teraBytesToBytes = (value: StringOrNumberOrUndefined): number => {
  const valueTB = Number(value);
  if (isNaN(valueTB)) return 0;
  //                     TB    GB     MB     KB    Bytes
  const valueBytes = valueTB * 1000 * 1000 * 1000 * 1000;
  return valueBytes;
};

export const bytesToTeraBytes = (value: StringOrNumberOrUndefined): string => {
  const valueBytes = Number(value);
  if (isNaN(valueBytes)) return "0";
  //                     B       KB     MB    GB    TB
  const valueTB = valueBytes / 1000 / 1000 / 1000 / 1000;
  return valueTB.toString();
};

export const bytesToGigaBytes = (value: StringOrNumberOrUndefined): string => {
  const valueBytes = Number(value);
  if (isNaN(valueBytes)) return "0";
  //                     B       KB     MB    GB
  const valueGB = valueBytes / 1000 / 1000 / 1000;
  return valueGB.toString();
};

export const bytesToGibiBytes = (value: StringOrNumberOrUndefined): string => {
  const valueBytes = Number(value);
  if (isNaN(valueBytes)) return "0";
  //                     B       KiB     MiB    GiB
  const valueGiB = valueBytes / 1024 / 1024 / 1024;
  return valueGiB.toString();
};

export const gigaBytesToBytes = (value: StringOrNumberOrUndefined): number => {
  const valueGB = Number(value);
  if (isNaN(valueGB)) return 0;
  //                     GB     MB     KB    Bytes
  const valueBytes = valueGB * 1000 * 1000 * 1000;
  return valueBytes;
};

export const gibiBytesToBytes = (value: StringOrNumberOrUndefined): number => {
  const valueGiB = Number(value);
  if (isNaN(valueGiB)) return 0;
  //                     GiB     MiB     KiB    Bytes
  const valueBytes = valueGiB * 1024 * 1024 * 1024;
  return valueBytes;
};

export const hoursToSeconds = (value: StringOrNumberOrUndefined): number => {
  const hourValue = Number(value);
  if (isNaN(hourValue)) return 0;
  //               Hour   M     S;
  const seconds = Math.round(hourValue * 60 * 60);
  return seconds;
};

export const secondsToHours = (value: StringOrNumberOrUndefined): string => {
  const secondValue = Number(value);
  if (isNaN(secondValue)) return "0";
  //               Hour   M     S;
  const hours = secondValue / 60 / 60;
  return hours.toString();
};
