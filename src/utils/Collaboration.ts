import CollaborationGroup from "src/api/models/CollaborationGroup";
import CollaborationMember from "src/api/models/CollaborationMember";

/**
 * Find the roles for a given member for a given collaboration
 * @param member Collaboration member
 * @param groups CollaborationGroups
 * @return [CollaborationGroup] List of Roles (groups in SRAM)
 */
export const getRolesForMemberInCollaboration = (member: CollaborationMember, groups: CollaborationGroup[]) => {
  const roles = new Set([
    ...groups.filter((group) => group.members.find((gm) => gm.eduperson_unique_id === member.eduperson_unique_id)),
  ]);

  return [...roles.values()];
};
