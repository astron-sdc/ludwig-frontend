import { Button, IconName, ProfileDropdown, Toggle } from "@astron-sdc/design-system";
import { useContext } from "react";
import { AuthContext } from "src/auth/AuthContext";
import { ThemeContext } from "./Theme/Context.ts";

const ProfileMenu = () => {
  const { darkmode, setDarkmode } = useContext(ThemeContext);
  const { authenticated, user, login, logout, restartSession } = useContext(AuthContext);

  const menu = [
    {
      sectionKey: "Main",
      sectionContent: [
        {
          key: "darkmode-toggle",
          icon: "Eye" as IconName,
          content: (
            <Toggle
              isSelected={darkmode}
              setIsSelected={setDarkmode}
              label="Dark mode"
              labelPlacement="left"
              wrapperClass="justify-between w-full"
            />
          ),
        },
      ],
    },
    {
      sectionKey: "Refresh Token",
      sectionContent: [
        {
          key: "refresh",
          icon: "InfoCircle" as IconName,
          content: <button onClick={restartSession}>Refresh Token</button>,
          customClass: "text-warning",
        },
      ],
    },
    {
      sectionKey: "Logout",
      sectionContent: [
        {
          key: "logout",
          icon: "Cross" as IconName,
          content: <button onClick={logout}>Logout</button>,
          customClass: "text-danger",
        },
      ],
    },
  ];

  if (authenticated && user) {
    return <ProfileDropdown label={user.name} color="primary" menu={menu} />;
  }

  return (
    <div>
      <Button onClick={login} label="Login" />
    </div>
  );
};
export default ProfileMenu;
