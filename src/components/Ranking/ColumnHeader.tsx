import { Typography } from "@astron-sdc/design-system";

const ColumnHeader = (columnHeaderText: string, title: string = "") => (
  <div className="grid grid-flow-col grid-rows-4 items-stretch text-left">
    <div className="row-span-2">{title !== "" && <Typography variant="h5" text={title} />}</div>
    <div className="row-span-2">
      <Typography text={columnHeaderText} variant="paragraph" />
    </div>
  </div>
);

const ColumnHeaderPadding = "1rem";

const ColumnHeaderBorder = {
  borderRight: "1px solid grey",
  paddingRight: ColumnHeaderPadding,
};

export { ColumnHeader, ColumnHeaderPadding, ColumnHeaderBorder };
