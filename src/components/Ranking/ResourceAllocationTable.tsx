import { Button, Spinner, Table } from "@astron-sdc/design-system";
import Api from "src/api/Api";
import Call from "src/api/models/Call";
import { calculateQuartile, piFromReviewedProposal } from "src/utils/Proposal";
import useSWRImmutable from "swr/immutable";
import { Dispatch, SetStateAction, useContext } from "react";
import ToastContext from "src/components/Toast/Context";
import Panel from "src/components/Panel/Panel";
import ReviewedProposal from "src/api/models/ReviewedProposal";
import { mapAssignedResourceColumns, ASSIGNED_RESOURCES_COLUMNS } from "src/components/Ranking/AssignedResourcesTable";
import { mapRequestedResourceColumns, REQUESTED_RESOURCES_COLUMNS } from "src/components/Ranking/RequestedResourcesTable";
import { Link } from "react-router-dom";
import CallPermissions from "src/api/models/CallPermissions";
import RESOURCE_ALLOCATION_TABLE_COLUMNS from "./ResourceAllocationTableColumns";
import { ProposalSelectionCheckbox } from "./ProposalSelectionCheckbox";

interface ResourceAllocationTableProps {
  call: Call;
  permissions: CallPermissions;
  selectedProposalsIds: number[];
  proposals: ReviewedProposal[];
  selectedProposalsChanged: Dispatch<SetStateAction<number[]>>;
}

const proposalQuartile = (proposal: ReviewedProposal, proposals: ReviewedProposal[]) =>
  proposal.review_rating ? `Q${calculateQuartile(proposal.review_rating, proposals)}` : "";

const ResourceAllocationTable = ({
  call,
  permissions,
  selectedProposalsIds,
  proposals,
  selectedProposalsChanged,
}: ResourceAllocationTableProps) => {
  const { data: requestedResources, isLoading: requestedResourcesLoading } = useSWRImmutable(
    `${call.id}/requested_resources`,
    () => Api.getRequestedResourcesForCall(call.id),
  );

  const { setErrorToastMessage } = useContext(ToastContext);

  const rows = proposals
    ? proposals.map((proposal) => {
        let mapped = {
          checkbox: (
            <ProposalSelectionCheckbox
              proposal={proposal}
              selectedProposalsIds={selectedProposalsIds}
              selectedProposalsChanged={selectedProposalsChanged}
            />
          ),
          rank: proposal.rank,
          code: proposal.project_code,
          title: (
            <Link className="underline" to={`/proposals/${proposal.id}/`}>
              {proposal.title}
            </Link>
          ),
          report: (
            <Button
              icon="Eye"
              color="neutral"
              label="Report"
              isDisabled={!permissions.export_call}
              onClick={() => setErrorToastMessage("Not yet implemented")}
            />
          ),
          pi: piFromReviewedProposal(proposal),
          quartile: proposalQuartile(proposal, proposals),
          avgRating: proposal.review_rating,
        };

        const correspondigResource = requestedResources
          ? requestedResources.find((res) => res.proposal_id === proposal.id)
          : undefined;

        if (correspondigResource)
          mapped = {
            ...mapped,
            ...mapRequestedResourceColumns(correspondigResource),
            ...mapAssignedResourceColumns(correspondigResource),
          };

        return mapped;
      })
    : [];

  return (
    <Panel className="px-0 py-0">
      {requestedResourcesLoading ? (
        <Spinner />
      ) : (
        <Table
          editMode="cell"
          headerContent={
            <Button
              icon="Download"
              label="Export"
              isDisabled={!permissions.export_call}
              color="secondary"
              onClick={() => setErrorToastMessage("Not yet implemented")}
            />
          }
          columns={[...RESOURCE_ALLOCATION_TABLE_COLUMNS, ...REQUESTED_RESOURCES_COLUMNS, ...ASSIGNED_RESOURCES_COLUMNS]}
          rows={rows}
        />
      )}
    </Panel>
  );
};

export default ResourceAllocationTable;
