import TextEditor, { optionsType } from "src/components/Targets/Editors/TextEditor";
import ProposalResources from "src/api/models/RequestedProposalResources";
import { formatSecondsAsHours, formatBytesAsGibibytes } from "src/utils/Resources";
import CellTemplate from "src/components/Targets/CellTemplate";
import { ColumnHeader, ColumnHeaderPadding } from "src/components/Ranking/ColumnHeader";

interface AssignedResources {
  assignedObservationTime: string;
  assignedStorage: string;
  assignedComputeTime: string;
  assignedSupportTime: string;
}

const columns = [
  {
    field: "assignedObservationTime",
    header: ColumnHeader("Observation time (h)", "Assigned"),
    bodyTemplate: (rowData: unknown) =>
      CellTemplate((rowData as AssignedResources).assignedObservationTime, false, () => true, false),
    style: {
      paddingLeft: ColumnHeaderPadding,
      width: "9%",
    },
  },
  {
    field: "assignedStorage",
    header: ColumnHeader("Storage (GiB)"),
    bodyTemplate: (rowData: unknown) =>
      CellTemplate((rowData as AssignedResources).assignedStorage, false, () => true, false),
  },
  {
    field: "assignedComputeTime",
    header: ColumnHeader("Compute (h)"),
    bodyTemplate: (rowData: unknown) =>
      CellTemplate((rowData as AssignedResources).assignedComputeTime, false, () => true, false),
  },
  {
    field: "assignedSupportTime",
    header: ColumnHeader("Support (h)"),
    bodyTemplate: (rowData: unknown) =>
      CellTemplate((rowData as AssignedResources).assignedSupportTime, false, () => true, false),
  },
];

export const ASSIGNED_RESOURCES_COLUMNS = columns.map((col) => ({
  ...col,
  editor: (options: unknown) => TextEditor(options as optionsType),
  style: {
    ...col.style,
    width: col.style?.width ?? "7%",
  },
}));

export const mapAssignedResourceColumns = (requestedResource: ProposalResources): AssignedResources => {
  return {
    assignedObservationTime: formatSecondsAsHours(requestedResource.observation_time_seconds),
    assignedStorage: formatBytesAsGibibytes(requestedResource.data_size_bytes),
    assignedComputeTime:
      requestedResource.pipeline_processing_time_seconds === 0
        ? ""
        : formatSecondsAsHours(requestedResource.pipeline_processing_time_seconds),
    assignedSupportTime: "",
  };
};
