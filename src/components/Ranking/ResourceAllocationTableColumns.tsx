import { ColumnHeader, ColumnHeaderBorder } from "./ColumnHeader";

const RESOURCE_ALLOCATION_TABLE_COLUMNS = [
  {
    field: "checkbox",
  },
  {
    field: "rank",
    header: ColumnHeader("Rank"),
  },
  {
    field: "code",
    header: ColumnHeader("Code"),
  },
  {
    field: "title",
    header: ColumnHeader("Title"),
  },
  {
    field: "report",
    header: ColumnHeader(""),
  },
  {
    field: "pi",
    header: ColumnHeader("PI"),
  },
  {
    field: "quartile",
    header: ColumnHeader("Quartile"),
    style: { width: "5%" },
  },
  {
    field: "avgRating",
    header: ColumnHeader("Avg. rating"),
    style: { ...ColumnHeaderBorder, width: "5%" },
  },
];

export default RESOURCE_ALLOCATION_TABLE_COLUMNS;
