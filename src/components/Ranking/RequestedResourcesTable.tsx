import ProposalResources from "src/api/models/RequestedProposalResources";
import { formatSecondsAsHours, formatBytesAsGibibytes } from "src/utils/Resources";
import { ColumnHeader, ColumnHeaderBorder, ColumnHeaderPadding } from "./ColumnHeader";

export const REQUESTED_RESOURCES_COLUMNS = [
  {
    field: "requestedObservationTime",
    header: ColumnHeader("Observation time (h)", "Requested"),
    style: {
      paddingLeft: ColumnHeaderPadding,
    },
  },
  {
    field: "requestedStorage",
    header: ColumnHeader("Storage (GiB)"),
  },
  {
    field: "requestedCompute",
    header: ColumnHeader("Compute (h)"),
    style: ColumnHeaderBorder,
  },
];

export const mapRequestedResourceColumns = (requestedResource: ProposalResources) => {
  return {
    requestedObservationTime: formatSecondsAsHours(requestedResource.observation_time_seconds),
    requestedStorage: formatBytesAsGibibytes(requestedResource.data_size_bytes),
    requestedCompute:
      requestedResource.pipeline_processing_time_seconds !== 0
        ? formatSecondsAsHours(requestedResource.pipeline_processing_time_seconds)
        : "",
  };
};
