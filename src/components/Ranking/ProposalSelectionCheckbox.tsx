import { Checkbox } from "@astron-sdc/design-system";
import { Dispatch, SetStateAction } from "react";
import ReviewedProposal from "src/api/models/ReviewedProposal";

type ProposalSelectionCheckboxProps = {
  proposal: ReviewedProposal;
  selectedProposalsIds: number[];
  selectedProposalsChanged: Dispatch<SetStateAction<number[]>>;
};

export const ProposalSelectionCheckbox = ({
  proposal,
  selectedProposalsIds,
  selectedProposalsChanged,
}: ProposalSelectionCheckboxProps) => {
  return (
    <Checkbox
      isSelected={selectedProposalsIds.includes(proposal.id)}
      setIsSelected={(value) =>
        selectedProposalsChanged(
          value ? [...selectedProposalsIds, proposal.id] : selectedProposalsIds.filter((p) => p !== proposal.id),
        )
      }
    />
  );
};
