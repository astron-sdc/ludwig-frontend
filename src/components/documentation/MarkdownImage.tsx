import { ImgHTMLAttributes } from "react";

const MarkdownImage = (props: ImgHTMLAttributes<HTMLImageElement>, docPathFolder: string) => {
  const newSrc = `${docPathFolder}/${props.src}`;
  return <img {...props} alt={props.alt || ""} src={newSrc} className="border shadow" />;
};

export default MarkdownImage;
