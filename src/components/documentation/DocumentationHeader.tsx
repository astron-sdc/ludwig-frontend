import { HTMLAttributes } from "react";
import { Typography } from "@astron-sdc/design-system";

const DocumentationHeader = (htmlProps: HTMLAttributes<HTMLElement>, variant: "h1" | "h2" | "h3" | "h4" | "h5") => {
  return <Typography text={htmlProps.children?.toString()} variant={variant} />;
};

export default DocumentationHeader;
