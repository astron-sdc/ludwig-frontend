import { TextInput } from "@astron-sdc/design-system";
import { SetStateAction } from "react";

const TextEditor = (options: {
  value: string;
  editorCallback: (action: SetStateAction<string>) => void;
}) => {
  return (
    <TextInput
      value={options.value}
      onValueChange={(e) => options.editorCallback && options.editorCallback(e)}
    />
  );
};

export default TextEditor;
