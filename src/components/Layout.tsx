import { ApplicationLayout, IconName, NotificationBar, Sidebar, SidebarItem, SidebarTop } from "@astron-sdc/design-system";
import { ReactNode, useContext } from "react";
import { AuthContext } from "src/auth/AuthContext";
import MenuBar, { MenuProps } from "src/components/Menu.tsx";
import { getTMSSURL } from "src/utils/external";

const SupportAppUrl = `${process.env.SUPPORT_APP_URL}`;

interface LayoutProps extends MenuProps {
  children?: ReactNode;
  isWithFooter?: boolean;
}

const sideBarTop: SidebarTop = {
  icon: "TULP" as IconName,
  text: "TULP",
  textLong: "TULP",
  linkTo: "/",
};

const Layout = ({ title, breadcrumbs, children, isWithFooter = false }: LayoutProps) => {
  const TMSSAppUrl = getTMSSURL();

  const { authenticated, permissions } = useContext(AuthContext);

  const items: SidebarItem[] = [
    ...(authenticated
      ? [
          ...(permissions?.is_admin
            ? [
                {
                  icon: "Datepicker",
                  text: "Cycles",
                  linkTo: "/cycles/",
                },
              ]
            : []),
          {
            icon: "Archive",
            text: "Call for proposals",
            linkTo: "/calls/",
          },
          {
            icon: "File",
            text: "Proposals",
            linkTo: "/overview/",
          },
          {
            icon: "Review",
            text: "Reviews",
            linkTo: "/reviews/",
          },
          ...(permissions?.view_call_ranking
            ? [
                {
                  icon: "Rank",
                  text: "Ranking/Allocation",
                  linkTo: "/ranking/",
                },
              ]
            : []),
        ]
      : []),
    {
      icon: "QuestionMark",
      text: "Documentation",
      linkTo: "/documentation/",
      showDivider: true,
    },
    ...(authenticated
      ? [
          {
            icon: "TMSS",
            linkTo: TMSSAppUrl,
            text: "TMSS",
            openInNewWindow: true,
          },
        ]
      : []),
    {
      icon: "InfoCircle",
      linkTo: SupportAppUrl,
      text: "SDC Support",
      openInNewWindow: true,
    },
  ];

  let className = `py-6 px-4 md:pl-2 md:pr-8 lg:pl-10 lg:pr:16 xl:pl-20 xl:pr-32 flex flex-col  `;
  if (isWithFooter) {
    className += " !h-dvh justify-between";
  }

  return (
    <ApplicationLayout
      side={<Sidebar items={items} top={sideBarTop} />}
      top={
        <>
          {permissions?.is_admin && (
            <div className="h-10">
              <NotificationBar
                message="You are accessing the tool as administrator."
                subMessage=""
                color="warning"
                iconName="AttentionCircle"
              />
            </div>
          )}
          <MenuBar title={title} breadcrumbs={breadcrumbs} />
        </>
      }
      content={<div className={className}>{children}</div>}
    />
  );
};

export default Layout;
