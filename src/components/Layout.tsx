import { ReactNode, useContext } from "react";
import FlexRow from "./FlexRow.tsx";
import { Breadcrumb, Toggle, Typography } from "@astron-sdc/design-system";
import { ThemeContext } from "src/components/Theme/Context.ts";

type LayoutProps = {
  title: string;
  breadcrumbs?: { name: string; href: string }[];
  children: ReactNode;
};

const Layout = ({ title, breadcrumbs, children }: LayoutProps) => {
  const { darkmode, setDarkmode } = useContext(ThemeContext);

  return (
    <div className={darkmode ? "dark" : ""}>
      <div className="min-h-dvh p-8 dark:bg-darkModeBlue bg-baseWhite dark:text-baseWhite text-mediumGrey">
        {/* TODO sidebar */}
        <FlexRow justify="end">
          <Toggle
            isSelected={darkmode}
            label="Dark mode"
            setIsSelected={setDarkmode}
          />
          {/* TODO user menu */}
        </FlexRow>
        <Typography text={title} variant="title" />
        {breadcrumbs && <Breadcrumb items={breadcrumbs} />}
        <div className="pt-2">{children}</div>
      </div>
    </div>
  );
};

export default Layout;
