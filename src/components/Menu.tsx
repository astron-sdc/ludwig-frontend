import { Breadcrumb, Typography } from "@astron-sdc/design-system";
import FlexRow from "src/components/FlexRow";
import ProfileMenu from "./ProfileMenu.tsx";

export interface MenuProps {
  title: string;
  breadcrumbs?: { name: string; href: string }[];
}

const MenuBar = ({ title, breadcrumbs }: MenuProps) => {
  return (
    <FlexRow justify="between" customClass="pt-6 px-4 md:pl-2 md:pr-8 lg:pl-10 lg:pr:16 xl:pl-20 xl:pr-32">
      <div>
        <Typography text={title} variant="h4" />
        {breadcrumbs && <Breadcrumb items={breadcrumbs} />}
      </div>

      <div>
        <ProfileMenu />
      </div>
    </FlexRow>
  );
};

export default MenuBar;
