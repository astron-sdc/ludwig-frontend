import { Dropdown } from "@astron-sdc/design-system";
import ChoiceQuestionModel from "src/api/models/Question/ChoiceQuestion";
import QuestionProps from "src/components/Questionnaire/QuestionProps";

const ChoiceQuestion = ({ question, onAnswer }: QuestionProps<ChoiceQuestionModel>) => {
  return (
    <Dropdown
      label={question.title}
      valueOptions={question.choices.map((choice) => ({
        value: choice,
        label: choice + "\u200B",
      }))}
      value={question.answer}
      isRequired={question.is_required}
      onValueChange={(value) => {
        onAnswer(value?.toString() ?? "");
      }}
    />
  );
};

export default ChoiceQuestion;
