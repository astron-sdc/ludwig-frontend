import ChoiceQuestion from "src/components/Questionnaire/ChoiceQuestion";
import ChoiceQuestionModel from "src/api/models/Question/ChoiceQuestion";
import NumberQuestion from "src/components/Questionnaire/NumberQuestion";
import NumberQuestionModel from "src/api/models/Question/NumberQuestion";
import QuestionProps from "src/components/Questionnaire/QuestionProps";
import TextQuestion from "src/components/Questionnaire/TextQuestion";
import TextQuestionModel from "src/api/models/Question/TextQuestion";
import QuestionModel from "src/api/models/Question/Question";

const Question = ({ question, onAnswer }: QuestionProps<QuestionModel>) => {
  switch (question.type) {
    case "TextQuestion":
      return <TextQuestion question={question as TextQuestionModel} onAnswer={onAnswer} />;
    case "NumberQuestion":
      return <NumberQuestion question={question as NumberQuestionModel} onAnswer={onAnswer} />;
    case "ChoiceQuestion":
      return <ChoiceQuestion question={question as ChoiceQuestionModel} onAnswer={onAnswer} />;
  }
};

export default Question;
