import { TextArea } from "@astron-sdc/design-system";
import TextQuestionModel, { validateTextQuestionAnswer } from "src/api/models/Question/TextQuestion";
import QuestionProps from "src/components/Questionnaire/QuestionProps";
import { useMemo } from "react";
import { getWordCount } from "src/utils/Validation";

const TextQuestion = ({ question, onAnswer }: QuestionProps<TextQuestionModel>) => {
  const isInvalid = useMemo(() => {
    const questionIsUnanswered = question.answer === null || question.answer === undefined;
    if (questionIsUnanswered) {
      return false;
    }

    return !validateTextQuestionAnswer(question);
  }, [question]);

  const errorMessage = useMemo(() => {
    if (!isInvalid) return "";

    const wordCount = getWordCount(question.answer);
    const charLength = question.answer.length;

    return `Please enter a valid text (currently ${charLength} character${charLength === 1 ? "" : "s"}, ${wordCount} word${wordCount === 1 ? "" : "s"})`;
  }, [isInvalid, question.answer]);

  return (
    <TextArea
      label={`${question.title} (${question.min_char_length} - ${question.max_char_length} characters, ${question.min_word_count} - ${question.max_word_count} words)`}
      value={question.answer?.toString() ?? ""}
      onValueChange={(value) => {
        onAnswer(value.toString());
      }}
      isRequired={question.is_required}
      isInvalid={isInvalid}
      errorMessage={errorMessage}
    />
  );
};

export default TextQuestion;
