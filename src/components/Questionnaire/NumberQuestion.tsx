import { TextInput } from "@astron-sdc/design-system";
import NumberQuestionModel, { validateNumberQuestionAnswer } from "src/api/models/Question/NumberQuestion";
import QuestionProps from "src/components/Questionnaire/QuestionProps";
import { useMemo } from "react";

const NumberQuestion = ({ question, onAnswer }: QuestionProps<NumberQuestionModel>) => {
  const isInvalid = useMemo(() => {
    if (question.answer === undefined) {
      return false;
    }

    return !validateNumberQuestionAnswer(question);
  }, [question]);

  return (
    <TextInput
      label={`${question.title} (${question.min_value} - ${question.max_value})`}
      value={question.answer?.toString() ?? ""}
      onValueChange={(value) => {
        onAnswer(value.toString());
      }}
      isRequired={question.is_required}
      isInvalid={isInvalid}
      errorMessage={isInvalid ? "Please enter a valid number" : ""}
    />
  );
};
export default NumberQuestion;
