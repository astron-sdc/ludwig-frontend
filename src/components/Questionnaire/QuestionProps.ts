import QuestionModel from "src/api/models/Question/Question";

interface QuestionProps<T extends QuestionModel> {
  question: T;
  onAnswer: (answer: string) => void;
}

export default QuestionProps;
