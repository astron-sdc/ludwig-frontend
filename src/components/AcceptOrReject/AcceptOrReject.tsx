import { Button } from "@astron-sdc/design-system";
import { Dispatch, SetStateAction, useContext, useState } from "react";
import Api from "src/api/Api";
import { ProposalAction } from "src/api/models/Proposal";
import FlexRow from "src/components/FlexRow";
import ToastContext from "src/components/Toast/Context";

type AcceptOrRejectProps = {
  proposalId: number;
  onStatusChanged?: () => void;
};

const AcceptOrReject = ({ proposalId, onStatusChanged }: AcceptOrRejectProps) => {
  const [isSavingAccept, setIsSavingAccept] = useState(false);
  const [isSavingReject, setIsSavingReject] = useState(false);
  const { setToastMessage } = useContext(ToastContext);

  const saveActions: {
    [ProposalAction.accept]: Dispatch<SetStateAction<boolean>>;
    [ProposalAction.reject]: Dispatch<SetStateAction<boolean>>;
  } = {
    [ProposalAction.accept]: setIsSavingAccept,
    [ProposalAction.reject]: setIsSavingReject,
  };

  const updateProposalStatus = async (action: ProposalAction.accept | ProposalAction.reject) => {
    saveActions[action](true);

    const response = await Api.proposalActions(proposalId, action);

    if (response.ok) {
      onStatusChanged?.();

      setToastMessage({
        title: "Success",
        message: `Proposal action '${action}' was executed successfully`,
        alertType: "positive",
        expireAfter: 3000,
      });
    } else {
      response.json().then((value) => {
        setToastMessage({
          title: `Error executing proposal action '${action}'`,
          message: value.error,
          alertType: "negative",
          expireAfter: 3000,
        });
      });
    }

    saveActions[action](false);
  };

  return (
    <FlexRow>
      <Button
        showLoader={isSavingReject}
        isDisabled={isSavingAccept || isSavingReject}
        loadingText="Rejecting..."
        label="Reject Proposal"
        icon="Cross"
        color="negative"
        onClick={() => updateProposalStatus(ProposalAction.reject)}
      />
      <Button
        showLoader={isSavingAccept}
        isDisabled={isSavingAccept || isSavingReject}
        loadingText="Accepting..."
        label="Accept Proposal"
        icon="Checkmark"
        color="positive"
        onClick={() => updateProposalStatus(ProposalAction.accept)}
      />
    </FlexRow>
  );
};

export default AcceptOrReject;
