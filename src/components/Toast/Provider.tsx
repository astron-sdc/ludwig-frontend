import { AlertProps, Alert } from "@astron-sdc/design-system";
import { ReactNode, useState } from "react";
import ToastContext from "./Context";

type ToastProviderProps = {
  children: ReactNode;
};

const ToastProvider = (props: ToastProviderProps) => {
  const [toastMessage, setToastMessage] = useState<AlertProps | null>(null);

  return (
    <ToastContext.Provider value={{ toastMessage, setToastMessage }}>
      {props.children}
      {toastMessage ? (
        <Alert
          title={toastMessage.title}
          alertType={toastMessage.alertType}
          message={toastMessage.message}
          expireAfter={toastMessage.expireAfter}
          onExpire={
            toastMessage.onExpire ||
            (() => {
              setToastMessage(null);
            })
          }
        />
      ) : (
        <></>
      )}
    </ToastContext.Provider>
  );
};

export default ToastProvider;
export type { ToastProviderProps };
