import { AlertProps, Alert } from "@astron-sdc/design-system";
import { ReactNode, useMemo, useState } from "react";
import ToastContext from "src/components/Toast/Context";

type ToastProviderProps = {
  children: ReactNode;
};

const ToastProvider = (props: ToastProviderProps) => {
  const [toastMessage, setToastMessage] = useState<AlertProps | null>(null);

  const setSuccessToastMessage = (message?: string) => {
    setToastMessage({
      title: "Success",
      message: message ?? "",
      alertType: "positive",
      expireAfter: 5000,
    });
  };

  const setErrorToastMessage = (message?: string) => {
    setToastMessage({
      title: "Error",
      message: message ?? "",
      alertType: "negative",
      expireAfter: 5000,
    });
  };

  const toast = useMemo(
    () => ({ toastMessage, setToastMessage, setSuccessToastMessage, setErrorToastMessage }),
    [toastMessage],
  );

  return (
    <ToastContext.Provider value={toast}>
      {props.children}
      {toastMessage ? (
        <Alert
          title={toastMessage.title}
          alertType={toastMessage.alertType}
          message={toastMessage.message}
          expireAfter={toastMessage.expireAfter}
          onExpire={
            toastMessage.onExpire ||
            (() => {
              setToastMessage(null);
            })
          }
        />
      ) : (
        <></>
      )}
    </ToastContext.Provider>
  );
};

export default ToastProvider;
export type { ToastProviderProps };
