import { AlertProps } from "@astron-sdc/design-system";
import { createContext } from "react";

type ToastContextType = {
  toastMessage: AlertProps | null;
  setToastMessage: React.Dispatch<React.SetStateAction<AlertProps | null>>;
};

const ToastContext = createContext<ToastContextType>({
  toastMessage: null,
  setToastMessage: () => {},
});

export default ToastContext;
export type { ToastContextType };
