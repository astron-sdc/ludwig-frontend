import { AlertProps } from "@astron-sdc/design-system";
import { createContext } from "react";

type ToastContextType = {
  toastMessage: AlertProps | null;
  setToastMessage: React.Dispatch<React.SetStateAction<AlertProps | null>>;
  setSuccessToastMessage: (message: string) => void;
  setErrorToastMessage: (message: string) => void;
};

const ToastContext = createContext<ToastContextType>({
  toastMessage: null,
  setToastMessage: () => {},
  setSuccessToastMessage: () => {},
  setErrorToastMessage: () => {},
});

export default ToastContext;
export type { ToastContextType };
