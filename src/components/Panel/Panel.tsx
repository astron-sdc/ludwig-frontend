type Props = {
  children: React.ReactNode;
  className?: string;
};

/**
 * Container Panel using a given amount of padding and colours for reuse.
 */
const Panel = (props: Props) => {
  const className =
    "space-y-4 rounded-[8px] bg-lightGrey/30 dark:bg-background-panel p-[20px] pb-[10px] mb-[1em] " + props.className;
  return <div className={className}>{props.children}</div>;
};

export default Panel;
