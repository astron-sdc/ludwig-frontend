import { Children, ReactNode } from "react";

type FormGridProps = {
  // width: "md" | "lg";
  children: ReactNode;
};

const FormGrid = ({ children }: FormGridProps) => {
  return (
    <div className="max-w-4xl rounded-[8px] bg-lightGrey/30 dark:bg-baseBlack/40 p-4 mb-4">
      {Children.map(children, (child, index) => (
        <div
          key={index}
          className="border-t border-grey/50 first:border-0 py-2"
        >
          {child}
        </div>
      ))}
    </div>
  );
};

export default FormGrid;
