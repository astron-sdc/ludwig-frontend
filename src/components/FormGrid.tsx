import { Children, ReactNode } from "react";

type FormGridProps = {
  // width: "md" | "lg";
  children: ReactNode;
  border?: boolean;
  customClass?: string;
};

const FormGrid = ({ children, border = true, customClass = "py-2" }: FormGridProps) => {
  return (
    <div className="w-full rounded-[8px] dark:bg-background-panel p-4 mb-4 design-shadow-r">
      {Children.map(children, (child, index) => (
        <div key={index} className={`${border ? "border-t border-grey/50 first:border-0" : ""} ${customClass}`}>
          {child}
        </div>
      ))}
    </div>
  );
};

export default FormGrid;
