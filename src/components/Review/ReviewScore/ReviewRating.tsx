import { TextInput, Button } from "@astron-sdc/design-system";
import { useContext, useEffect, useMemo, useState } from "react";
import Api from "src/api/Api";
import FlexRow from "src/components/FlexRow";
import ToastContext from "src/components/Toast/Context";
import { validateNumber } from "src/utils/Validation";
import useSWRImmutable from "swr/immutable";

type ReviewRatingProps = {
  proposalId: number;
};

const ReviewRating = ({ proposalId }: ReviewRatingProps) => {
  const [rating, setRating] = useState<string>("");
  const [isSaving, setIsSaving] = useState(false);
  const { setToastMessage } = useContext(ToastContext);

  const getReview = async () => await Api.getProposalReview(proposalId);

  const fetchKey = `/api/v1/proposals/${proposalId}}/reviews/current_user_review/"`;

  const {
    data: fetchedReview,
    error,
    isLoading,
  } = useSWRImmutable(fetchKey, getReview, {
    revalidateOnMount: true,
  });

  useEffect(() => {
    setRating(fetchedReview?.ranking?.toString() ?? "");
  }, [fetchedReview]);

  const saveRating = async () => {
    if (!rating) return;
    setIsSaving(true);

    try {
      await Api.saveProposalReviewRating(proposalId, Number(rating));
      setToastMessage({
        title: "Success",
        message: "Rating was saved successfully",
        alertType: "positive",
        expireAfter: 5000,
      });
    } catch {
      setToastMessage({
        title: "Error",
        message: "Failed to save rating",
        alertType: "negative",
        expireAfter: 5000,
      });
    } finally {
      setIsSaving(false);
    }
  };

  const isSavingOrLoading = isSaving || isLoading;
  const reviewNotFoundErrorMessage = "404";
  const errorIsAnActualFetchError = error && error.message !== reviewNotFoundErrorMessage;

  const isRatingInvalid = useMemo(() => {
    if (!rating) return true;
    return !validateNumber(Number(rating));
  }, [rating]);

  return (
    <FlexRow>
      <TextInput
        labelClass="w-full"
        isDisabled={isSavingOrLoading || errorIsAnActualFetchError}
        labelPlacement="outside-left"
        label="Please give an overall numerical rating (1-5):"
        value={rating}
        isInvalid={isRatingInvalid}
        onValueChange={setRating}
      />
      <Button
        label={"Grade proposal"}
        loadingText={isLoading ? "Loading..." : "Saving..."}
        icon="Next"
        showLoader={isSavingOrLoading}
        isDisabled={isSavingOrLoading || errorIsAnActualFetchError || isRatingInvalid}
        color={"positive"}
        onClick={saveRating}
      />
    </FlexRow>
  );
};

export default ReviewRating;
