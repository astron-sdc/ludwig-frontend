import { Avatar } from "@nextui-org/react";
import ReviewComment from "src/api/models/ReviewComment";
import FlexRow from "src/components/FlexRow";
import { formatDateTime } from "src/utils/DateTime";

type ReviewCommentDetailProps = {
  reviewComment: ReviewComment;
};

const ReviewCommentDetail = ({ reviewComment }: ReviewCommentDetailProps) => {
  return (
    <div className="border-t border-grey/50">
      <FlexRow customClass="pt-2">
        <div className="">
          <Avatar className="bg-primary" name={reviewComment.reviewer_name} />
        </div>
        <div className="space-y-4 overflow-hidden">
          <FlexRow justify="between">
            <strong title={reviewComment.reviewer_name} className="truncate">
              {reviewComment.reviewer_name}
            </strong>
            <strong className="shrink-0">{formatDateTime(reviewComment.created_at)}</strong>
          </FlexRow>
          <FlexRow>
            <div>
              <p className="break-words whitespace-pre-wrap">{reviewComment.text}</p>
            </div>
          </FlexRow>
        </div>
      </FlexRow>
    </div>
  );
};

export default ReviewCommentDetail;
