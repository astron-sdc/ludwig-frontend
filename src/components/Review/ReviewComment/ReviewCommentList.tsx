import { Button, GeneralError, NoEntries, Spinner, TextArea, Typography } from "@astron-sdc/design-system";
import Panel from "src/components/Panel/Panel";
import { useContext, useEffect, useMemo, useRef, useState } from "react";
import ReviewComment from "src/api/models/ReviewComment";
import useSWRImmutable from "swr/immutable";
import Api from "src/api/Api";
import FormGrid from "src/components/FormGrid";
import ToastContext from "src/components/Toast/Context";
import ProposalPermissions from "src/api/models/ProposalPermissions";
import ReviewCommentDetail from "./ReviewCommentDetail";

type ReviewCommentListProps = {
  proposalId: number;
  permissions: ProposalPermissions;
};

const SANE_COMMENT_MAX_LENGTH = 5000;

const ReviewCommentList = ({ proposalId, permissions }: ReviewCommentListProps) => {
  const [reviewComments, setReviewComments] = useState<ReviewComment[]>([]);
  const [newReviewComment, setNewReviewComment] = useState("");
  const commentList = useRef<HTMLDivElement>(null);
  const { setToastMessage } = useContext(ToastContext);
  const [isSaving, setIsSaving] = useState(false);
  const isInvalid = useMemo(() => !newReviewComment?.trim(), [newReviewComment]);

  const canWriteComments = permissions.write_science_review || permissions.write_technical_review;

  const fetchReviewComments = async () => {
    return await Api.getReviewComments(proposalId);
  };

  const fetchKey = `/api/v1/proposals/${proposalId}}/comments"`;

  const {
    data: fetchedReviewComments,
    error,
    isLoading,
  } = useSWRImmutable(fetchKey, fetchReviewComments, {
    revalidateOnMount: true,
  });

  useEffect(() => {
    setReviewComments(fetchedReviewComments || []);
  }, [fetchedReviewComments]);

  useEffect(() => {
    const isInitialRender = reviewComments.length === fetchedReviewComments?.length;
    if (isInitialRender) return;
    commentList?.current?.scrollTo(0, commentList?.current?.scrollHeight);
  }, [fetchedReviewComments?.length, reviewComments]);

  if (error) {
    return (
      <FormGrid>
        <GeneralError
          title=""
          content={<Typography text="Error encountered when fetching review comments." variant="paragraph" />}
        />
      </FormGrid>
    );
  }

  if (isLoading) {
    return (
      <FormGrid>
        <div className="flex justify-center">
          <Spinner />
        </div>
      </FormGrid>
    );
  }

  const saveNewComment = async () => {
    setIsSaving(true);

    try {
      const savedReviewComment = await Api.saveReviewComment(proposalId, { text: newReviewComment });
      setReviewComments([...reviewComments, savedReviewComment]);
      setNewReviewComment("");

      setToastMessage({
        title: "Success",
        message: "Comment was saved successfully",
        alertType: "positive",
        expireAfter: 5000,
      });
    } catch {
      setToastMessage({
        title: "Error",
        message: "Failed to save comment",
        alertType: "negative",
        expireAfter: 5000,
      });
    } finally {
      setIsSaving(false);
    }
  };

  return (
    <Panel>
      <Typography text="Feedback" variant="h4" />
      <div className="flex flex-col space-y-4 ">
        {reviewComments.length ? (
          <div ref={commentList} className="space-y-4 flex flex-col space-y-4 h-[calc(100vh-50rem)] overflow-y-scroll">
            {reviewComments.map((reviewComment) => (
              <ReviewCommentDetail key={reviewComment.id} reviewComment={reviewComment} />
            ))}
          </div>
        ) : (
          <NoEntries
            title=""
            content={
              <Typography
                text={`There's no feedback yet. ${canWriteComments ? "Share your thoughts by adding a comment below." : ""}`}
                variant="paragraph"
              />
            }
          />
        )}
        {canWriteComments && (
          <div className="flex flex-col space-y-4">
            <TextArea
              value={newReviewComment}
              maxLength={SANE_COMMENT_MAX_LENGTH}
              onValueChange={setNewReviewComment}
              minRows={6}
              maxRows={12}
            />
            <Button
              label={`Add comment as ${permissions.write_science_review ? "scientific" : "technical"} reviewer`}
              icon="Edit"
              loadingText="Saving..."
              showLoader={isSaving}
              isDisabled={isInvalid || isSaving}
              color="primary"
              onClick={saveNewComment}
            />
          </div>
        )}
      </div>
    </Panel>
  );
};

export default ReviewCommentList;
