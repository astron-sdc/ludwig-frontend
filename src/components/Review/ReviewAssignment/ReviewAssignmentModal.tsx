import { Dropdown, Modal, Typography } from "@astron-sdc/design-system";
import { useState } from "react";
import CollaborationMember from "src/api/models/CollaborationMember";
import { PanelType, ProposalPanelTypePairing, ReviewerType } from "src/api/models/Review";
import { ModalButtonAction } from "src/components/Modal/Modal";
import { capitalizeFirstLetter } from "src/utils/Strings";

type ReviewAssignmentModalProps = {
  membersPerPanelType: Record<PanelType, CollaborationMember[]> | undefined;
  reviewerType: ReviewerType;
  selectedProposalPanelTypePairs: ProposalPanelTypePairing[];
  onModalClosed?: (
    action: ModalButtonAction,
    setSelectedMembersForPanelTypes?: Record<PanelType, string | undefined>,
  ) => void;
  showLoader: boolean;
};

const ReviewAssignmentModal = ({
  membersPerPanelType,
  reviewerType,
  selectedProposalPanelTypePairs,
  onModalClosed,
  showLoader,
}: ReviewAssignmentModalProps) => {
  const selectedPanelTypes = [...new Set(selectedProposalPanelTypePairs.map((pair) => pair.panelType))];

  const [selectedMembersForPanelTypes, setSelectedMembersForPanelTypes] = useState<Record<PanelType, string | undefined>>(
    Object.fromEntries(selectedPanelTypes.map((panelType) => [panelType, undefined])) as Record<
      PanelType,
      string | undefined
    >,
  );

  const getCallMemberValues = (membersPerPanelType: CollaborationMember[]): { value: string; label: string }[] => {
    if (!membersPerPanelType) return [{ value: "", label: "" }];
    return membersPerPanelType.map((member) => ({
      label: member.name,
      value: member.eduperson_unique_id,
    }));
  };

  return (
    <Modal
      isOpen={true}
      onOpenChange={() => onModalClosed?.(ModalButtonAction.close)}
      title={`Assign ${reviewerType} reviewer${selectedPanelTypes.length > 1 ? "s" : ""}`}
      buttons={[
        {
          label: "Assign",
          onClick: () => onModalClosed?.(ModalButtonAction.submit, selectedMembersForPanelTypes),
          isDisabled: Object.keys(selectedMembersForPanelTypes).every(
            (panelType) => selectedMembersForPanelTypes[panelType as PanelType] === undefined,
          ),
          showLoader: showLoader,
          loadingText: "Assigning...",
        },
        {
          label: "Cancel",
          color: "secondary",
          onClick: () => onModalClosed?.(ModalButtonAction.cancel),
        },
      ]}
    >
      {selectedProposalPanelTypePairs.map((pair) => (
        <Typography
          key={`${pair.proposal.id},${pair.panelType}`}
          text={`Proposal title: ${pair.proposal.title}`}
          variant="paragraph"
        />
      ))}

      {selectedPanelTypes.map((panelType: PanelType) => (
        <div data-testid={`reviewer-dropdown${panelType}`} key={panelType}>
          <Dropdown
            isDisabled={!membersPerPanelType![panelType]}
            label={`Select ${capitalizeFirstLetter(reviewerType)} ${capitalizeFirstLetter(panelType)} Reviewer`}
            value={selectedMembersForPanelTypes[panelType]}
            valueOptions={getCallMemberValues(membersPerPanelType![panelType])}
            onValueChange={(newValue) =>
              setSelectedMembersForPanelTypes((prev) => ({
                ...prev,
                [panelType]: newValue,
              }))
            }
          />
          {!membersPerPanelType![panelType]?.length && (
            <Typography text={`This call has no ${panelType} panel members.`} variant="errorMessage" />
          )}
        </div>
      ))}
    </Modal>
  );
};
export default ReviewAssignmentModal;
