import { Icon, Typography } from "@astron-sdc/design-system";
import { useContext } from "react";
import { Link } from "react-router-dom";
import CallCollaboration from "src/api/models/CallCollaboration";
import CallPermissions from "src/api/models/CallPermissions";
import { AuthContext } from "src/auth/AuthContext";

type ReviewerNameOrAssignmentButtonProps = {
  reviewerId: string | undefined;
  callCollaboration: CallCollaboration | undefined;
  permissions: CallPermissions;
  handleClick: () => void;
};
const ReviewerNameOrAssignmentButton = ({
  reviewerId,
  callCollaboration,
  permissions,
  handleClick,
}: ReviewerNameOrAssignmentButtonProps) => {
  const { user, permissions: globalPermissions } = useContext(AuthContext);
  const canAssignReviewers =
    globalPermissions?.is_admin || permissions.manage_members_science_panel || permissions.manage_members_technical_panel; // // FIXME: make AssignReviewersPermission

  if (canAssignReviewers && !reviewerId) {
    return (
      <div className="flex flex-row gap-2 ">
        <Link to="#" className="underline flex" onClick={handleClick} onKeyDown={handleClick}>
          <span className="flex place-items-center">
            <Icon name="Edit" />
            <Typography text="Assign reviewer" variant="paragraph" />
          </span>
        </Link>
      </div>
    );
  }
  return reviewerId === user?.uid ? (
    <Typography text="Assigned to me" variant="paragraph" />
  ) : (
    <Typography
      text={callCollaboration?.members.find((c) => c.eduperson_unique_id === reviewerId)?.name}
      variant="paragraph"
    />
  );
};

export default ReviewerNameOrAssignmentButton;
