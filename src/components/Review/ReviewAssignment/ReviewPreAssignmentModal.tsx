import { ColorType, Dropdown, Modal, Typography } from "@astron-sdc/design-system";
import { useState } from "react";
import CollaborationMember from "src/api/models/CollaborationMember";
import { PanelType } from "src/api/models/Review";
import { ModalButtonAction } from "src/components/Modal/Modal";
import { capitalizeFirstLetter } from "src/utils/Strings";

type ReviewPreAssignmentModalProps = {
  callMembers: CollaborationMember[] | undefined;
  onModalClosed?: (action: ModalButtonAction, selectedCallMemberUids?: string[], selectedPanelTypes?: PanelType[]) => void;
  showLoader: boolean;
  enabledPanelTypes: PanelType[];
};

const ReviewPreAssignmentModal = ({
  callMembers,
  onModalClosed,
  showLoader,
  enabledPanelTypes,
}: ReviewPreAssignmentModalProps) => {
  const [selectedCallMemberUids, setSelectedCallMemberUids] = useState<string[]>([]);
  const [selectedPanelTypes, setSelectedPanelTypes] = useState<string[]>([]);

  const getCallMemberValues = (): { value: string; label: string }[] => {
    if (!callMembers) return [{ value: "", label: "" }];
    return callMembers.map((member) => ({
      label: member.name,
      value: member.eduperson_unique_id,
    }));
  };

  const panelTypes: { value: string; label: string; badgeColor: ColorType }[] = [
    { value: PanelType.scientific, label: capitalizeFirstLetter(PanelType.scientific), badgeColor: "neutral" },
    { value: PanelType.technical, label: capitalizeFirstLetter(PanelType.technical), badgeColor: "warning" },
  ];

  return (
    <Modal
      isOpen={true}
      onOpenChange={() => onModalClosed?.(ModalButtonAction.close)}
      title="Assign members"
      buttons={[
        {
          label: "Assign",
          onClick: () =>
            onModalClosed?.(
              ModalButtonAction.submit,
              selectedCallMemberUids,
              selectedPanelTypes.map((panelType) => PanelType[panelType as keyof typeof PanelType]),
            ),
          isDisabled: !selectedCallMemberUids.length,
          showLoader: showLoader,
          loadingText: "Assigning...",
        },
        {
          label: "Cancel",
          color: "secondary",
          onClick: () => onModalClosed?.(ModalButtonAction.cancel),
        },
      ]}
    >
      <div className="mt-2" data-testid="reviewer-dropdown">
        <Dropdown
          isDisabled={!callMembers}
          label={"Select member(s)"}
          multiSelect={true}
          valueOptions={getCallMemberValues()}
          values={selectedCallMemberUids}
          onValuesChange={setSelectedCallMemberUids}
        />
      </div>
      <div className="mb-2" data-testid="panelType-dropdown">
        <Dropdown
          label={"Assign selected member(s) to review panel"}
          multiSelect={true}
          valueOptions={panelTypes.filter((pt) =>
            enabledPanelTypes.includes(PanelType[pt.value as keyof typeof PanelType]),
          )}
          values={selectedPanelTypes}
          onValuesChange={setSelectedPanelTypes}
        ></Dropdown>
      </div>
      {!callMembers?.length && <Typography text="This call has no members." variant="errorMessage" />}
    </Modal>
  );
};
export default ReviewPreAssignmentModal;
