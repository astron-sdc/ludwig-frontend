import {
  ActionMenu,
  Button,
  Checkbox,
  ColorType,
  Dropdown,
  NoEntries,
  Spinner,
  Table,
  Typography,
} from "@astron-sdc/design-system";
import ReviewedProposal from "src/api/models/ReviewedProposal.ts";
import { ReactNode, SetStateAction, useContext, useState } from "react";
import Api from "src/api/Api.ts";
import { AuthContext } from "src/auth/AuthContext";
import { Link, useNavigate } from "react-router-dom";
import Call from "src/api/models/Call";
import { formatDateTime } from "src/utils/DateTime";
import { ProposalStatus } from "src/api/models/Proposal";

import Review, { PanelType, ProposalPanelTypePairing, ReviewerType } from "src/api/models/Review";
import { capitalizeFirstLetter } from "src/utils/Strings";
import FormGrid from "src/components/FormGrid";
import useSWRImmutable from "swr/immutable";
import { piFromReviewedProposal } from "src/utils/Proposal";
import ToastContext from "src/components/Toast/Context";
import { mutate } from "swr";
import { ModalButtonAction } from "src/components/Modal/Modal";
import ProposalStatusBadge from "src/components/ProposalStatusBadge";
import FlexRow from "src/components/FlexRow";
import { statusToColor } from "src/utils/color";
import { getPanelMembersPerPanelType } from "src/helper/CollaborationHelper";
import CallPermissions from "src/api/models/CallPermissions";
import ReviewAssignmentModal from "./ReviewAssignmentModal";
import ReviewerNameOrAssignmentButton from "./ReviewerNameOrAssignmentButton";

type Values = number | string | ReactNode | undefined;

const COLUMNS = [
  {
    field: "checkbox",
    header: "",
  },
  {
    field: "title",
    header: "Title",
  },
  {
    field: "code",
    header: "Code",
  },
  {
    field: "status",
    header: "Status",
  },
  {
    field: "panel",
    header: "Panel",
  },
  {
    field: "submitted",
    header: "Submitted",
  },
  {
    field: "pi_creator",
    header: "PI/Creator",
  },
  {
    field: "primary_reviewer",
    header: "Primary Reviewer",
  },
  {
    field: "secondary_reviewer",
    header: "Secondary Reviewer",
  },
  {
    field: "actions",
    header: " ",
  },
];

const ReviewAssignmentTable = (props: { call: Call; permissions: CallPermissions }) => {
  const { call, permissions } = props;
  const { authenticated, user } = useContext(AuthContext);
  const [showModal, setShowModal] = useState<boolean>(false);
  const { setToastMessage } = useContext(ToastContext);
  const [selectedProposals, setSelectedProposals] = useState<ProposalPanelTypePairing[]>([]);
  const navigate = useNavigate();
  const [reviewerTypeClicked, setReviewerTypeClicked] = useState<ReviewerType | undefined>(undefined);

  const setStatusFilters = (e: SetStateAction<string[]>) => {
    localStorage.setItem(`${call.id}_filter`, JSON.stringify(e));
    setSelectedStatusFilters(e);
  };

  const storedFilters = localStorage.getItem(`${call.id}_filter`);
  const [selectedStatusFilters, setSelectedStatusFilters] = useState<string[]>(
    storedFilters ? JSON.parse(storedFilters) : [ProposalStatus.underReview.toString()],
  );

  const [isSaving, setIsSaving] = useState(false);
  const fetchKey = `/api/v1/calls${call.id}/proposals`;
  const {
    data: fetchedProposals,
    error,
    isLoading,
  } = useSWRImmutable<ReviewedProposal[]>(fetchKey, () => Api.getProposalsForCall(call.id));

  const callMemberFetchKey = `/api/v1/calls/${call.id}}/members/"`;
  const { data: callCollaboration } = useSWRImmutable(callMemberFetchKey, () => Api.getCallMembers(call.id));
  const proposalToTableColumns = (proposal: ReviewedProposal): Record<string, Values> => ({
    key: proposal.id,
    submitted: proposal.submitted_at ? formatDateTime(proposal.submitted_at) : "-",
    code: proposal.project_code,
    title: (
      <Link className="underline" to={`/proposals/${proposal.id}/`}>
        {proposal.title}
      </Link>
    ),
    pi_creator: piFromReviewedProposal(proposal),
    status: <ProposalStatusBadge status={proposal.status} />,
  });

  if (error) {
    throw Error(error);
  }

  if (isLoading || !user || !authenticated) {
    return (
      <FormGrid>
        <Spinner />
      </FormGrid>
    );
  }

  const setIsCheckboxSelected = (
    _value: boolean | ((prevState: boolean) => boolean),
    proposal: ReviewedProposal,
    panelType: PanelType,
  ) => {
    if (_value) {
      setSelectedProposals([...selectedProposals, { proposal, panelType }]);
    } else {
      setSelectedProposals(selectedProposals.filter((p) => p.panelType !== panelType || p.proposal.id !== proposal.id));
    }
  };

  const proposalCheckbox = (proposal: ReviewedProposal, panelType: PanelType) => {
    return proposal.reviews.filter((review) => review.panel_type === panelType).length < 2 ? (
      <Checkbox
        isSelected={selectedProposals.some((p) => p.proposal.id === proposal.id && p.panelType === panelType)}
        setIsSelected={(value) => setIsCheckboxSelected(value, proposal, panelType)}
        label=""
      />
    ) : null;
  };

  const proposalActions = (proposal: ReviewedProposal, panelType: PanelType) => {
    return proposal.reviews.some((r) => r.panel_type === panelType && r.reviewer_id === user.uid) ? (
      <Button
        label="Review"
        icon="Magnifier"
        color="primary"
        onClick={() => navigate(`/proposals/${proposal.id?.toString()}/review`)}
      />
    ) : null;
  };

  const reviewerNameOrAssignmentButton = (reviewerType: ReviewerType, proposal: ReviewedProposal, panelType: PanelType) => {
    const reviewers = proposal.reviews.filter(
      (review) => review.panel_type === panelType && review.reviewer_type === reviewerType,
    );
    return (
      <ReviewerNameOrAssignmentButton
        permissions={permissions}
        key={`${proposal.id}${reviewerType}${panelType}`}
        callCollaboration={callCollaboration}
        reviewerId={reviewers[0]?.reviewer_id}
        handleClick={() => {
          setSelectedProposals([{ panelType, proposal }]);
          setReviewerTypeClicked(reviewerType);
          setShowModal(true);
        }}
      />
    );
  };

  const mapProposals = (): Record<string, Values>[] => {
    const mappedProposals: Record<string, Values>[] = [];
    if (!fetchedProposals) {
      return mappedProposals;
    }

    const filteredProposals = fetchedProposals.filter(
      (proposal) => !selectedStatusFilters.length || selectedStatusFilters.includes(proposal.status),
    );

    for (const proposal of filteredProposals) {
      // If a proposal is under review, populate extra columns and show the proposal twice, once for each panel type.
      if (proposal.status == ProposalStatus.underReview) {
        for (const panelType of Object.values(PanelType)) {
          const mappedProposal: Record<string, Values> = {
            ...proposalToTableColumns(proposal),
            panel: capitalizeFirstLetter(panelType),
            primary_reviewer: reviewerNameOrAssignmentButton(ReviewerType.primary, proposal, panelType),
            secondary_reviewer: reviewerNameOrAssignmentButton(ReviewerType.secondary, proposal, panelType),
            is_selected: false,
            checkbox: proposalCheckbox(proposal, panelType),
            actions: proposalActions(proposal, panelType),
          };
          mappedProposals.push(mappedProposal);
        }
        // If proposal is not under review, don't show the checkboxes, Panel column, Primary/Secondary Reviewer columns, Review button
      } else {
        mappedProposals.push(proposalToTableColumns(proposal));
      }
    }
    return mappedProposals;
  };

  const assignReviewerClicked = (reviewerType: ReviewerType): void => {
    if (!selectedProposals?.length) {
      setToastMessage({
        title: "No proposals selected!",
        message: "Please select proposals for assignment.",
        alertType: "negative",
        expireAfter: 3000,
      });
    } else {
      setReviewerTypeClicked(reviewerType);
      setShowModal(true);
    }
  };

  const assignReviewers = async (selectedMembersForPanelTypes: Record<PanelType, string | undefined>) => {
    try {
      const reviews: Review[] = [];
      selectedProposals.forEach((pair) => {
        const reviewerId = selectedMembersForPanelTypes[pair.panelType];
        if (reviewerId !== undefined) {
          reviews.push({
            proposal_id: pair.proposal.id,
            panel_type: pair.panelType,
            reviewer_id: reviewerId,
          } as Review);
        }
      });

      if (reviews.length > 0) {
        await Api.assignProposalReviewers(call.id, reviews, reviewerTypeClicked!);
      }

      setToastMessage({
        title: "Success",
        message: "Reviewer was successfully assigned.",
        alertType: "positive",
        expireAfter: 3000,
      });

      mutate(fetchKey);
    } catch (ex) {
      setToastMessage({
        title: "Error assigning reviewers.",
        message: `${ex}`,
        alertType: "negative",
        expireAfter: 3000,
      });
    }
  };

  const onReviewAssignmentModalClosed = async (
    action: ModalButtonAction,
    selectedMembersForPanelTypes?: Record<PanelType, string | undefined>,
  ) => {
    if (action === ModalButtonAction.submit) {
      setIsSaving(true);
      await assignReviewers(selectedMembersForPanelTypes!);
    }

    setShowModal(false);
    setIsSaving(false);
    setSelectedProposals([]);
    setReviewerTypeClicked(undefined);
  };

  const statusFilterValueOptions: { value: string; label: string; badgeColor?: ColorType }[] = Object.values(
    ProposalStatus,
  ).map((status) => ({
    value: status,
    label: capitalizeFirstLetter(status?.replace("_", " ")),
    badgeColor: statusToColor(status),
  }));

  return (
    <>
      {showModal && (
        <ReviewAssignmentModal
          showLoader={isSaving}
          membersPerPanelType={getPanelMembersPerPanelType(callCollaboration)}
          reviewerType={reviewerTypeClicked!}
          selectedProposalPanelTypePairs={selectedProposals}
          onModalClosed={(buttonAction, selectedCallMember) =>
            onReviewAssignmentModalClosed(buttonAction, selectedCallMember)
          }
        />
      )}

      <Table
        columns={COLUMNS}
        headerContent={
          <div className="flex flex-col">
            <div className="flex flex-row gap-4 pb-[20px]">
              <FlexRow customClass="flex flex-row w-full">
                <div className="min-w-32">
                  <Dropdown
                    label="Status filter:"
                    valueOptions={statusFilterValueOptions}
                    values={selectedStatusFilters}
                    onValuesChange={(e) => setStatusFilters(e)}
                    multiSelect={true}
                  />
                </div>
                <div className="self-end">
                  {/* TODO: only show button if user has assign_reviewers permission */}
                  <ActionMenu
                    label="Assign reviewers"
                    isDisabled={selectedProposals.length === 0}
                    menu={[
                      {
                        sectionKey: "primary",
                        sectionContent: [
                          {
                            key: "primaryReviewer",
                            content: (
                              <button onClick={() => assignReviewerClicked(ReviewerType.primary)}>Primary reviewer</button>
                            ),
                          },
                        ],
                      },
                      {
                        sectionKey: "secondary",
                        sectionContent: [
                          {
                            key: "secondaryReviewer",
                            content: (
                              <button onClick={() => assignReviewerClicked(ReviewerType.secondary)}>
                                Secondary reviewer
                              </button>
                            ),
                          },
                        ],
                      },
                    ]}
                  />
                </div>
              </FlexRow>
            </div>
          </div>
        }
        rows={!fetchedProposals ? [] : mapProposals()}
        emptyMessage={
          <NoEntries
            title=""
            content={
              <FlexRow>
                <Typography text="No proposals with statuses matching the filters." variant="paragraph" />
              </FlexRow>
            }
          />
        }
      />
    </>
  );
};

export default ReviewAssignmentTable;
