import { Table } from "@astron-sdc/design-system";
import { formatDateTime } from "src/utils/DateTime.ts";
import ReviewedProposal from "src/api/models/ReviewedProposal.ts";
import { ReactNode, useState } from "react";
import { Link } from "react-router-dom";
import { formatNumber } from "src/utils/Numbers.ts";
import { piFromReviewedProposal } from "src/utils/Proposal";

interface ReviewTableProps {
  proposals?: ReviewedProposal[];
}

// Allowed types in the table
type Values = number | string | ReactNode | undefined;

const COLUMNS = [
  {
    field: "title",
    header: "Proposal Title",
  },
  {
    field: "pi",
    header: "PI",
  },
  {
    field: "submitted",
    header: "Submitted",
  },
  {
    field: "review_rating",
    header: "Rating",
  },
  {
    field: "feedback",
    header: "Feedback",
  },
];

const reviewedProposalsToTableColumns = (proposal: ReviewedProposal): Record<string, Values> => ({
  key: proposal.id,
  title: (
    <Link className="underline" to={`/proposals/${proposal.id}/`}>
      {proposal.title}
    </Link>
  ),
  pi: piFromReviewedProposal(proposal),
  submitted: proposal.status === "under_review" ? formatDateTime(proposal.submitted_at) : "Not Submitted",
  review_rating: proposal.review_rating === null ? "?" : formatNumber(proposal.review_rating, { fractionDigits: 2 }),
  feedback: `${proposal.reviews.length} reviews`,
});

const ReviewTable = (props: ReviewTableProps) => {
  const { proposals } = props;
  const dataRows = !proposals ? [] : proposals.map(reviewedProposalsToTableColumns);
  const [rowOrder, setRowOrder] = useState(dataRows);
  return <Table columns={COLUMNS} isReorderable={true} rows={rowOrder} onReorder={setRowOrder} />;
};

export default ReviewTable;
