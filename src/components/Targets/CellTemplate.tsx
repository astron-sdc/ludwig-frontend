import { Icon, TextInput } from "@astron-sdc/design-system";
import FlexRow from "src/components/FlexRow";

const CellTemplate = (
  value: string,
  isRequired: boolean,
  validator?: (value: string) => boolean,
  showIcon: boolean = true,
) => (
  <FlexRow customClass="items-center p-0">
    <TextInput value={value ?? ""} />
    {showIcon && (
      <Icon
        name="Edit"
        color={(isRequired && !value) || (value && validator && !validator(value)) ? "negative" : "secondary"}
      />
    )}
  </FlexRow>
);

export default CellTemplate;
