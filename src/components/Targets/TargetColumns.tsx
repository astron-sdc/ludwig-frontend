import TextEditor, { optionsType } from "src/components/Targets/Editors/TextEditor";
import Target from "src/api/models/Target";
import onTextEditComplete, { TextEditCompleteEvent } from "src/components/Targets/Editors/EditorCallbacks";
import {
  validateDmsCoordinate,
  validateFieldOrder,
  validateGroup,
  validateHmsCoordinate,
  validateSubgroup,
} from "src/components/Targets/Utils/ValidateTarget";
import CellTemplate from "src/components/Targets/CellTemplate.tsx";
import { PriorityValueToLabel } from "src/helper/PriorityHelper";
import { ColumnProps } from "@astron-sdc/design-system";
import PriorityEditor, { priorityTypeEditor } from "src/components/Targets/Editors/PriorityEditor";

const TARGET_COLUMNS: ColumnProps[] = [
  {
    field: "name",
    header: "Name",
    editor: (options: unknown) => TextEditor(options as optionsType),
    onCellEditComplete: (e: unknown) => onTextEditComplete(e as TextEditCompleteEvent),
    bodyTemplate: (rowData: unknown) => CellTemplate((rowData as Target).name, true),
    style: {
      width: "15%",
    },
  },
  {
    field: "resolveName",
    style: {
      width: "6%",
    },
  },
  {
    field: "x_hms_formatted",
    header: "RA",
    editor: (options: unknown) => TextEditor(options as optionsType, validateHmsCoordinate, "Invalid format"),
    onCellEditComplete: (e: unknown) => onTextEditComplete(e as TextEditCompleteEvent),
    bodyTemplate: (rowData: unknown) => CellTemplate((rowData as Target).x_hms_formatted, true, validateHmsCoordinate),
    style: {
      width: "10%",
    },
  },
  {
    field: "y_dms_formatted",
    header: "Dec",
    editor: (options: unknown) => TextEditor(options as optionsType, validateDmsCoordinate, "Invalid format"),
    onCellEditComplete: (e: unknown) => onTextEditComplete(e as TextEditCompleteEvent),
    bodyTemplate: (rowData: unknown) => CellTemplate((rowData as Target).y_dms_formatted, true, validateDmsCoordinate),
    style: {
      width: "10%",
    },
  },
  {
    field: "system",
    header: "System",
    style: {
      width: "10%",
    },
  },
  {
    field: "group",
    header: (<span>Group &ensp;</span>) as unknown as string,
    editor: (options: optionsType) => TextEditor(options, validateGroup, "Group must be an integer"),
    bodyTemplate: (rowData: unknown) => CellTemplate((rowData as Target).group as string, false, validateGroup),
    onCellEditComplete: (e: unknown) => onTextEditComplete(e as TextEditCompleteEvent),
    style: { width: "6%" },
    sortable: true,
  },
  {
    field: "subgroup",
    header: (<span>Subgroup &ensp;</span>) as unknown as string,
    editor: (options: optionsType) => TextEditor(options, validateSubgroup, "subgroup must be empty or an number"),
    bodyTemplate: (rowData: unknown) => CellTemplate((rowData as Target).subgroup, false, validateGroup),
    onCellEditComplete: (e: unknown) => onTextEditComplete(e as TextEditCompleteEvent),
    style: { width: "6%" },
    sortable: true,
  },
  {
    field: "field_order",
    header: (<span>Position &ensp;</span>) as unknown as string,
    editor: (options: optionsType) =>
      TextEditor(options, validateFieldOrder, "Field order in the Specification definition must be empty or an integer"),
    bodyTemplate: (rowData: unknown) => CellTemplate((rowData as Target).field_order, false, validateGroup),
    onCellEditComplete: (e: unknown) => onTextEditComplete(e as TextEditCompleteEvent),
    style: { width: "6%" },
    sortable: true,
  },
  {
    field: "requested_priority",
    header: "Requested priority",
    editor: (options: unknown) => {
      (options as priorityTypeEditor).isRequestedValues = true;
      return PriorityEditor(options as priorityTypeEditor);
    },
    onCellEditComplete: (e: unknown) => onTextEditComplete(e as TextEditCompleteEvent),
    bodyTemplate: (rowData: unknown) => CellTemplate((rowData as Target).requested_priority, false),
    style: {
      width: "10%",
    },
  },
  {
    field: "assigned_priority",
    header: "Assigned priority",
    editor: (options: unknown) => PriorityEditor(options as priorityTypeEditor),
    onCellEditComplete: (e: unknown) => onTextEditComplete(e as TextEditCompleteEvent),
    bodyTemplate: (rowData: unknown) => CellTemplate(PriorityValueToLabel((rowData as Target).assigned_priority), false),
    style: {
      width: "10%",
    },
  },
  {
    field: "notes",
    header: "Notes",
    editor: (options: unknown) => TextEditor(options as optionsType),
    onCellEditComplete: (e: unknown) => onTextEditComplete(e as TextEditCompleteEvent),
    bodyTemplate: (rowData: unknown) => CellTemplate((rowData as Target).notes, false),
    style: {
      width: "20%",
    },
  },
  {
    field: "calculate",
    header: "",
    style: {
      width: "5%",
    },
  },
  {
    field: "delete",
    style: {
      width: "5%",
    },
  },
];

export default TARGET_COLUMNS;
