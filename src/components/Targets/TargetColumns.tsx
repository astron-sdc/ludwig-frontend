import { Icon } from "@astron-sdc/design-system";
import TextEditor from "../TextEditor";
import { onTextEditComplete, onRaDecEditComplete } from "./EditorCallbacks";
import Target from "src/api/models/Target";

const cellTemplate = (value: string, isRequired: boolean) => (
  <div className="flex justify-between">
    <div className="text-ellipsis">{value}</div>
    <Icon name="Edit" color={isRequired && !value ? "negative" : "secondary"} />
  </div>
);

const TARGET_COLUMNS = [
  {
    field: "name",
    header: "Name",
    editor: TextEditor,
    onCellEditComplete: onTextEditComplete,
    bodyTemplate: (rowData: unknown) =>
      cellTemplate((rowData as Target).name, true),
    style: {
      width: "15%",
    },
  },
  {
    field: "resolveName",
    header: "",
    style: {
      width: "10%",
    },
  },
  {
    field: "x_hms_formatted",
    header: "RA",
    editor: TextEditor,
    onCellEditComplete: onRaDecEditComplete,
    bodyTemplate: (rowData: unknown) =>
      cellTemplate((rowData as Target).x_hms_formatted, true),
    style: {
      width: "20%",
    },
  },
  {
    field: "y_dms_formatted",
    header: "Dec",
    editor: TextEditor,
    onCellEditComplete: onRaDecEditComplete,
    bodyTemplate: (rowData: unknown) =>
      cellTemplate((rowData as Target).y_dms_formatted, true),
    style: {
      width: "20%",
    },
  },
  {
    field: "system",
    header: "System",
    style: {
      width: "10%",
    },
  },
  {
    field: "notes",
    header: "Notes",
    editor: TextEditor,
    onCellEditComplete: onTextEditComplete,
    bodyTemplate: (rowData: unknown) =>
      cellTemplate((rowData as Target).notes, false),
    style: {
      width: "20%",
    },
  },
  {
    field: "delete",
    header: "",
    style: {
      width: "5%",
    },
  },
];

export default TARGET_COLUMNS;
