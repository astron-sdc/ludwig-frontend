import TargetRow from "src/components/Targets/Models/TargetRow";
import { Button } from "@astron-sdc/design-system";

interface Props {
  target: TargetRow;
  deleteTarget: (row: TargetRow) => void;
}

const DeleteButton = (props: Props) => {
  const { target, deleteTarget } = props;

  return (
    <Button
      label="Delete"
      icon="Cross"
      color="negative"
      onClick={() => {
        deleteTarget(target);
      }}
    />
  );
};

export default DeleteButton;
