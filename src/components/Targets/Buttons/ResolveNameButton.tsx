import TargetRow from "src/components/Targets/Models/TargetRow";
import { Button } from "@astron-sdc/design-system";
import { Dispatch, SetStateAction, useContext } from "react";
import ResolvedTarget from "src/api/models/ResolvedTarget";
import Api from "src/api/Api";
import ToastContext from "src/components/Toast/Context";

interface Props {
  target: TargetRow;
  setTargets: Dispatch<SetStateAction<TargetRow[]>>;
  updateTarget: (target: TargetRow, targetProps: Partial<TargetRow>) => TargetRow;
  resolveTimeout: number;
}

const ResolveNameButton = ({ target, setTargets, updateTarget, resolveTimeout }: Props) => {
  const { setToastMessage } = useContext(ToastContext);

  const resolveName = async (target: TargetRow) => {
    let resolvedTarget: ResolvedTarget;
    try {
      if (target.name) {
        resolvedTarget = await Api.resolveTargetName(target.name);
      }
    } catch {
      setToastMessage({
        title: "Error",
        message: `Failed to resolve name ${target.name}`,
        alertType: "negative",
        expireAfter: 5000,
      });
    } finally {
      setTargets((old) =>
        old.map((t) =>
          t.rowId === target.rowId
            ? updateTarget(t, {
                x_hms_formatted: resolvedTarget?.x_hms_formatted,
                y_dms_formatted: resolvedTarget?.y_dms_formatted,
                name: target.name,
                isResolving: false,
              })
            : t,
        ),
      );
    }
  };

  // resolve on a timeout to allow clicking on "resolve" while
  // the target name input field still has focus
  const resolveNameOnTimeout = () => {
    return new Promise<void>((resolve) => {
      setTimeout(() => {
        resolveName(target);
        resolve();
      }, resolveTimeout);
    });
  };

  const handleClick = async () => {
    setTargets((old) =>
      old.map((oldTarget) =>
        oldTarget.rowId === target.rowId ? updateTarget(oldTarget, { isResolving: true }) : oldTarget,
      ),
    );
    if (resolveTimeout) {
      await resolveNameOnTimeout();
    } else {
      await resolveName(target);
    }
  };

  return (
    <Button
      color="secondary"
      icon="Magnifier"
      label="Resolve"
      loadingText="Resolve"
      isDisabled={target.isResolving}
      showLoader={target.isResolving}
      onClick={handleClick}
    />
  );
};

export default ResolveNameButton;
