import TargetRow from "src/components/Targets/Models/TargetRow";
import { Button } from "@astron-sdc/design-system";
import ProposalPermissions from "src/api/models/ProposalPermissions";

interface CalculateButtonProps {
  targetRow: TargetRow;
  permissions: ProposalPermissions;
  setSelectedCalculateTarget: (row: TargetRow) => void;
}

const CalculateButton = ({ targetRow, permissions, setSelectedCalculateTarget }: CalculateButtonProps) => {
  const label = permissions.write_calculation ? "Calculate" : "View results";

  return (
    <Button
      label={label}
      icon="Eye"
      color="secondary"
      onClick={() => {
        setSelectedCalculateTarget(targetRow);
      }}
    />
  );
};

export default CalculateButton;
