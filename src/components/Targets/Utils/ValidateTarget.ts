const validateCoordinate = (coordinate: string, pattern: RegExp): boolean => !!pattern.exec(coordinate);

const validateHmsCoordinate = (coordinate: string): boolean => validateCoordinate(coordinate, /^\d\d:\d\d:\d\d(\.\d+)?$/);

const validateDmsCoordinate = (coordinate: string): boolean =>
  validateCoordinate(coordinate, /^[+-]\d\d:\d\d:\d\d(\.\d+)?$/);

const validateGroup = (group: string): boolean => Number.isInteger(Number(group));
const validateSubgroup = (subgroup: string): boolean => {
  return subgroup === "" || Number.isInteger(Number(subgroup));
};

const validateFieldOrder = (fieldOrder: string): boolean => {
  return fieldOrder === "" || Number.isInteger(Number(fieldOrder));
};

export { validateHmsCoordinate, validateDmsCoordinate, validateGroup, validateSubgroup, validateFieldOrder };
