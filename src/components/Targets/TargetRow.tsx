import Target from "src/api/models/Target";

interface TargetRow extends Target {
  rowId: string;
  resolveName: JSX.Element;
  delete: JSX.Element;
  isResolving?: boolean;
}

export default TargetRow;
