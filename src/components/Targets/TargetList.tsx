import { Button, GeneralError, NoEntries, Spinner, Table, Typography } from "@astron-sdc/design-system";
import { useContext, useEffect, useState } from "react";
import Api from "src/api/Api";
import useSWRImmutable from "swr/immutable";
import { mutate } from "swr";
import { DataTableValueArray } from "primereact/datatable";
import ToastContext from "src/components/Toast/Context";
import FormGrid from "src/components/FormGrid.tsx";
import TARGET_COLUMNS from "src/components/Targets/TargetColumns";
import TargetRow from "src/components/Targets/Models/TargetRow";
import ResolveNameButton from "src/components/Targets/Buttons/ResolveNameButton";
import DeleteButton from "src/components/Targets/Buttons/DeleteButton";
import Target from "src/api/models/Target";
import { v4 as uuidv4 } from "uuid";
import ProposalPermissions from "src/api/models/ProposalPermissions";
import CalculatorOverview from "src/components/Calculator/CalculatorOverview";
import CalculateButton from "./Buttons/CalculateButton";

type TargetListProps = {
  proposalId: number;
  permissions: ProposalPermissions;
  resolveTimeout?: number;
};

const TargetList = ({ proposalId, permissions, resolveTimeout = 200 }: TargetListProps) => {
  const [targetRows, setTargetRows] = useState<TargetRow[]>([]);
  const [selectedTargetRow, setSelectedTargetRow] = useState<TargetRow>();
  const [isSaving, setIsSaving] = useState(false);
  const { setSuccessToastMessage, setErrorToastMessage } = useContext(ToastContext);
  const canAddTarget = permissions.write_proposal;
  const [highlightedRows, setHighlightedRows] = useState<DataTableValueArray>([]);

  const updateTarget = (target: TargetRow, targetProps: Partial<TargetRow>): TargetRow => {
    const updatedTarget = { ...target, ...targetProps };
    setButtons([updatedTarget]);
    return updatedTarget;
  };

  const deleteTarget = (target: TargetRow) => {
    setTargetRows((old) => old.filter((t) => t !== target));
  };

  const newRow = () => {
    return {
      rowId: uuidv4(),
    };
  };

  const addTarget = () => {
    const newTarget: TargetRow = {
      ...newRow(),
      id: 0,
      name: "",
      delete: <></>,
      calculate: <></>,
      resolveName: <></>,
      x_hms_formatted: "",
      y_dms_formatted: "",
      system: "J2000",
      notes: "",
      group: "1",
      subgroup: "1",
      field_order: "1",
      assigned_priority: "?",
      requested_priority: "A",
    };

    setButtons([newTarget]);
    setTargetRows((old) => [...old, newTarget]);
  };

  const fetchTargets = async () => {
    let targetRows = (await Api.getTargets(proposalId)) as TargetRow[];
    targetRows = targetRows.map((target) => ({ ...target, ...newRow() }));
    setButtons(targetRows);
    return targetRows;
  };

  const setButtons = (targetRows: TargetRow[]) => {
    for (const targetRow of targetRows) {
      permissions.write_proposal && (targetRow.delete = <DeleteButton target={targetRow} deleteTarget={deleteTarget} />);
      targetRow.resolveName = (
        <ResolveNameButton
          target={targetRow}
          setTargets={setTargetRows}
          updateTarget={updateTarget}
          resolveTimeout={resolveTimeout}
        />
      );
      permissions.read_calculation &&
        (targetRow.calculate = (
          <CalculateButton
            targetRow={targetRow}
            permissions={permissions}
            setSelectedCalculateTarget={(row) => {
              setSelectedTargetRow(row);
              setHighlightedRows([row]);
            }}
          />
        ));
    }
  };

  const fetchKey = `/api/v1/proposals/${proposalId}/targets`;

  const refetchData = () => {
    mutate(fetchKey);
  };

  const {
    data: fetchedTargets,
    error,
    isLoading,
  } = useSWRImmutable(fetchKey, fetchTargets, {
    revalidateOnMount: true,
  });

  useEffect(() => {
    setTargetRows(fetchedTargets as TargetRow[]);
  }, [fetchedTargets]);

  if (error) {
    return (
      <FormGrid>
        <GeneralError
          title=""
          content={<Typography text="Error encountered when fetching targets." variant="paragraph" />}
        />
      </FormGrid>
    );
  }

  if (isLoading) {
    return (
      <FormGrid>
        <Spinner />
      </FormGrid>
    );
  }

  const targetRowToTarget = (targetRow: TargetRow): Target => {
    return {
      id: targetRow.id,
      group: targetRow.group,
      subgroup: targetRow.subgroup,
      requested_priority: targetRow.requested_priority,
      assigned_priority: targetRow.assigned_priority,
      field_order: targetRow.field_order,
      name: targetRow.name,
      x_hms_formatted: targetRow.x_hms_formatted,
      y_dms_formatted: targetRow.y_dms_formatted,
      system: targetRow.system,
      notes: targetRow.notes,
    } as Target;
  };

  const updateTargets = async () => {
    if (!targetRows.length) return;

    setIsSaving(true);
    try {
      await Api.updateTargets(
        proposalId,
        targetRows.map((t) => targetRowToTarget(t)),
      );
      refetchData();
      setSuccessToastMessage("Targets were saved successfully");
    } catch (ex) {
      setErrorToastMessage(`Failed to save target: ${ex}`);
    } finally {
      setIsSaving(false);
    }
  };

  return (
    <div className="flex flex-row gap-4">
      <div>
        <div className="space-y-6 pt-4">
          <Table
            headerContent={
              canAddTarget && (
                <div className="pb-4">
                  <Button label="Add Target" icon="Plus" color="neutral" onClick={addTarget} />
                </div>
              )
            }
            highlightedRows={highlightedRows}
            columns={TARGET_COLUMNS}
            rows={targetRows}
            editMode="cell"
            emptyMessage={
              <NoEntries
                title=""
                content={
                  <Typography
                    text={
                      canAddTarget ? "Click 'Add Target' to add new targets." : "This proposal does not have any Targets"
                    }
                    variant="paragraph"
                  />
                }
              />
            }
          />
          {canAddTarget && (
            <Button
              label="Save targets"
              loadingText="Saving..."
              showLoader={isSaving}
              isDisabled={isSaving}
              color="primary"
              onClick={updateTargets}
            />
          )}
        </div>
      </div>
      {permissions.read_calculation && (
        <div className="w-auto">
          <CalculatorOverview selectedTargetId={selectedTargetRow?.id} proposalId={proposalId} permissions={permissions} />
        </div>
      )}
    </div>
  );
};

export default TargetList;
