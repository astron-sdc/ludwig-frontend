import { Button, Spinner, Table } from "@astron-sdc/design-system";
import { useContext, useEffect, useState } from "react";
import Api from "src/api/Api";
import useSWRImmutable from "swr/immutable";
import { mutate } from "swr";
import TargetRow from "./TargetRow";
import TARGET_COLUMNS from "./TargetColumns";
import ResolvedTarget from "src/api/models/ResolvedTarget";
import ToastContext from "src/components/Toast/Context";

type TargetListProps = {
  proposalId: number;
};

const TargetList = (props: TargetListProps) => {
  const [targets, setTargets] = useState<TargetRow[]>([]);
  const [isSaving, setIsSaving] = useState(false);
  const { setToastMessage } = useContext(ToastContext);

  const ResolveNameButton = (target: TargetRow) => {
    return (
      <Button
        color="secondary"
        icon="Magnifier"
        label="Resolve"
        loadingText="Resolve"
        isDisabled={target.isResolving}
        showLoader={target.isResolving}
        onClick={async () => {
          setTargets((old) =>
            old.map((t) =>
              t.rowId === target.rowId
                ? updateTarget(t, { isResolving: true })
                : t,
            ),
          );

          const resolvePromise = new Promise<void>((resolve) => {
            setTimeout(() => {
              resolveName(target);
              resolve();
            }, 200);
          });

          await resolvePromise;
        }}
      />
    );
  };

  const DeleteButton = (target: TargetRow) => (
    <Button
      label="Delete"
      icon="Cross"
      color="negative"
      onClick={() => {
        deleteTarget(target);
      }}
    />
  );

  const updateTarget = (
    target: TargetRow,
    targetProps: Partial<TargetRow>,
  ): TargetRow => {
    const updatedTarget = { ...target, ...targetProps };
    setButtons([updatedTarget]);
    return updatedTarget;
  };

  const resolveName = async (target: TargetRow) => {
    let resolvedTarget: ResolvedTarget;

    try {
      if (target.name) {
        resolvedTarget = await Api.resolveTargetName(target.name);
      }
    } catch {
      setToastMessage({
        title: "Error",
        message: `Failed to resolve name ${target.name}`,
        alertType: "negative",
        expireAfter: 5000,
      });
    } finally {
      setTargets((old) =>
        old.map((t) =>
          t.rowId === target.rowId
            ? updateTarget(t, {
                x_hms_formatted: resolvedTarget?.x_hms_formatted,
                y_dms_formatted: resolvedTarget?.y_dms_formatted,
                name: target.name,
                isResolving: false,
              })
            : t,
        ),
      );
    }
  };

  const deleteTarget = (target: TargetRow) => {
    setTargets((old) => old.filter((t) => t !== target));
  };

  const addTarget = () => {
    const newTarget: TargetRow = {
      rowId: crypto.randomUUID(),
      id: 0,
      name: "",
      delete: <></>,
      resolveName: <></>,
      x_hms_formatted: "",
      y_dms_formatted: "",
      system: "ICRS",
      notes: "",
    };

    setButtons([newTarget]);
    setTargets((old) => [...old, newTarget]);
  };

  const fetchTargets = async () => {
    let targetRows = (await Api.getTargets(props.proposalId)) as TargetRow[];
    targetRows = targetRows.map((t) => ({ ...t, rowId: crypto.randomUUID() }));
    setButtons(targetRows);
    return targetRows;
  };

  const setButtons = (targetRows: TargetRow[]) => {
    for (const targetRow of targetRows) {
      targetRow.delete = DeleteButton(targetRow);
      targetRow.resolveName = ResolveNameButton(targetRow);
    }
  };

  const fetchKey = `/api/v1/proposals/${props.proposalId}}/targets"`;

  const refetchData = () => {
    mutate(fetchKey);
  };

  const {
    data: fetchedTargets,
    error,
    isLoading,
  } = useSWRImmutable(fetchKey, fetchTargets, {
    revalidateOnMount: true,
  });

  useEffect(() => {
    setTargets(fetchedTargets as TargetRow[]);
  }, [fetchedTargets]);

  // TODO: error state
  if (error) {
    return <div>Something went wrong</div>;
  }

  if (isLoading) {
    return <Spinner />;
  }

  const updateTargets = async () => {
    if (!targets.length) return;
    setIsSaving(true);

    try {
      await Api.updateTargets(props.proposalId, targets);
      refetchData();
      setToastMessage({
        title: "Success",
        message: "Targets were saved succesfully",
        alertType: "positive",
        expireAfter: 5000,
      });
    } catch {
      setToastMessage({
        title: "Error",
        message: "Failed to save targets",
        alertType: "negative",
        expireAfter: 5000,
      });
    } finally {
      setIsSaving(false);
    }
  };

  return (
    <div className="space-y-6 pt-4">
      <Table
        headerContent={
          <div className="pb-4">
            <Button
              label="Add Target"
              icon="Plus"
              color="neutral"
              onClick={addTarget}
            />
          </div>
        }
        columns={TARGET_COLUMNS}
        rows={targets}
        editMode="cell"
      ></Table>
      <Button
        label="Save targets"
        loadingText="Saving..."
        showLoader={isSaving}
        isDisabled={isSaving}
        color="primary"
        onClick={updateTargets}
      />
    </div>
  );
};

export default TargetList;
