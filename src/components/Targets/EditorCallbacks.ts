import TargetRow from "./TargetRow";

type EditableTargetStringFields =
  | "name"
  | "x_hms_formatted"
  | "y_dms_formatted"
  | "notes";

const onTextEditComplete = (e: {
  rowData: TargetRow;
  newValue: string;
  field: EditableTargetStringFields;
}) => {
  const { rowData, newValue, field } = e;
  rowData[field] = newValue;
};

const onRaDecEditComplete = (e: {
  rowData: TargetRow;
  newValue: string;
  field: EditableTargetStringFields;
  originalEvent: React.SyntheticEvent;
}) => {
  const { rowData, newValue, field, originalEvent: event } = e;
  const pattern: RegExp =
    field === "x_hms_formatted"
      ? /^\d\d:\d\d:\d\d(\.\d{1,2})?$/
      : /^[+-]\d\d:\d\d:\d\d(\.\d)?$/;

  if (newValue.match(pattern)) {
    rowData[field] = newValue;
  } else {
    event.preventDefault();
  }
};

export { onTextEditComplete, onRaDecEditComplete };
