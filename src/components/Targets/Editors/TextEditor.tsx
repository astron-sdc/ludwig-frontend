import { TextInput } from "@astron-sdc/design-system";
import { SetStateAction } from "react";

export type optionsType = {
  value: string;
  editorCallback?: (action: SetStateAction<string>) => void;
};

const TextEditor = (options: optionsType, validator?: (value: string) => boolean, errorMessage?: string) => {
  return (
    <TextInput
      value={options.value}
      onValueChange={(e) => options.editorCallback && options.editorCallback(e)}
      isInvalid={validator && options.value ? !validator(options.value) : false}
      errorMessage={errorMessage}
    />
  );
};

export default TextEditor;
