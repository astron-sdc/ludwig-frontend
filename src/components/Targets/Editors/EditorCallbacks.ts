import TargetRow from "src/components/Targets/Models/TargetRow";

type EditableTargetStringFields = "group" | "name" | "x_hms_formatted" | "y_dms_formatted" | "notes";

export type TextEditCompleteEvent = {
  rowData: TargetRow;
  newValue: string;
  field: EditableTargetStringFields;
};

const onTextEditComplete = (e: TextEditCompleteEvent) => {
  const { rowData, newValue, field } = e;
  rowData[field] = newValue;
};

export default onTextEditComplete;
