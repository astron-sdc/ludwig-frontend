import { Dropdown } from "@astron-sdc/design-system";
import { SetStateAction } from "react";
import { getSpecificPriorityValues } from "src/helper/PriorityHelper";

export type priorityTypeEditor = {
  value: string;
  isRequestedValues: boolean;
  editorCallback?: (action: SetStateAction<string | undefined>) => void;
};

const PriorityEditor = (options: priorityTypeEditor) => {
  return (
    <Dropdown
      valueOptions={getSpecificPriorityValues(options.isRequestedValues)}
      onValueChange={(e) => {
        options.editorCallback && options.editorCallback(e);
      }}
    />
  );
};

export default PriorityEditor;
