import { FC, useContext } from "react";
import Panel from "src/components/Panel/Panel";
import { Chart, ChartData, GeneralError, Spinner, Typography } from "@astron-sdc/design-system";
import Call from "src/api/models/Call.ts";
import Api from "src/api/Api";
import useSWRImmutable from "swr/immutable";
import SkyDistribution from "src/api/models/SkyDistribution";
import { secondsToHours } from "src/utils/Numbers";
import { ThemeContext } from "./Theme/Context.ts";

interface AllocationDetailsPanelProps {
  call: Call;
  selectedProposalsIds: number[];
}

const AllocationDetailsPanel: FC<AllocationDetailsPanelProps> = ({ call, selectedProposalsIds }) => {
  const { darkmode } = useContext(ThemeContext);

  const {
    data: sky_distribution,
    isLoading,
    error,
  } = useSWRImmutable(`getSkyDistribution(${call.id})`, () => Api.getSkyDistribution(call.id));

  if (isLoading)
    return (
      <Panel>
        <Typography text="Allocation details" variant="h5" />
        <Spinner />
      </Panel>
    );

  if (error) return <GeneralError title="Something went wrong!" content="" />;

  const labels = Array.from(Array(24).keys()).map(String);

  const proposalFilter = (sd: SkyDistribution) => {
    // No filtering applied
    if (selectedProposalsIds.length == 0) return true;

    return selectedProposalsIds.includes(sd.proposal_id);
  };

  const data = sky_distribution
    ?.filter(proposalFilter)
    .reduce((prev, cur) => prev.map((val, idx) => val + cur.sky_distribution[idx]), Array(24).fill(0))
    .map(secondsToHours)
    .map(Number);

  const skyData: ChartData = {
    labels: labels,
    datasets: [
      {
        label: "Selected",
        data: data ?? [],
      },
    ],
  };

  return (
    <Panel>
      <Typography text="Allocation details" variant="h5" />
      <Chart
        chartType="bar"
        data={skyData}
        useDarkMode={darkmode}
        x_label="RA (hour)"
        y_label="Time (hours)"
        legend_position="right"
      />
    </Panel>
  );
};

export default AllocationDetailsPanel;
