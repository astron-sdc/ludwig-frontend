import { Alert, Dropdown, Modal, TextInput } from "@astron-sdc/design-system";
import { Dispatch, FC, SetStateAction, useContext, useState } from "react";
import { validateEmail } from "src/utils/Validation";
import Api from "src/api/Api";
import Proposal from "src/api/models/Proposal";
import ToastContext from "src/components/Toast/Context";
import FormGrid from "src/components/FormGrid";

interface Props {
  inviteOpen: boolean;
  setInviteOpen: Dispatch<SetStateAction<boolean>>;
  proposal: Proposal;
}

interface Warning {
  title: string;
  message: string;
}

const InviteModal: FC<Props> = (props) => {
  const { setToastMessage } = useContext(ToastContext);

  const { inviteOpen, setInviteOpen, proposal } = props;
  const [loading, setLoading] = useState(false);
  const [emailList, setEmailList] = useState("");
  const [group, setGroup] = useState<string | undefined>(proposal.groups[0].identifier);

  const [warning, setWarning] = useState<Warning | null>(null);

  const isValid = emailList.split(",").reduce((prev, cur) => prev && validateEmail(cur.trim()), true);
  const options = proposal.groups.map((grp) => ({ label: grp.name, value: grp.identifier }));

  const closeModal = () => {
    setEmailList("");
    setWarning(null);
    setGroup(proposal.groups[0].identifier);
    setInviteOpen(false);
  };

  const expireWarning = () => {
    setWarning(null);
  };

  const handleInvite = async () => {
    try {
      if (isValid && group) {
        const email_addresses = emailList.split(",").map((email) => email.trim());
        setLoading(true);
        await Api.inviteMembers(proposal.id, email_addresses, [group]);
        setLoading(false);
        closeModal();
        setToastMessage({
          title: "Invite(s) sent!",
          message: "Members should receive an email soon.",
          alertType: "positive",
          expireAfter: 5000,
        });
      }
    } catch (_) {
      setWarning({
        title: "Invite(s) not sent",
        message: "Check email address(es). Are they already invited? If the problem persists, contact the helpdesk.",
      });
      setToastMessage({
        title: "Invite(s) not sent!",
        message: "Error sending invites",
        alertType: "negative",
        expireAfter: 5000,
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <Modal
      isOpen={inviteOpen}
      onOpenChange={closeModal}
      title="Invite members"
      buttons={[
        {
          label: "Send",
          onClick: handleInvite,
          showLoader: loading,
          loadingText: "Sending invite(s)",
        },
        {
          label: "Cancel",
          onClick: closeModal,
          color: "secondary",
          isDisabled: loading,
        },
      ]}
    >
      <FormGrid>
        <div className="flex flex-col">
          <TextInput
            labelPlacement="left"
            label="Email addresses separated by commas"
            value={emailList}
            onValueChange={setEmailList}
            isInvalid={!isValid}
            errorMessage={!isValid ? "Invalid email adress" : ""}
          />
          <Dropdown valueOptions={options} value={group} onValueChange={setGroup} label="Group" />

          {warning && (
            <Alert
              title={warning.title}
              message={warning.message}
              alertType="warning"
              expireAfter={10000}
              onExpire={expireWarning}
            />
          )}
        </div>
      </FormGrid>
    </Modal>
  );
};

export default InviteModal;
