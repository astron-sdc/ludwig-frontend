import { Button, TextInput, Typography } from "@astron-sdc/design-system";
import Panel from "src/components/Panel/Panel.tsx";
import CallCollaboration from "src/api/models/CallCollaboration.ts";
import CallPermissions from "src/api/models/CallPermissions.ts";
import { useContext, useState } from "react";
import ReviewPreAssignmentModal from "src/components/Review/ReviewAssignment/ReviewPreAssignmentModal";
import { ModalButtonAction } from "src/components/Modal/Modal";
import Api from "src/api/Api";
import ToastContext from "src/components/Toast/Context";
import { PanelType } from "src/api/models/Review";
import CallMembersTable from "./CallMembersTable";

interface MembersTabProps {
  callCollaboration?: CallCollaboration;
  permissions: CallPermissions;
  callId: number;
  onMembersChanged?: () => void;
}

const CallMembersTab: React.FC<MembersTabProps> = (props) => {
  const [showAssignReviewPanelModal, setShowAssignReviewPanelModal] = useState(false);
  const { callCollaboration, permissions } = props;
  const [isSavingReviewPanel, setIsSavingReviewPanel] = useState(false);
  const { setToastMessage } = useContext(ToastContext);
  const showLinks = permissions.view_call_members && callCollaboration;
  const canManageReviewPanel = permissions.manage_members_science_panel || permissions.manage_members_technical_panel;

  const panelTypePermissionMap = new Map<PanelType, boolean>([
    [PanelType.scientific, permissions.manage_members_science_panel],
    [PanelType.technical, permissions.manage_members_technical_panel],
  ]);

  const enabledPanelTypes = [...panelTypePermissionMap.entries()]
    .filter(([, value]) => value)
    .map(([panelType]) => panelType);

  const onReviewPreAssignmentModalClosed = async (
    action: ModalButtonAction,
    selectedCallMemberUids?: string[],
    selectedPanelTypes?: PanelType[],
  ) => {
    if (action === ModalButtonAction.submit && selectedCallMemberUids && selectedPanelTypes) {
      try {
        setIsSavingReviewPanel(true);
        await Api.preAssignCallReviewers(props.callId, selectedCallMemberUids, selectedPanelTypes);
        setToastMessage({
          title: "Success",
          message: "Reviewers were successfully assigned to panels.",
          alertType: "positive",
          expireAfter: 3000,
        });
        props.onMembersChanged?.();
      } catch (ex) {
        setToastMessage({
          title: "Error assigning reviewers to panels.",
          message: `${ex}`,
          alertType: "negative",
          expireAfter: 3000,
        });
      }
    }
    setShowAssignReviewPanelModal(false);
    setIsSavingReviewPanel(false);
  };

  return (
    <>
      {showAssignReviewPanelModal && (
        <ReviewPreAssignmentModal
          callMembers={callCollaboration?.members}
          enabledPanelTypes={enabledPanelTypes}
          showLoader={isSavingReviewPanel}
          onModalClosed={onReviewPreAssignmentModalClosed}
        />
      )}
      <div className="pt-4">
        <Panel>
          <Typography text="Members" variant="h4" />
          {canManageReviewPanel && (
            <Button color="primary" label="Manage review panel" onClick={() => setShowAssignReviewPanelModal(true)} />
          )}
          {callCollaboration && <CallMembersTable callCollaboration={callCollaboration} />}
        </Panel>
      </div>
      <div className="pt-4">
        {showLinks && (
          <Panel>
            <Typography text="SRAM Links" variant="h4" />
            <Typography text="Links open in a new tab" variant="note" />

            <div className="flex space-x-8">
              <a href={callCollaboration.collaboration_url} target="_blank" rel="noopener noreferrer">
                <Button color="primary" label="Manage collaboration in SRAM" />
              </a>

              <a href={callCollaboration.members_url} target="_blank" rel="noopener noreferrer">
                <Button color="neutral" label="Manage members in SRAM" />
              </a>

              <a href={callCollaboration.groups_url} target="_blank" rel="noopener noreferrer">
                <Button color="neutral" label="Manage groups in SRAM" />
              </a>
            </div>

            <Typography text="Invitation link" variant="h5" />
            <Typography text="Share this link to allow users to request access" variant="note" />

            <TextInput value={callCollaboration.invite_link!} isReadOnly={true} />

            <a className="mt-4 block" href={callCollaboration.join_requests_url} target="_blank" rel="noopener noreferrer">
              <Button color="primary" label="Manage join requests in SRAM" />
            </a>
          </Panel>
        )}
      </div>
    </>
  );
};

export default CallMembersTab;
