import Proposal from "src/api/models/Proposal.ts";
import ProposalPermissions from "src/api/models/ProposalPermissions.ts";
import { Button, TextArea, TextInput, Typography } from "@astron-sdc/design-system";
import Panel from "src/components/Panel/Panel.tsx";
import FlexRow from "src/components/FlexRow.tsx";
import { FC, useState } from "react";
import ProposalMembersTable from "./ProposalMembersTable.tsx";
import InviteModal from "./InviteModal.tsx";

interface MembersTabProps {
  proposal: Proposal;
  permissions: ProposalPermissions;
}

const ProposalMembersTab: FC<MembersTabProps> = (props) => {
  const { proposal, permissions } = props;

  const [inviteOpen, setInviteOpen] = useState(false);
  const openInviteModal = () => setInviteOpen(true);

  return (
    <>
      <div className="pt-4">
        <FlexRow>
          <Panel className="w-full">
            <Typography text="Members" variant="h4" />
            <ProposalMembersTable proposal={proposal} permissions={permissions} />
            <InviteModal inviteOpen={inviteOpen} setInviteOpen={setInviteOpen} proposal={proposal} />
            <Button icon="Plus" color="neutral" label="Add members" onClick={openInviteModal} />
          </Panel>
          {proposal.authors?.length ? (
            <Panel className="w-1/3">
              <TextArea
                label="Authors at time of last submission"
                isReadOnly={true}
                isDisabled={false}
                value={proposal.authors.join("\n")}
              />
            </Panel>
          ) : null}
        </FlexRow>
      </div>
      <div className="pt-4">
        {permissions.manage_members && (
          <Panel>
            <Typography text="SRAM Links" variant="h4" />
            <Typography text="Links open in a new tab" variant="note" />

            <div className="flex space-x-8">
              <a href={proposal.collaboration_url} target="_blank" rel="noopener noreferrer">
                <Button color="neutral" label="Manage collaboration in SRAM" />
              </a>

              <a href={proposal.members_url} target="_blank" rel="noopener noreferrer">
                <Button color="neutral" label="Manage members in SRAM" />
              </a>

              <a href={proposal.groups_url} target="_blank" rel="noopener noreferrer">
                <Button color="neutral" label="Manage groups in SRAM" />
              </a>
            </div>

            <Typography text="Invitation link" variant="h5" />
            <Typography text="Share this link to allow users to request access" variant="note" />

            <TextInput value={proposal.invite_link!} isReadOnly={true} />

            <a className="mt-4 block" href={proposal.join_requests_url} target="_blank" rel="noopener noreferrer">
              <Button color="primary" label="Manage join requests in SRAM" />
            </a>
          </Panel>
        )}
      </div>
    </>
  );
};

export default ProposalMembersTab;
