import CollaborationMember from "src/api/models/CollaborationMember.ts";
import { Button, Table } from "@astron-sdc/design-system";
import Proposal from "src/api/models/Proposal.ts";
import ProposalPermissions from "src/api/models/ProposalPermissions";
import { useContext, useState } from "react";
import { ModalButtonAction } from "src/components/Modal/Modal";
import Api from "src/api/Api";
import ToastContext from "src/components/Toast/Context";
import { useRevalidator } from "react-router-dom";
import { getRolesForMemberInCollaboration } from "src/utils/Collaboration";
import MemberGroupEditModal from "./MemberGroupEditModal";

const COLUMNS = [
  {
    field: "name",
    header: "Name",
  },
  {
    field: "affiliation",
    header: "Affiliation / Institution",
  },
  {
    field: "roles",
    header: "Roles",
  },
  {
    field: "actions",
    header: "",
  },
];

interface ProposalMembersTableProps {
  proposal: Proposal;
  permissions: ProposalPermissions;
}

const ProposalMembersTable = ({ proposal, permissions }: ProposalMembersTableProps) => {
  const [showModal, setShowModal] = useState<boolean>(false);
  const [isSaving, setIsSaving] = useState(false);
  const [selectedMember, setSelectedMember] = useState<CollaborationMember | null>(null);
  const { setToastMessage } = useContext(ToastContext);
  const revalidator = useRevalidator();

  const memberToColumns =
    ({ proposal, permissions }: ProposalMembersTableProps) =>
    (member: CollaborationMember) => {
      const roles = getRolesForMemberInCollaboration(member, proposal.groups);

      const editMemberGroupsClicked = (member: CollaborationMember): void => {
        setSelectedMember(member);
        setShowModal(true);
      };

      return {
        name: member.name,
        affiliation: member.schac_home_organisation,
        roles: roles.length ? roles.map((role) => role.name).join(", ") : "No role assigned",
        actions: (
          <span className="flex w-8 space-x-4">
            {permissions.manage_members && (
              <>
                <div data-testid="Edit member">
                  <Button icon="Edit" color="neutral" onClick={() => editMemberGroupsClicked(member)} />
                </div>
                <Button isDisabled={true} icon="Cross" />
              </>
            )}
          </span>
        ),
      };
    };

  const onEditMemberGroupsModalClosed = async (
    action: ModalButtonAction,
    selectedMember?: CollaborationMember,
    selectedGroup?: string,
  ) => {
    if (action === ModalButtonAction.submit && selectedMember) {
      setIsSaving(true);
      try {
        await Api.updateRole(proposal.id, selectedMember.eduperson_unique_id, selectedGroup);
        revalidator.revalidate();
        setToastMessage({
          title: "Success",
          message: "Role was successfully updated.",
          alertType: "positive",
          expireAfter: 3000,
        });
      } catch (ex) {
        setToastMessage({
          title: "Error updating role.",
          message: `${ex}`,
          alertType: "negative",
          expireAfter: 3000,
        });
      }
    }

    setSelectedMember(null);
    setIsSaving(false);
    setShowModal(false);
  };

  const rows = proposal.members.map(memberToColumns({ proposal, permissions }));

  return (
    <>
      {showModal && selectedMember && (
        <MemberGroupEditModal
          member={selectedMember}
          showLoader={isSaving}
          onModalClosed={onEditMemberGroupsModalClosed}
          availableGroups={proposal.groups}
          assignedGroups={getRolesForMemberInCollaboration(selectedMember, proposal.groups)}
        />
      )}
      <Table columns={COLUMNS} rows={rows} />
    </>
  );
};

export default ProposalMembersTable;
