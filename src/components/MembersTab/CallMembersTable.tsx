import { Badge, ColorType, Table } from "@astron-sdc/design-system";
import CallCollaboration from "src/api/models/CallCollaboration";
import { PanelType } from "src/api/models/Review";
import { getRolesForMemberInCollaboration } from "src/utils/Collaboration";
import { capitalizeFirstLetter } from "src/utils/Strings";

const COLUMNS = [
  {
    field: "name",
    header: "Name",
  },
  {
    field: "reviewPanel",
    header: "Review panel",
  },
];

interface CallMembersTableProps {
  callCollaboration: CallCollaboration;
}

const panelTypes: { value: string; label: string; badgeColor: ColorType }[] = [
  { value: "sci_reviewer", label: capitalizeFirstLetter(PanelType.scientific), badgeColor: "neutral" },
  { value: "tech_reviewer", label: capitalizeFirstLetter(PanelType.technical), badgeColor: "warning" },
];

const CallMembersTable = ({ callCollaboration }: CallMembersTableProps) => {
  const rows = callCollaboration.members.map((member) => ({
    name: member.name,
    reviewPanel: getRolesForMemberInCollaboration(member, callCollaboration.groups)
      .filter((role) => ["tech_reviewer", "sci_reviewer"].includes(role.short_name)) // TODO: define role names
      .map((role) => panelTypes.find((pt) => pt.value === role.short_name))
      .map((pt) => (
        <span key={pt?.label ?? ""} className="pr-1">
          <Badge text={pt?.label ?? ""} color={pt?.badgeColor} />
        </span>
      )),
  }));

  return <Table columns={COLUMNS} rows={rows} />;
};

export default CallMembersTable;
