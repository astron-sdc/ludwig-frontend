import { Dropdown, Modal, TextInput } from "@astron-sdc/design-system";
import { ModalButtonAction } from "src/components/Modal/Modal";
import FormGrid from "src/components/FormGrid";
import FlexRow from "src/components/FlexRow";
import CollaborationGroup from "src/api/models/CollaborationGroup";
import { useState } from "react";
import CollaborationMember from "src/api/models/CollaborationMember";

type MemberGroupEditModalProps = {
  onModalClosed?: (action: ModalButtonAction, selectedMember?: CollaborationMember, selectedGroup?: string) => void;
  member: CollaborationMember;
  showLoader: boolean;
  availableGroups: CollaborationGroup[];
  assignedGroups: CollaborationGroup[];
};

const MemberGroupEditModal = ({
  member,
  availableGroups,
  assignedGroups,
  showLoader,
  onModalClosed,
}: MemberGroupEditModalProps) => {
  // Our backend (and SRAM) supports multiple roles, but we only allow selecting one for now
  // Try to select the PI role if multiple roles are available, otherwise pick the first one
  const assignedGroup = assignedGroups.filter((grp) => grp.short_name === "pi")[0] ?? assignedGroups[0];
  const [selectedGroup, setSelectedGroup] = useState<string | undefined>(assignedGroup?.identifier ?? "");

  return (
    <Modal
      isOpen={true}
      onOpenChange={() => onModalClosed?.(ModalButtonAction.close)}
      title="Change member profile"
      buttons={[
        {
          label: "Update",
          onClick: () => onModalClosed?.(ModalButtonAction.submit, member, selectedGroup),
          showLoader: showLoader,
          loadingText: "Updating...",
        },
        {
          label: "Cancel",
          color: "secondary",
          isDisabled: showLoader,
          onClick: () => onModalClosed?.(ModalButtonAction.cancel),
        },
      ]}
    >
      <FormGrid>
        <FlexRow>
          <div className="flex flex-col gap-4">
            <TextInput labelPlacement="left" label="Name" isReadOnly={true} isDisabled={true} value={member.name} />
            <Dropdown
              labelPlacement="outside"
              label="Role"
              value={selectedGroup}
              valueOptions={availableGroups.map((group) => ({
                label: group.name,
                value: group.identifier,
              }))}
              onValueChange={setSelectedGroup}
              isRequired={false}
            ></Dropdown>
          </div>
        </FlexRow>
      </FormGrid>
    </Modal>
  );
};

export default MemberGroupEditModal;
