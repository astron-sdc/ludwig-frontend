import { ReactNode, useState } from "react";
import { ThemeContext } from "src/components/Theme/Context.ts";

type Props = {
  children: ReactNode;
};
const ThemeProvider = (props: Props) => {
  // Use system settings for default value
  const [darkmode, setDarkmode] = useState(() => {
    try {
      return window.matchMedia("(prefers-color-scheme: dark)").matches;
    } catch (_) {
      return false; // defaults to light mode if browser does not support media queries.
    }
  });

  return (
    <ThemeContext.Provider
      value={{
        darkmode: darkmode,
        setDarkmode: setDarkmode,
      }}
    >
      {props.children}
    </ThemeContext.Provider>
  );
};

export default ThemeProvider;
