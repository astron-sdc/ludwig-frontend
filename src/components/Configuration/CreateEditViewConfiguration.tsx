import { Button, Dropdown, Spinner, TextInput, Toggle } from "@astron-sdc/design-system";
import React, { useContext, useEffect, useState } from "react";
import FlexRow from "src/components/FlexRow";
import FormGrid from "src/components/FormGrid";
import { useParams } from "react-router-dom";
import ToastContext from "src/components/Toast/Context.ts";
import { saveConfiguration } from "src/helper/ProposalHelper";
import { ProposalConfiguration, Telescopes } from "src/api/models/ProposalConfiguration";
import { bytesToTeraBytes, hoursToSeconds, secondsToHours, teraBytesToBytes } from "src/utils/Numbers";
import useSWRImmutable from "swr/immutable";
import Api from "src/api/Api";
import { mutate } from "swr";
type CreateEditViewProposalConfigurationProps = {
  proposalId: number;
  canEditConfiguration: boolean;
};

const CreateEditViewProposalConfiguration = ({
  proposalId,
  canEditConfiguration,
}: CreateEditViewProposalConfigurationProps) => {
  const { setToastMessage } = useContext(ToastContext);
  const params = useParams();
  const isViewMode = params["command"] === "view" || !canEditConfiguration;

  const [auto_ingest, setAuto_ingest] = useState<boolean>(true);
  const { data: proposalConfiguration, isLoading } = useSWRImmutable<ProposalConfiguration>(
    "proposalconfig" + proposalId,
    () => Api.getProposalConfiguration(proposalId),
  );

  const [autoPin, setAutoPin] = useState<boolean>(proposalConfiguration?.auto_pin || false);
  const [piggybackAllowedAartfaac, setPiggybackAllowedAartfaac] = useState<boolean>(
    proposalConfiguration?.piggyback_allowed_aartfaac || true,
  );
  const [piggybackAllowedTbb, setPiggybackAllowedTbb] = useState<boolean>(
    proposalConfiguration?.piggyback_allowed_tbb || true,
  );

  const [telescope, setTelescope] = useState<string | undefined>(Telescopes.LOFAR);
  const [canTrigger, setCanTrigger] = useState<boolean>(proposalConfiguration?.can_trigger || false);
  const [enableQAWorkflow, setEnableQAWorkflow] = useState<boolean>(proposalConfiguration?.enable_qa_workflow || true);

  const [expert, setExpert] = useState<boolean>(proposalConfiguration?.expert || false);
  const [privateData, setPrivateData] = useState<boolean>(proposalConfiguration?.private_data || false);
  const [sendQaWorkflowNotifications, setSendQaWorkflowNotifications] = useState<boolean>(
    proposalConfiguration?.send_qa_workflow_notifications || true,
  );

  const [isInitialized, setIsInitialized] = useState<boolean>(false);
  const [filler, setFiller] = useState<boolean>(proposalConfiguration?.filler || false);
  const [triggerAllowedNumbers, setTriggerAllowedNumbers] = useState<string>("0");
  const [triggerPriority, setTriggerPriority] = useState<string>(
    proposalConfiguration?.trigger_priority?.toString() || "1000",
  );

  const [primaryStorageTB, setPrimaryStorageTB] = useState<string>(
    bytesToTeraBytes(proposalConfiguration?.primary_storage_bytes),
  );

  const [secondaryStorageTB, setSecondaryStorageTB] = useState<string>(
    bytesToTeraBytes(proposalConfiguration?.secondary_storage_bytes),
  );

  const [processingTimeHours, setProcessingTimeHours] = useState<string>(
    secondsToHours(proposalConfiguration?.processing_time_seconds),
  );
  const [supportTimeHours, setSupportTimeHours] = useState<string>(
    secondsToHours(proposalConfiguration?.support_time_seconds),
  );

  const [observingTimePrioAAwardedHours, setObservingTimePrioAAwardedHours] = useState<string>(
    secondsToHours(proposalConfiguration?.observing_time_prio_A_awarded_seconds),
  );
  const [observingTimePrioBAwardedHours, setObservingTimePrioBAwardedHours] = useState<string>(
    secondsToHours(proposalConfiguration?.observing_time_prio_B_awarded_seconds),
  );
  const [observingTimeCombinedAwardedHours, setObservingTimeCombinedAwardedHours] = useState<string>(
    secondsToHours(proposalConfiguration?.observing_time_combined_awarded_seconds),
  );
  const [observingTimeCommissioningAwardedHours, setObservingTimeCommissioningAwardedHours] = useState<string>(
    secondsToHours(proposalConfiguration?.observing_time_commissioning_awarded_seconds),
  );
  // Update the state when the data loads
  useEffect(() => {
    if (proposalConfiguration && !isInitialized) {
      setAutoPin(proposalConfiguration.auto_pin);
      setAuto_ingest(proposalConfiguration.auto_ingest);
      setPiggybackAllowedAartfaac(proposalConfiguration.piggyback_allowed_aartfaac || true);
      setPiggybackAllowedTbb(proposalConfiguration.piggyback_allowed_tbb || true);
      setTelescope(proposalConfiguration.telescope);
      setCanTrigger(proposalConfiguration?.can_trigger || false);
      setExpert(proposalConfiguration?.expert || false);
      setPrivateData(proposalConfiguration?.private_data || false);
      setSendQaWorkflowNotifications(proposalConfiguration?.send_qa_workflow_notifications || true);
      setFiller(proposalConfiguration?.filler || false);
      setTriggerAllowedNumbers(proposalConfiguration?.trigger_allowed_numbers + "");
      setTriggerPriority(proposalConfiguration?.trigger_priority?.toString() || "1000");
      setPrimaryStorageTB(bytesToTeraBytes(proposalConfiguration?.primary_storage_bytes));
      setSecondaryStorageTB(bytesToTeraBytes(proposalConfiguration?.secondary_storage_bytes));
      setProcessingTimeHours(secondsToHours(proposalConfiguration?.processing_time_seconds));
      setSupportTimeHours(secondsToHours(proposalConfiguration?.support_time_seconds));
      setObservingTimePrioAAwardedHours(secondsToHours(proposalConfiguration?.observing_time_prio_A_awarded_seconds));
      setObservingTimePrioBAwardedHours(secondsToHours(proposalConfiguration?.observing_time_prio_B_awarded_seconds));
      setObservingTimeCombinedAwardedHours(secondsToHours(proposalConfiguration?.observing_time_combined_awarded_seconds));
      setObservingTimeCommissioningAwardedHours(
        secondsToHours(proposalConfiguration?.observing_time_commissioning_awarded_seconds),
      );
      setIsInitialized(true);
    }
  }, [proposalConfiguration, isInitialized]);

  const handleSaveProposalConfiguration = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    e.stopPropagation();
    saveConfiguration(proposalId, {
      id: proposalConfiguration?.id,
      telescope: telescope as Telescopes,
      auto_ingest: auto_ingest,
      auto_pin: autoPin,
      can_trigger: canTrigger,
      enable_qa_workflow: enableQAWorkflow,
      expert: expert,
      filler: filler,
      trigger_allowed_numbers: isNaN(Number(triggerAllowedNumbers)) ? 0 : Number(triggerAllowedNumbers),
      trigger_priority: isNaN(Number(triggerPriority)) ? 0 : Number(triggerPriority),
      piggyback_allowed_aartfaac: piggybackAllowedAartfaac,
      piggyback_allowed_tbb: piggybackAllowedTbb,
      private_data: privateData,
      send_qa_workflow_notifications: sendQaWorkflowNotifications,
      primary_storage_bytes: teraBytesToBytes(primaryStorageTB),
      secondary_storage_bytes: teraBytesToBytes(secondaryStorageTB),
      processing_time_seconds: hoursToSeconds(processingTimeHours),
      support_time_seconds: hoursToSeconds(supportTimeHours),
      observing_time_prio_A_awarded_seconds: hoursToSeconds(observingTimePrioAAwardedHours),
      observing_time_prio_B_awarded_seconds: hoursToSeconds(observingTimePrioBAwardedHours),
      observing_time_combined_awarded_seconds: hoursToSeconds(observingTimeCombinedAwardedHours),
      observing_time_commissioning_awarded_seconds: hoursToSeconds(observingTimeCommissioningAwardedHours),
    })
      .then((proposalconfig) => {
        mutate("proposalconfig" + proposalId, proposalconfig, false);
        setToastMessage({
          title: "Saved Proposal Configuration",
          message: "Succes",
          alertType: "positive",
          expireAfter: 5000,
        });
      })
      .catch((error) => {
        setToastMessage({
          title: "Error saving Proposal Configuration",
          message: error.message,
          alertType: "negative",
          expireAfter: 5000,
        });
      });
  };
  /*
      const getProposalConfigurationValues = (): { value: string; label: string }[] => {
        if (!tmssProposalConfigurations) return [{ value: "", label: "" }];
        return tmssProposalConfigurations.map((singleProposalConfiguration) => ({
          label: singleProposalConfiguration.name,
          value: singleProposalConfiguration.url,
        }));
      };
    
    
                isSelected={true}
                setIsSelected={setIsDirectAction}
      */
  if (isLoading) return <Spinner label="Fetching Configuration" />;
  return (
    <form onSubmit={handleSaveProposalConfiguration}>
      <FlexRow>
        <FormGrid>
          <Dropdown
            labelPlacement="outside-left"
            label="Destination telescope"
            value={telescope}
            valueOptions={[{ value: Telescopes.LOFAR, label: Telescopes.LOFAR }]}
            isRequired={true}
            onValueChange={setTelescope}
          ></Dropdown>
        </FormGrid>
        <FormGrid> </FormGrid>
        <FormGrid> </FormGrid>
      </FlexRow>
      <FlexRow>
        <FormGrid>
          <Toggle
            label="Filler"
            labelPlacement="left"
            isDisabled={isViewMode}
            isSelected={filler}
            setIsSelected={setFiller}
          />

          <Toggle
            label="Allow commensal AARTFAAC"
            labelPlacement="left"
            isDisabled={isViewMode}
            isSelected={piggybackAllowedAartfaac}
            setIsSelected={setPiggybackAllowedAartfaac}
          />
          <Toggle
            label="Allow commensal TBB"
            labelPlacement="left"
            isDisabled={isViewMode}
            isSelected={piggybackAllowedTbb}
            setIsSelected={setPiggybackAllowedTbb}
          />
        </FormGrid>
        <FormGrid>
          <Toggle
            label="Prevent Automatic Deletion"
            labelPlacement="left"
            isDisabled={isViewMode}
            isSelected={autoPin}
            setIsSelected={setAutoPin}
          />

          <Toggle
            label="Enable QA Workflow"
            labelPlacement="left"
            isDisabled={isViewMode}
            isSelected={enableQAWorkflow}
            setIsSelected={setEnableQAWorkflow}
          />
          <Toggle
            label="Ingest Automatically"
            labelPlacement="left"
            isDisabled={isViewMode}
            isSelected={auto_ingest}
            setIsSelected={setAuto_ingest}
          />
        </FormGrid>
        <FormGrid>
          <Toggle
            label="Private Observation"
            labelPlacement="left"
            isDisabled={isViewMode}
            isSelected={privateData}
            setIsSelected={setPrivateData}
          />

          <Toggle
            label="Experts Required"
            labelPlacement="left"
            isDisabled={isViewMode}
            isSelected={expert}
            setIsSelected={setExpert}
          />
          <Toggle
            label="Send QA Progress Notifications"
            labelPlacement="left"
            isDisabled={isViewMode}
            isSelected={sendQaWorkflowNotifications}
            setIsSelected={setSendQaWorkflowNotifications}
          />
        </FormGrid>
      </FlexRow>
      <FlexRow>
        <FormGrid>
          <TextInput
            value={triggerAllowedNumbers.toString()}
            onValueChange={setTriggerAllowedNumbers}
            label="Allowed number of Triggers"
            inputMode="numeric"
            isRequired={true}
            labelPlacement="outside-left"
            isDisabled={isViewMode}
          />

          <TextInput
            value={triggerPriority}
            onValueChange={setTriggerPriority}
            label="Trigger Priority"
            inputMode="numeric"
            isRequired={true}
            labelPlacement="outside-left"
            isDisabled={isViewMode}
          />

          <Toggle
            label="Allows Trigger Submission"
            labelPlacement="left"
            isDisabled={isViewMode}
            isSelected={canTrigger}
            setIsSelected={setCanTrigger}
          />
        </FormGrid>
        <FormGrid>
          <TextInput
            value={secondaryStorageTB}
            onValueChange={setSecondaryStorageTB}
            label="LTA Storage (TB)"
            inputMode="numeric"
            isRequired={true}
            labelPlacement="outside-left"
            isDisabled={isViewMode}
          />

          <TextInput
            value={primaryStorageTB}
            onValueChange={setPrimaryStorageTB}
            label="CEP STorage (TB)"
            inputMode="numeric"
            isRequired={true}
            labelPlacement="outside-left"
            isDisabled={isViewMode}
          />

          <TextInput
            value={processingTimeHours.toString()}
            onValueChange={setProcessingTimeHours}
            label="CEP Processing Time (Hours)"
            inputMode="numeric"
            isRequired={true}
            labelPlacement="outside-left"
            isDisabled={isViewMode}
          />

          <TextInput
            value={supportTimeHours.toString()}
            onValueChange={setSupportTimeHours}
            label="Support Time (Hours)"
            inputMode="numeric"
            isRequired={true}
            labelPlacement="outside-left"
            isDisabled={isViewMode}
          />
        </FormGrid>
        <FormGrid>
          <TextInput
            value={observingTimePrioAAwardedHours.toString()}
            onValueChange={setObservingTimePrioAAwardedHours}
            label="Observing Time Prio A (Hours)"
            inputMode="numeric"
            isRequired={true}
            labelPlacement="outside-left"
            isDisabled={isViewMode}
          />

          <TextInput
            value={observingTimePrioBAwardedHours.toString()}
            onValueChange={setObservingTimePrioBAwardedHours}
            label="Observing Time Prio B (Hours)"
            inputMode="numeric"
            isRequired={true}
            labelPlacement="outside-left"
            isDisabled={isViewMode}
          />
          <TextInput
            value={observingTimeCombinedAwardedHours.toString()}
            onValueChange={setObservingTimeCombinedAwardedHours}
            label="Observing Time (Hours)"
            inputMode="numeric"
            isRequired={true}
            labelPlacement="outside-left"
            isDisabled={isViewMode}
          />
          <TextInput
            value={observingTimeCommissioningAwardedHours.toString()}
            onValueChange={setObservingTimeCommissioningAwardedHours}
            label="Observing Time Commissioning  (Hours)"
            inputMode="numeric"
            isRequired={true}
            labelPlacement="outside-left"
            isDisabled={isViewMode}
          />
        </FormGrid>
      </FlexRow>
      {canEditConfiguration && (
        <FlexRow justify="start">
          <Button
            label="Cancel"
            icon="Cross"
            color="negative"
            onClick={() => {
              setIsInitialized(false);
            }}
          />
          <Button label="Save" icon="Next" color="positive" type="submit" />
        </FlexRow>
      )}
    </form>
  );
};

export default CreateEditViewProposalConfiguration;
