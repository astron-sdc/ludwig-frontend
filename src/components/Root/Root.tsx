import { QueryClient, QueryClientProvider } from "react-query";
import { Outlet } from "react-router-dom";
import ThemeProvider from "src/components/Theme/Provider.tsx";
import ToastProvider from "src/components/Toast/Provider";

const Root = () => {
  const queryClient = new QueryClient();
  return (
    <ThemeProvider>
      <QueryClientProvider client={queryClient}>
        <ToastProvider>
          <Outlet />
        </ToastProvider>
      </QueryClientProvider>
    </ThemeProvider>
  );
};

export default Root;
