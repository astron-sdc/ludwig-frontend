import { Badge } from "@astron-sdc/design-system";
import { ProposalStatus } from "src/api/models/Proposal";
import { statusToColor } from "src/utils/color";

const ProposalStatusBadge = (props: { status: ProposalStatus }) => {
  return <Badge text={props.status?.replace("_", " ") ?? ""} color={statusToColor(props.status)} />;
};

export default ProposalStatusBadge;
