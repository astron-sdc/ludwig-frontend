export enum ModalButtonAction {
  close = "close",
  submit = "submit",
  cancel = "cancel",
}
