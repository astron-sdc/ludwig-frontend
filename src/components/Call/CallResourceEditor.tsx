import { TextInput, Typography } from "@astron-sdc/design-system";
import { useMemo } from "react";
import CallResources from "src/api/models/CallResources";
import FormGrid from "src/components/FormGrid";
import { bytesToGigaBytes, gigaBytesToBytes, hoursToSeconds, secondsToHours } from "src/utils/Numbers";
import { validateNumber } from "src/utils/Validation";

type CallResourceEditorProps = {
  callResources: CallResources;
  isReadOnly: boolean;
  onResourcesUpdated?: (value: CallResources) => void;
};

const CallResourceEditor = ({ callResources, onResourcesUpdated, isReadOnly }: CallResourceEditorProps) => {
  const endContentClass = "text-lightGrey !opacity-50";
  const inputEndContent = (unit: string) => <Typography variant="paragraph" customClass={endContentClass} text={unit} />;
  const isResourceValueInvalid = (value: string, min: number = 0, max: number = Number.MAX_VALUE) => {
    if (!value) return true;
    return !validateNumber(Number(value), min, max);
  };

  const isObservingTimeInvalid = useMemo(() => {
    return isResourceValueInvalid(callResources.observing_time_seconds.toString());
  }, [callResources.observing_time_seconds]);

  const isCpuTimeInvalid = useMemo(() => {
    return isResourceValueInvalid(callResources.cpu_time_seconds.toString());
  }, [callResources.cpu_time_seconds]);

  const isStorageSpaceInvalid = useMemo(() => {
    return isResourceValueInvalid(callResources.storage_space_bytes.toString());
  }, [callResources.storage_space_bytes]);

  const isSupportTimeInvalid = useMemo(() => {
    return isResourceValueInvalid(callResources.support_time_seconds.toString());
  }, [callResources.support_time_seconds]);

  return (
    <>
      {!isReadOnly && <Typography text="Available resources" variant="h4" />}
      <FormGrid>
        <TextInput
          value={secondsToHours(callResources.observing_time_seconds)}
          onValueChange={(e) =>
            onResourcesUpdated!({ ...callResources, observing_time_seconds: hoursToSeconds(Number(e)) })
          }
          label="Observing time (in hours)"
          labelPlacement="outside-left"
          isReadOnly={isReadOnly}
          isDisabled={isReadOnly}
          type="number"
          isInvalid={isObservingTimeInvalid}
          endContent={inputEndContent("hours")}
        />
        <TextInput
          value={secondsToHours(callResources.cpu_time_seconds)}
          onValueChange={(e) => onResourcesUpdated!({ ...callResources, cpu_time_seconds: hoursToSeconds(Number(e)) })}
          label="CPU time (in hours)"
          labelPlacement="outside-left"
          isReadOnly={isReadOnly}
          isDisabled={isReadOnly}
          type="number"
          isInvalid={isCpuTimeInvalid}
          endContent={inputEndContent("hours")}
        />
        <TextInput
          value={bytesToGigaBytes(callResources.storage_space_bytes)}
          onValueChange={(e) => onResourcesUpdated!({ ...callResources, storage_space_bytes: gigaBytesToBytes(Number(e)) })}
          label="Storage space (in GB)"
          labelPlacement="outside-left"
          isReadOnly={isReadOnly}
          isDisabled={isReadOnly}
          type="number"
          isInvalid={isStorageSpaceInvalid}
          endContent={inputEndContent("GB")}
        />
        <TextInput
          value={secondsToHours(callResources.support_time_seconds)}
          onValueChange={(e) => onResourcesUpdated!({ ...callResources, support_time_seconds: hoursToSeconds(Number(e)) })}
          label="Support time (in hours)"
          labelPlacement="outside-left"
          isReadOnly={isReadOnly}
          isDisabled={isReadOnly}
          type="number"
          isInvalid={isSupportTimeInvalid}
          endContent={inputEndContent("hours")}
        />
      </FormGrid>
    </>
  );
};

export default CallResourceEditor;
