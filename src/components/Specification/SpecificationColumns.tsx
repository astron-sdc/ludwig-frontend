import Specification from "src/api/models/Specification";
import { ColumnProps } from "@astron-sdc/design-system";

const SPECIFICATION_COLUMNS: ColumnProps[] = [
  {
    field: "id",
    header: "Id",
    sortable: true,
    bodyTemplate: (rowData: unknown) => {
      const spec = rowData as Specification;
      return <> {spec.id} </>;
    },
    style: {
      width: "1%",
    },
  },
  {
    field: "name",
    header: "Name",
    sortable: true,
    bodyTemplate: (rowData: unknown) => {
      const spec = rowData as Specification;
      return <> {spec.name} </>;
    },
    style: {
      width: "15%",
    },
  },
  {
    field: "description",
    header: "Description",
    sortable: true,
    bodyTemplate: (rowData: unknown) => {
      const spec = rowData as Specification;
      return <> {spec.description} </>;
    },
    style: {
      width: "20%",
    },
  },
  {
    field: "group_id_number",
    header: "Group",
    sortable: true,
    bodyTemplate: (rowData: unknown) => {
      const spec = rowData as Specification;
      return <> {spec.group_id_number} </>;
    },
    style: {
      width: "20%",
    },
  },

  {
    field: "requested_priority",
    header: "requested priority",
    sortable: true,
    bodyTemplate: (rowData: unknown) => {
      const spec = rowData as Specification;
      return <> {spec.requested_priority} </>;
    },
    style: {
      width: "20%",
    },
  },

  {
    field: "assigned_priority",
    header: "assigned priority",
    sortable: true,
    bodyTemplate: (rowData: unknown) => {
      const spec = rowData as Specification;
      return <> {spec.assigned_priority?.replace("?", "")} </>;
    },
    style: {
      width: "20%",
    },
  },
  {
    field: "edit",
    header: "",
    style: {
      width: "5%",
    },
  },
  {
    field: "delete",
    header: "",
    style: {
      width: "5%",
    },
  },
];

export default SPECIFICATION_COLUMNS;
