import ProposalPermissions from "src/api/models/ProposalPermissions";
import SpecificationList from "./SpecificationList";

type SpecificationOverViewProps = {
  proposalId: number;
  permissions: ProposalPermissions;
  isOnSyncPage: boolean;
};

const SpecificationOverView = ({ proposalId, permissions, isOnSyncPage }: SpecificationOverViewProps) => {
  const canEditSpecification = isOnSyncPage && permissions.write_proposal;

  return (
    <div className="flex flex-row gap-4">
      <div className="w-full">
        <SpecificationList proposalId={proposalId} canEditSpecification={canEditSpecification}></SpecificationList>
      </div>
    </div>
  );
};

export default SpecificationOverView;
