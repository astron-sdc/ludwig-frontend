import Specification from "src/api/models/Specification";

interface SpecificationRow extends Specification {
  rowId: string;
  delete: JSX.Element;
  edit: JSX.Element;
}

export default SpecificationRow;
