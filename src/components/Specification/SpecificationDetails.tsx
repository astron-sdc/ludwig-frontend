import useSWRImmutable from "swr/immutable";
import Panel from "src/components/Panel/Panel";
import { TextArea, TextInput, Dropdown, Button } from "@astron-sdc/design-system";
import Specification from "src/api/models/Specification";
import { useContext, useState, useEffect, useRef } from "react";
import { useParams, useLoaderData, useNavigate } from "react-router-dom";

import ApiTmss from "src/api/ApiTmss";
import {
  SchedulingUnitObservingStrategy,
  TemplateDefinition,
  SpecificationDetailsFormStatus,
  PriorityQueue,
} from "@astron-sd/telescope-specification-models";

import "flatpickr/dist/flatpickr.css";
import Layout from "src/components/Layout";
import ApiSpecification from "src/api/ApiSpecification";
import ToastContext from "src/components/Toast/Context";
import { IsObservableResult } from "src/api/models/DefinitionObservableResult";
import { PriorityQueueAllAssignd, PriorityQueueNotDetermined } from "src/helper/PriorityHelper";
import FlexRow from "src/components/FlexRow";
import FormGrid from "src/components/FormGrid";
import SpecificationDetailsFormWrapper from "./SpecificationDetailsFormWrapper";

const SpecificationDetails = () => {
  const navigate = useNavigate();
  const loadedSpecification = useLoaderData() as Specification;

  const specification = useRef<Specification>(loadedSpecification || {});
  const params = useParams();
  let title = "Specification";

  const CRUMBS = [
    {
      name: "Science Portal",
      href: "/",
    },
    {
      name: "Proposal Tool",
      href: "/",
    },
    {
      name: "Edit Proposal",
      href: "/proposals/" + params.proposalId + "/specification",
    },
    {
      name: title,
      href: "",
    },
  ];
  const { setToastMessage } = useContext(ToastContext);
  const [isNew, setIsNew] = useState(!loadedSpecification);
  if (isNew) title = "Create Specification";
  const [name, setName] = useState(specification.current?.name ?? "");
  const [description, setDescription] = useState(specification.current?.description ?? "");
  const [definition, setDefinition] = useState(specification.current?.definition);
  const [group, setGroup] = useState(specification.current?.group_id_number ?? 1);
  const [isMetaValid, setIsMetaValid] = useState<boolean>(!isNew);
  const [isObservableResult, setIsObservableResult] = useState<IsObservableResult | undefined>();
  const [strategyUrl, setStrategyUrl] = useState<string | undefined>();
  const [strategyKey, setStrategyKey] = useState<string | undefined>(); // we do not want cached results in the SpecificationDetailsForm when switching from Strategy and returning to the same Strategy
  const [requestedPriority, setRequestedPriority] = useState<string | undefined>(
    specification.current?.requested_priority ?? PriorityQueue.PriorityQueueA,
  );
  const [assignedPriority, setAssignedPriority] = useState<string | undefined>(
    specification.current?.assigned_priority ?? PriorityQueueNotDetermined,
  );

  const { data: strategies } = useSWRImmutable<SchedulingUnitObservingStrategy[]>("strategies", () =>
    ApiTmss.getStrategies(),
  );
  const [sortedStrategies, setSortedStrategies] = useState<SchedulingUnitObservingStrategy[]>();
  const [formstatus, setFormstatus] = useState<SpecificationDetailsFormStatus>({
    isValidTaskEditor: true,
    isValidConstraintsEditor: true,
  });

  const recomposeSpecification = () => {
    const spec: Specification = {
      group_id_number: String(group),
      name: name,
      description: description,
      strategy: strategyUrl ?? "",
      proposal: params.proposalId,
      requested_priority: requestedPriority,
      assigned_priority: assignedPriority /* we need right checks for this later on*/,
      definition: definition,
      id: loadedSpecification?.id,
    };
    return spec;
  };

  useEffect(() => {
    const isValid: boolean = name.length > 0 && description.length > 0 && (!!strategyUrl || !isNew);
    setIsMetaValid(isValid);
  }, [name, description, strategyUrl, isNew]);

  useEffect(() => {
    const key = specification.current?.strategy + `${Date.now()}`;
    setStrategyKey(key);
    specification.current = recomposeSpecification();
  }, [strategyUrl]); // eslint-disable-line react-hooks/exhaustive-deps

  useEffect(() => {
    if (!strategies || strategyUrl) return;
    if (loadedSpecification?.strategy) {
      setStrategyUrl(loadedSpecification.strategy);
      return;
    }
    if (strategies.length == 1) {
      setStrategyUrl(strategies[0].url);
    }
  }, [strategies, strategyUrl, loadedSpecification]);

  useEffect(() => {
    if (!strategies || sortedStrategies) return;
    const filtered = strategies.filter((strategy) => strategy.purpose_value === "production");
    const resortedStrategies = filtered ? [...filtered].sort((a, b) => a.name.localeCompare(b.name)) : [];
    setSortedStrategies(resortedStrategies);
  }, [strategies, sortedStrategies]);

  const getStrategyValues = () => {
    if (sortedStrategies) {
      return sortedStrategies.map((strategy) => ({
        label: `${strategy.name} - v${strategy.version}`,
        value: strategy.url,
      }));
    }
    return [{ label: "Loading..", value: "Loading" }];
  };

  const getPriorityValues = (): { value: string; label: string }[] => {
    return PriorityQueue.AllQueues.map((queue) => ({
      label: queue,
      value: queue,
    }));
  };

  const getAssignedPriorityValues = (): { value: string; label: string }[] => {
    return PriorityQueueAllAssignd.map((queue) => ({
      label: queue,
      value: queue,
    }));
  };

  const GotoSpecification = async () => {
    const targetNavigation = `/proposals/${params.proposalId}/specification/`;
    navigate(targetNavigation);
  };

  const Validate = async () => {
    if (!strategyKey || !strategies) return;
    const tobeValidatedSpeficication = recomposeSpecification();
    if (!tobeValidatedSpeficication.definition) return;
    const currentStrategy = strategies.find((strategy) => strategy.url === strategyUrl);
    if (!currentStrategy) return;

    const isObservableResult = await ApiTmss.isObservable(tobeValidatedSpeficication.definition, currentStrategy.id);
    if (isObservableResult.is_observable.message) {
      isObservableResult.is_observable.message = isObservableResult.is_observable.message.replace(/\\n/g, "\n"); // restore line feed
      isObservableResult.is_observable.message = isObservableResult.is_observable.message.replace(
        "specifications_doc is",
        "specification document (specifications_doc node) is",
      ); // make it a little bit more readable. The validation is a technical feedback.
    }
    setIsObservableResult(isObservableResult);
  };

  const CreateSpecification = async () => {
    const newSpeficication = recomposeSpecification();
    let isSaved: boolean = false;
    let savedspecification;
    try {
      savedspecification = await ApiSpecification.createSpecification(newSpeficication);
      if (savedspecification?.id && savedspecification?.id > 0) {
        setIsNew(false);
        isSaved = true;
        setToastMessage({
          title: "Success",
          message: "Specification was created successfully and got  Id:" + savedspecification.id,
          alertType: "positive",
          expireAfter: 5000,
        });
      }
    } catch {
      setToastMessage({
        title: "Error",
        message: "Specification was not created",
        alertType: "negative",
        expireAfter: 5000,
      });
    }
    if (isSaved && savedspecification) {
      specification.current = savedspecification;
      const targetNavigation = `/proposals/${params.proposalId}/specification/${savedspecification.id}/edit`;
      navigate(targetNavigation);
    }
  };

  const handleDefinitionCallback = async (definition: TemplateDefinition, status: SpecificationDetailsFormStatus) => {
    setDefinition(definition);
    setFormstatus(status);
    console.log("formstatus", status, formstatus);
  };

  const SaveSpecification = async () => {
    const isSaved: boolean = false;
    const newSpeficication = recomposeSpecification();
    newSpeficication.id = specification.current.id;
    let savedspecification;
    try {
      savedspecification = await ApiSpecification.updateSpecification(newSpeficication);
      if (savedspecification?.id && savedspecification?.id == specification.current.id) {
        setIsNew(false);
        setToastMessage({
          title: "Success",
          message: "Specification was saved successfully with id " + savedspecification.id,
          alertType: "positive",
          expireAfter: 5000,
        });
      }
    } catch {
      setToastMessage({
        title: "Error",
        message: "Specification was not saved",
        alertType: "negative",
        expireAfter: 5000,
      });
    }
    if (isSaved && savedspecification) {
      specification.current = savedspecification;
    }
  };

  return (
    <Layout title={title} breadcrumbs={CRUMBS}>
      <Panel className="h-[70px] ">
        <div className="float-right flex ">
          {isNew && (
            <div className="mr-4">
              <Button
                label="Create Specification"
                icon="File"
                color="neutral"
                isDisabled={!isMetaValid}
                onClick={CreateSpecification}
              />
            </div>
          )}
          <div className="mr-4">
            <Button label="Close" icon="Cross" color="neutral" onClick={GotoSpecification} />
          </div>
          {!isNew && (
            <div className="mr-4">
              <Button
                label="Save Specification"
                icon="File"
                color="primary"
                isDisabled={!isMetaValid}
                onClick={SaveSpecification}
              />
            </div>
          )}
          <div>
            <Button label="Validate Observability" icon="Review" color="secondary" onClick={Validate} />
          </div>
        </div>
      </Panel>
      {isObservableResult && (
        <Panel>
          <pre style={{ maxWidth: "100vw", overflow: "auto", maxHeight: "200px", textWrap: "balance" }}>
            {isObservableResult.is_observable.message}{" "}
          </pre>
        </Panel>
      )}
      <FlexRow>
        <FormGrid>
          <TextInput
            value={name}
            labelPlacement="outside-left"
            maxLength={155}
            label="Name"
            isRequired={true}
            onValueChange={setName}
          ></TextInput>
          <TextArea
            labelPlacement="outside-left"
            label="Description"
            isRequired={true}
            value={description}
            onValueChange={setDescription}
            maxLength={255}
          ></TextArea>
        </FormGrid>
        <FormGrid>
          <TextInput
            labelPlacement="outside-left"
            label="Target Group"
            isRequired={true}
            value={group.toLocaleString()}
            onValueChange={(value) => setGroup(Number(value))}
          ></TextInput>
          <Dropdown
            label="Request Priority"
            labelPlacement="outside-left"
            value={requestedPriority} // fixes warning about changing to controlled component
            valueOptions={getPriorityValues()}
            onValueChange={setRequestedPriority}
            isRequired={true}
          />
          <Dropdown
            label="Assigned Priority"
            labelPlacement="outside-left"
            value={assignedPriority} // fixes warning about changing to controlled component
            valueOptions={getAssignedPriorityValues()}
            onValueChange={setAssignedPriority}
            isRequired={true}
          />
          {strategies ? (
            <Dropdown
              label="Strategy"
              labelPlacement="outside-left"
              value={strategyUrl ?? ""} // fixes warning about changing to controlled component
              valueOptions={getStrategyValues()}
              onValueChange={setStrategyUrl}
              isRequired={true}
              isDisabled={!isNew}
            />
          ) : (
            <Panel>Loading Strategies</Panel>
          )}
        </FormGrid>
      </FlexRow>
      <SpecificationDetailsFormWrapper
        key={strategyKey}
        specification={specification.current}
        definitionCallback={handleDefinitionCallback}
        strategies={strategies}
      />
    </Layout>
  );
};

export default SpecificationDetails;
