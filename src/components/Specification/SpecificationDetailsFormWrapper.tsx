import Panel from "src/components/Panel/Panel";
import Specification from "src/api/models/Specification";
import { useState, useEffect } from "react";
import useSWRImmutable from "swr/immutable";
import ApiTmss from "src/api/ApiTmss";
import {
  SchedulingUnitObservingStrategy,
  TaskTemplate,
  SchedulingConstraintsTemplate,
  CommonSchemaTemplate,
  TaskDraftList,
  EditorError,
  TemplateDefinition,
} from "@astron-sd/telescope-specification-models";

import { SpecificationDetailsForm } from "@astron-sd/telescope-specification";
import "flatpickr/dist/flatpickr.css";

type Props = {
  specification: Specification;
  definitionCallback: (definition: TemplateDefinition, status: SpecificationDetailsFormStatus) => void;
  strategies?: SchedulingUnitObservingStrategy[];
  children?: React.ReactNode;
};

export interface SpecificationDetailsFormStatus {
  isValidTaskEditor: boolean;
  isValidConstraintsEditor: boolean;
  taskErrors?: EditorError[];
}
const SpecificationDetailsFormWrapper = (props: Props) => {
  const specification = props.specification;
  const [strategy, setStrategy] = useState<SchedulingUnitObservingStrategy>();
  const [systemUTC, setSystemUTC] = useState<string>();
  const { data: commonSchemaTemplate } = useSWRImmutable<CommonSchemaTemplate>("commonSchemaTemplate", () =>
    ApiTmss.getCommonSchema(),
  );
  const { data: constraintTemplates } = useSWRImmutable<SchedulingConstraintsTemplate[]>("constraintTemplates", () =>
    ApiTmss.getSchedulingConstraintsTemplate(),
  );
  const { data: taskTemplates } = useSWRImmutable<TaskTemplate[]>("taskTemplates", () => ApiTmss.getTaskTemplate());
  const { data: taskFilters } = useSWRImmutable<TaskDraftList>("taskFilters", () => ApiTmss.getTaskFilterDefinition());

  const init = async () => {
    try {
      if (!systemUTC) {
        const serverUTC = await ApiTmss.getUtcNow();
        setSystemUTC(serverUTC);
      }
      await Promise.all([commonSchemaTemplate, constraintTemplates, taskTemplates, taskFilters]);
      setStrategy(props.strategies?.find((strategy) => strategy.url === specification?.strategy));
    } catch (error) {
      console.log(error);
    }
  };

  useEffect(() => {
    init();
  }, [specification]); // eslint-disable-line react-hooks/exhaustive-deps

  if (!strategy?.description) {
    return null;
  }
  return (
    <div>
      <Panel> {strategy?.description} </Panel>
      <SpecificationDetailsForm
        definition={specification.definition}
        commonSchemaTemplate={commonSchemaTemplate}
        taskTemplates={taskTemplates}
        taskFilters={taskFilters}
        constraintTemplates={constraintTemplates}
        systemUTC={systemUTC}
        definitionCallback={props.definitionCallback}
        strategy={strategy}
        disable={false}
        isDebugLoggingEnabled={false}
      />
      {props.children}
    </div>
  );
};

export default SpecificationDetailsFormWrapper;
