import { Button, GeneralError, NoEntries, Spinner, Table, Typography } from "@astron-sdc/design-system";
import { useEffect, useState, useContext } from "react";
import ToastContext from "src/components/Toast/Context";
import ApiSpecification from "src/api/ApiSpecification";
import useSWRImmutable from "swr/immutable";
import FormGrid from "src/components/FormGrid.tsx";
import SPECIFICATION_COLUMNS from "src/components/Specification/SpecificationColumns";
import SpecificationRow from "src/components/Specification/SpecificationRow";
import { NavigateFunction, useNavigate } from "react-router-dom";
import { ServerResult } from "src/utils/Fetch";
import { v4 as uuidv4 } from "uuid";

type SpecificationListProps = {
  proposalId: number;
  canEditSpecification: boolean;
};

const EditButton = (specification: SpecificationRow, proposalId: number, navigate: NavigateFunction) => {
  return (
    <Button
      label="Edit"
      icon="Edit"
      onClick={() => {
        navigate("/proposals/" + proposalId + "/specification/" + specification.id + "/edit");
      }}
    />
  );
};

const DeleteButton = (
  specification: SpecificationRow,
  proposalId: number,
  handleDeleteResult: (result: ServerResult, specification: SpecificationRow) => void,
) => (
  <Button
    label="Delete"
    icon="Cross"
    color="negative"
    onClick={async () => {
      const result = await ApiSpecification.deleteSpecification(specification.id, proposalId);
      handleDeleteResult(result, specification);
    }}
  />
);

const SpecificationList = ({ proposalId, canEditSpecification }: SpecificationListProps) => {
  const [specifications, setSpecifications] = useState<SpecificationRow[]>([]);

  const navigate = useNavigate();

  const { setToastMessage } = useContext(ToastContext);

  const handleDeleteResult = (result: ServerResult, specification: SpecificationRow) => {
    if (result.isSuccessful) {
      console.log(specification);
      setSpecifications((old) => old.filter((spec) => spec !== specification));
      setToastMessage({
        title: "Deletetion was a success",
        message: "Specification " + specification.name + " was successfully deleted.",
        alertType: "positive",
        expireAfter: 5000,
      });
    } else {
      setToastMessage({
        title: "Failed to delete",
        message: "Specification " + specification.name + " was not deleted.",
        alertType: "negative",
        expireAfter: 5000,
      });
    }
  };

  const addSpecification = () => {
    navigate("/proposals/" + proposalId + "/specification/create");
  };

  const fetchSpecifications = async () => {
    let SpecificationRows = (await ApiSpecification.getSpecifications(proposalId)) as SpecificationRow[];
    SpecificationRows = SpecificationRows.map((t) => ({
      ...t,
      rowId: uuidv4(),
    }));
    if (canEditSpecification) setButtons(SpecificationRows);
    return SpecificationRows;
  };

  const setButtons = (SpecificationRows: SpecificationRow[]) => {
    for (const SpecificationRow of SpecificationRows) {
      SpecificationRow.delete = DeleteButton(SpecificationRow, proposalId, handleDeleteResult);
      SpecificationRow.edit = EditButton(SpecificationRow, proposalId, navigate);
    }
  };

  const fetchKey = `/api/v1/proposals/${proposalId}}/specifications"`;

  const {
    data: fetchedSpecifications,
    error,
    isLoading,
  } = useSWRImmutable(fetchKey, fetchSpecifications, {
    revalidateOnMount: true,
  });

  useEffect(() => {
    setSpecifications(fetchedSpecifications as SpecificationRow[]);
  }, [fetchedSpecifications]);

  if (error) {
    return (
      <FormGrid>
        <GeneralError
          title=""
          content={<Typography text="Error encountered when fetching Specifications." variant="paragraph" />}
        />
      </FormGrid>
    );
  }

  if (isLoading) {
    return (
      <FormGrid>
        <Spinner />
      </FormGrid>
    );
  }

  return (
    <div className="space-y-6 pt-4">
      <Table
        headerContent={
          <div className="pb-4">
            {canEditSpecification && (
              <Button label="Add Specification" icon="Plus" color="neutral" onClick={addSpecification} />
            )}
          </div>
        }
        columns={SPECIFICATION_COLUMNS}
        rows={specifications}
        emptyMessage={
          <NoEntries
            title=""
            content={
              <Typography
                text={
                  canEditSpecification
                    ? "Click 'Add Specification' to add new Specifications."
                    : "This proposal does not have any Specifications."
                }
                variant="paragraph"
              />
            }
          />
        }
      ></Table>
    </div>
  );
};

export default SpecificationList;
