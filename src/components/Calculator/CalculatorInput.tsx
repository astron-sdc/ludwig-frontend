import { Button, DateInput, Dropdown, Icon, TextInput, Typography } from "@astron-sdc/design-system";
import { SetStateAction, useEffect, useState } from "react";
import FormGrid from "src/components/FormGrid";
import FlexRow from "src/components/FlexRow";
import { validateNumber } from "src/utils/Validation";
import CalculatorInputs, {
  AntennaSet,
  ATeamSources,
  Calibrators,
  ChannelsPerSubband,
  MAX_NO_CORE_STATIONS,
  MAX_NO_INTERNATIONAL_STATIONS,
  MAX_NO_REMOTE_STATIONS,
  Pipeline,
} from "src/api/models/CalculatorInputs";
import ProposalPermissions from "src/api/models/ProposalPermissions";
import { Link } from "react-router-dom";
import { dateIsTodayOrInFuture, formatDateYYYYMMDD } from "src/utils/DateTime";
import Target from "src/api/models/Target";
import Panel from "src/components/Panel/Panel";

type CalculatorInputProps = {
  proposalId: number;
  input: CalculatorInputs;
  targets: Target[];
  selectedTargetId?: number;
  permissions: ProposalPermissions;
  isCollapsed: boolean;
  toggleCollapsed?: () => void;
  onCalculateClicked: (proposalId: number, inputs: CalculatorInputs) => Promise<void>;
};

type InputType =
  | SetStateAction<number>
  | SetStateAction<string[]>
  | SetStateAction<string>
  | SetStateAction<string | undefined>
  | SetStateAction<Date>;

const CalculatorInput = ({
  proposalId,
  input,
  targets,
  selectedTargetId,
  permissions,
  isCollapsed,
  toggleCollapsed,
  onCalculateClicked,
}: CalculatorInputProps) => {
  const [isCalculating, setIsCalculating] = useState(false);
  const [observationDate, setObservationDate] = useState<Date>(new Date());

  const [calculatorInputs, setCalculatorInputs] = useState<CalculatorInputs>(input);

  useEffect(() => {
    if (input) {
      setCalculatorInputs(input);
    }
  }, [input]);

  const isNumberInvalid = (value: number, min: number, max: number): boolean => !validateNumber(value, min, max);

  const inputsInvalid = (): boolean =>
    !calculatorInputs.pipeline ||
    !selectedTargetId ||
    !dateIsTodayOrInFuture(observationDate) ||
    !atLeastTwoStationsSelected();

  const atLeastTwoStationsSelected = (): boolean =>
    calculatorInputs.no_core_stations + calculatorInputs.no_international_stations + calculatorInputs.no_remote_stations >=
    2;

  const handleInputChange = (field: keyof CalculatorInputs, value: SetStateAction<InputType>) => {
    setCalculatorInputs((prevInputs) => ({
      ...prevInputs,
      [field]: value,
    }));
  };

  const calculateClicked = async () => {
    setIsCalculating(true);
    await onCalculateClicked(proposalId, {
      ...calculatorInputs,
      observation_date: formatDateYYYYMMDD(observationDate),
      pipeline: calculatorInputs.pipeline || "none",
      target: selectedTargetId,
    });
    setIsCalculating(false);
  };

  const getSelectedTargetCoordinates = (): string => {
    if (!calculatorInputs.target) return "";
    const selectedTarget = targets.find((target) => target.id === Number(calculatorInputs.target));
    return `${selectedTarget?.x_hms_formatted} ${selectedTarget?.y_dms_formatted}`;
  };

  const selectedTargetName = targets.find((tgt) => tgt.id === selectedTargetId)?.name.toString();

  return (
    <FormGrid customClass={`${isCollapsed ? "py-0" : "py-2"}`} border={false}>
      <Link to="#" className="cursor-pointer gap-2 items-center mb-2" onClick={toggleCollapsed} onKeyDown={toggleCollapsed}>
        <FlexRow justify="between" customClass="px-4">
          {selectedTargetName ? (
            <Typography variant="h4" text={`Calculate target: ${selectedTargetName}`} />
          ) : (
            <Typography text="Calculate" variant="h4" />
          )}
          <Icon customClass="p-auto" name={isCollapsed ? "Dropdown" : "Up"} />
        </FlexRow>
      </Link>
      {!isCollapsed && (
        <Panel>
          <Panel className="px-0 py-0">
            <Typography text="Observational setup" variant="h5" />
            <FlexRow>
              <div className="w-1/2 flex flex-col gap-2">
                <TextInput
                  value={calculatorInputs.observation_time_seconds.toString()}
                  onValueChange={(value) => handleInputChange("observation_time_seconds", value)}
                  label="Observation time (seconds)"
                  labelClass="w-full"
                  labelPlacement="outside-left"
                  inputMode="numeric"
                />
                <TextInput
                  value={calculatorInputs.no_core_stations.toString()}
                  onValueChange={(value) => handleInputChange("no_core_stations", value)}
                  isInvalid={isNumberInvalid(Number(calculatorInputs.no_core_stations), 0, MAX_NO_CORE_STATIONS)}
                  label={`No. of core stations (0-${MAX_NO_CORE_STATIONS})`}
                  labelClass="w-full"
                  labelPlacement="outside-left"
                  inputMode="numeric"
                />
                <TextInput
                  value={calculatorInputs.no_remote_stations.toString()}
                  onValueChange={(value) => handleInputChange("no_remote_stations", value)}
                  isInvalid={isNumberInvalid(Number(calculatorInputs.no_remote_stations), 0, MAX_NO_REMOTE_STATIONS)}
                  label={`No. of remote stations (0-${MAX_NO_REMOTE_STATIONS})`}
                  labelClass="w-full"
                  labelPlacement="outside-left"
                  inputMode="numeric"
                />
                <TextInput
                  value={calculatorInputs.no_international_stations.toString()}
                  onValueChange={(value) => handleInputChange("no_international_stations", value)}
                  isInvalid={isNumberInvalid(
                    Number(calculatorInputs.no_international_stations),
                    0,
                    MAX_NO_INTERNATIONAL_STATIONS,
                  )}
                  label={`No. of international stations (0-${MAX_NO_INTERNATIONAL_STATIONS})`}
                  labelClass="w-full"
                  labelPlacement="outside-left"
                  inputMode="numeric"
                />
              </div>
              <div className="w-1/2 flex flex-col gap-2">
                <Dropdown
                  value={calculatorInputs.no_channels_per_subband.toString()}
                  valueOptions={Object.keys(ChannelsPerSubband).map((key) => ({
                    label: ChannelsPerSubband[key as keyof typeof ChannelsPerSubband],
                    value: ChannelsPerSubband[key as keyof typeof ChannelsPerSubband],
                  }))}
                  isRequired={true}
                  label="No. of channels per subband"
                  labelClass="w-full"
                  labelPlacement="outside-left"
                  onValueChange={(value) => handleInputChange("no_channels_per_subband", value)}
                />
                <TextInput
                  value={calculatorInputs.no_subbands.toString()}
                  onValueChange={(value) => handleInputChange("no_subbands", value)}
                  isInvalid={isNumberInvalid(Number(calculatorInputs.no_subbands), 1, 488)}
                  label="No. of subbands (1-488)"
                  labelClass="w-full"
                  labelPlacement="outside-left"
                  inputMode="numeric"
                />
                <TextInput
                  value={calculatorInputs.integration_time_seconds.toString()}
                  onValueChange={(value) => handleInputChange("integration_time_seconds", value)}
                  label="Integration time (seconds)"
                  labelClass="w-full"
                  labelPlacement="outside-left"
                  inputMode="numeric"
                />
                <Dropdown
                  labelPlacement="outside-left"
                  labelClass="w-full"
                  label="Antenna set"
                  value={calculatorInputs.antenna_set}
                  valueOptions={Object.keys(AntennaSet).map((key) => ({
                    label: key,
                    value: AntennaSet[key as keyof typeof AntennaSet],
                  }))}
                  isRequired={true}
                  onValueChange={(value) => handleInputChange("antenna_set", value)}
                />
              </div>
            </FlexRow>
            <hr className="text-grey dark:text-mediumGrey" />
          </Panel>
          <Panel className="px-0 py-0">
            <Typography text="Target setup" variant="h5" />
            <FlexRow>
              <div className="w-1/2 flex flex-col gap-2">
                {targets && (
                  <TextInput
                    value={selectedTargetName ?? "No target selected"}
                    isReadOnly={true}
                    label="Selected target"
                    labelClass="w-full"
                    labelPlacement="outside-left"
                    inputMode="text"
                  />
                )}
                <TextInput
                  value={getSelectedTargetCoordinates()}
                  isDisabled={true}
                  isReadOnly={true}
                  label="Coordinates"
                  labelClass="w-full"
                  labelPlacement="outside-left"
                  inputMode="numeric"
                />
                <DateInput
                  value={observationDate}
                  onValueChange={(value) => setObservationDate(value as Date)}
                  label="Observation date"
                  labelPlacement="outside-left"
                  labelClass="w-full"
                  isRequired={true}
                  isInvalid={!dateIsTodayOrInFuture(observationDate)}
                  errorMessage="Observation date cannot be in the past"
                  isDisabled={false}
                  forceUTC={true}
                />
              </div>
              <div className="w-1/2 flex flex-col gap-2">
                <Dropdown
                  labelPlacement="outside-left"
                  label="Calibrators"
                  multiSelect={true}
                  values={calculatorInputs.calibrators}
                  valueOptions={Object.keys(Calibrators).map((key) => ({
                    label: Calibrators[key as keyof typeof Calibrators],
                    value: Calibrators[key as keyof typeof Calibrators],
                  }))}
                  isRequired={true}
                  onValuesChange={(value) => handleInputChange("calibrators", value)}
                />
                <Dropdown
                  label="A-team sources"
                  labelPlacement="outside-left"
                  multiSelect={true}
                  values={calculatorInputs.a_team_sources}
                  valueOptions={Object.keys(ATeamSources).map((key) => ({
                    label: ATeamSources[key as keyof typeof ATeamSources],
                    value: ATeamSources[key as keyof typeof ATeamSources],
                  }))}
                  isRequired={true}
                  onValuesChange={(values) => handleInputChange("a_team_sources", values)}
                />
              </div>
            </FlexRow>
            <hr className="text-grey dark:text-mediumGrey" />
          </Panel>
          <Panel className="px-0 py-0">
            <Typography text="Pipeline setup" variant="h5" />
            <FlexRow>
              <div className="w-1/2 flex flex-col gap-2">
                <Dropdown
                  labelPlacement="outside-left"
                  labelClass="w-full"
                  label="Pipeline"
                  value={calculatorInputs.pipeline}
                  valueOptions={Object.keys(Pipeline).map((key) => ({
                    label: key,
                    value: Pipeline[key as keyof typeof Pipeline],
                  }))}
                  isRequired={true}
                  onValueChange={(value) => handleInputChange("pipeline", value)}
                />
                <Dropdown
                  labelPlacement="outside-left"
                  labelClass="w-full"
                  label="Enable dysco compression"
                  value={calculatorInputs.enable_dysco_compression?.toString()}
                  valueOptions={[
                    { value: "true", label: "Enable" },
                    { value: "false", label: "Disable" },
                  ]}
                  isRequired={true}
                  onValueChange={(value) => handleInputChange("enable_dysco_compression", value)}
                  isDisabled={calculatorInputs.pipeline === Pipeline.None}
                />
              </div>
              <div className="w-1/2 flex flex-col gap-2">
                <TextInput
                  value={calculatorInputs.frequency_averaging_factor?.toString() ?? ""}
                  onValueChange={(value) => handleInputChange("frequency_averaging_factor", value)}
                  label="Frequency averaging factor"
                  labelClass="w-full"
                  labelPlacement="outside-left"
                  inputMode="numeric"
                  isDisabled={calculatorInputs.pipeline === Pipeline.None}
                />
                <TextInput
                  value={calculatorInputs.time_averaging_factor?.toString() ?? ""}
                  onValueChange={(value) => handleInputChange("time_averaging_factor", value)}
                  label="Time averaging factor"
                  labelClass="w-full"
                  labelPlacement="outside-left"
                  inputMode="numeric"
                  isDisabled={calculatorInputs.pipeline === Pipeline.None}
                />
              </div>
            </FlexRow>
          </Panel>
          {permissions.write_calculation && (
            <FlexRow>
              <Button
                isDisabled={inputsInvalid()}
                showLoader={isCalculating}
                color="positive"
                icon="TMSS"
                label="Calculate"
                onClick={calculateClicked}
              />
              {!atLeastTwoStationsSelected() && (
                <Typography text="At least two stations must be selected." variant="errorMessage" />
              )}
            </FlexRow>
          )}
        </Panel>
      )}
    </FormGrid>
  );
};

export default CalculatorInput;
