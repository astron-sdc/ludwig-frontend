import Panel from "src/components/Panel/Panel";
import { Icon, NoResults, Spinner, Typography } from "@astron-sdc/design-system";
import CalculatorResult from "src/api/models/CalculatorOutputs";
import { bytesToGibiBytes, formatNumber, secondsToHours } from "src/utils/Numbers";
import { Link } from "react-router-dom";
import FlexRow from "src/components/FlexRow";
import CalculatorSensitivityResultsTable from "./CalculatorSensitivityResultsTable";

type CalculatorOutputProps = {
  result?: CalculatorResult;
  onCloseClicked: () => void;
  showLoader?: boolean;
};

const CalculatorOutput = ({ result, onCloseClicked, showLoader }: CalculatorOutputProps) => {
  const resultLabel = result === undefined ? "Calculator results" : `Calculator results: ${result.target_name}`;

  const showPipelineProcessingTime = (result: CalculatorResult) =>
    result.pipeline_processing_time_seconds !== 0 && (
      <>
        <Typography text="Pipeline processing time (in hours):" variant="paragraph" />
        <Typography
          text={formatNumber(Number(secondsToHours(result.pipeline_processing_time_seconds)), {
            fractionDigits: 3,
          })}
          variant="paragraph"
        />
      </>
    );

  const showProcessedDataSize = (result: CalculatorResult) =>
    result.processed_data_size_bytes !== 0 && (
      <>
        <Typography text="Processed size (GiB):" variant="paragraph" />
        <Typography
          text={formatNumber(Number(bytesToGibiBytes(result.processed_data_size_bytes)), { fractionDigits: 2 })}
          variant="paragraph"
        />
      </>
    );

  const showRawDataSize = (result: CalculatorResult) => (
    <>
      <Typography text="Raw size (GiB):" variant="paragraph" />
      <Typography
        text={formatNumber(Number(bytesToGibiBytes(result.raw_data_size_bytes)), { fractionDigits: 2 })}
        variant="paragraph"
      />
    </>
  );

  if (showLoader)
    return (
      <Panel>
        <Spinner />
      </Panel>
    );

  return (
    <Panel>
      <FlexRow justify="between" customClass="px-4">
        <Typography variant="h4" text={resultLabel} />
        <Link to="#" className="cursor-pointer gap-2 items-center mb-2" onClick={onCloseClicked} onKeyDown={onCloseClicked}>
          <Icon customClass="p-auto" name="Cross" />
        </Link>
      </FlexRow>
      <Typography
        customClass="px-4"
        variant="paragraph"
        text={
          <Link
            to="https://support.astron.nl/luci/"
            target="blank"
            rel="noopener noreferrer"
            className="underline font-body"
          >
            LOFAR Unified Calculator for Imaging
          </Link>
        }
      />
      {result === undefined ? (
        <NoResults
          title=""
          content={<Typography text="There are no calculator results for this target." variant="paragraph" />}
        />
      ) : (
        <div className="px-4">
          <hr className="text-grey dark:text-mediumGrey pb-4" />
          <div className="grid grid-cols-2 gap-2">
            {showRawDataSize(result)}
            {showProcessedDataSize(result)}
            {showPipelineProcessingTime(result)}
          </div>
          <div className="py-8">
            <Typography customClass="w-full" text="Sensitivity results" variant="h5" />
            <CalculatorSensitivityResultsTable results={[result]} />
          </div>
        </div>
      )}
    </Panel>
  );
};

export default CalculatorOutput;
