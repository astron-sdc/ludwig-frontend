import { Table } from "@astron-sdc/design-system";
import CalculatorResult from "src/api/models/CalculatorOutputs";
import { formatNumber } from "src/utils/Numbers";

type CalculatorSensitivityResultsTableProps = {
  results: CalculatorResult[];
};

const CalculatorSensitivityResultsTable = ({ results }: CalculatorSensitivityResultsTableProps) => {
  const rows = results.map((result) => ({
    target_name: result.target_name,
    mean_elevation: formatNumber(result.mean_elevation, { fractionDigits: 2 }),
    theoretical_rms_jansky_beam: formatNumber(result.theoretical_rms_jansky_beam, { fractionDigits: 2 }),
    effective_rms_jansky_beam: `${formatNumber(result.effective_rms_jansky_beam, { fractionDigits: 2 })} ± ${formatNumber(result.effective_rms_jansky_beam_err, { fractionDigits: 2 })}`,
  }));

  const COLUMNS = [
    {
      field: "target_name",
      header: "Target Names",
    },
    {
      field: "mean_elevation",
      header: "Mean elevation (deg):",
    },
    {
      field: "theoretical_rms_jansky_beam",
      header: "Theoretical RMS (μJy/beam):",
    },
    {
      field: "effective_rms_jansky_beam",
      header: "Effective RMS (μJy/beam):",
    },
  ];
  return <Table columns={COLUMNS} rows={rows} />;
};

export default CalculatorSensitivityResultsTable;
