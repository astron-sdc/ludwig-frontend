import { useContext, useState, useEffect, useMemo } from "react";
import Api from "src/api/Api";
import useSWR from "swr";
import CalculatorInputs, { defaultCalculatorInputs } from "src/api/models/CalculatorInputs";
import CalculatorResult from "src/api/models/CalculatorOutputs";
import ToastContext from "src/components/Toast/Context";
import { GeneralError, Spinner, Typography } from "@astron-sdc/design-system";
import Target from "src/api/models/Target";
import FormGrid from "src/components/FormGrid";
import ProposalPermissions from "src/api/models/ProposalPermissions";
import CalculatorInput from "./CalculatorInput";
import CalculatorOutput from "./CalculatorOutput";

type CalculatorOverviewProps = {
  proposalId: number;
  selectedTargetId?: number;
  permissions: ProposalPermissions;
};

const CalculatorOverview = ({ proposalId, selectedTargetId, permissions }: CalculatorOverviewProps) => {
  const { setErrorToastMessage } = useContext(ToastContext);
  const [newResult, setNewResult] = useState<CalculatorResult | undefined>();
  const [showCalculatorInput, setShowCalculatorInput] = useState<boolean>(true);
  const [showCalculatorResult, setShowCalculatorResult] = useState<boolean>(true);
  const [showResultLoader, setShowResultLoader] = useState<boolean>(false);
  const [calculatorResults, setCalculatorResults] = useState<CalculatorResult[]>([]);
  const [calculatorInputs, setCalculatorInputs] = useState<CalculatorInputs[]>([]);

  const {
    data: fetchedInputs,
    error: inputsError,
    isLoading: isInputsLoading,
  } = useSWR<CalculatorInputs[]>(`/api/v1/proposals/${proposalId}/calculator-inputs/`, () =>
    Api.getCalculatorInputs(proposalId),
  );
  const {
    data: fetchedTargets,
    error: targetsError,
    isLoading: isTargetsLoading,
  } = useSWR<Target[]>(`/api/v1/proposals/${proposalId}/targets/`, () => Api.getTargets(proposalId));
  const {
    data: fetchedResults,
    error: resultsError,
    isLoading: isResultsLoading,
  } = useSWR<CalculatorResult[]>(`/api/v1/proposals/${proposalId}/calculator-results/`, () =>
    Api.getCalculatorResults(proposalId),
  );

  useEffect(() => {
    if (fetchedResults) setCalculatorResults(fetchedResults);
  }, [fetchedResults]);

  useEffect(() => {
    if (fetchedInputs) setCalculatorInputs(fetchedInputs);
  }, [fetchedInputs]);

  useEffect(() => {
    if (newResult) {
      setCalculatorResults((prevResults) => {
        const resultExists = prevResults.some((r) => r.target === newResult.target);
        return resultExists
          ? prevResults.map((r) => (r.target === newResult.target ? newResult : r))
          : [...prevResults, newResult];
      });
    }
  }, [newResult]);

  useEffect(() => {
    if (selectedTargetId !== null) {
      setShowCalculatorResult(true); // Show (re-open) the output panel again if the selected target changes or new results exist
    }
  }, [calculatorResults, selectedTargetId]);

  const selectedResult = useMemo(
    () => calculatorResults.find((result) => result.target === selectedTargetId),
    [calculatorResults, selectedTargetId],
  );

  const selectedInput = useMemo(
    () => calculatorInputs.find((input) => input.target === selectedTargetId) ?? defaultCalculatorInputs,
    [calculatorInputs, selectedTargetId],
  );

  const error = inputsError || targetsError || resultsError;
  const isLoading = isInputsLoading || isTargetsLoading || isResultsLoading;

  const makeCalculation = async (proposalId: number, inputs: CalculatorInputs): Promise<void> => {
    setShowResultLoader(true);
    try {
      const result = await Api.calculateResults(proposalId, inputs);
      setNewResult(result);
    } catch (error) {
      setErrorToastMessage(`${error}`);
      setShowCalculatorInput(true);
    } finally {
      setShowResultLoader(false);
    }
  };

  if (error) {
    return (
      <FormGrid>
        <GeneralError title="" content={<Typography text={`Error encountered: ${error}`} variant="paragraph" />} />
      </FormGrid>
    );
  }

  if (isLoading) return <Spinner />;

  return (
    <div className="pt-4">
      <div>
        {permissions.write_calculation && !isLoading && calculatorInputs && fetchedTargets && (
          <CalculatorInput
            input={selectedInput}
            targets={fetchedTargets}
            selectedTargetId={selectedTargetId}
            onCalculateClicked={makeCalculation}
            toggleCollapsed={() => setShowCalculatorInput(!showCalculatorInput)}
            isCollapsed={!showCalculatorInput}
            proposalId={proposalId}
            permissions={permissions}
          />
        )}
        {permissions.read_calculation && showCalculatorResult && selectedTargetId && (
          <CalculatorOutput
            onCloseClicked={() => setShowCalculatorResult(false)}
            showLoader={isResultsLoading || showResultLoader}
            result={selectedResult}
          />
        )}
      </div>
    </div>
  );
};

export default CalculatorOverview;
