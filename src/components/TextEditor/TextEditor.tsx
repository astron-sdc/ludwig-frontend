import { Button, TextArea } from "@astron-sdc/design-system";
import { useContext, useMemo, useState } from "react";
import Api from "src/api/Api";
import ToastContext from "src/components/Toast/Context";
import { getWordCount } from "src/utils/Validation";

type TextEditorProps = {
  proposalId: number;
  text: string;
  setText: React.Dispatch<React.SetStateAction<string>>;
  disabled: boolean;
  label: string; //  (e.g., Abstract)
  maxWordCount?: number;
  fieldname: string; //(e.g. abstract)
  validateText: (text: string) => boolean;
  invalidMessage: string; // Message to show when the text is invalid
};

const TextEditor = ({
  proposalId,
  text,
  setText,
  disabled,
  label,
  maxWordCount,
  validateText,
  invalidMessage,
  fieldname,
}: TextEditorProps) => {
  const [isSaving, setIsSaving] = useState(false);
  const { setToastMessage } = useContext(ToastContext);

  const wordCount = useMemo(() => {
    return getWordCount(text);
  }, [text]);

  const isTextInvalid = useMemo(() => {
    return !text || !validateText(text);
  }, [text, validateText]);

  const saveText = async () => {
    try {
      setIsSaving(true);
      await Api.updateProposal(proposalId, { [fieldname]: text }); // Dynamic field update
      setToastMessage({
        title: "Success",
        message: `${label} was saved successfully`,
        alertType: "positive",
        expireAfter: 5000,
      });
    } catch {
      setToastMessage({
        title: "Error",
        message: `Failed to save ${label}`,
        alertType: "negative",
        expireAfter: 5000,
      });
    } finally {
      setIsSaving(false);
    }
  };

  const maxWordCountLabel = maxWordCount ? `/${maxWordCount}` : "";
  const unitLabel = `word${wordCount === 1 ? "" : "s"})`;

  return (
    <div className="flex flex-col gap-2">
      <TextArea
        value={text}
        onValueChange={setText}
        isRequired={true}
        minRows={8}
        isReadOnly={disabled}
        isInvalid={isTextInvalid}
        errorMessage={isTextInvalid ? invalidMessage : ""}
      />
      {!disabled && (
        <Button
          isDisabled={isTextInvalid || isSaving}
          color="secondary"
          icon="Layer"
          showLoader={isSaving}
          loadingText="Saving..."
          label={`Save (${wordCount}${maxWordCountLabel} ${unitLabel}`}
          onClick={saveText}
        />
      )}
    </div>
  );
};

export default TextEditor;
export type { TextEditorProps };
