import { Typography } from "@astron-sdc/design-system";

type OverlayProps = {
  children: React.ReactNode;
  title: string;
};

const Overlay = ({ children, title }: OverlayProps) => {
  return (
    <div className="w-2/3 h-2/3 p-6 pl-8 z-20 border-l-[1px] border-t-[1px] border-[#dcdcdc] dark:border-white fixed overflow-y-scroll bottom-0 right-0 rounded-tl-[8px] bg-[#f4f4f4] dark:bg-[#000014]">
      <Typography text={title} variant="h3" />
      <div>{children}</div>
    </div>
  );
};

export default Overlay;
