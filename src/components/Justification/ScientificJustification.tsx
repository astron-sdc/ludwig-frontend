import { Button, FileInput, Typography } from "@astron-sdc/design-system";
import { filesize } from "filesize";
import { useContext, useEffect, useState } from "react";
import Api from "src/api/Api";
import ToastContext from "src/components/Toast/Context";

type ScientificJustificationProps = {
  proposalId: number;
  disabled: boolean;
};

type OnlineFile = {
  filename: string;
  filesize: number;
};

const ScientificJustification = ({ proposalId, disabled }: ScientificJustificationProps) => {
  const toast = useContext(ToastContext);

  const [existingFile, setExistingFile] = useState<OnlineFile | null>(null);
  const [files, setFiles] = useState<File[]>([]);

  const [uploading, setUploading] = useState(false);

  useEffect(() => {
    Api.getProposal(proposalId).then((proposal) => {
      if (proposal.scientific_justification_filename) {
        setExistingFile({
          filename: proposal.scientific_justification_filename,
          filesize: proposal.scientific_justification_filesize!,
        });
      }
    });
  }, [proposalId]);

  const handleUpload = async () => {
    if (files.length === 1 && !uploading) {
      setUploading(true);
      Api.updateProposalViaForm(proposalId, {
        scientific_justification: files[0],
      })
        .then((proposal) => {
          if (proposal.scientific_justification_filename) {
            toast.setToastMessage({
              title: "File uploaded",
              message: `${proposal.scientific_justification_filename} has been uploaded`,
              alertType: "positive",
              expireAfter: 5000,
            });
            setExistingFile({
              filename: proposal.scientific_justification_filename,
              filesize: proposal.scientific_justification_filesize!,
            });
            setFiles([]);
          }
        })
        .catch((reason) => {
          toast.setToastMessage({
            title: "Could not upload file",
            message: `${reason}`,
            alertType: "negative",
            expireAfter: 5000,
          });
          console.error(reason);
        })
        .finally(() => {
          setUploading(false);
        });
    }
  };

  return (
    <div>
      <Typography text="Scientific Justification" variant="h5" />
      <div className="pt-2">
        {existingFile && (
          <div className="pb-4">
            <a href={Api.getScientificJustificationURL(proposalId)} rel="noreferrer" data-testid="download-link">
              <Button
                label={`${existingFile.filename} (${filesize(existingFile.filesize, { standard: "iec" })})`}
                icon="Download"
              />
            </a>
          </div>
        )}
        <FileInput
          files={files}
          onFilesChange={setFiles}
          accept="application/pdf"
          label="Upload new document"
          disabled={disabled}
        />
        <div className="pt-4">
          <Button
            label="Upload"
            icon="Download"
            isDisabled={files.length === 0 || uploading || disabled}
            onClick={handleUpload}
          />
        </div>
      </div>
    </div>
  );
};

export default ScientificJustification;
