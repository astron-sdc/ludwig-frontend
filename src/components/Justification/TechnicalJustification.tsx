import { Button, Spinner, Typography } from "@astron-sdc/design-system";
import { useContext, useEffect, useMemo, useState } from "react";
import Api from "src/api/Api";
import useSWRImmutable from "swr/immutable";
import QuestionModel, { validateAnswer } from "src/api/models/Question/Question";
import FormGrid from "src/components/FormGrid";
import Question from "src/components/Questionnaire/Question";
import ToastContext from "src/components/Toast/Context";
import { v4 as uuidv4 } from "uuid";

type TechnicalJustificationProps = {
  proposalId: number;
  disabled: boolean;
};

const TechnicalJustification = ({ proposalId, disabled }: TechnicalJustificationProps) => {
  const [questions, setQuestions] = useState<QuestionModel[]>([]);
  const [isSaving, setIsSaving] = useState(false);
  const { setToastMessage } = useContext(ToastContext);

  const fetchQuestions = async () => {
    try {
      return await Api.getQuestions(proposalId);
    } catch (error) {
      if (error instanceof Error && error.message === "404") {
        return [];
      }
      throw error;
    }
  };

  const {
    data: fetchedQuestions,
    error,
    isLoading,
  } = useSWRImmutable(proposalId.toString(), fetchQuestions, {
    revalidateOnMount: true,
  });

  useEffect(() => {
    setQuestions(
      fetchedQuestions?.map((q) => {
        return {
          ...q,
          rowId: uuidv4(),
        };
      }) as QuestionModel[],
    );
  }, [fetchedQuestions]);

  const allAnswersAreValid = useMemo(() => {
    const questionIsUnanswered = (question: QuestionModel) => question.answer === null || question.answer === undefined;
    return questions?.every((q) => questionIsUnanswered(q) || validateAnswer(q));
  }, [questions]);

  if (error) {
    throw Error(error);
  }

  if (isLoading) {
    return <Spinner />;
  }

  const saveTechnicalJustification = async () => {
    if (!allAnswersAreValid) {
      return;
    }

    setIsSaving(true);

    try {
      await Api.saveAnswers(
        proposalId,
        questions.map((q) => ({
          question_id: q.id,
          answer: q.answer,
        })),
      );

      setToastMessage({
        title: "Success",
        message: "Answers were saved successfully",
        alertType: "positive",
        expireAfter: 5000,
      });
    } catch (error) {
      setToastMessage({
        title: "Error",
        message: "Failed to save answers",
        alertType: "negative",
        expireAfter: 5000,
      });
    } finally {
      setIsSaving(false);
    }
  };

  const updateQuestionWithAnswer = (question: QuestionModel, answer: string) => {
    setQuestions((old) =>
      old.map((q) => {
        if (q.rowId === question.rowId) {
          return {
            ...q,
            answer,
          };
        }
        return q;
      }),
    );
  };

  return (
    <div>
      <Typography text="Technical Justification" variant="h5" />
      <div className="pt-2">
        <FormGrid>
          {questions?.map((question) => (
            <Question
              key={question.rowId}
              question={question}
              onAnswer={(answer: string) => {
                updateQuestionWithAnswer(question, answer);
              }}
            />
          ))}
          <Button
            label="Save technical justification"
            loadingText="Saving..."
            showLoader={isSaving}
            onClick={saveTechnicalJustification}
            isDisabled={!questions?.length || !allAnswersAreValid || isSaving || disabled}
          />
        </FormGrid>
      </div>
    </div>
  );
};

export default TechnicalJustification;
