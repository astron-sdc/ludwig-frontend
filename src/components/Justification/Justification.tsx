import ScientificJustification from "src/components/Justification/ScientificJustification";
import TechnicalJustification from "src/components/Justification/TechnicalJustification";
import Panel from "src/components/Panel/Panel";

type JustificationProps = {
  proposalId: number;
  disabled: boolean;
};

const Justification = ({ proposalId, disabled }: JustificationProps) => {
  return (
    <div className="pt-4">
      <Panel>
        <ScientificJustification proposalId={proposalId} disabled={disabled} />
      </Panel>
      <Panel>
        <TechnicalJustification proposalId={proposalId} disabled={disabled} />
      </Panel>
    </div>
  );
};

export default Justification;
