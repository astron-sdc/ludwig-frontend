FROM nginx:mainline-alpine

COPY dist /usr/share/nginx/html/proposal/
COPY nginx/default.conf /etc/nginx/conf.d/default.conf

EXPOSE 80
