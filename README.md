# Ludwig Proposal Tool frontend

This is the frontend application for the Ludwig (Lofar utility for wildly interesting galaxies) Proposal tool.

## Deployments

- https://sdc-dev.astron.nl/proposal/
- https://sdc.astron.nl/proposal/

## Contributing

Ludwig is a React + TypeScript application that uses the [Vite](https://vitejs.dev/) buildchain.
You can check out this repository and run:

Make sure a recent version of nodejs (>= 20) and npm (10.4.0) is available on your system.

```bash
npm install
npm run dev
```

There is a precommit hook to automatically lint staged files and add the linting
changes back to the commit. To lint changed but not staged files, you can run:

```bash
npm run lint-changed
```

The script `npm run lint` is used in CI.

To run the test suite you can run:

```bash
npm run test
```

By default, it will use a transparent proxy to the backend instance at https://sdc-dev.astron.nl/proposal_backend
If you want to change this, you need to edit [vite.config.ts](./vite.config.ts)

```typescript
export default defineConfig({
    server: {
        proxy: {
            "/proposal-backend": "http://localhost:8000",   // asumes backend running at the default django port
        },
    },
    ...
});
```

### Using Visual Studio Code

Recommendations and usage notes for Visual Studio Code.

**TypeScript Version**

When using Visual Studio Code, don't forget to configure your project to use the workspace version of TypeScript (i.e., the version installed into `node_modules`). Otherwise, you will likely run into errors and warnings when working with TypeScript.

**Linting**

Install the Visual Code Extension ESLint to perform the same linting as configured in the CI pipeline.

**Extensions**

The following extensions are recommended for this project:

- ESLint
- PostCSS Language Support

## Architectural breakdown

**Project structure**

The root folder of the project contains mostly configuration. Application source is located in the `src` folder and tests are located in the `src/__tests__` folder. The application consists of pages (`/pages`) which can be navigated to using a link. Pages consist of components (`/components`).

**State management**

State management is kept simple, until the application grows to a point where it needs something more complex. The `useState` hook is used on a page to keep state which is [passed to children components using props](https://react.dev/learn/sharing-state-between-components). If and when the application becomes too large for this approach, things like reducers, contexts, and stores can be considered.

When state is a complex object, you might have update functions that only need to update part of the state, leaving the other values the same. React has the SetStateFunction type, which can be used like this:

```typescript
const [state, setState] = useState({
  a: "some_value",
  b: 42,
  c: false,
});

function myHandler(evt: Event) {
  setState((prev) => ({ ...prev, c: true })); // leaves a and b as is;
}
```

**Querying**

It is possible to use the `useEffect` hook to query data from external API's, but there are a few downsides such as [boilerplate (loading state, error handling), timing issues with rendering and a lack of preloading/caching](https://react.dev/learn/synchronizing-with-effects).

One of the recommended alternatives for `useEffect` that is used: [React Query](https://tanstack.com/query/v3/docs/react/overview).

Additionally, React Router provides [Loaders](https://reactrouter.com/en/main/route/loader) to load data on page navigation.
Use the following guideline on which of these methods to use:

- When fetching data on a page that is dependent on state on that page that may change, you will need to deal with loading/caching and so on. In this case, prefer to use React Query.
- When fetching data for a page that is dependent only on a route param for that page (i.e., a 'detail' page), prefer to use the React Router `useLoaderData` hook.

Api calls are all wrapped in the [Api.ts](src/api/Api.ts) file. Note the use of absolute paths in the uri without a domain name.
This allows the same front-end code to be hosted on different domains (e.g. sdc-dev.astron.nl and sdc.astron.nl) without a rebuild of the front-end code/container.

**Routing**

[React Router](https://reactrouter.com/en/main) is used to setup routing in the application. In `main.tsx` a BrowserRouter is configured to enable routing to pages.

**Styling**

We use components from the [Astron Design System](https://sdc.astron.nl/design-system/).
No particular styling library is used (yet). [CSS modules](https://github.com/css-modules/css-modules) are used for scoping. A postcss nesting plugin is used to enable Sass-like syntax.

## License

Apache License 2.0
