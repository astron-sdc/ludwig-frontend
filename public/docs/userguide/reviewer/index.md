# Reviewer
This page describes how to use the proposal tool as a reviewer. The following topics are described:
<br>
<br>
* Providing comments
* Scoring
* Accepting or rejecting a proposal

## Providing comments

As soon as a proposal is under review, a reviewer can add review comments. On the "Proposal Overview" page (which can be reached by clicking the "Proposals" icon in the icon bar on the left side of the window), the "Status" field indicates when a proposal is under review:
<br>
<br>
![Proposal under review](./images/proposal_overview_under_review.png)
<br>
As a reviewer, click the "Edit" button to navigate to the "Edit Proposal" screen screen of a proposal under review. On the right side of the screen, a "Feedback" section is visible:
<br>
<br>
![Proposal under review](./images/add_review_comment.png)
<br>

The feedback section contains all comments provided by the reviewers of this proposal. You may enter a comment in the comment box at the botom of the section and click the "Add comment" button to add your comment.

## Scoring

A proposal under review can be rated on a scale from 1 through 5. On the "Edit Proposal" screen, a proposal under review may be given a rating. A bar is visible on the bottom of the screen:
<br>
<br>
![Proposal under review](./images/rate_a_proposal.png)
<br>

Enter a rating on the scale indicated, and click the "Grade proposal" button to save your rating.

## Accepting or rejecting a proposal

A proposal under review can be either rejected or accepted. On the "Edit Proposal" screen, a proposal under review can be accepted or rejected. A bar is visible on the bottom of the screen:
<br>
<br>
![Proposal under review](./images/accept_or_reject_proposal.png)
<br>

* Click the "Reject Proposal" button to reject the proposal. The proposal will now be in the "rejected" state
* Click the "Accept Proposal" button to accept the proposal. The proposal will now be in the "accepted" state
