# Account management

## Creation

The proposal tool uses Surf's Research Access Management ([SRAM](https://sram.surf.nl/)).
This allows you to log in with the same credentials from your institute without setting up a new account.
<br><br>
- Access is by invite only, an invitation can be acquired by sending an email to `brentjens@astron.nl`.
<br><br>
- Please check your email (and spam) for an invitation that looks like this (where the name of the person inviting you is mentioned rather than Klaas Kliffen):
![invitation](./images/invitation.png)

<br>
<br>
- On clicking the link, you will be asked to select an institute you are affiliated with:

![selecting an institute](./images/select_institute.png)

<br>
<br>
**NOTE:** If you are not affiliated with an institute, you can choose to create an account with [eduID NL](https://eduid.nl/en/).

- Follow the steps for logging in with your chosen institute and take note of the Privacy and Acceptable use policies.

- You should now be able to log-in to the proposal tool.

## Updating  & deleting information

- Go to the [SRAM Profile Page](https://sram.surf.nl/profile). An overview of the information retrieved from your instiute
  is shown here. For updating your information, please contact the administrative contact of your own institute.
- On the same page it's possible to delete your account from the SRAM systems. **Note** that this will prevent you from
  using the services provided by ASTRON and your access to data may no longer be linked to a new account if you want
  to create a new one. 
- If you change institutes, it's possible to link your new institute account with an existing SRAM
  account. Please see [the SRAM documentation for details](https://servicedesk.surf.nl/wiki/display/IAM/Link+another+account)


