# Principal Investigator (PI)
This page describes how to use the proposal tool as a principal investigator. The following topics are described:
<br>
<br>
* Creating a proposal
* Uploading a PDF file
* Adding targets
* Answering questions

## Creating a proposal
To create a proposal, first click on the "Call for proposals" icon in the icon bar on the left side of the window:
<br>
<br>

![Call for proposals icon in the icon bar](./images/icon_bar_call_for_proposals.png)
<br>
Then select the appropriate call and click on the "Create new proposal" button on the right side of the row for your proposal call:
<br>
<br>

![Create a new proposal button](./images/open_calls_list_create_new_proposal.png)
<br>
You will be taken to the "Create Proposal" page and presented with a form for creating a new proposal:
<br>
<br>

![Create proposal form](./images/create_proposal_form.png)
<br>
The title should be the one on your commissioning test Confluence page, e.g. "LOFAR2-5981-LOFAR shall be able to track celestial objects". Copy and paste your abstract into the "Abstract" field, and upload the PDF of your commissioning test page. You can create such a PDF by clicking on the "..." to the right of the "share" menu item on your Confluence page and selecting the "Export to PDF" option:
<br>
<br>

![Export to PDF in Confluence](./images/confluence_export_to_PDF.png)
<br>
Rename it to a sensible name before uploading it, i.e. *LOFAR2-5981.pdf* (please note the file size limit of **10 MiB**). Finally, click the "Create" button. You will be notified when the proposal is created succesfully, and subsequently taken to the "Edit Proposal" page where you can add and change various details.

## Viewing and uploading a (new) PDF file

After creating the proposal, you are taken to the "Edit Proposal" page. Click on the "Justification" tab. On this tab, you can view the currently uploaded PDF by clicking on the button with the filename:
<br>
<br>
![Download currently uploaded PDF](./images/scientific_justificiation_pdf_upload.png)
<br>
Additionally, you can select a new file and upload it using the "Upload" button. Please note that this **replaces** your current PDF **without** the option of restoring it.

## Adding targets

From the "Edit Proposal" page, click on the "Target objects" tab to start adding target objects to your proposal:
<br>
<br>
![Add target objects](./images/add_target_objects_screen.png)
<br>

* Click the "Add Target" button to add extra target rows
* Click the "Delete" button on a target row to remove that row
* For each target row, you can manually specify RA and Dec. It is preferred, however, to specify a name in the "Name" field and use the "Resolve" button to resolve the target name automatically to RA and Dec. Any name supported by the [sesame CDS name resolver](https://cds.unistra.fr/cgi-bin/Sesame), implemented through [Astropy](https://www.astropy.org/) can be resolved

* Click the "Save targets" button to finalize all your changes and save the targets as they appear on screen

## Answering questions

From the "Edit Proposal" page, click on the "Justification" tab to answer questions in the "Technical Justification" section:
<br>
<br>
![Add target objects](./images/technical_justification_answer_questions.png)
<br>

In support of your proposal, a number of questions are asked. They should be answered in a concise manner. Some questions consist of a choice to be made (e.g., "Yes", "No"), while others require a text or number input. Please observe limits such as max character and word counts. When you are done, click the "Save technical justification" button to save all the answers.