/**
 *  Copyright 2023 ASTRON
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

import react from "@vitejs/plugin-react";
import { defineConfig } from "vitest/config";
import tsconfigPaths from "vite-tsconfig-paths";

// https://vitejs.dev/config/
export default defineConfig({
  optimizeDeps: {},
  server: {
    proxy: {
      "/proposal-backend": "http://localhost:4180",
    },
  },
  plugins: [react(), tsconfigPaths()],
  define: {
    "process.env.TMSS_APP_URL": JSON.stringify("http://localhost:8008"),
    "process.env.SUPPORT_APP_URL": JSON.stringify("https://support.astron.nl/jira/servicedesk/customer/portal/7"),
  },
  test: {
    globals: true,
    environment: "jsdom",
    setupFiles: ["./src/setupTests.ts"],
    coverage: {
      provider: "v8",
      reporter: ["text", "lcov", "html"],
    },
  },
  resolve: {
    alias: {
      src: "/src", // required for absolute imports
    },
  },
});
